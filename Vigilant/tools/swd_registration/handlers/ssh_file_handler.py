import os
import paramiko
from lxml import objectify
from utils.tbconfig import MONGO, SVN, REPO, LOCAL_PATH
from utils import logger


class SSHFileHandler(object):
    def __init__(self, hostaddress='', username='', password='', xml_path='',
                 zip_path='', repo_path=''):
        self.hostaddress = hostaddress
        self.username = username
        self.password = password
        self.xml_path = xml_path
        self.zip_path = zip_path
        self.repo_path = repo_path

    def get_file_list(self):
        if os.path.exists(self.xml_path) and os.path.exists(self.zip_path):
            return self.xml_path, self.zip_path
        elif os.path.exists(self.zip_path) and not os.path.exists(self.xml_path):
            return self.zip_path
        else:
            return None

    def read_xml(self):
        try:
            with open(self.xml_path, 'r+') as xml_file:
                return xml_file.read()
        except IOError:
            logger.error('No such file: {0}'.format(self.xml_path))

    def parse_xml(self, xml_content):
        product_dict = {}
        if xml_content:
            xml_content = objectify.fromstring(xml_content)
            product_dict.update({'main_package':
                                     {xml_content.Name.text:
                                          xml_content.Version.text}})
            for node in xml_content.Dependencies.iterchildren():
                product_dict.update({node.Name.text: node.Version.text})
        logger.info('Package Dependencies and details: {0}'.format(product_dict))
        return product_dict

    def get_product_dict_no_xml(self):
        product_dict = {}
        path = self.zip_path.split('/')[-1].split('-')
        name = path[0]
        version = path[-1].split('.zip')[0]
        product_dict.update({'main_package': {name: version}})
        logger.info('Package Dependencies and details: {0}'.format(product_dict))
        return product_dict

    def create_ssh_client(self):
        ssh_client = paramiko.SSHClient()
        ssh_client.load_system_host_keys()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(self.hostaddress, username=self.username,
                           password=self.password, look_for_keys=False)
        logger.info('SSH connection to {0} created.'.format(self.hostaddress))
        return ssh_client

    def ssh_execute_cmd(self, ssh_client, cmd_to_execute):
        logger.info('Executing comamnd on remote server.')
        return ssh_client.exec_command(cmd_to_execute)

    def ssh_copy_file(self, ssh_client, local_file_path, remote_file_path):
        sftp = ssh_client.open_sftp()
        retry_count = 0
        while retry_count < 3:
            try:
                if sftp.stat(remote_file_path):
                    logger.info('File {0} already available, not coping.'.format(local_file_path))
                    sftp.close()
                    return True
            except IOError:
                logger.info('Trying to copy file, retry count: {0}'.format(retry_count))
                sftp.put(local_file_path, remote_file_path)
                retry_count += 1
        if retry_count == 3:
            logger.error('Could not copy file at {0} to remote server.'.format(local_file_path))

    def close_ssh_connection(self, ssh_client):
        ssh_client.close()
        logger.info('SSH connection closed.')
