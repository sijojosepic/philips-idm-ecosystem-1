#!/usr/bin/env python

from bottle import Bottle, BaseRequest
from tank.controllers import (
    audit,
    country,
    dashboard,
    event,
    exception,
    fact,
    field,
    info,
    site,
    state,
    subscription,
    tasks,
    tags,
    geo_user,
    geo_user_audit,
    iam_user,
    site_rules,
    site_notes,
    lhost_utilities
)
from tank.tnconfig import MONGODB
from tank.util import get_logger


log = get_logger()


def get_app():
    app = Bottle()
    db_params = dict(uri = MONGODB['URL'],
                     db_name = MONGODB['DB_NAME'],
                     username = MONGODB['USER_NAME'],
                     password = MONGODB['PASSWORD'],
                     protocol = MONGODB['PROTOCOL'])

    app.mount('/audit/', audit.get_app(**db_params))
    app.mount('/info/', info.get_app(**db_params))
    app.mount('/dashboard/', dashboard.get_app(**db_params))
    app.mount('/subscription/', subscription.get_app(**db_params))
    app.mount('/facts/', fact.get_app(**db_params))
    app.mount('/event/', event.get_app(**db_params))
    app.mount('/exceptions/', exception.get_app(**db_params))
    app.mount('/field/', field.get_app(**db_params))
    app.mount('/site/', site.get_app(**db_params))
    app.mount('/country/', country.get_app(**db_params))
    app.mount('/state/', state.get_app(**db_params))
    app.mount('/tags/', tags.get_app(**db_params))
    app.mount('/geo_user/', geo_user.get_app(**db_params))
    app.mount('/geo_user_audit/', geo_user_audit.get_app(**db_params))
    app.mount('/iam_user/', iam_user.get_app(**db_params))
    app.mount('/site_rules/', site_rules.get_app(**db_params))
    app.mount('/notes/', site_notes.get_app(**db_params))
    app.mount('/lhost_utilities/', lhost_utilities.get_app(**db_params))
    app.merge(tasks.get_app())
    app.get('/health', callback=health)
    return app


def health():
    log.debug('Returning Health')
    return 'OK'


BaseRequest.MEMFILE_MAX = 1024 * 512 * 2
app = get_app()


if __name__ == '__main__':
    log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    app.run(host='0.0.0.0', port=8080, debug=True)
