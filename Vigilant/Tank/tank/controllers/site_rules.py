from bottle import request
from tank.models.site_rules import RulesModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.post('/save_rules', callback=save_rules)
    app.get('/rules/<siteid>', callback=get_rules_with_siteid)
    app.get('/rules/list/', callback=get_rule_list)
    app.post('/rules/cache', callback=update_rules_in_cache)
    app.get('/maintenance/disable/<siteid>', callback=disable_maintaince)
    app.get('/rules', callback=get_rules)
    app.post('/rules/assignment', callback=save_assignment_rule)
    app.delete('/rules/<siteid>/<rule_type>', callback=delete_assignment_rule)
    return app


def save_rules(mongodb):
    return RulesModel(mongodb).save_rules(request.json)


def get_rules_with_siteid(mongodb, siteid):
    return RulesModel(mongodb).get_rules_with_siteid(siteid)


def get_rule_list(mongodb):
    return RulesModel(mongodb).get_rule_list()


def update_rules_in_cache(mongodb):
    return RulesModel(mongodb).update_rules_in_cache()


def disable_maintaince(mongodb, siteid):
    return RulesModel(mongodb).disable_maintaince(siteid)


def get_rules(mongodb):
    return RulesModel(mongodb).get_rules(request.query)


def save_assignment_rule(mongodb):
    return RulesModel(mongodb).save_assignment_rule(request.json)


def delete_assignment_rule(mongodb, siteid, rule_type):
    return RulesModel(mongodb).delete_assignment_rule(siteid, rule_type)
