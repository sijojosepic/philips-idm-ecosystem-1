from tank.models.subscription import SubscriptionModel
from tank.util import get_bottle_app_with_mongo, get_logger


log = get_logger()

def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/groups', callback=get_groups)
    app.get('/groups/<path:path>', callback=get_groups)
    app.get('/names', callback=get_names)
    app.get('/names/<path:path>', callback=get_names)
    app.get('/details', callback=get_subscriptions)
    return app


def get_groups(mongodb, path=''):
    model = SubscriptionModel(mongodb, path)
    log.debug('Getting subscription groups in path %s', model.slashed_path)
    return model.get_groups()


def get_names(mongodb, path=''):
    log.debug('Getting subscriptions in path %s', path)
    return SubscriptionModel(mongodb, path).get_names()


def get_subscriptions(mongodb):
    log.debug('Getting detailed subscriptions list')
    return SubscriptionModel(mongodb).get_subscriptions()
