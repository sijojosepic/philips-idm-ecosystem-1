from bottle import request
from tank.models.lhost_utilities import LhostUtilitiesModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/tags', callback=get_tags_list)
    app.get('/scanners', callback=get_scanners_list)
    return app


def get_tags_list(mongodb):
    return LhostUtilitiesModel(mongodb).get_tags_list()

def get_scanners_list(mongodb):
    return LhostUtilitiesModel(mongodb).get_scanners_list()