import requests
import time
from base64 import b64encode, b64decode,encodestring
from urllib import urlencode
from tank.tnconfig import IAM_API_ENDPOINTS, IAM_CREDENTIALS, COLLECTIONS, HTTP_PROXY, HTTPS_PROXY, IDM_USER_CREATE, \
    IDM_USER_EXISTS, ADMIN_USER_ID, ADMIN_USER_PASS, PROXY_FLAG, AUTH_HEADER
from tank.util import create_hmac, get_bottle_app_with_mongo, FernetCrypto, UserValidator
from bottle import request
from tank.models.iam_user import IAMUserModel

def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.post('/login', callback=SignIn(request).post)
    app.post('/create', callback=SignUp(request).post)
    app.post('/reset', callback=PWChange(request).post)
    app.post('/logout', callback=LogoutUser(request).post)
    return app


class IAMAccessor(object):
    request = requests
    response = ''
    url = IAM_API_ENDPOINTS['IAM_URL']
    uri_path = ''
    headers = {}
    status_code = ''
    json = {},
    collection = None

    def __init__(self):
        pass

    def post(self, mongodb, payload='json'):
        if payload == 'json':
            r = self.request.post(self.get_url(), json=self.get_payload(), headers=self.get_headers(), proxies=self.get_proxy())
        else:
            r = self.request.post(self.get_url(), data=urlencode(self.get_payload()), headers=self.get_headers(),
                                  proxies=self.get_proxy())
        r.raise_for_status()
        try:
            self.response = r.json()
        except ValueError:
            self.response = ''
        self.headers = r.headers
        self.status_code = r.status_code
        return self.response

    def put(self):
        r = self.request.put(self.get_url(), json=self.get_payload(), headers=self.get_headers(), proxies=self.get_proxy())
        r.raise_for_status()

    def get(self):
        r = self.request.get(self.get_url(), headers=self.get_headers())
        r.raise_for_status()
        self.response = r.json

    def get_response(self):
        return self.response

    def sig_header(self):
        secret_key_prefix = 'DHPWS'
        sign_time = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())
        secret = ''.join([secret_key_prefix, FernetCrypto.decrypt(IAM_CREDENTIALS['SECRET_KEY'])])
        sig = create_hmac(b64encode(sign_time), secret)
        sig_header = ("HmacSHA256;Credential:{0};SignedHeaders:SignedDate;"
                      "Signature:{1}".format(FernetCrypto.decrypt(IAM_CREDENTIALS['SHARED_KEY']), b64encode(sig)))
        return sig_header

    def auth_string(self, result_type):
        base64string = encodestring('%s:%s' % (FernetCrypto.decrypt(IAM_CREDENTIALS['CLIENT_ID']),
                                               FernetCrypto.decrypt(IAM_CREDENTIALS['CLIENT_SECRET']))).replace('\n', '')
        return "%s %s" % (result_type , base64string)

    def get_headers(self):
        return self.headers

    def get_payload(self):
        return self.json

    def get_url(self):
        return self.url + self.uri_path

    def get_proxy(self):
        proxy_dict = {}
        if PROXY_FLAG:
            proxy_dict = {
                "http": HTTP_PROXY,
                "https": HTTPS_PROXY
            }
        return proxy_dict


class SignIn(IAMAccessor):
    idm_url = IAM_API_ENDPOINTS['IDM_URL']
    uri_path = IAM_API_ENDPOINTS['OAUTH2_LOGIN_URI']
    info_path = IAM_API_ENDPOINTS['OAUTH2_LOGIN_INFO']
    uuid_path = IAM_API_ENDPOINTS['UUID_URI']
    user_info_path = IAM_API_ENDPOINTS['USER_INFO_URI']
    access_token = ''
    admin_token = ''
    login_id = ''
    request_obj = None

    def __init__(self, request):
        IAMAccessor.__init__(self)
        self.request_obj = request

    def get_payload(self):
        return { 'grant_type': 'password','username': self.request_obj.json.get('loginId'),
                 'password': self.request_obj.json.get('password')}

    def post(self, mongodb, payload='text'):
        post_response = super(SignIn, self).post(mongodb, payload)
        if post_response:
            self.access_token = post_response.get('access_token')
            user_validator_instance = UserValidator()
            user_validator_instance.add_token(post_response)
            self.login_id = self.get_user_email()
            user_details = IAMUserModel(mongodb).get_user_details(self.login_id)
            ret_obj = { 'message': 'Success',
                        'access_token': post_response.get('access_token'),
                        'refresh_token': post_response.get('refresh_token'),
                        'loginId': self.login_id
                    }
            if len(user_details) != 0:
                ret_obj['firstname'] = user_details[0]['givenName']
                ret_obj['lastname'] = user_details[0]['familyName']
            else:
                user_details = self.get_user_information(mongodb)
                ret_obj['firstname'] = user_details['firstname']
                ret_obj['lastname'] = user_details['lastname']
            return  ret_obj

    def get_user_email(self):
        r = self.request.get(self.url + self.info_path, headers=self.get_user_info_headers(), proxies=self.get_proxy())
        r.raise_for_status()
        login_id = r.json().get('sub')
        return login_id

    def get_admin_token(self):
        admin_payload = {'grant_type': 'password', 'username': b64decode(ADMIN_USER_ID),
                        'password': b64decode(ADMIN_USER_PASS)}
        r = self.request.post(self.get_url(), data=urlencode(admin_payload), headers=self.get_headers(),
                        proxies=self.get_proxy())
        r.raise_for_status()
        admin_token =  r.json().get('access_token')
        return admin_token

    def get_user_uuid(self, login_id, fetch_token=False):
        r = self.request.get("{0}{1}{2}".format(self.idm_url, self.uuid_path, login_id),
                             headers=self.get_user_uuid_headers(fetch_token), proxies=self.get_proxy())
        r.raise_for_status()
        if r.json().get('exchange'):
            uuid_respose = r.json().get('exchange').get('users')
            uuid = uuid_respose[0].get('userUUID')
            return uuid

    def get_user_information(self, mongodb):
        uuid = self.get_user_uuid(self.login_id, fetch_token=True)
        user_details = {}
        r = self.request.get("{0}{1}{2}".format(self.idm_url, self.user_info_path, uuid),
                             headers=self.get_user_info_headers(), proxies=self.get_proxy())
        r.raise_for_status()
        user_info_response = r.json().get('exchange')
        if user_info_response:
            user_details['email'] = user_info_response.get('loginId')
            user_details['firstname'] = user_info_response.get('profile').get('givenName')
            user_details['lastname'] = user_info_response.get('profile').get('familyName')
            IAMUserModel(mongodb).add_new_user(uuid, user_details)
        return user_details

    def get_headers(self):
        return {
            'Accept': 'application/json',
            'Authorization': self.auth_string('Basic'),
            'Content-Type': 'application/x-www-form-urlencoded',
            'api-version' : '1'
        }

    def get_user_info_headers(self):
        return {
            'Accept': 'application/json',
            'Authorization': ('Bearer %s' % self.access_token).encode("utf-8")
        }

    def get_user_uuid_headers(self, fetch_token=False):
        if fetch_token:
            self.admin_token = self.get_admin_token()
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': ('Bearer %s' % self.admin_token).encode("utf-8")
        }


# Creates user under same organization
class SignUp(IAMAccessor):
    uri_path = IAM_API_ENDPOINTS['NEW_USER_URI']
    url = IAM_API_ENDPOINTS['IDM_URL']
    request_obj = None
    mongodb = None
    access_token = ''

    def __init__(self, request):
        IAMAccessor.__init__(self)
        self.request_obj = request

    def get_payload(self):
        return {
            "resourceType": "Person",
            "managingOrganization": FernetCrypto.decrypt(IAM_CREDENTIALS['ORGANIZATION_ID']),
            "name": {
                "text": "",
                "family": self.request_obj.json.get('lastname'),
                "given": self.request_obj.json.get('firstname'),
                "prefix": ""
            },
            "telecom": [{
                "system": "email",
                "value": self.request_obj.json.get('email')
            }, {
                "system": "mobile",
                "value": self.request_obj.json.get('mobile')
            }]
        }

    def post(self, mongodb, payload='json'):
        post_response = super(SignUp, self).post(mongodb, payload)
        if post_response:
            if self.status_code == IDM_USER_EXISTS:
                return {'status_code': self.status_code, 'message': 'Email already registered'}
            elif self.status_code == IDM_USER_CREATE:
                uuid = self.headers.get('Location').split("/")[4]
                IAMUserModel(mongodb).add_new_user(uuid, self.request_obj.json)
                return {'status_code': self.status_code, 'message': 'Registeration Success!! Verification Email has been sent'}

    def get_headers(self):
        return {
            'hsdp-api-signature': self.sig_header(),
            'SignedDate': time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()),
            'api-version': 1,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': self.get_auth_string()
        }

    def get_auth_string(self):
        url = IAM_API_ENDPOINTS['IAM_URL'] + IAM_API_ENDPOINTS['OAUTH2_LOGIN_URI']
        headers = {
            'Accept': 'application/json',
            'Authorization': self.auth_string('Basic'),
            'Content-Type': 'application/x-www-form-urlencoded',
            'api-version': '1'
        }
        payload = {'grant_type': 'password','username': b64decode(ADMIN_USER_ID), 'password': b64decode(ADMIN_USER_PASS)}
        r = self.request.post(url, data= urlencode(payload), headers=headers , proxies=self.get_proxy())
        self.access_token = r.json().get('access_token')
        return ('Bearer %s' % self.access_token).encode("utf-8")


class PWChange(IAMAccessor):
    uri_path = IAM_API_ENDPOINTS['RECOVER_PASSWORD']
    url = IAM_API_ENDPOINTS['IDM_URL']

    def __init__(self, request):
        IAMAccessor.__init__(self)
        self.request_obj = request

    def get_payload(self):
        return {
            "resourceType": "Parameters",
            "parameter": [{
                'name': 'recoverPassword',
                'resource': {
                    'loginId': self.request_obj.json.get('email')
                }
            }]
        }

    def get_headers(self):
        return {
            'Content-Type': 'application/json',
            'hsdp-api-signature': self.sig_header(),
            'api-version': 1,
            'SignedDate': time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()),
            'Accept': 'application/json',
        }


class LogoutUser(IAMAccessor):
    url = IAM_API_ENDPOINTS['IAM_URL']
    uri_path = IAM_API_ENDPOINTS['LOGOUT_USER_URI']
    refresh_token = ''

    def __init__(self, request):
        IAMAccessor.__init__(self)
        self.request_obj = request

    def get_payload(self):
        return { 'token': self.request_obj.headers[AUTH_HEADER]}

    def post(self, mongodb, payload='text'):
        self.refresh_token = self.request_obj.headers[AUTH_HEADER]
        post_response = super(LogoutUser, self).post(mongodb, payload)
        user_validator_instance = UserValidator()
        user_validator_instance.delete_token(self.refresh_token)

    def get_headers(self):
        return {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': self.auth_string('Basic'),
            'Accept': 'application/json'
        }






