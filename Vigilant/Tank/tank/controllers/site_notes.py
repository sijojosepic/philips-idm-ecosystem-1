from bottle import request
from tank.models.site_notes import NotesModel
from tank.util import get_bottle_app_with_mongo

def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/<siteid>', callback=get_notes)
    app.post('/', callback=save_notes)
    app.delete('/', callback=delete_notes)
    return app

def get_notes(mongodb, siteid):
    return NotesModel(mongodb).get_notes(siteid, request.query)

def save_notes(mongodb):
    return NotesModel(mongodb).save_notes(request.json)

def delete_notes(mongodb):
    return NotesModel(mongodb).delete_notes(request.query['ids'])
