from bottle import request
from tank.models.dashboard import DashboardModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/', callback=get_dashboard_data)
    app.get('/unreachableservices/', callback=get_unreachable_services)
    app.get('/unreachableservices/count', callback=get_unreachable_services_count)
    app.get('/namespace/containers/<container>', callback=get_namespace_container)
    app.get('/modules', callback=get_modules)
    app.get('/stats', callback=get_stats)
    app.get('/stats/products', callback=get_products_stats)
    app.get('/appservices/<applists>', callback=get_app_services)
    app.get('/appservices/', callback=get_app_services)
    app.get('/stats/devices', callback=get_device_stats)
    app.post('/case_number/', callback=update_case_number)
    app.post('/acknowledge', callback=acknowledge_events)
    app.delete('/acknowledge/<id>', callback=unacknowledge_events)
    return app


def get_dashboard_data(mongodb):
    return DashboardModel(mongodb).get_dashboard(request.query)


def get_unreachable_services(mongodb):
    return DashboardModel(mongodb).get_unreachable_services()

def get_unreachable_services_count(mongodb):
    return DashboardModel(mongodb).get_unreachable_services_count()

def update_case_number(mongodb):
    identifier = '{siteid}-{hostname}-{service}'.format(**request.json)
    return DashboardModel(mongodb).update_case_number(identifier, request.json['case_number'])


def get_namespace_container(mongodb, container):
    return DashboardModel(mongodb).get_namespace_container(container, request.query)

def get_modules(mongodb):
    return DashboardModel(mongodb).get_modules()

def get_stats(mongodb):
    return DashboardModel(mongodb).get_stats()

def get_products_stats(mongodb):
    return DashboardModel(mongodb).get_prod_stats()

def get_device_stats(mongodb):
    return DashboardModel(mongodb).get_stats()

def acknowledge_events(mongodb):
    return DashboardModel(mongodb).acknowledge_events(request.json['ids'], request.json['tag'], request.json.get('ext_info',None))

def unacknowledge_events(mongodb,id):
    return DashboardModel(mongodb).unacknowledge_events(id)

def get_app_services(mongodb, applists=[]):
    return DashboardModel(mongodb).get_app_services(request.query, applists)