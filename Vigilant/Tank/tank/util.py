import logging
import time
import re
import bottle_mongo
from bottle import Bottle, response, request, abort
from datetime import datetime, timedelta
import hmac
import hashlib
from cryptography.fernet import Fernet
from tank.tnconfig import FERNET_SECRET, IAM_API_ENDPOINTS, IAM_CREDENTIALS, PROXY_FLAG, HTTP_PROXY, HTTPS_PROXY, \
     IGNORE_URI_PATHS, TOKEN_REFRESH_PEROID, AUTH_HEADER, FORBIDDEN_STATUS_CODE, VALID_AUTH_HEADERS, REDIS
import redis, json
from urllib import urlencode
from base64 import encodestring
import requests

from phimutils.plogging import LOG_DATE_FORMAT, LOG_FORMAT


def get_logger():
    log = logging.getLogger('phim.tank')
    log.setLevel(logging.INFO)

    logging.Formatter.converter = time.gmtime
    formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)

    handler_console = logging.StreamHandler()
    handler_console.setFormatter(formatter)
    log.addHandler(handler_console)

    return log


def get_bottle_app_with_mongo(uri, db_name, username, password, protocol):
    app = Bottle()
    dns = "{protocol}://{username}:{password}@{uri}".format(protocol=protocol, username=FernetCrypto.decrypt(username),
                                                            password=FernetCrypto.decrypt(password), uri=uri)

    mongo_plugin = bottle_mongo.MongoPlugin(uri=dns, db=db_name)
    app.install(mongo_plugin)
    user_validator_instance = UserValidator()
    app.add_hook("before_request", user_validator_instance.validate_request)
    return app


def get_projection(items):
    result = dict((key, 1) for key in items)
    result['_id'] = 0
    return result


def get_keys_in_site(site_facts):
    result = set()
    for host in site_facts:
        result.update(host.keys())
    return list(result)


def get_version_parts(version):
    return re.split(r'\.|,', version)


def is_version_match(left, right):
    left_parts, right_parts = map(get_version_parts, [left, right])
    return all(map(lambda parts: parts[0] == parts[1], zip(left_parts, right_parts)))


def get_date_to_string_field_projection(field):
    return {'$dateToString': {'format': '%Y/%m/%d  %H:%M:%S', 'date': field}}


def rename_id(label, item):
    """Destructively rename _id with the label"""
    item[label] = item.pop('_id')
    return item


def get_list_from_param(param):
    """Get a list from a parameter. It is assumed that the parameter may be a comma separated string"""
    if param is None:
        return []
    return filter(None, set(x.strip() for x in param.split(',')))

def create_hmac(data, secret):
    h = hmac.new(secret, digestmod=hashlib.sha256)
    h.update(data)
    return h.digest()


class UserValidator(object):

    def __init__(self):
        self.redis_plugin = RedisInstance.get_redis_instance()
        self.refresh_token = ''

    def get_proxy(self):
        proxy_dict = {}
        if PROXY_FLAG:
            proxy_dict = {
                "http": HTTP_PROXY,
                "https": HTTPS_PROXY
            }
        return proxy_dict

    def get_headers(self):
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': self.auth_string('Basic')
        }

    def auth_string(self, result_type):
        base64string = encodestring('%s:%s' % (FernetCrypto.decrypt(IAM_CREDENTIALS['CLIENT_ID']),
                                               FernetCrypto.decrypt(IAM_CREDENTIALS['CLIENT_SECRET']))).replace('\n', '')
        return "%s %s" % (result_type , base64string)

    def add_token(self, payload):
        refresh_token = payload.get('refresh_token')
        expiry_time = int(payload.get('expires_in'))
        payload['expiry_time'] = (datetime.now() + timedelta(seconds=expiry_time)).strftime('%Y-%m-%d %H:%M:%S')
        self.redis_plugin.set(refresh_token, json.dumps(payload))
        self.redis_plugin.expire(refresh_token, expiry_time)

    def validate_request_headers(self):
        try:
            self.refresh_token = request.headers[AUTH_HEADER]
        except KeyError:
            keys = set(dict(request.headers).keys())
            if set(VALID_AUTH_HEADERS).issubset(keys):
                    return False
            else:
                abort(FORBIDDEN_STATUS_CODE, 'Bad Requsest')
        return True

    def validate_request(self):
        uri_path = request.urlparts[2]
        if (uri_path not in IGNORE_URI_PATHS) and self.validate_request_headers():
            if self.redis_plugin.get(self.refresh_token):
                payload = json.loads(self.redis_plugin.get(self.refresh_token))
                expiry_time = datetime.strptime(payload['expiry_time'], '%Y-%m-%d %H:%M:%S')
                if (expiry_time - datetime.now()).seconds < TOKEN_REFRESH_PEROID:
                    self.update_token(self.refresh_token)
            else:
                abort(FORBIDDEN_STATUS_CODE,'Bad Requsest')

    def update_token(self, refresh_token):
        uri_path = IAM_API_ENDPOINTS['OAUTH2_LOGIN_URI']
        url = IAM_API_ENDPOINTS['IAM_URL']
        payload = { 'grant_type': 'refresh_token','refresh_token': refresh_token}
        r = requests.post(url + uri_path, data=urlencode(payload), headers=self.get_headers(),
                              proxies=self.get_proxy())
        post_response = r.json()
        post_response['refresh_token'] = refresh_token
        self.add_token(post_response)

    def delete_token(self, refresh_token):
        self.redis_plugin.delete(refresh_token)


class RedisInstance(object):

    @staticmethod
    def get_redis_instance():
        return redis.Redis(host=REDIS['HOST'])


class QueryBuilder(object):
    parameters = {}

    def __init__(self, query):
        self.query = query

    def process_query(self):
        pass

    def get_filter(self):
        self.process_query()
        parameters = ((field, self.query.get(query_key)) for query_key, field in self.parameters.iteritems())
        return dict(filter(lambda x: x[1], parameters))

    def get_datetime(self, timestamp):
        if timestamp is not None:
            return datetime.strptime(timestamp, '%Y-%m-%d')

    def get_between_filter(self, start, end):
        result = {}
        if start is not None:
            result['$gte'] = start
        if end is not None:
            result['$lte'] = end
        return result

    def get_between_dates_filter(self, start, end):
        return self.get_between_filter(self.get_datetime(start), self.get_datetime(end))


class FernetCrypto(object):

    @staticmethod
    def fernet():
        secret = bytes(FERNET_SECRET)
        return Fernet(secret)

    @classmethod
    def encrypt(cls, value):
        cipher_suite = cls.fernet()
        return cipher_suite.encrypt(value)

    @classmethod
    def decrypt(cls, value):
        cipher_suite = cls.fernet()
        return cipher_suite.decrypt(value)

