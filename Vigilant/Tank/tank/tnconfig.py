CELERY_PATH = '/usr/lib/celery'
CELERY_CONFIGURATION_OBJECT = 'celeryconfig'
IN_TASK_NAME = 'phim_backoffice.routers.inbound'

## REDIS
REDIS = {
    'HOST': 'redis.phim.isyntax.net',
    'PORT': '6379'
}

## MONGODB / Collections
MONGODB = {
    'URL': 'mongo.phim.isyntax.net:27017/',
    'RETRIES': 2,
    'RETRY_DELAY': 10,
    'DB_NAME': 'somedb',
    'USER_NAME':'gAAAAABbGlSzLK42jTsneAHsJQLMnVT367HYbgpTnH9Eq_6vYpF3d5xSZ9swzg0ysZmrfzchU61yTxLFmMj_F30x2jYJWSWguA==',
    'PASSWORD':'gAAAAABbGlSzXd1AHpJkwPfs9utSGdu-l3mUHHoVELw0CQhXuFB2w9Aoh3ZSJ4zdKV1nP53_KfG4P-Plbyon-pT-4pNpqViCpfmHoS0qNGA8qFASdfsBplc=',
    'PROTOCOL': 'mongodb'
}

COLLECTIONS = {
    'ALIVES': 'alives',
    'AUDIT': 'audit',
    'COUNTRY': 'country',
    'DASHBOARD': 'dashboard',
    'EVENTS': 'events',
    'EXCEPTIONS': 'exceptions',
    'FACTS': 'facts',
    'IAM_USER': 'iam_user',
    'LHOST_UTILITIES': 'lhost_utilities',
    'NAMESPACES': 'namespaces',
    'SITE_MAINTENANCE_RULES': 'site_maintenance_rules',
    'SITE_NOTES': 'site_notes',
    'SITES': 'sites',
    'STATES': 'states',
    'SUBSCRIPTIONS': 'subscriptions',
    'TAGS': 'tags',
    'USERKEYS': 'userkeys',
    'USER': 'user',
    'USER_AUDIT': 'user_audit'
}

PUBLIC_MODULES = {
    'ISP': 'Intellispace PACS',
    'LN': 'Legacy Nagios',
    'SWD': 'Software Distribution',
    'Windows': 'Window Related Facts',
    'Components': 'List of Software Components',
    'PCM': 'Package Configuration Management'
}

PUBLIC_HOST_KEYS = ['address', 'domain']
NEB_IP_NAMESPACE = 'Administrative__Philips__Host__Information__IPAddress'
NEB_HOSTNAME = 'localhost'
MONITOR_URL_TEMPLATE = 'https://{address}/thruk'

HANDLER_SERVICES = ['Product__IntelliSpace__PACS__iSyntaxServer__Aggregate']

IAM_CREDENTIALS = {
    # Development credentials
    # 'ORGANIZATION_ID': 'gAAAAABan3tnUSriaR419jExuGpKwOh-wTStFfF5FqW1N1VbxnwIoDAYx9K6LNWV6OpnNn_exfdoV0RKAXdzBiLG2FfAf-JmbVBknsvgfvyKl2w-uNrh-rnWZQa1QUhFH7iJNkqaT9Z5',
    # 'ORGANIZATION_NAME': 'gAAAAABan3u0p124RWiThSVhDm39LDODyi52Wuv_vfcBqPZOE26h7gBrb6qxt0v-coGOv4BMrlo0rSSEoTLZyhJgBLatpg-cvDYq_omdNbgbp2_66Jj85Us=',
    # 'SHARED_KEY': 'gAAAAABan3vwqz1LzSutujw7N2e1MG5h8eEJJ0TbsMmoSZ0quH3i0Ud1eUoyQdOzV-fDfsxvD7Wl6SlT3Oh6utoNslg2sr6mDgTkayFsTLiamAyXGy_wGlDeHG__Q6zKsfY60P5axupu',
    # 'SECRET_KEY': 'gAAAAABan3wV0T_aao75lUI3uLdPFNpkLubXJ3N7Dk8o4jO3oGkYPh4Mfiv5GpZdofzByIyOnzyWpOiaqU_jODJGfEyr0MVNIBizhiHKiJh6UXZa0V1KdVntTLjhVi50dyLTM8ELAkHE',
    # 'CLIENT_ID': 'gAAAAABan3xE0dseFbR25jp5K7UH-7ksO5ICh1UlV2V14QdjfmLuSDgD0rqEUT85u8GbESwb-U7YeSj-Svhl4-L9elwqw9QZzKjWkvfN68CtTPip63d3G7s=',
    # 'CLIENT_SECRET': 'gAAAAABan3xoPNJMMDa4uQt846ZqwbDz38hWXDzxFigcImPT_yzLPYKDSayB2-4Ug5-HKGaDnmAzEt5VA1Z2BQv_fTZJkOQm_w=='

    # Integration credentials
    'ORGANIZATION_ID': 'gAAAAABa8-VD21m36Bumb9oovVa73z2f_rKCGAK05U5A1ou3WNmvjc0sAgOkUiMXsOJFQTTGW3hoaWm7aMR-Laf49t3eWE77EipO4l_aVaEU6ai4M4eSyyDgJzlWzH4Rv-lArcEbY_Ly',
    'ORGANIZATION_NAME': 'gAAAAABa8-Toy1Fb5PzbI_aAqouK8Slu1Kqqn3BG4Zcxd5trq5vBWQ322ISqsPrIAc0BJHgd64ZN2YoNwEqlk-9_eJ0ow9-Sp1Ta8-YgYIRDYcr5o969ssM=',
    'SHARED_KEY': 'gAAAAABa8-V9CGEIFw4JYAahN76_Fu0S7vaf-v-Q_KGIGnKHF9sp7UCm1l8oRPZNJyw2WI-_OUSRXLnhueul4LYF4_FQ5qQeuuhuEXO-3K4_aFb7faGlV3TDT8tvdJhvumvlMgTmQHvV',
    'SECRET_KEY': 'gAAAAABa8-XpIaqzFMo4bm2kxTpTl2pFKz4BciR_NDE03-8YwLfy9MXN1YIKY79ZQ1_TbaP5c6vUtsBsD50wRKEiNfbwzkcZGonYQEspK8yvHa_hPDU4IgShu9iM3vrNH8hZ054TINp1',
    'CLIENT_ID': 'gAAAAABa8-aqfK10tUh-gs9zqh1KsQCmNu2sXUaqF_MMcb-gfDA27DqBpRkfWs9dPf24XnZFAtlv4o4kX5OaMeGsX5XU1EHCK2skTB3gGIyTQ0cwsIQN0zg=',
    'CLIENT_SECRET': 'gAAAAABa8-cS-mwifXLrDWrBwxdwKbEd0k0Qh9CsERMb2wKVvwN4N70yMrnEOiGJWuz1CxUuTdzuYH5A4AkcMtWgXirc4iJ5_m1boVUYO1xuzroIFjCIYEE='
}

IAM_API_ENDPOINTS = {
    # Development Endpoints
    # 'IAM_URL': 'https://iam-integration.us-east.philips-healthsuite.com/',
    # 'IDM_URL': 'https://idm-integration.us-east.philips-healthsuite.com',
    # Integration Endpoints
    'IAM_URL': 'https://iam-service.us-east.philips-healthsuite.com/',
    'IDM_URL': 'https://idm-service.us-east.philips-healthsuite.com/',
    'LOGIN_URI': '/security/authentication/token/login',
    'OAUTH2_LOGIN_URI': '/authorize/oauth2/token',
    'OAUTH2_LOGIN_INFO': 'authorize/oauth2/userinfo',
    'NEW_USER_URI': '/authorize/identity/User',
    'RECOVER_PASSWORD': '/authorize/identity/User/$recover-password',
    'LOGOUT_USER_URI': '/authorize/oauth2/revoke',
    'UUID_URI': '/security/users?loginId=',
    'USER_INFO_URI': '/security/users/'
}

UNREACHABLE_SERVICES = ['Product__IDM__Nebuchadnezzar__Alive__Status',
                        'Administrative__Philips__Host__Reachability__Status'];
TOKEN_TIMEOUT = 300 #Number in seconds
TOKEN_SECRET = 'secret'
PROXY_FLAG = False
HTTP_PROXY  = "http://165.225.96.34:9480"
HTTPS_PROXY = "http://165.225.96.34:9480"
IDM_USER_CREATE = 201
IDM_USER_EXISTS = 200
# Development admin details
# ADMIN_USER_ID = 'bWFsYXRlc2gubXlsYXJhcHBhY2hhckBwaGlsaXBzLmNvbQ=='
# ADMIN_USER_PASS = 'UGhpbGlwc0lkbUAxMjM0'
# Integration admin details
ADMIN_USER_ID = 'bmF2ZWVuLnBlbnVtYWxhQHBoaWxpcHMuY29t'
ADMIN_USER_PASS = 'UGhpbGlwc18xNA=='
FERNET_SECRET = '-BEhr4qfb-GMRMZU1YK-Q6ifPIrQkKstA5cHbxHaF9M='
TOKEN_REFRESH_PEROID = 300
FORBIDDEN_STATUS_CODE = 401 #Status code for expired request
IGNORE_URI_PATHS = ['/iam_user/login', '/iam_user/create', '/iam_user/reset']
AUTH_HEADER = 'Authorization'
VALID_AUTH_HEADERS = ['X-Auth-Valid', 'X-Requested-With']
NOTES_MAXIMUM_LENGTH = 256
NOTES_MAXIMUM_COUNT = 10
NOTES_MAXIMUM_COUNT_ERROR_CODE = 601
NOTES_MAXIMUM_LENGTH_ERROR_CODE = 602
OK_STATUS_CODE = 200

IBC = {
    'SVN_URL': 'http://sc.phim.isyntax.net/svn/ibc/sites/',
    'SVN_USERNAME': 'operator',
    'SVN_PASSWORD': 'st3nt0r',
}
LHOST_IGNORE_KEYS = ['password']
