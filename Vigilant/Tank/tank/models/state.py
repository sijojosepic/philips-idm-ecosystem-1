from tank.tnconfig import COLLECTIONS
from tank.util import get_projection, QueryBuilder


class StateModel(object):
    def __init__(self, mongodb, siteid):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['STATES']]
        self.siteid = siteid
        self.filter = {}
        
    def get_state(self, hostname, key):
        match = self.collection.find_one(
            {'hostname': hostname, 'siteid': self.siteid},
            get_projection([key])
        )
        return {'result': match.get(key)}

    def get_siteids(self):
        result = self.collection.distinct('siteid')
        return {'siteids': sorted(list(result))}

    def get_mappings(self, hostname):
        site_list = self.get_siteids().get('siteids')
        result = self.collection.aggregate([
            {'$match': { 'hostname': hostname,'siteid': {'$in': site_list}}},
            {'$group': { '_id' : { 'siteid' : '$siteid', 'ip' : '$Administrative__Philips__Host__Information__IPAddress'}}},
            {'$project' : { '_id':0 , 'siteid':'$_id.siteid', 'address':'$_id.ip'}}
        ])
        return {'result': list(result)}

    def get_namespace_services(self, query):
        self.filter = StateQuery(query).get_filter()
        self.filter['siteid'] = self.siteid
        match = self.collection.find(self.filter, {'_id':0, 'siteid':0, 'hostname':0})
        services = list(match)
        service_namespaces = set(service_namespace for service in services for service_namespace in service.keys())
        return {'result': list(service_namespaces)}

class StateQuery(QueryBuilder):
    parameters = {
        'siteid': 'siteid',
        'hostname': 'hostname'
    }
