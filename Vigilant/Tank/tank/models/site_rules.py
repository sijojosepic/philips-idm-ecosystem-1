from tank.tnconfig import COLLECTIONS
from datetime import datetime
from tank.util import RedisInstance, get_logger, get_date_to_string_field_projection, get_projection, QueryBuilder
import json

log = get_logger()


class RulesModel(object):
    def __init__(self, mongodb):
        self.collection = mongodb[COLLECTIONS['SITE_MAINTENANCE_RULES']]
        self.hash_set = 'SITE_MAINTAINENCE_RULES'
        self.rds = RedisInstance.get_redis_instance()
        self.maintance_type = 'maintenance'

    def save_rules(self, request):
        start_time = datetime.strptime(request['start_time'], "%Y-%m-%d %H:%M:%S")
        end_time = datetime.strptime(request['end_time'], "%Y-%m-%d %H:%M:%S")
        self.collection.update(
            {"type": self.maintance_type, "siteid": request['siteid']},
            {
                "type": self.maintance_type,
                "siteid": request['siteid'],
                "nodes": request['nodes'],
                "services": request['services'],
                "user": request['user'],
                "start_time": start_time,
                "end_time": end_time,
                "remark": request['maintanance_remark']
            },
            upsert=True)
        self.update_rules_in_cache()
        return {'valid': True}

    def get_rules_with_siteid(self, siteid):
        site_ids = self.collection.aggregate([
            {'$match': {'siteid': siteid, 'type': self.maintance_type}},
            {'$project': {
                '_id': 0,
                'siteid': '$siteid',
                'start_time': get_date_to_string_field_projection('$start_time'),
                'end_time': get_date_to_string_field_projection('$end_time'),
                'remark': '$remark'
            }}
        ])
        return {'result': list(site_ids)}

    def get_rule_list(self):
        return {'sites': list(self.collection.distinct('siteid', {'type': self.maintance_type}))}

    def get_all_sites_under_maintanance(self):
        return {'rules': list(self.collection.find({'type': self.maintance_type}))}

    def disable_maintaince(self, siteid):
        self.collection.delete_one({'siteid': siteid, 'type': self.maintance_type})
        self.update_rules_in_cache()
        return {'result': True}

    def update_rules_in_cache(self):
        rules = self.get_all_sites_under_maintanance()
        try:
            self.rds.delete(self.hash_set)
            [(self.set_site_cache(rule),
              self.set_site_nodes_cache(rule),
              self.set_site_node_service_cache(rule)
              )
             for rule in rules['rules']]
        except Exception as e:
            log.error("Redis update failed because of ", e.message)
            return True

    def get_hash_val(self, rule):
        return json.dumps({'start_time': datetime.strftime(rule.get('start_time'),'%Y-%m-%d %H:%M:%S.%f'),
                'end_time': datetime.strftime(rule.get('end_time'),'%Y-%m-%d %H:%M:%S.%f')})

    def set_site_cache(self, rule):
        self.rds.hset(self.hash_set, '{siteid}'.format(siteid=rule.get('siteid')), self.get_hash_val(rule))

    def set_site_nodes_cache(self, rule):
        for node in rule.get('nodes', []):
            self.rds.hset(self.hash_set, '{siteid}_{node}'.format(siteid=rule.get('siteid'), node=node),
                          self.get_hash_val(rule))

    def set_site_node_service_cache(self, rule):
        if not rule.get('nodes', None):
            [self.rds.hset(self.hash_set,
                           '{siteid}_{node}_{service}'.format(siteid=rule.get('siteid'), node=node,
                                                              service=service),
                           self.get_hash_val(rule)) for node in rule.get('nodes') for service in
             rule.get('services', [])]

        else:
            [self.rds.hset(self.hash_set,
                           '{siteid}_{service}'.format(siteid=rule.get('siteid'), service=service),
                           self.get_hash_val(rule)) for service in
             rule.get('services', [])]



    def get_rules(self, query):
        self.filter = SiteRulesQuery(query).get_filter()
        result = self.collection.aggregate([
            {'$match': self.filter},
            {'$project': self.get_projection()}
        ])
        return {'result':list(result)}


    def save_assignment_rule(self, request):
        if self.block_rule_update(request['siteid'], request['type']):
            self.collection.insert_one({
                'siteid': request['siteid'],
                'user': request['user'],
                'type':request['type'],
                'start_time': datetime.now()
            })
            return {'valid': True}
        else:
            return {'valid': False}

    def block_rule_update(self, siteid, type):
        return True if len(list(self.collection.find({'siteid': siteid, 'type': type}))) == 0 else False

    def delete_assignment_rule(self, siteid, rule_type):
        self.collection.delete_one({'siteid': siteid, 'type':rule_type})
        self.update_rules_in_cache()

    def get_projection(self):
        return {
                '_id': 0,
                'siteid': '$siteid',
                'user':'$user',
                'type':'$type',
                'maintanance_remark':'$maintanance_remark',
                'services':'$services',
                'nodes':'$nodes',
                'start_time': get_date_to_string_field_projection('$start_time'),
                'end_time': get_date_to_string_field_projection('$end_time')
        }


class SiteRulesQuery(QueryBuilder):
    parameters = {
        'siteid': 'siteid',
        'type': 'type',
    }
