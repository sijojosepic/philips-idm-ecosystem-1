import jwt, json
from tank.tnconfig import COLLECTIONS, TOKEN_TIMEOUT, TOKEN_SECRET
from datetime import datetime


class GeoUserAuditModel(object):
    def __init__(self, mongodb, request, headers):
        self.mongodb = mongodb
        self.request = request
        self.headers = headers
        self.collection = mongodb[COLLECTIONS['USER_AUDIT']]
        self.status = {'valid': False}

    def set_rules_audit(self):
        token = self.headers['x-token']
        json_str = self.decrypt_token(token)
        if 'valid' in json_str:
            return json_str
        else:
            self.status['valid'] = True
            self.update_audit_request(json_str)
        return self.status

    def decrypt_token(self, token):
        try:
            tk = jwt.decode(token, TOKEN_SECRET)
        except Exception as ex:
            return self.status
        return json.loads(json.dumps(tk))

    def update_audit_request(self, json_str):
        data = {}
        data['uname'] = json_str['key']
        data['timestamp'] = datetime.now().strftime("%y-%m-%d-%H-%M-%S")
        data['request object'] = self.request
        data['action'] = self.request['action']
        data['payload'] = self.request['payload']
        self.collection.insert_one(data)

