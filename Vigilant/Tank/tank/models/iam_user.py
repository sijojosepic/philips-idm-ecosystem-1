from tank.tnconfig import COLLECTIONS
from datetime import datetime


class IAMUserModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['IAM_USER']]

    def get_user_details(self, loginId):
        return list(self.collection.find({'loginId': loginId},{"_id":0,"givenName":1,"familyName":1}))

    def add_new_user(self, uuid, request_obj):
        self.collection.update_one({'userUUID': uuid}, {'$set': {'loginId': request_obj.get('email'),
                                    'givenName': request_obj.get('firstname'), 'familyName': request_obj.get('lastname'),
                                    'timestamp': datetime.now()}}, upsert=True)





