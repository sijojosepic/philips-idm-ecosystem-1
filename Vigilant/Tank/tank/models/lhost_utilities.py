from tank.tnconfig import COLLECTIONS
from tank.util import get_logger


class LhostUtilitiesModel(object):
    def __init__(self, mongodb):
        self.collection = mongodb[COLLECTIONS['LHOST_UTILITIES']]
        self.tags_type = 'tag'
        self.scanners_type = 'scanner'

    def get_tags_list(self):
        return {'tags': self.collection.distinct('name', {'type': self.tags_type})}

    def get_scanners_list(self):
        return {'scanners': sorted(self.collection.distinct('name', {'type': self.scanners_type}))}