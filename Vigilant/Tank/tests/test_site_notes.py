import re
import unittest
from mock import patch, MagicMock, call


class TankNotesModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import site_notes
        self.site_notes = site_notes
        self.site_notes.COLLECTIONS = {'SITE_NOTES': 'site_notes'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.site_notes.NotesModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_notes(self):
        self.site_notes.SiteNotesQuery = MagicMock(name='SiteNotesQuery')
        self.site_notes.SiteNotesQuery.get_filter.return_value={'siteid':'ABC'}
        self.model.collection.aggregate.return_value=[{'elementid':'ABC-1532690598', 'siteid':'ABC', 'message':'Added a node', 'created_by':'user1', 'created_on':'2018-07-02 22:00:00'}]
        self.assertEqual(self.model.get_notes('ABC', {}), {'result':[{'elementid':'ABC-1532690598', 'siteid':'ABC', 'message':'Added a node', 'created_by':'user1', 'created_on':'2018-07-02 22:00:00'}]})

    def test_save_notes(self):
        self.notes_payload = {"message" : "Sample Notes", "siteid" : "DLP00","created_by" : "idmuser"}
        self.assertEqual(self.model.save_notes(self.notes_payload), {'code':200})

    def test_save_notes_on_exceeding_maximum_notes_count(self):
        self.notes_payload = {"message" : "Sample Notes", "siteid" : "DLP00","created_by" : "idmuser"}
        self.model.get_notes = MagicMock(name='get_notes')
        self.model.get_notes.return_value={'result':[self.notes_payload]*30}
        self.assertEqual(self.model.save_notes(self.notes_payload), {'code':601})

    def test_save_notes_on_exceeding_maximum_notes_length(self):
        self.notes_payload = {"message" : "Sample Notes"*1000, "siteid" : "DLP00","created_by" : "idmuser"}
        self.assertEqual(self.model.save_notes(self.notes_payload), {'code':602})

    def test_delete_one_note(self):
        self.model.delete_notes('1')
        self.model.collection.delete_one.assert_called_once_with({'_id': '1'})

    def test_delete_multiple_notes(self):
        self.model.delete_notes('1,2,3')
        self.assertEqual(self.model.collection.delete_one.call_count, 3)

if __name__ == '__main__':
    unittest.main()
