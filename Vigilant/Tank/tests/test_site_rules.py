import re
import unittest
from mock import patch, MagicMock, call



class TankRulesModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_tank = MagicMock(name='tank')
        self.redis = MagicMock(name='redis')
        self.mock_bottle_mongo = MagicMock(name='bottle_mongo')
        self.mock_bottle = MagicMock(name='bottle')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {
            'bottle_mongo': self.mock_bottle_mongo,
            'bottle': self.mock_bottle,
            'phimutils': self.mock_phimutils,
            'phimutils.plogging': self.mock_phimutils.plogging,
            'datetime': self.mock_datetime,
            'tank.util.RedisInstance': self.mock_tank.util.RedisInstance,
            'tank.util': self.mock_tank.util,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import site_rules
        self.site_rules = site_rules
        self.site_rules.COLLECTIONS = {'SITE_MAINTENANCE_RULES': 'site_maintenance_rules'}

        self.mongodb = MagicMock(name='mongo_db')
        self.request = {
            "siteid": "ABC01",
            "nodes": ["ABC01-192.168", "IST01-192.169", "IST01-192.170"],
            "services": ["Host__Information__output", "Nodes__Inforamtion__State", "Host__Information__Deployment"],
            "start_time": "2018-07-02 22:00:00",
            "end_time": "2018-07-02 22:00:00",
            "user": "ABCD",
            "maintanance_remark": "Os upgrade"
        }
        self.model = self.site_rules.RulesModel(self.mongodb)
        self.model.hash_set = 'TEST'
        self.model.get_hash_val = MagicMock(name='get_hash_val', return_value={'start_time': 'ST', 'end_time': 'ET'})
        self.model.rds = self.redis

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_rules(self):
        self.mock_datetime.datetime.strptime.return_value = '2018-07-02 22:00:00'
        self.model.save_rules(self.request)
        self.model.collection.update.assert_called_once_with(
            {"type": "maintenance", "siteid": "ABC01"},
            {
                "type": "maintenance",
                "siteid": "ABC01",
                "nodes": ["ABC01-192.168", "IST01-192.169", "IST01-192.170"],
                "services": ["Host__Information__output", "Nodes__Inforamtion__State", "Host__Information__Deployment"],
                "user": "ABCD",
                "start_time": "2018-07-02 22:00:00",
                "end_time": "2018-07-02 22:00:00",
                "remark": "Os upgrade"
            }, upsert=True)

    def test_get_rules_with_siteid(self):
        self.model.collection.aggregate.return_value = ["ABC01", "ABC02"]
        self.assertEqual(
            self.model.get_rules_with_siteid('TEST'),
            {'result': ["ABC01", "ABC02"]}
        )
        self.mock_datetime.datetime.strptime.return_value = 'YYYY-MM-DD HH:II:SS'
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'type': 'maintenance', 'siteid': 'TEST'}},
            {'$project': {'remark': '$remark',
                          'start_time': self.site_rules.get_date_to_string_field_projection.return_value,
                          '_id': 0,
                          'siteid': '$siteid',
                          'end_time': self.site_rules.get_date_to_string_field_projection.return_value}
             }
        ])
        self.assertEqual(self.site_rules.get_date_to_string_field_projection.call_count, 2)

    def test_get_all_sites_under_maintanance(self):
        self.model.collection.find.return_value = ["SITE1", "SITE2"]
        self.assertEqual(
            self.model.get_all_sites_under_maintanance(),
            {'rules': self.model.collection.find.return_value}
        )
        self.model.collection.find.assert_called_once_with({'type': self.model.maintance_type})

    def test_get_rule_list(self):
        self.model.collection.distinct.return_value = [{"INFO1": "INFO2"}]
        self.assertEqual(
            self.model.get_rule_list(),
            {'sites': [{"INFO1": "INFO2"}]}
        )

    def test_update_rules_in_cache(self):
        rule = {'siteid': 'SIT', 'nodes': ['N1', 'N2'], 'services': ['S1', 'S2']}
        self.model.get_all_sites_under_maintanance = MagicMock(name="get_all_sites_under_maintanance",
                                                               return_value={'rules': [rule]})
        self.model.set_site_cache = MagicMock(name='set_site_cache')
        self.model.set_site_node_service_cache = MagicMock(name='set_site_node_service_cache')
        self.model.set_site_nodes_cache = MagicMock(name='set_site_nodes_cache')

        self.model.update_rules_in_cache()
        self.model.rds.delete.assert_called_once_with(self.model.hash_set)
        self.model.set_site_cache.assert_called_once_with(rule)
        self.model.set_site_nodes_cache.assert_called_once_with(rule)
        self.model.set_site_node_service_cache.assert_called_once_with(rule)

    def test_update_multiple_rules_in_cache(self):
        rule = {'siteid': 'SIT', 'nodes': ['N1', 'N2'], 'services': ['S1', 'S2'], 'start_time': 'ST', 'end_time': 'ET'}
        rule2 = {'siteid': 'SIT1', 'nodes': ['N3', 'N4'], 'services': ['S3', 'S4'], 'start_time': 'ST',
                 'end_time': 'ET'}
        self.model.get_all_sites_under_maintanance = MagicMock(name="get_all_sites_under_maintanance",
                                                               return_value={'rules': [rule, rule2]})

        self.model.set_site_cache = MagicMock(name='set_site_cache')
        self.model.set_site_node_service_cache = MagicMock(name='set_site_node_service_cache')
        self.model.set_site_nodes_cache = MagicMock(name='set_site_nodes_cache')

        self.model.update_rules_in_cache()
        self.model.set_site_cache.assert_has_calls([
            call(rule),
            call(rule2)
        ])
        self.model.set_site_nodes_cache.assert_has_calls([
            call(rule),
            call(rule2)
        ])
        self.model.set_site_node_service_cache.assert_has_calls([
            call(rule),
            call(rule2)
        ])

    def test_set_site_cache(self):
        rule = {'siteid': 'SIT', 'nodes': ['N1', 'N2'], 'services': ['S1', 'S2']}
        self.model.set_site_cache(rule)
        self.redis.hset.assert_called_once_with(self.model.hash_set, 'SIT', self.model.get_hash_val.return_value)

    def test_set_site_nodes_cache(self):
        rule = {'siteid': 'SIT', 'nodes': ['N1', 'N2'], 'services': ['S1', 'S2']}
        self.model.set_site_nodes_cache(rule)
        self.redis.hset.assert_has_calls([
            call(self.model.hash_set, 'SIT_N1', {'start_time': 'ST', 'end_time': 'ET'}),
            call(self.model.hash_set, 'SIT_N2', {'start_time': 'ST', 'end_time': 'ET'})
        ])
        self.assertEqual(self.redis.hset.call_count, 2)

    def test_set_site_nodes_cache_with_no_nodes(self):
        rule = {'siteid': 'SIT', 'nodes': []}
        self.model.set_site_nodes_cache(rule)
        self.redis.hset.assert_not_called()
        self.assertEqual(self.redis.hset.call_count, 0)

    def set_site_node_service_cache_with_no_nodes(self):
        rule = {'siteid': 'SIT', 'nodes': [], 'services': ['S1', 'S2']}
        self.model.set_site_nodes_cache(rule)
        self.redis.hset.assert_has_calls([
            call(self.model.hash_set, 'SIT_S1', {'start_time': 'ST', 'end_time': 'ET'}),
            call(self.model.hash_set, 'SIT_S2', {'start_time': 'ST', 'end_time': 'ET'})
        ])
        self.assertEqual(self.redis.hset.call_count, 2)

    def set_site_node_service_cache_with_nodes(self):
        rule = {'siteid': 'SIT', 'nodes': ['N1'], 'services': ['S1', 'S2']}
        self.model.set_site_nodes_cache(rule)
        self.redis.hset.assert_has_calls([
            call(self.model.hash_set, 'SIT_N1_S1', {'start_time': 'ST', 'end_time': 'ET'}),
            call(self.model.hash_set, 'SIT_N1_S2', {'start_time': 'ST', 'end_time': 'ET'})
        ])
        self.assertEqual(self.redis.hset.call_count, 2)

    def test_get_rules(self):
        self.site_rules.SiteRulesQuery = MagicMock(name='SiteRulesQuery')
        self.site_rules.SiteRulesQuery.get_filter.return_value = {'siteid': 'ABC', 'type': 'assignment'}
        self.model.collection.aggregate.return_value = [
            {'siteid': 'ABC', 'type': 'assignment', 'user': 'user1', 'start_time': '2018-07-02 22:00:00'}]
        self.assertEqual(self.model.get_rules({'siteid': 'ABC', 'type': 'assignment'}), {
            'result': [{'siteid': 'ABC', 'type': 'assignment', 'user': 'user1', 'start_time': '2018-07-02 22:00:00'}]})

    def test_save_assignment_rule(self):
        self.assignment_request = {'siteid': 'ABC', 'type': 'assignment', 'user': 'user1'}
        self.mock_datetime.datetime.now.return_value = '2018-07-02 22:00:00'
        self.model.block_rule_update = MagicMock(name='block_rule_update')
        self.model.block_rule_update.return_value = True
        self.model.save_assignment_rule(self.assignment_request)
        self.model.collection.insert_one.assert_called_once_with({
            'siteid': 'ABC',
            'user': 'user1',
            'type': 'assignment',
            'start_time': '2018-07-02 22:00:00'
        })

    def test_block_save_assignment_rule(self):
        self.assignment_request = {'siteid': 'ABC', 'type': 'assignment', 'user': 'user1'}
        self.model.block_rule_update = MagicMock(name='block_rule_update')
        self.model.block_rule_update.return_value = False
        self.assertEqual(self.model.save_assignment_rule(self.assignment_request), {'valid': False})

    def test_block_rule_update(self):
        self.model.collection.find.return_value = {'siteid': 'ABC00', 'type': 'assignment', 'user': 'user1'}
        self.model.block_rule_update('ABC00', 'assignment')
        self.model.collection.find.assert_called_once_with({'siteid': 'ABC00', 'type': 'assignment'})
        self.assertEqual(self.model.block_rule_update('ABC00', 'assignment'), False)

    def test_delete_assignment_rule(self):
        self.model.delete_assignment_rule('ABC', 'assignment')
        self.model.collection.delete_one.assert_called_once_with({'type': 'assignment', 'siteid': 'ABC'})


if __name__== '__main__':
    unittest.main()


