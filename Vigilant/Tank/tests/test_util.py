import unittest
from datetime import datetime

from mock import patch, MagicMock


class TankUtilTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_bottle_mongo = MagicMock(name='bottle_mongo')
            self.mock_bottle = MagicMock(name='bottle')
            self.mock_phimutils = MagicMock(name='phimutils')
            modules = {
                'bottle_mongo': self.mock_bottle_mongo,
                'bottle': self.mock_bottle,
                'phimutils': self.mock_phimutils,
                'phimutils.plogging': self.mock_phimutils.plogging
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            from tank import util
            self.util = util

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class TankUtilTestCase(TankUtilTest.TestCase):
    def test_is_version_match_exact_match(self):
        self.assertTrue(self.util.is_version_match('4,4,4,4', '4,4,4,4'))

    def test_is_version_match_left_shorter_match(self):
        self.assertTrue(self.util.is_version_match('4,4', '4,4,4,4'))

    def test_is_version_match_right_shorter_match(self):
        self.assertTrue(self.util.is_version_match('4.4.4.4', '4,4'))

    def test_is_version_match_alpha_match(self):
        self.assertTrue(self.util.is_version_match('3,6a,22', '3,6a,22,1'))

    def test_is_version_match_no_match(self):
        self.assertFalse(self.util.is_version_match('4,4,4,4', '4,3'))

    def test_get_projection_empty(self):
        self.assertEqual(self.util.get_projection([]), {'_id': 0})

    def test_get_projection(self):
        self.assertEqual(self.util.get_projection(['hostname', 'key1']), {'key1': 1, 'hostname': 1, '_id': 0})

    def test_get_keys_in_site(self):
        site_facts = [
            {
                'LN': {
                    'alias': 'magic server',
                    'use': 'equallogic_san'
                }
            },
            {
                'ISP': {
                    'version': '4,4,2,1',
                    'module_type': '8',
                },
                'Windows': {
                    'flags': ['domain_needed']
                }
            },
            {
                'ISP': {
                    'version': '4,4,2,1',
                    'module_type': '16',
                },
                'Windows': {
                    'flags': ['domain_needed']
                }
            }
        ]
        result = self.util.get_keys_in_site(site_facts)
        expected = ['Windows', 'ISP', 'LN']
        self.assertEqual(len(result), len(expected))
        self.assertEqual(sorted(result), sorted(expected))

    def test_get_keys_in_site_no_matching(self):
        result = self.util.get_keys_in_site([])
        self.assertEqual(result, [])

    def test_get_date_to_string_field_projection(self):
        self.assertEqual(
            self.util.get_date_to_string_field_projection('$time_here'),
            {'$dateToString': {'date': '$time_here', 'format': '%Y/%m/%d  %H:%M:%S'}}
        )

    def test_rename_id(self):
        self.assertEqual(
            self.util.rename_id('new_one', {'_id': 'idval', 'key2': 'val2'}),
            {'key2': 'val2', 'new_one': 'idval'}
        )

    def test_get_list_from_param(self):
        self.assertEqual(
            self.util.get_list_from_param(' some, list,here,vals,here '),
            ['vals', 'list', 'some', 'here']
        )

    def test_get_list_from_param_none(self):
        self.assertEqual(self.util.get_list_from_param(None), [])


class TankUtilQueryBuilderTestCase(TankUtilTest.TestCase):
    def setUp(self):
        TankUtilTest.TestCase.setUp(self)
        self.util.QueryBuilder.parameters = {
            'siteid': 'siteid',
            'state': 'state',
            'service': 'service'
        }
        self.query_builder = self.util.QueryBuilder({})

    def test_query_builder(self):
        self.query_builder.process_query = MagicMock(name='process_query')
        self.query_builder.query = {
            'siteid': 'ABC00',
            'state': 'TEXAS',
            'service': 'service1'
        }
        self.assertEqual(
            self.query_builder.get_filter(),
            {'state': 'TEXAS', 'siteid': 'ABC00', 'service': 'service1'}
        )
        self.query_builder.process_query.assert_called_once_with()

    def test_query_builder_no_valid(self):
        self.query_builder.query = {
            'site': 'ABC00',
            'status': 'ERROR',
        }
        self.assertEqual(self.query_builder.get_filter(), {})

    def test_get_datetime(self):
        self.assertEqual(self.query_builder.get_datetime('1995-05-10'), datetime(1995, 05, 10))

    def test_get_datetime_none(self):
        self.assertEqual(self.query_builder.get_datetime(None), None)

    def test_get_datetime_invalid(self):
        self.assertRaises(ValueError, self.query_builder.get_datetime, 'this is not a date')

    def test_get_between_both(self):
        self.assertEqual(self.query_builder.get_between_filter(2, 4), {'$lte': 4, '$gte': 2})

    def test_get_between_start_only(self):
        self.assertEqual(self.query_builder.get_between_filter(2, None), {'$gte': 2})

    def test_get_between_end_only(self):
        self.assertEqual(self.query_builder.get_between_filter(None, 4), {'$lte': 4})

    def test_get_between_dates(self):
        self.query_builder.get_datetime = MagicMock(name='get_datetime', side_effect=[4, 8])
        self.query_builder.get_between_filter = MagicMock(name='get_between_filter')
        self.assertEqual(
            self.query_builder.get_between_dates_filter('1995-04-05', '2000-12-12'),
            self.query_builder.get_between_filter.return_value
        )
        self.query_builder.get_between_filter.assert_called_once_with(4, 8)


if __name__ == '__main__':
    unittest.main()
