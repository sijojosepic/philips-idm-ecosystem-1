import unittest
from mock import MagicMock, patch


class TankTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_bottle_mongo = MagicMock(name='bottle_mongo')
        self.mock_bottle = MagicMock(name='bottle')
        self.mock_tank = MagicMock()
        modules = {
            'bottle_mongo': self.mock_bottle_mongo,
            'bottle': self.mock_bottle,
            'tank': self.mock_tank,
            'tank.controllers': self.mock_tank.controllers,
            'tank.tnconfig': self.mock_tank.tnconfig,
            'tank.util': self.mock_tank.util,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import tank_app
        self.tank = tank_app

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_health(self):
        self.assertEqual(self.tank.health(), 'OK')


if __name__ == '__main__':
    unittest.main()
