import unittest
from mock import MagicMock, patch, call, PropertyMock

from pymongo.errors import PyMongoError


class MongoHandlerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        self.mock_pymongo = MagicMock(name='pymongo')
        self.mock_utils = MagicMock(name='utils')
        self.mock_app_config = MagicMock(name='app_config')
        self.mock_cryptography = MagicMock(name='cryptography')
        modules = {
            'logging': self.mock_logging,
            'utils': self.mock_utils,
            'pymongo': self.mock_pymongo,
            'app_config': self.mock_app_config,
            'cryptography': self.mock_cryptography,
            'cryptography.fernet': self.mock_cryptography.fernet,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import mongo_handler
        self.module = mongo_handler
        self.module.FERNET_SK = 'FERNET_SK'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_mongo_connection(self):
        mock_auth_url = MagicMock(name='_mongo_auth_url')
        mock_auth_url.return_value = 'auth_url'
        self.module._mongo_auth_url = mock_auth_url
        connection_mock = MagicMock(name='connection')
        connection_mock.__getitem__.return_value = 'con'
        self.mock_pymongo.MongoClient.return_value = connection_mock

        connection = self.module.mongo_connection('uri', 'username',
                                                  'password', 'protocol',
                                                  'auth_source', 'database')
        self.assertEqual(connection, 'con')
        self.mock_pymongo.MongoClient.assert_called_once_with(
            'auth_url', connect=False)
        mock_auth_url.assert_called_once_with('uri', 'username',
                                              'password', 'protocol',
                                              'auth_source')
        connection_mock.__getitem__.assert_called_once_with('database')

    def test_mongo_connection_exception(self):
        self.mock_pymongo.MongoClient.side_effect = PyMongoError
        self.assertRaises(
            PyMongoError, self.module.mongo_connection, *('uri', 'username',
                                                          'password', 'protocol',
                                                          'auth_source', 'database'))

    def test_mongo_auth_url(self):
        mock_FernetCrypto = MagicMock(name='FernetCrypto')
        mock_FernetCrypto.return_value.decrypt.side_effect = ['u1', 'p1']
        self.module.FernetCrypto = mock_FernetCrypto
        auth_url = self.module._mongo_auth_url('uri', 'username',
                                               'password', 'protocol',
                                               'auth_source')
        self.assertEqual(auth_url, 'protocol://u1:p1@uri?authSource=auth_source')
        self.assertEqual(mock_FernetCrypto().decrypt.call_count, 2)

    def test_fernet_crypto_decrypt(self):
        with patch('mongo_handler.FernetCrypto.cryptor', new_callable=PropertyMock) as cryptor:
            fernet_obj = self.module.FernetCrypto()
            self.assertEqual(fernet_obj.decrypt('abcd'), cryptor().decrypt.return_value)

    def test_ferent_crypto_cryptor(self):
        fernet_obj = self.module.FernetCrypto()
        fernet_obj._cryptor = None
        self.assertEqual(self.mock_cryptography.fernet.Fernet.return_value, fernet_obj.cryptor)
        self.mock_cryptography.fernet.Fernet.assert_called_once_with(
            'FERNET_SK')

    def test_ferent_crypto_cryptor_not_None(self):
        fernet_obj = self.module.FernetCrypto()
        fernet_obj._cryptor = 'not None'
        self.assertEqual('not None', fernet_obj.cryptor)


if __name__ == '__main__':
    unittest.main()
