import unittest
from mock import MagicMock, patch, call


class S3WrapperTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_socket = MagicMock(name='socket')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'socket': self.mock_socket,
            'phimutils.message': self.mock_phimutils.message
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.mock_socket.gethostname.return_value = 'host_name'
        self.mock_socket.gethostbyname.return_value = 'host_address'
        import phim_backoffice.billing.event
        self.event = phim_backoffice.billing.event

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_load(self):
        self.mock_socket.gethostname.assert_called_once_with()
        self.mock_socket.gethostbyname.assert_called_once_with('host_name')

    def test_event_message(self):
        self.event.HOST_NAME = 'host_name'
        self.event.HOST_ADDRESS = 'host_address'
        self.mock_phimutils.message.Message.return_value = 'msg'
        event_msg = self.event.event_message('s1', 'n_t', 'state', {'a': 'b'})
        self.assertEqual(event_msg, 'msg')
        exp_msg_call = [call(hostaddress='host_address', hostname='host_name', payload={
                             'a': 'b'}, service='Administrative__Philips__ISPACSBilling__Upload__Status', siteid='s1', state='state', type='n_t')]
        self.assertEqual(
            self.mock_phimutils.message.Message.mock_calls, exp_msg_call)
