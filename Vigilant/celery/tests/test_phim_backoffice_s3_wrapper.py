import unittest
from mock import MagicMock, patch  # , call, mock_open


class S3WrapperTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(
            name='cached_property', cached_property=property)
        self.mock_boto = MagicMock(name='boto')
        self.mock_s3 = MagicMock(name='s3')
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'cached_property': self.mock_cached_property,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
            'boto': self.mock_boto,
            'boto.s3': self.mock_boto.s3,
            'boto.s3.key': self.mock_boto.s3.key,
            'boto.s3.connection': self.mock_boto.s3.connection,
            'boto.exception': self.mock_boto.exception,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.billing.s3_wrapper
        self.s3_wrapper = phim_backoffice.billing.s3_wrapper

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_s3_wrapper_initialization(self):
        self.mock_boto.connect_s3.return_value = 'conn'
        s3_obj = self.s3_wrapper.S3Wrapper('access_key', 'secret_key', 'bucket_name')
        self.assertEqual(s3_obj._bucket_name, 'bucket_name')
        self.mock_boto.connect_s3.assert_called_once_with('access_key', 'secret_key')
        self.assertEqual(s3_obj.conn, 'conn')

    def test_s3_wrapper_bucket(self):
        conn_mock = MagicMock(name='conn')
        conn_mock.get_bucket.return_value = 'bucket'
        self.mock_boto.connect_s3.return_value = conn_mock
        s3_obj = self.s3_wrapper.S3Wrapper('access_key', 'secret_key', 'bucket_name')
        self.assertEqual(s3_obj.bucket, 'bucket')
        conn_mock.get_bucket.assert_called_once_with('bucket_name')

    def test_s3_wrapper_s3Key(self):
        self.mock_boto.s3.key.Key.return_value = 'S3Key'
        s3_obj = self.s3_wrapper.S3Wrapper('access_key', 'secret_key', 'bucket_name')
        s3_obj.bucket = 'bucket'
        self.assertEqual(s3_obj.S3Key, 'S3Key')
        self.mock_boto.s3.key.Key.assert_called_once_with('bucket')

    def test_s3_file_name_with_folder(self):
        s3_obj = self.s3_wrapper.S3Wrapper('access_key', 'secret_key', 'bucket_name')
        f_name = s3_obj._s3_file_name('folder', 'filename')
        self.assertEqual(f_name, 'folder/filename')

    def test_s3_file_name_without_folder(self):
        s3_obj = self.s3_wrapper.S3Wrapper('access_key', 'secret_key', 'bucket_name')
        f_name = s3_obj._s3_file_name(None, 'filename')
        self.assertEqual(f_name, 'filename')

    def test_s3_wrapper_upload(self):
        key_mock = MagicMock(name='S3Key')
        s3_fname_mk = MagicMock(name='_s3_file_name')
        s3_fname_mk.return_value = 'folder/file'
        key_mock.set_contents_from_filename.return_value = 10
        self.mock_boto.s3.key.Key.return_value = key_mock
        s3_obj = self.s3_wrapper.S3Wrapper('access_key', 'secret_key', 'bucket_name')
        s3_obj._s3_file_name = s3_fname_mk
        s3_obj.upload('file_name', 'absolute_file_path', 'folder')
        key_mock.set_contents_from_filename.assert_called_once_with('absolute_file_path')
        s3_fname_mk.assert_called_once_with('folder', 'file_name')
        self.assertEqual(key_mock.key, 'folder/file')


if __name__ == '__main__':
    unittest.main()
