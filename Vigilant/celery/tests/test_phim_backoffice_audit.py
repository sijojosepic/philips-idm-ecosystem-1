import unittest
from mock import MagicMock, patch, DEFAULT


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class AuditTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        self.backoffice_enums = MagicMock(name="backoffice_enums")
        modules = {
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.helpers': self.mock_pb.helpers,
            'phim_backoffice.celery': self.mock_pb.celery,
            'phim_backoffice.persist': self.mock_pb.persist,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
            'phim_backoffice.backoffice_enums': self.backoffice_enums
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.audit
        import phim_backoffice.persist
        self.audit = phim_backoffice.audit
        self.datastore_patch = patch.multiple(
            'phim_backoffice.audit',
            find_one=DEFAULT
        )
        self.mock_datastore = self.datastore_patch.start()
        self.audit.datetime = MagicMock(name='utcnow')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_insert_dated_audit(self):
        self.audit.insert_dated_audit('distribution', {'name': 'anywhere-1.2'})
        self.audit.insert_one.assert_called_once_with(
            'AUDIT',
            {
                'type': 'distribution',
                'name': 'anywhere-1.2',
                'timestamp': self.audit.datetime.utcnow.return_value,
            }
        )

    def test_update_dated_audit(self):
        self.audit.update_dated_audit({'key1': 'val1'}, {'name': 'anywhere-1.2'})
        self.audit.update_one.assert_called_once_with(
            'AUDIT',
            {'key1': 'val1'},
            {
                'name': 'anywhere-1.2',
                'timestamp': self.audit.datetime.utcnow.return_value,
            }
        )

    def test_update_distribution(self):
        self.audit.update_dated_audit = MagicMock(name='update_dated_audit')
        self.audit.get_validated_fields.return_value = {'name': 'anywhere-1.2'}
        self.audit.update_distribution({'key1': 'val1'},
                                       name='anywhere-1.2',
                                       rules=['multisitedeployment'],
                                       status='Submitted',
                                       createdby='IDM Portal',
                                       timestamp='2016-03-21T11:14:37.412Z',
                                       modifiedby='IDM Portal',
                                       type='distribution',
                                       siteid='AMC01',
                                       version='VX',
                                       size='0B'
                                       )
        self.audit.update_dated_audit.assert_called_once_with({'key1': 'val1'}, {'name': 'anywhere-1.2'})
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.DISTRIBUTION_FIELDS,
            createdby='IDM Portal',
            modifiedby='IDM Portal',
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            siteid='AMC01',
            status='Submitted',
            timestamp='2016-03-21T11:14:37.412Z',
            type='distribution',
            version='VX',
            size='0B'
        )

    def test_insert_distribution(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        self.audit.get_validated_fields.return_value = {'name': 'anywhere-1.2'}
        self.audit.insert_distribution(
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            status='Submitted',
            createdby='IDM Portal',
            timestamp='2016-03-21T11:14:37.412Z',
            modifiedby='IDM Portal',
            type='distribution',
            siteid='AMC01',
            version='VX'
        )
        self.audit.insert_dated_audit.assert_called_once_with('distribution', {'name': 'anywhere-1.2'})
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.DISTRIBUTION_FIELDS,
            createdby='IDM Portal',
            modifiedby='IDM Portal',
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            siteid='AMC01',
            status='Submitted',
            timestamp='2016-03-21T11:14:37.412Z',
            type='distribution',
            version='VX'
        )

    def test_insert_deployment(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        self.audit.get_validated_fields.return_value = {'packagename': 'anywhere-1.2'}
        self.audit.insert_deployment(
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            status='Submitted',
            createdby='IDM Portal',
            timestamp='2016-03-21T11:14:37.412Z',
            modifiedby='IDM Portal',
            type='distribution',
            siteid=['AMC01', 'BENHC'],
            version='v1'
        )
        self.audit.insert_dated_audit.assert_called_once_with('deployment', {'packagename': 'anywhere-1.2'})
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.DEPLOYMENT_FIELDS,
            createdby='IDM Portal',
            modifiedby='IDM Portal',
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            siteid=['AMC01', 'BENHC'],
            status='Submitted',
            timestamp='2016-03-21T11:14:37.412Z',
            type='distribution',
            version='v1'
        )

    def test_update_package_status(self):
        self.audit.insert_distribution = MagicMock(name='insert_distribution')
        payload = {'localhost': {'PCM': [{'version': 'X.Y.Z', 'name': 'XXX',
                                          'status': 'Available', 'fsize': '0 KB'}]}}
        siteid = "ID"
        data = {
                'subscription': 'XXX',
                'version': 'X.Y.Z',
                'siteid': siteid,
                'status':'Available',
                'size' : '0 KB'
            }
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        self.audit.find_one.return_value = {}
        self.audit.get_validated_fields.return_value = data
        self.audit.update_package_status(siteid, payload)
        data['user'] = 'IDM Portal'
        self.audit.insert_distribution.assert_called_once_with(**data)

    def test_deployment_status(self):
        payload = {'status': 'Success', 'deployment_id': 'ID', 'siteid': 'siteid','output':'X:Y'}
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        self.audit.get_validated_fields.return_value = payload
        self.audit.find_one.return_value = {'status': 'Success', 'timestamp': 'ts', '_id': '_id', 'type': 'deployment'}
        self.audit.deployment_status(**payload)
        self.mock_datastore['find_one'].assert_called_once_with('AUDIT', {'type': 'deployment', 'deployment_id': 'ID'})
        self.audit.insert_dated_audit.assert_called_once_with('deployment', payload)
        self.audit.poperator_del.assert_called_once_with('siteid', 'PCM_Manifest', payload)

    def test_login(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        payload = {'email': 'test@philips.com', 'headers': {'agent': 'Chrome', 'version': 'X'}}
        self.audit.get_validated_fields.return_value = payload
        self.audit.login(**payload)
        self.audit.insert_dated_audit.assert_called_once_with('login', payload)
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.USER_FIELDS,
            email='test@philips.com',
            headers={'agent': 'Chrome', 'version': 'X'}
        )

    def test_case(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        payload = {'case_number': 'CCCNNN', 'hostname': 'HOSTNAME', 'service': 'SEVICE', 'siteid': 'SITEID',
                   'uname': 'test@philips.com'}

        self.audit.get_validated_fields.return_value = payload
        self.audit.case_log(**payload)
        self.audit.insert_dated_audit.assert_called_once_with('case', payload)
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.CASE_FIELDS,
            case_number='CCCNNN',
            hostname='HOSTNAME',
            service='SEVICE',
            siteid='SITEID',
            uname='test@philips.com'
        )

    def test_ack(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        payload = {'hostname': 'HOSTNAME', 'service': 'SEVICE', 'siteid': 'SITEID',
                   'uname': 'test@philips.com', 'tag': 'TAG'}

        self.audit.get_validated_fields.return_value = payload
        self.audit.ack_log(**payload)
        self.audit.insert_dated_audit.assert_called_once_with('ack', payload)
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.ACK_FIELDS,
            tag='TAG',
            hostname='HOSTNAME',
            service='SEVICE',
            siteid='SITEID',
            uname='test@philips.com'
        )

    def test_site_maintenance_log(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        payload = {'type': 'maintenace_enable', 'siteid': 'SITEID', 'user': 'test@philips.com',
                   'start_time': '2018-11-2 18:30:0', 'end_time': '2018-11-3 18:30:0', 'nodes': [], 'services': [],
                   'maintanance_remark': 'Os update'}
        validated_payload = {'type': 'maintenace_enable', 'siteid': 'SITEID', 'user': 'test@philips.com',
                   'start_time': '2018-11-2 18:30:0', 'end_time': '2018-11-3 18:30:0', 'maintanance_remark': 'Os update'}
        self.audit.get_validated_fields.return_value = validated_payload
        self.audit.site_maintenance_log(**payload)
        self.audit.insert_dated_audit.assert_called_once_with('maintenace_enable', validated_payload)
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.MAINTENANCE_RULES_FIELDS,
            type='maintenace_enable',
            siteid='SITEID',
            user='test@philips.com',
            start_time='2018-11-2 18:30:0',
            end_time='2018-11-3 18:30:0',
            nodes=[],
            services=[],
            maintanance_remark='Os update'
        )

    def test_assignment_log(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        payload = {'type': 'site_assign', 'siteid': 'SITEID',
                   'user': 'test@philips.com'}
        self.audit.get_validated_fields.return_value = payload
        self.audit.assignment_log(**payload)
        self.audit.insert_dated_audit.assert_called_once_with('site_assign', payload)
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.ASSIGNMENT_FIELDS,
            type='site_assign', 
            siteid='SITEID',
            user='test@philips.com'
        )


if __name__ == '__main__':
    unittest.main()
