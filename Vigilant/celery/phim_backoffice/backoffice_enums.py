# Define your all back office enums here.

class PackageStatus:
    SUBMITTED = 'Submitted'
    STARTED = 'Started'
    INPROGRESS = 'In Progress'
    FAILED = 'Failed'
    AVAILABLE = 'Available'
    NONE = 'None'
