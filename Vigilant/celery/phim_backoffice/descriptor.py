from __future__ import absolute_import

from phim_backoffice.datastore import upsert_one_by_id, delete_one_by_id, update_one_by_id
from phim_backoffice.helpers import get_validated_fields
from celery.utils.log import get_logger
from phim_backoffice.celery import app


logger = get_logger(__name__)
SITE_FIELDS = frozenset(['hbdbsiteid', 'name', 'pacs_version', 'production', 'neb_address', 'country'])


@app.task(ignore_result=True)
def update_site_descriptors(siteid, **kwargs):
    data = get_validated_fields(SITE_FIELDS, **kwargs)
    logger.debug('Updating site descriptors siteid %s data: %s', siteid, data)
    update_descriptors('SITES', siteid, data)


def update_descriptors(collection, identifier, data):
    upsert_one_by_id(collection, identifier, data)
    delete_one_by_id('EXCEPTIONS', identifier)
