import unittest
from mock import MagicMock, patch, call, PropertyMock


class NagiosConfigurationTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
            self.mock_jinja2 = MagicMock(name='jinja2')
            self.mock_json = MagicMock(name='json')
            self.mock_pynag = MagicMock(name='pynag')
            modules = {
                'cached_property': self.mock_cached_property,
                'pynag': self.mock_pynag,
                'jinja2': self.mock_jinja2,
                'json': self.mock_json,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import poperator.nagios_configuration
            self.module = poperator.nagios_configuration

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class NagiosConfigurationTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.nagios_configuration = self.module.NagiosConfiguration()
        self.nagios_configuration.hostgroups = MagicMock(name='hostgroups')

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_false_on_missing_use_attribute(self):
        self.assertFalse(self.nagios_configuration)

    def test_true_on_having_use_attribute(self):
        self.nagios_configuration.add_attribute('use', 'some-server')
        self.assertTrue(self.nagios_configuration)

    def test_add_use_attribute_none(self):
        self.nagios_configuration.add_attribute('use', None)
        self.assertFalse(self.nagios_configuration)

    def test_add_and_get_attribute(self):
        self.nagios_configuration.add_attribute('hostname', 'host1')
        self.assertEqual(self.nagios_configuration.get_attribute('hostname'), 'host1')

    def test_add_to_hostgroups_attribute(self):
        self.nagios_configuration.add_attribute('hostgroups', '+some-servers')
        self.nagios_configuration.hostgroups.add.assert_called_once_with('+some-servers')

    def test_get_model(self):
        self.nagios_configuration.add_attribute('hostname', 'host1')
        self.nagios_configuration.add_attribute('hostgroups', '+some-servers')
        result = self.nagios_configuration.get_model()
        self.assertEqual(
            result.mock_calls,
            [
                call.set_attribute('hostname', 'host1'),
                call.set_attribute('hostgroups', self.nagios_configuration.hostgroups.__str__.return_value)
            ]
        )


class HostgroupsAttributeTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.hostgroups_attribute = self.module.HostgroupsAttribute()

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_false_on_empty(self):
        self.assertFalse(self.hostgroups_attribute)

    def test_true_on_not_empty(self):
        self.hostgroups_attribute.add('group1')
        self.assertTrue(self.hostgroups_attribute)

    def test_str_empty(self):
        self.assertEqual(str(self.hostgroups_attribute), '')

    def test_add_one_noplus_and_add_one_plus(self):
        self.hostgroups_attribute.add('hostgroup1')
        self.hostgroups_attribute.add('+hostgroup2')
        self.assertEqual(str(self.hostgroups_attribute), 'hostgroup1,hostgroup2')

    def test_add_one_plus_and_add_one_noplus(self):
        self.hostgroups_attribute.add('+hostgroup1')
        self.hostgroups_attribute.add('hostgroup2')
        self.assertEqual(str(self.hostgroups_attribute), 'hostgroup2')

    def test_add_two_plus(self):
        self.hostgroups_attribute.add('+hostgroup1')
        self.hostgroups_attribute.add('+hostgroup2')
        self.assertEqual(str(self.hostgroups_attribute), '+hostgroup1,hostgroup2')


class NagiosConfigurationRuleTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        raw_rule = {
            'attribute': 'hostname',
        }
        self.nagios_configuration_rule = self.module.NagiosConfigurationRule(raw_rule)

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_no_attribute(self):
        self.assertRaises(KeyError, self.module.NagiosMapConfigurationRule, {})

    def test_set_condition_none(self):
        self.assertTrue(self.nagios_configuration_rule.condition is None)

    @patch('poperator.nagios_configuration.NagiosConfigurationRule.environment', new_callable=PropertyMock)
    def test_set_condition(self, mock_environment):
        raw_rule = {
            'attribute': 'hostname',
            'when': 'somevar == "val1"'
        }
        mock_environment.return_value.from_string.return_value.render.return_value = ' True '
        self.nagios_configuration_rule = self.module.NagiosConfigurationRule(raw_rule)
        mock_environment.return_value.from_string.assert_called_once_with(
            '{% if somevar == "val1" %} True {% else %} False {% endif %}'
        )
        self.assertTrue(self.nagios_configuration_rule.condition({'var': 'val'}))

    def test_eval(self):
        self.nagios_configuration_rule.get_value = MagicMock(name='get_value')
        self.assertEqual(
            self.nagios_configuration_rule.eval({}),
            ('hostname', self.nagios_configuration_rule.get_value.return_value)
        )

    def test_eval_none_value(self):
        self.nagios_configuration_rule.get_value = MagicMock(name='get_value', return_value=None)
        self.assertEqual(self.nagios_configuration_rule.eval({}), None)

    def test_get_value_unimplemented(self):
        self.assertRaises(NotImplementedError, self.nagios_configuration_rule.get_value, {})

    @patch('poperator.nagios_configuration.NagiosConfigurationRule.environment', new_callable=PropertyMock)
    def test_render_bare(self, mock_environment):
        template = mock_environment.return_value.from_string
        self.assertEqual(
            self.nagios_configuration_rule.render_bare('hostname', {'hostname': 'host1'}),
            template.return_value.render.return_value
        )
        template.assert_called_once_with('{{hostname}}')
        template.return_value.render.assert_called_once_with({'hostname': 'host1'})


class NagiosMapConfigurationRuleTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        raw_rule = {
            'attribute': 'hostname',
            'map': {
                'one': 'val1',
                'two': 'val2',
            },
            'source': 'node1.var1',
            'default': 'defval'
        }
        self.nagios_configuration_rule = self.module.NagiosMapConfigurationRule(raw_rule)

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_no_source(self):
        raw_rule = {
            'attribute': 'hostname',
            'map': {},
        }
        self.assertRaises(KeyError, self.module.NagiosMapConfigurationRule, raw_rule)

    def test_get_value_in_map(self):
        self.nagios_configuration_rule.render_bare = MagicMock(name='render_bare', return_value='one')
        self.assertEqual(self.nagios_configuration_rule.get_value({'data1': 'kdata1'}), 'val1')

    def test_get_value_not_in_map_default_set(self):
        self.nagios_configuration_rule.render_bare = MagicMock(name='render_bare', return_value='not_there')
        self.assertEqual(self.nagios_configuration_rule.get_value({'data1': 'kdata1'}), 'defval')
        self.nagios_configuration_rule.render_bare.assert_called_once_with('node1.var1', {'data1': 'kdata1'})


class NagiosValueConfigurationRuleTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        raw_rule = {
            'attribute': 'hostname',
            'value': 'name_of_host',
        }
        self.nagios_configuration_rule = self.module.NagiosValueConfigurationRule(raw_rule)

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_no_value(self):
        raw_rule = {
            'attribute': 'hostname',
        }
        self.assertRaises(KeyError, self.module.NagiosValueConfigurationRule, raw_rule)

    def test_get_value(self):
        self.nagios_configuration_rule.render_bare = MagicMock(name='render_bare')
        self.assertEqual(self.nagios_configuration_rule.get_value({'data1': 'kdata1'}),
                         self.nagios_configuration_rule.render_bare.return_value)
        self.nagios_configuration_rule.render_bare.assert_called_once_with('name_of_host', {'data1': 'kdata1'})


class NagiosConfigurationRuleFactoryTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.nagios_configuration_rule_factory = self.module.NagiosConfigurationRuleFactory()

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_map_rule(self):
        raw_rule = {
            'attribute': 'hostname',
            'map': {},
            'source': 'key2'
        }
        result = self.nagios_configuration_rule_factory.get_nagios_rule(raw_rule)
        self.assertEqual(type(result), self.module.NagiosMapConfigurationRule)

    def test_value_rule(self):
        raw_rule = {
            'attribute': 'hostname',
            'value': 'val2'
        }
        result = self.nagios_configuration_rule_factory.get_nagios_rule(raw_rule)
        self.assertEqual(type(result), self.module.NagiosValueConfigurationRule)


class NagiosConfigurationRuleSetTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.nagios_configuration_rule_set = self.module.NagiosConfigurationRuleSet()

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_load_dict(self):
        self.module.NagiosConfigurationRuleFactory = MagicMock(name='NagiosConfigurationRuleFactory')
        raw_ruleset = [
            {'hostname': [ 'SENTINEL1', 'SENTINEL2' ]},
            {'LN': ['ASENTINEL']}
        ]
        mock_get_nagios_rule = self.module.NagiosConfigurationRuleFactory.return_value.get_nagios_rule
        mock_get_nagios_rule.side_effect = lambda x: 'rule_{0}'.format(x)
        self.nagios_configuration_rule_set.load_dict(raw_ruleset)
        self.assertEqual(
            self.nagios_configuration_rule_set.rule_set,
            [
                {
                    'rules': ['rule_SENTINEL1', 'rule_SENTINEL2'],
                    'required_key': 'hostname'
                },
                {
                    'rules': ['rule_ASENTINEL'],
                    'required_key': 'LN'
                }
            ]
        )

    def test_get_results(self):
        data = {'hostname': 'host1', 'mod1': {'key1': 'val1'}}
        self.nagios_configuration_rule_set.rule_set = [
            {'rules': ['rule_1_1', 'rule_1_2'], 'required_key': 'hostname'},
            {'rules': ['rule_2_1', 'rule_2_2'], 'required_key': 'LN'},
            {'rules': ['rule_3_1'], 'required_key': 'hostname'}
        ]
        def outcomer(x, rules):
            for rule in rules:
                yield 'outcome form rule {rule}'.format(rule=rule)

        self.nagios_configuration_rule_set.get_outcomes = MagicMock(name='get_outcomes', side_effect=outcomer)
        self.assertEqual(
            list(self.nagios_configuration_rule_set.get_results(data)),
            ['outcome form rule rule_1_1', 'outcome form rule rule_1_2', 'outcome form rule rule_3_1']
        )
        self.assertEqual(
            self.nagios_configuration_rule_set.get_outcomes.call_args_list,
            [
                call({'mod1': {'key1': 'val1'}, 'hostname': 'host1'}, ['rule_1_1', 'rule_1_2']),
                call({'mod1': {'key1': 'val1'}, 'hostname': 'host1'}, ['rule_3_1'])
            ]
        )

    def test_get_outcomes(self):
        data = {'hostname': 'host1', 'mod1': {'key1': 'val1'}}
        mocked_rules = [MagicMock(name='rule1'), MagicMock(name='rule2')]
        result = self.nagios_configuration_rule_set.get_outcomes(data, mocked_rules)
        self.assertEqual(list(result), [mocked_rules[0].eval.return_value, mocked_rules[1].eval.return_value])

    def test_eval_no_rule_set(self):
        self.assertRaises(self.module.NagiosConfigurationRuleException, self.nagios_configuration_rule_set.eval, {})

    def test_eval(self):
        self.module.NagiosConfiguration = MagicMock(name='NagiosConfiguration')
        self.nagios_configuration_rule_set.rule_set = True
        self.nagios_configuration_rule_set.get_results = MagicMock(
            name='get_results',
            return_value=iter([('atr1', 'val1'), ('atr2', 'val2')])
        )
        result = self.nagios_configuration_rule_set.eval({'hostname': 'host1'})
        self.nagios_configuration_rule_set.get_results.assert_called_once_with({'hostname': 'host1'})
        self.assertEqual(result.mock_calls, [call.add_attribute('atr1', 'val1'), call.add_attribute('atr2', 'val2')])


if __name__ == '__main__':
    unittest.main()
