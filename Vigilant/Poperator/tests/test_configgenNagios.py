import unittest
from mock import MagicMock, patch, call


class ConfiggenNagiosTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        self.mock_poperator = MagicMock(name='poperator')
        modules = {
            'logging': self.mock_logging,
            'poperator.nagios_configuration': self.mock_poperator.nagios_configuration,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from poperator import configgenNagios
        self.configgenNagios = configgenNagios

        var1 = [
            {
                'hostname': 'host1',
                'address': '10.0.0.1',
            },
            {
                'hostname': 'host02',
                'address': '10.0.0.2',
            }
        ]
        credentials = {'host1': {'u': 'u1', 'p': 'p1'}, 'host02': {'u': 'u2', 'p': 'p2'}}
        self.cn = self.configgenNagios.ConfiggenNagios(
            var1, credentials=credentials)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_filename(self):
        self.assertEqual(self.cn.get_filename('host1'), 'hosts/host1.cfg')

    def test_generate(self):
        mock_rule_set = self.configgenNagios.NagiosConfigurationRuleSet
        self.assertEqual(
            self.cn.generate(),
            {
                'hosts/host1.cfg': mock_rule_set.return_value.eval.return_value.__str__.return_value,
                'hosts/host02.cfg': mock_rule_set.return_value.eval.return_value.__str__.return_value
            }
        )
        expected_call = [call({'credentials': {'p': 'p1', 'u': 'u1'}, 'hostname': 'host1', 'address': '10.0.0.1'}),
                         call({'credentials': {'p': 'p2', 'u': 'u2'}, 'hostname': 'host02', 'address': '10.0.0.2'})]
        self.assertEqual(mock_rule_set().eval.call_args_list, expected_call)

    def test_generate_exception(self):
        self.mock_poperator.nagios_configuration.NagiosConfigurationRuleSet(
        ).eval.side_effect = Exception()
        self.assertEqual(self.cn.generate(), {})


if __name__ == '__main__':
    unittest.main()
