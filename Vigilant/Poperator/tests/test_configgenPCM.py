import json
import unittest
from mock import MagicMock, patch, Mock


class ConfiggenTestPCM(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        self.mock_request = MagicMock(name='requests')
        self.mock_tbconfig = MagicMock(name='tbconfig')
        modules = {
            'logging': self.mock_logging,
            'requests': self.mock_request,
            'tbconfig': self.mock_tbconfig
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from poperator import configgenPCM
        self.configgenPCM = configgenPCM
        self.configgenPCM.SITE_INFO_MODULE_TYPES = {'Database': 'DBNode', 'Hl7': 'HL7Node'}

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_generate(self):
        config = self.configgenPCM.ConfiggenPCM(self.manifest_one)
        config.get_facts = Mock(return_value=[])
        generation = config.generate()
        expected = {'siteinfo/SiteInfo_id.xml': self.site_xml_one,
                    'siteinfo/pcm_manifest_id.json': json.dumps(self.manifest_one, sort_keys=True, indent=4)}
        self.maxDiff = None
        self.assertEqual(generation, expected)

    def test_update_data(self):
        config = self.configgenPCM.ConfiggenPCM(self.manifest_two)
        config.update_data()
        config.data.pop('deployment_id')
        self.assertEqual(config.data, self.manifest_one)
    
    def test_get_facts(self):
        config = self.configgenPCM.ConfiggenPCM(self.manifest_two)
        config.data = {'siteid' : 'site'}
        mock_get = MagicMock(name='get_request')
        json_val = {'result': [{'module_type': 'Database'}]}
        mock_get.json.return_value = json_val
        self.mock_request.get.return_value = mock_get
        expected = [{'module_type': 'Database'}]
        self.assertEqual(config.get_facts(), expected)

    def test_add_noncore_node_details(self):
        config = self.configgenPCM.ConfiggenPCM(self.manifest_two)
        json_val = [{'module_type': 'Database', 'hostname': 'mock_host'}]
        module_type = {'Database': 'DBNode', 'Hl7': 'HL7Node'}
        config.get_facts = Mock(return_value=json_val)
        config.data = {'hosts': {'idm02mi1.idm02.isyntax.net': {'RoleType': '8', 'host_vault': {}}},'a':'b'}
        config.add_noncore_node_details()
        expected_output = {'a': 'b', 'HL7Node': 'NA', 'hosts': {'idm02mi1.idm02.isyntax.net': {'RoleType': '8', 'host_vault': {}}}, 'DBNode': 'NA'}
        self.assertEqual(config.data, expected_output)

    manifest_one = {
            "DBSharedPath": "NA",
            "site_vault": {"ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="},
            "SharedDataBaseHost": "NA",
            "site_data": {
                "LocalizationCode": "ENG",
                "DeploymentScripts": "C:\\provision\\isite\\deploymentscripts",
                "ISiteServiceAccountUser": "DEVO3\\iSiteService",
                "Federated": "False",
                "SolutionRootFolder": "S:\\Philips\\Apps\\iSite"},
            "DBPassword": "NA",
            "DBNode": "NA",
            "DBUsername": "NA",
            "HL7Node": "NA",
            "deployment_id": "id",
            "hosts": {
             "DEV03AM1.DEV03.iSyntax.net": {
                "RoleType": "ActiveMirror",
                "host_vault": {
                    "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="},
                "Sequence": "5",
                "ip": "167.81.184.117",
                "packages": ["AWV", "PCM-2.3.1.1.234.0"],
                "host_data": {
                    "RoleType": "ActiveMirror"
                }
             },
             "DEV03IF1.DEV03.iSyntax.net": {
                "RoleType": "Infrastructure",
                "host_vault": {
                },
                "Sequence": "1",
                "ip": "167.81.184.117",
                "packages": ["AWV-1.3", "PCM-2.3.1.1.234.0"],
                "host_data": {
                    "RoleType": "Infrastructure"
                }
             }
             },
            "siteid": "DEV03"
        }

    manifest_two =  {
            "HL7Node": "NA",
            "DBNode": "NA",
            "DBUsername": "NA",
            "DBPassword": "NA",
            "DBSharedPath": "NA",
            "SharedDataBaseHost": "NA",
            "siteid": "DEV03",
            "site_vault": {
                "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="
            },
            "site_data": {
                "Federated": "False",
                "LocalizationCode": "ENG",
                "ISiteServiceAccountUser": "DEVO3\\iSiteService",
                "SolutionRootFolder": "S:\\Philips\\Apps\\iSite",
                "DeploymentScripts": "C:\\provision\\isite\\deploymentscripts"
            },
            "deployment_id": "id",
            "hosts": {
                "DEV03IF1.DEV03.iSyntax.net": {
                    "RoleType": "Infrastructure",
                    "Sequence": "1",
                    "ip": "167.81.184.117",
                    "host_vault": {},
                    "host_data": {
                        "RoleType": "Infrastructure"
                    },
                    "packages": [
                        "AWV1234-1.3",
                        "PCM1234-2.3.1.1.234.0"
                    ]
                },
                "DEV03AM1.DEV03.iSyntax.net": {
                    "RoleType": "ActiveMirror",
                    "Sequence": "5",
                    "ip": "167.81.184.117",
                    "host_vault": { "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="},
                    "host_data": {
                        "RoleType": "ActiveMirror"
                    },
                    "packages": [
                        "AWV",
                        "PCM-2.3.1.1.234.0"
                    ]
                }
            }
        }

    site_xml_one = """<?xml version='1.0' encoding='UTF-8'?>
<SiteMap>
  <SiteBag>
    <Keys>
      <Key Encrypted="true" KeyName="ISiteServiceAccountPassword">fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg==</Key>
      <Key Encrypted="false" KeyName="LocalizationCode">ENG</Key>
      <Key Encrypted="false" KeyName="DeploymentScripts">C:\provision\isite\deploymentscripts</Key>
      <Key Encrypted="false" KeyName="ISiteServiceAccountUser">DEVO3\iSiteService</Key>
      <Key Encrypted="false" KeyName="Federated">False</Key>
      <Key Encrypted="false" KeyName="SolutionRootFolder">S:\Philips\Apps\iSite</Key>
      <Key Encrypted="false" KeyName="SiteID">DEV03</Key>
      <Key Encrypted="false" KeyName="HL7Node">NA</Key>
      <Key Encrypted="false" KeyName="DBNode">NA</Key>
      <Key Encrypted="false" KeyName="DBUsername">NA</Key>
      <Key Encrypted="true" KeyName="DBPassword">NA</Key>
      <Key Encrypted="false" KeyName="DBSharedPath">NA</Key>
      <Key Encrypted="false" KeyName="SharedDataBaseHost">NA</Key>
    </Keys>
  </SiteBag>
  <Roles>
    <Role Sequence="5">
      <IPv4>167.81.184.117</IPv4>
      <FQDN>DEV03AM1.DEV03.iSyntax.net</FQDN>
      <Packages>
        <PackageInfo Version="" Name="AWV"/>
        <PackageInfo Version="2.3.1.1.234.0" Name="PCM"/>
      </Packages>
      <RoleBag>
        <Key Encrypted="true" KeyName="ISiteServiceAccountPassword">fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg==</Key>
        <Keys>
          <Key Encrypted="false" KeyName="RoleType">ActiveMirror</Key>
        </Keys>
      </RoleBag>
    </Role>
    <Role Sequence="1">
      <IPv4>167.81.184.117</IPv4>
      <FQDN>DEV03IF1.DEV03.iSyntax.net</FQDN>
      <Packages>
        <PackageInfo Version="1.3" Name="AWV"/>
        <PackageInfo Version="2.3.1.1.234.0" Name="PCM"/>
      </Packages>
      <RoleBag>
        <Keys>
          <Key Encrypted="false" KeyName="RoleType">Infrastructure</Key>
        </Keys>
      </RoleBag>
    </Role>
  </Roles>
</SiteMap>\n"""


if __name__ == '__main__':
    unittest.main()
