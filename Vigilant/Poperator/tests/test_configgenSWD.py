import unittest
from mock import MagicMock, patch


class ConfiggenTestSWDCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        modules = {
            'logging': self.mock_logging,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from poperator import configgenSWD
        self.configgenSWD = configgenSWD

        var1 = ['sub1', 'sub2']
        self.cswd = self.configgenSWD.ConfiggenSWD(var1)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_filename(self):
        self.assertEqual(self.cswd.get_filename('sub1'), 'subscription_links/sub1')

    def test_generate(self):
        self.assertEqual(self.cswd.generate(), {'subscription_links/sub1': '', 'subscription_links/sub2': ''})


if __name__ == '__main__':
    unittest.main()
