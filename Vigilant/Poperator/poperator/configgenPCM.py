import logging
import json
import re
import requests
from lxml.etree import tostring
from lxml.builder import E
from tbconfig import SITE_INFO_MODULE_TYPES


spit_bucket = logging.getLogger(__name__)


class ConfiggenPCM(object):
    def __init__(self, data, **kwargs):
        self.data = data
        #Set encrypted as True
        self.encrypt_keys = ['DomainAdminPassword']

    def generate(self):
        deployment_id = self.data.pop('deployment_id')
        self.update_data()
        self.add_noncore_node_details()
        return { "siteinfo/pcm_manifest_{deployment_id}.json".format(deployment_id=deployment_id) : json.dumps(self.data, sort_keys=True, indent=4),
                 "siteinfo/SiteInfo_{deployment_id}.xml".format(deployment_id=deployment_id): self.create_xml()}

    def update_data(self):
        for host in self.data.get('hosts'):
            package_list = self.data['hosts'][host].get('packages')
            packages = []
            for package in package_list:
                package_with_version = package.split('-')
                package_name = re.split("[^a-zA-Z]*", package_with_version[0])[0]
                try:
                    package_name = package_name + '-' + package_with_version[1]
                except IndexError:
                    pass
                finally:
                    packages.append(package_name)
            self.data['hosts'][host]['packages'] = packages

    def get_facts(self):
        siteid = self.data['siteid']
        proxies = {'http': None, 'https': None,}
        url = 'http://portal.phim.isyntax.net/pma/facts/{0}/modules/ISP'.format(siteid)
        try:
            response = requests.get(url, timeout=10, verify=False,
                                    headers={'X-Requested-With': True},
                                    proxies=proxies).json()
            return response['result']
        except Exception:
            return []

    def check_for_tag(self, facts, hostname):
        try:
            for hosts in facts:
                host = hosts.get('hostname')
                if host == hostname:
                    tags = hosts.get('ISP', {}).get('tags')
                    if 'production' in tags:
                        return 'production'
                    elif 'test' in tags:
                        return 'test'
                    else:
                        return None
            return None
        except:
            return None

    def add_noncore_node_details(self):
        facts = self.get_facts()
        hostname = self.data['hosts'].keys()[0]
        tag = self.check_for_tag(facts, hostname)
        if tag:
            self.data['DBUsername'] = 'NA'
            self.data['DBPassword'] = 'NA'
            self.data['DBSharedPath'] = 'NA'
            self.data['SharedDataBaseHost'] = 'NA'
            for host in facts:
                tags = host.get('ISP',{}).get('tags')
                if not tags or tag not in tags:
                    continue
                module_type = host.get('ISP',{}).get('module_type')
                if module_type in SITE_INFO_MODULE_TYPES.keys():
                    self.data[SITE_INFO_MODULE_TYPES[module_type]] = host['hostname']
        for req_module_type in SITE_INFO_MODULE_TYPES.values():
            if req_module_type not in self.data.keys():
                self.data[req_module_type] = 'NA'

    def add_site_keys(self, key_name, encrypted, text):
        xml_key = E.Key(KeyName=key_name, Encrypted=encrypted)
        xml_key.text = self.data[text]
        return xml_key

    def create_xml(self):

        site_keys = E.Keys()
        if "site_vault" in self.data :
            for site_vault_key, site_vault_value in self.data["site_vault"].items() :
                xml_key = E.Key(KeyName=site_vault_key, Encrypted="true")
                xml_key.text = site_vault_value
                site_keys.append(xml_key)

        if "site_data" in self.data:
            for site_data_key, site_data_value in self.data["site_data"].items() :
                encrypted = "false"
                if site_data_key in self.encrypt_keys:
                    encrypted = "true"
                xml_key = E.Key(KeyName=site_data_key, Encrypted=encrypted)
                xml_key.text = site_data_value
                site_keys.append(xml_key)

        if 'siteid' in self.data:
            site_keys.append(self.add_site_keys('SiteID', 'false', 'siteid'))

        if 'HL7Node' in self.data:
            site_keys.append(self.add_site_keys('HL7Node', 'false', 'HL7Node'))

        if 'DBNode' in self.data:
            site_keys.append(self.add_site_keys('DBNode', 'false', 'DBNode'))
            site_keys.append(self.add_site_keys('DBUsername', 'false', 'DBUsername'))
            site_keys.append(self.add_site_keys('DBPassword', 'true', 'DBPassword'))
            site_keys.append(self.add_site_keys('DBSharedPath', 'false', 'DBSharedPath'))
            site_keys.append(self.add_site_keys('SharedDataBaseHost', 'false', 'SharedDataBaseHost'))

        roles = E.Roles()
        for host_key, host_value in self.data["hosts"].items() :

            ipv4 = E.IPv4()
            ipv4.text = host_value["ip"]
            fqdn = E.FQDN()
            fqdn.text = host_key

            packages = E.Packages()
            for package in host_value["packages"] :
                pcm_package_components = package.rsplit('-', 1)
                if len(pcm_package_components) > 1 :
                    packages.append(E.PackageInfo(Name=pcm_package_components[0], Version=pcm_package_components[1]))
                else :
                    packages.append(E.PackageInfo(Name=pcm_package_components[0], Version=""))

            host_keys = E.RoleBag()
            if "host_vault" in host_value :
                for host_vault_key, host_vault_value in host_value["host_vault"].items() :
                    xml_key = E.Key(KeyName=host_vault_key, Encrypted="true")
                    xml_key.text = host_vault_value

                    host_keys.append(xml_key)

            if "host_data" in host_value :
                for data_key, data_value in host_value["host_data"].items() :
                    keys = E.Keys()
                    xml_key = E.Key(KeyName=data_key, Encrypted="false")
                    xml_key.text = data_value
                    keys.append(xml_key)
                    host_keys.append(keys)
            roles.append(E.Role(ipv4, fqdn, packages, host_keys,Sequence=host_value['Sequence']))

        return tostring(E.SiteMap((E.SiteBag(site_keys)), roles), pretty_print=True, xml_declaration=True, encoding='UTF-8')
