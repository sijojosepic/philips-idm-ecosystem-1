import os
import logging
from pkg_resources import resource_filename
from nagios_configuration import NagiosConfigurationRuleSet


spit_bucket = logging.getLogger(__name__)


class ConfiggenNagios(object):
    def __init__(self, data, **kwargs):
        self.data = data
        self.hosts_dir = 'hosts'
        self.configuration_rules = resource_filename(__name__, 'nagios_configuration_rules.yml')
        self.rule_set = NagiosConfigurationRuleSet()
        self.rule_set.load(self.configuration_rules)
        self.credentials = kwargs.get('credentials') if kwargs.get('credentials') else {}

    def get_filename(self, hostname):
        return os.path.join(self.hosts_dir, hostname + '.cfg')

    def generate(self):
        configs = {}
        for host in self.data:
            try:
                host.update({'credentials': self.credentials.get(host['hostname'])})
                host_config = self.rule_set.eval(host)
                if host_config:
                    configs[self.get_filename(host['hostname'])] = str(host_config)
            except Exception:
                spit_bucket.exception('Discovery : - Error while processing host %s', host)
        return configs

# var1 = [
#     {
#         'hostname': 'host1',
#         'address': '10.0.0.1',
#         'manufacturer': 'VMware, Inc.',
#         'ISP': {
#             'module_type': '2',
#             'anywhere_version': '1.2',
#             'version': '4,4,3,1',
#             'identifier': '4x',
#             'flags': ['domain_needed', 'anywhere']
#         }
#     },
#     {
#         'hostname': 'host02',
#         'address': '10.0.0.254',
#         'LN': {
#             'alias': 'HOST02',
#             'use': 'esx_server'
#         }
#     }
# ]
# hh = ConfiggenNagios(var1)
#
# config = hh.generate()
