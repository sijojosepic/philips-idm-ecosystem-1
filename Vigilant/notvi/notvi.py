#!/usr/bin/env python
from bottle import run, route, template, redirect

from pymongo import MongoClient
MONGOINST = 'mongodb://mongodb01.sfohi.philips.com:27017/'

client = MongoClient(MONGOINST)
db = client.somedb
collection = db.events


@route('/notvi')
def notvisi():
    sites = collection.distinct("siteid")
    nx = list(sites)
    print nx
    return nx



@route('/notvi/<siteid>')
def notviho(siteid):
    notifications = collection.find({"siteid": siteid}).distinct("hostname")
    nx = list(notifications)
    print nx
    return nx

    
@route('/notvi/<siteid>/<hostname>')
def notvi(siteid, hostname):
    notifications = collection.find({"siteid": siteid, "hostname": hostname}).sort('timestamp', 1)
    nx = list(notifications)
    print nx
    return template('notvi', siteid=siteid, hostname=hostname, notifications=nx)
    
    
@route('/notdel/<siteid>/<hostname>')
def notdel(siteid, hostname):
    notifications = collection.remove({"siteid": siteid, "hostname": hostname})
    redirect('/notvi/%s/%s' % (siteid, hostname))
        
    
if __name__ == '__main__':  
    run(host='0.0.0.0', port=8081, debug=True)
