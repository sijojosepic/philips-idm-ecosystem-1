from celery import Task
from celery.utils import cached_property
from pymongo import MongoClient, errors


class MongoTask(Task):
    abstract = True
    errors = errors
    _uri = None
    _name = None

    @cached_property
    def mc(self):
        return MongoClient(self._uri)
        
    @cached_property
    def db(self):
        return self.mc[self._name]

    @mc.deleter
    def mc(self, value):
        if value is not None:
            value.close()
        
    def __del__(self):
        del(self.mc)


def create_mongo_task(uri, name):
    class MDT(MongoTask):
        abstract = True
        _uri = uri
        _name = name

    return MDT
