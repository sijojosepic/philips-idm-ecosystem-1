
class DictConditional(dict):
    def is_allowed(self, value):
        raise NotImplementedError

    def __init__(self, *args, **kwargs):
        self.update(*args, **kwargs)

    def __setitem__(self, key, value):
        if not self.is_allowed(value):
            if key in self:
                del self[key]
        else:
            dict.__setitem__(self, key, value)

    def update(self, *args, **kwargs):
        if args:
            if len(args) > 1:
                raise TypeError('update expected at most 1 arguments, got %d' % len(args))
            other = dict(args[0])
            for key in other:
                self[key] = other[key]
        for key in kwargs:
            self[key] = kwargs[key]

    def setdefault(self, key, value=None):
        if key not in self:
            self[key] = value
        return self[key]


class DictNoEmpty(DictConditional):
    def is_allowed(self, value):
        return value is not None and value != ''


class DictNoFalsey(DictConditional):
    def is_allowed(self, value):
        return value is not None
