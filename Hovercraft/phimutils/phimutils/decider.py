import json
from itertools import product
from namespace import NS_DIVIDER

 
class TermTreeNode(dict):
    def __init__(self):
        self.subtrahends = set()
        self.addends = set()
        super(TermTreeNode, self).__init__()
 
    def __getitem__(self, key):
        if key in self:
            return self.get(key)
        return self.setdefault(key, TermTreeNode())
 
    def update_data(self, new_data):
        for entry in new_data:
            if entry.startswith('!'):
                self.subtrahends.add(entry[1:])
            else:
                self.addends.add(entry)
 
    def add_data(self, data, sub=''):
        key = self
        chain = filter(None, sub.split(NS_DIVIDER))
        for nk in chain:
            key = key[nk]
        key.update_data(data)
 

class Decision(object):
    def __init__(self):
        self.result = set()
 
    def digest(self, term_node):
        self.result |= term_node.addends
        self.result -= term_node.subtrahends
 
    def as_set(self):
        return self.result
 

class Decider(object):
    def __init__(self):
        # Namespaces not bound to a site
        self.unbound_namespaces = TermTreeNode()
        self.root = TermTreeNode()
 
    def parse_rule(self, sites, namespaces, data):
        if sites:
            if not namespaces:
                namespaces = ['']
            for site, namespace in product(sites, namespaces):
                self.root[site].add_data(data, namespace)
        else:
            if namespaces:
                # Add to namespaces / all sites
                for namespace in namespaces:
                    self.unbound_namespaces.add_data(data, namespace)
            else:
                # Add to root / all sites
                self.root.add_data(data)
 
    def load(self, rules):
        for rule in rules:
            self.parse_rule(rule['sites'], rule['namespaces'], rule['result'])

    def load_from_file(self, filename):
        with open(filename) as rules_file:
            rules = json.load(rules_file)['rules']
        self.load(rules)

    def decide(self, siteid, namespace):
        ns_chain = namespace.split(NS_DIVIDER)
        result = Decision()
        result.digest(self.root)
        site_node = self.root[siteid]
        result.digest(site_node)
        unbound_node = self.unbound_namespaces
        for category in ns_chain:
            unbound_node = unbound_node[category]
            result.digest(unbound_node)
            site_node = site_node[category]
            result.digest(site_node)
        return result.as_set()
