import subprocess
import shlex


def execute_command(command, stdin=None, stdout=None, stderr=None, shell=False):
    """
    calls subprocess.check_call to execute commands with error handling.  function requires
    command and its arguments.  The command passes itself to subprocess as a list of strings separated
    by white space. Shlex is used to split the arguments to the command in such a manner that arguments with white
    space are handled appropriately.  This allows for more complex commands to be handled by this functions. Also, by
    default shell is set to False so that python will call the process directly.  If you are attempting to run
    multiple commands together you may want to set shell=True.  This will interact directly with the shell; however,
    it is considered a big security risk.

    Use stdin, stdout, stderr to specify the executed programs standard input, output, and error file handles.  Valid
    values are PIPE, an existing file descriptor (a positive integer), an existing file object, and None.

    For more information on using subprocess see section 17.1.1.1 of the subprocess management python documentation.

    MODULES REQUIRED:
    subprocess, shlex

    USAGE:
    Single simple command:
    execute_command("ls -la")

    Multiple commands:
    execute_command("cd /tmp;touch foo", shell=True)
    """
    try:
        args = shlex.split(command)
        subprocess.check_call(args, stdin=stdin, stdout=stdout, stderr=stderr, shell=shell)
    except subprocess.CalledProcessError:
        raise
    except OSError:
        raise
    except Exception:
        raise
