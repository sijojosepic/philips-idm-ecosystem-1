import unittest
from lxml import objectify, etree
from mock import MagicMock, patch


class PhimutilsPCMTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_enums = MagicMock(name='enums')

        modules = {
            'enums': self.mock_enums,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import pcm
        self.module = pcm
        self.PCMPackage = pcm.PCMPackage

        self.module.objectify = MagicMock(name='objectify')
        self.mock_get_root = self.module.objectify.parse.return_value
        self.mock_enums.FileExtensions.ZIP_SUFFIX = '.zip'
        self.mock_enums.FileExtensions.PART_SUFFIX = '.part'
        self.mock_enums.PackageStatus.INPROGRESS = 'In Progress'
        self.mock_enums.PackageStatus.FAILED = 'Failed'
        self.mock_enums.PackageStatus.AVAILABLE = 'Available'
        self.mock_enums.PackageStatus.NONE = 'None'


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_pcm_name(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_sample_one)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.Name, 'AWV')

    def test_pcm_name_bad_xml(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_bad_xml)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertRaises(AttributeError, getattr, pcm_one, 'Name')

    def test_pcm_verison(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_sample_one)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.Version, '1.3.12.0.1862.24')

    def test_pcm_verison_bad_xml(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_bad_xml)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertRaises(AttributeError, getattr, pcm_one, 'Version')

    def test_pcm_package_name(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_sample_one)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.Package_Name, 'AWV-1.3.12.0.1862.24')

    def test_pcm_description(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_sample_one)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.Description, 'IntelliSpace PACS Anywhere')

    def test_pcm_description_bad_xml(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_bad_xml)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertRaises(AttributeError, getattr, pcm_one, 'Description')


    def test_pcm_get_dependencies(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_sample_one)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.get_dependencies(), {'DepChildOne' : '1.2','DepChildTwo' : '1.5.4.3'})

    def test_pcm_get_dependencies_none(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_sample_two)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.get_dependencies(), {})

    def test_pcm_get_dependencies_bad_xml_package_name(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_bad_package_name)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.get_dependencies(), {'DepChildOne': '1.2'})

    def test_pcm_get_dependencies_bad_xml_package_version(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_bad_package_version)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.get_dependencies(), {'DepChildTwo': '1.5.4.3'})

    def test_pcm_get_dependencies_bad_xml(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_bad_xml)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertRaises(AttributeError, pcm_one.get_dependencies)

    def test_pcm_get_required_databags(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_sample_one)
        pcm_one = self.PCMPackage('SomeFile')
        expected_databags = ['ISiteServiceAccountPassword', 'ISiteServiceAccountUser', 'SolutionRootFolder']
        self.assertEqual(pcm_one.get_required_databags(), expected_databags)

    def test_pcm_get_required_databags_none(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_sample_two)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.get_required_databags(), [])

    def test_pcm_get_required_databags_bad_xml(self):
        self.mock_get_root.getroot.return_value = objectify.fromstring(self.xml_bad_xml)
        pcm_one = self.PCMPackage('SomeFile')
        self.assertEqual(pcm_one.get_required_databags(), [])

    def test_pcm_zip_file(self):
        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.zip_file, 'somefile.zip')

    def test_pcm_part_file(self):
        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.part_file, 'somefile.zip.part')

    @patch('os.path.isfile')
    def test_pcm_file_exists_case_one(self, mock_isfile):
        mock_isfile.return_value = True

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.file_exists("somefile.xml"), True)
        mock_isfile.assert_called_once_with("somefile.xml")

    @patch('os.path.isfile')
    def test_pcm_file_exists_case_two(self, mock_isfile):
        mock_isfile.return_value = False

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.file_exists("somefile.xml"), False)
        mock_isfile.assert_called_once_with("somefile.xml")

    def test_pcm_get_download_status_and_filename_case_one(self):
        self.PCMPackage.file_exists = MagicMock(return_value=True)

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.get_download_status_and_filename(), (True, 'somefile.zip.part'))
        self.PCMPackage.file_exists.assert_called_once_with("somefile.zip.part")

    def test_pcm_get_download_status_and_filename_case_two(self):
        self.PCMPackage.file_exists = MagicMock(side_effect=[False, True])

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.get_download_status_and_filename(), (False, 'somefile.zip'))
        self.PCMPackage.file_exists.assert_called_with("somefile.zip")

    def test_pcm_get_download_status_and_filename_case_three(self):
        self.PCMPackage.file_exists = MagicMock(side_effect=[False, False])

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.get_download_status_and_filename(), (False, None))
        self.PCMPackage.file_exists.assert_called_with("somefile.zip")

    @patch('os.path.getsize')
    def test_pcm_get_file_size(self, mock_getsize):
        mock_getsize.return_value = 100
        self.PCMPackage.convert_size = MagicMock(return_value="10 KB")

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.get_file_size("somefile.zip"), "10 KB")
        mock_getsize.assert_called_once_with("somefile.zip")
        self.PCMPackage.convert_size.assert_called_once_with(100)

    def test_pcm_get_download_status_and_size_case_one(self):
        self.PCMPackage.get_download_status_and_filename = MagicMock(return_value= (True, 'somefile.zip.part'))
        self.PCMPackage.get_file_size = MagicMock(return_value="10 KB")

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.get_download_status_and_size(), ('In Progress', '10 KB'))
        self.PCMPackage.get_download_status_and_filename.assert_called_once_with()
        self.PCMPackage.get_file_size.assert_called_once_with('somefile.zip.part')

    def test_pcm_get_download_status_and_size_case_two(self):
        self.PCMPackage.get_download_status_and_filename = MagicMock(return_value= (False, 'somefile.zip'))
        self.PCMPackage.get_file_size = MagicMock(return_value="10 KB")

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.get_download_status_and_size(), ('Available', '10 KB'))
        self.PCMPackage.get_download_status_and_filename.assert_called_once_with()
        self.PCMPackage.get_file_size.assert_called_once_with('somefile.zip')

    def test_pcm_get_download_status_and_size_case_three(self):
        self.PCMPackage.get_download_status_and_filename = MagicMock(return_value= (False, None))
        self.PCMPackage.get_file_size = MagicMock(return_value="10 KB")

        pcm_one = self.PCMPackage('somefile.xml')
        self.assertEqual(pcm_one.get_download_status_and_size(), ('None', '0B'))
        self.PCMPackage.get_file_size.assert_not_called()

    def test_pcm_convert_size_case_one(self):
        pcm_one = self.PCMPackage('somefile.xml')
        size_bytes = 1500
        self.assertEqual(pcm_one.convert_size(size_bytes), '1.46 KB')

    def test_pcm_convert_size_case_two(self):
        pcm_one = self.PCMPackage('somefile.xml')
        size_bytes = 0
        self.assertEqual(pcm_one.convert_size(size_bytes), '0B')

    @patch("math.log")
    def test_pcm_convert_size_case_three(self, mock_mathlog):
        mock_mathlog.side_effect = ValueError

        pcm_one = self.PCMPackage('somefile.xml')
        size_bytes = 1500
        self.assertEqual(pcm_one.convert_size(size_bytes), '0B')


    xml_sample_one = """<?xml version="1.0"?>
<Package xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" PackageSchemaVersion="2.0.0.0" xmlns="http://tempuri.org/Package.xsd">
  <Name>AWV</Name>
  <Version>1.3.12.0.1862.24</Version>
  <TargetOperatingSystem>Windows</TargetOperatingSystem>
  <Description>IntelliSpace PACS Anywhere</Description>
  <OwnerEmail>noone@philips.com</OwnerEmail>
  <Checksum>04e0f67386b749a2ab67678d100c7ef4</Checksum>
  <RemoveRequiresPayload>False</RemoveRequiresPayload>
  <UpgradePerformsRemove xsi:nil="true" />
  <GroupPackage>False</GroupPackage>
  <PreConditions>
    <Condition>
      <Test>FileExists</Test>
      <TestObject>c:\provision\isite\deploymentscripts\deploy.ps1</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>
    <Condition>
      <Test>FolderExists</Test>
      <TestObject>[databag@SolutionRootFolder]</TestObject>
      <TestData />
      <ExpectedResult>True</ExpectedResult>
    </Condition>
  </PreConditions>
  <Dependencies>
    <OtherPackage>
        <Name>DepChildOne</Name>
        <Version>1.2</Version>
    </OtherPackage>
    <OtherPackage>
        <Name>DepChildTwo</Name>
        <Version>1.5.4.3</Version>
    </OtherPackage>
  </Dependencies>
  <InstallActions>
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PackageScripts\InstallAWV.ps1</ActionFile>
      <ActionArguments />
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>c:\provision\isite\deploymentscripts\deploy.ps1</ActionFile>
      <ActionArguments>-AnywhereOnly -SolutionRootFolder [databag@SolutionRootFolder] -ISiteServiceAccountUser [databag@ISiteServiceAccountUser] -ISiteServiceAccountPassword [databag@ISiteServiceAccountPassword]</ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
  </InstallActions>
  <UpgradeActions>
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PackageScripts\InstallAWV.ps1</ActionFile>
      <ActionArguments />
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>c:\provision\isite\deploymentscripts\deploy.ps1</ActionFile>
      <ActionArguments>-AnywhereOnly -SolutionRootFolder [databag@SolutionRootFolder] -ISiteServiceAccountUser [databag@ISiteServiceAccountUser] -ISiteServiceAccountPassword [databag@ISiteServiceAccountPassword]</ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
  </UpgradeActions>
  <RemoveActions>
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PackageScripts\RemoveAWV.ps1</ActionFile>
      <ActionArguments />
      <ActionTimeout>10000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
  </RemoveActions>
  <RemovedConditions>
    <Condition>
      <Test>WebUrl</Test>
      <TestObject>https://localhost/Anywhere/apppage.html</TestObject>
      <TestData>200</TestData>
      <ExpectedResult>False</ExpectedResult>
    </Condition>
  </RemovedConditions>
</Package>
"""

    xml_sample_two= """<?xml version="1.0"?>
<Package xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" PackageSchemaVersion="2.0.0.0" xmlns="http://tempuri.org/Package.xsd">
  <Name>AWV</Name>
  <Version>1.3.12.0.1862.24</Version>
  <TargetOperatingSystem>Windows</TargetOperatingSystem>
  <Description>IntelliSpace PACS Anywhere</Description>
  <Dependencies>
  </Dependencies>
</Package>
"""


    xml_bad_xml= """<?xml version="1.0"?>
<Manifest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" PackageSchemaVersion="2.0.0.0" xmlns="http://tempuri.org/Package.xsd"></Manifest>
"""

    xml_bad_package_name = """<?xml version="1.0"?>
<Package xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" PackageSchemaVersion="2.0.0.0" xmlns="http://tempuri.org/Package.xsd">
  <Name>AWV</Name>
  <Version>1.3.12.0.1862.24</Version>
  <TargetOperatingSystem>Windows</TargetOperatingSystem>
  <Description>IntelliSpace PACS Anywhere</Description>
  <Dependencies>
    <OtherPackage>
        <Name>DepChildOne</Name>
        <Version>1.2</Version>
    </OtherPackage>
    <OtherPackage>
        <Version>1.5.4.3</Version>
    </OtherPackage>
  </Dependencies>

</Package>
"""

    xml_bad_package_version = """<?xml version="1.0"?>
<Package xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" PackageSchemaVersion="2.0.0.0" xmlns="http://tempuri.org/Package.xsd">
  <Name>AWV</Name>
  <Version>1.3.12.0.1862.24</Version>
  <TargetOperatingSystem>Windows</TargetOperatingSystem>
  <Description>IntelliSpace PACS Anywhere</Description>
  <Dependencies>
    <OtherPackage>
        <Name>DepChildOne</Name>
    </OtherPackage>
    <OtherPackage>
        <Name>DepChildTwo</Name>
        <Version>1.5.4.3</Version>
    </OtherPackage>
  </Dependencies>

</Package>
"""

if __name__ == '__main__':
    unittest.main()