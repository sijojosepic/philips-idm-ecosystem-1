#!/usr/bin/env python
import json
import logging
from time import sleep
from datetime import timedelta, datetime
from urlparse import urlparse

import requests
import argparse

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
handler_console = logging.StreamHandler()
log.addHandler(handler_console)
# handler_file = logging.FileHandler('/var/log/celery/trainman.log')
# log.addHandler(handler_file)


JSON_HEADER = {'content-type': 'application/json'}
AUTH_TOKEN = ('thrukadmin', '1nf0M@t1cs')


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Script to issue a web call and setup a service to expect a callback')
    parser.add_argument('-u', '--url',
                        help='url to check',
                        required=True)
    parser.add_argument('-c', '--callback_address',
                        help='Callback address to use',
                        required=True)
    parser.add_argument('-t', '--timeout',
                        help='Timeout of script in seconds',
                        type=int,
                        required=True)
    parser.add_argument('-p', '--packages',
                        help='Packages to pass in the call',
                        required=True)
    parser.add_argument('-k', '--package_root',
                        help='Package root to pass in the call',
                        required=True)
    parser.add_argument('-s', '--site_info_file',
                        help='Site info file to pass in the call',
                        required=True)
    parser.add_argument('-id', '--deployment_id',
                        help='Id associated with each deplyoment',
                        required=True)
    parser.add_argument('-addr', '--pcm_status_collect',
                        help='Status back address',
                        required=True)
    parser.add_argument('-v', '--version',
                        help='Package Version',
                        required=True)

    results = parser.parse_args(args)
    return (
        results.url,
        results.callback_address,
        results.timeout,
        results.packages,
        results.package_root,
        results.site_info_file,
        results.deployment_id,
        results.pcm_status_collect,
        results.version
    )


def safe_post(url, data):
    try:
        log.info('POST : %s', url)
        post_request = requests.post(url, data=data, headers=JSON_HEADER, auth=AUTH_TOKEN, verify=False)
        post_request.raise_for_status()
    except requests.exceptions.RequestException as ex:
        log.error('POST : %s raised %s',url, ex)
        return False
    return True


def safe_get(url):
    try:
        log.info('GET : %s', url)
        request_value = requests.get(url, headers=JSON_HEADER, auth=AUTH_TOKEN, verify=False)
    except requests.exceptions.RequestException as ex:
        log.error('GET : %s raised %s', url, ex)
        return None
    return request_value


def call_payload(callback_address, packages, package_root, site_info_file, deplyoment_id, version):
    callback_pieces = urlparse(callback_address)

    payload = {
        'Repository': 'https://{hostname}'.format(hostname=callback_pieces.hostname),
        'SiteInformation': site_info_file,
        'PackageRoot': package_root,
        'Data': [
            'PackageScripts\\install-pcm.ps1'
        ],
        'Packages': [
            packages
        ],
        'Version': [
            version
        ],
        'Workflow': [
            'download',
            'install',
            'bootstrap'
        ],
        'Callbacks': [callback_address],
        'Deployment_Id': deplyoment_id,
        'credentials': AUTH_TOKEN
    }
    return json.dumps(payload)


def trainman(url, callback_address, timeout, packages, package_root, site_info_file, deplyoment_id, pcm_status_collect,
             version):
    # URL can be given as IP or FQDN, however Shinken State is stored under FQDN so ensure conversion
    # url = 'http: //192.168.52.83:8182/pcm/bootstrap'
    callback_pieces = urlparse(callback_address)
    url_pieces = urlparse(url)
    callback_address = '{callback_address}/{hostname}'.format(callback_address=callback_address,
                                                              hostname=url_pieces.hostname)
    pcm_status_collect = 'http://{hostname}{pcm_status_collect}'.format(hostname=callback_pieces.hostname,
                                                                         pcm_status_collect=pcm_status_collect)
    log.info('INFO : Callback set to %s', callback_address)

    # Sets so we know this node should be reported on
    safe_post(callback_address,
              json.dumps({'Status': 'UNKNOWN', 'Infos': {'DeploymentId': deplyoment_id, 'output': " "}}))

    payload = call_payload(callback_address, packages, package_root, site_info_file, deplyoment_id, version)
    log.info("url is + " + url)
    log.info(payload)
    pcm_call = safe_post(url, payload)

    if not pcm_call:
        log.error('ERROR : Unable to invoke PCM on %s', url_pieces.hostname)
        safe_post(callback_address, json.dumps({'state': 'WARNING'}))
        safe_post(pcm_status_collect,
                  json.dumps({'deployment_id': deplyoment_id, 'status': 'Failed', 'output': ' Unable to invoke PCM on {host}'.format( host=url_pieces.hostname)}))
        return

    timer = datetime.now() + timedelta(seconds=timeout)
    while timer > datetime.now():
        current_result = safe_get(callback_address+'/'+deplyoment_id)
        if current_result and current_result.text in ['OK', 'CRITICAL']:
            log.info('Found state %s for %s', current_result, url_pieces.hostname)
            status_transform = {'OK': 'Success', 'CRITICAL': 'Failed'}
            status_output = {'OK': 'Success', 'CRITICAL': 'PCM:Received Critical status from PCM'}
            safe_post(pcm_status_collect,
                      json.dumps({'deployment_id': deplyoment_id, 'status': status_transform[current_result.text],
                                  'output': status_output[current_result.text]}))
            return
        sleep(10)
    log.warning('WARN : PCM Timed out for %s', url_pieces.hostname)
    safe_post(pcm_status_collect,
              json.dumps({'deployment_id': deplyoment_id, 'status': 'Failed', 'output': 'PCM:Timed Out'}))


if __name__ == '__main__':
    trainman(*check_arg())
