#!/usr/bin/env python

"""
DESCRIPTION:
This script is for initializing the IDM On-site node post deployment.  It will setup the following:
Configure the network
Configure DNS
Modify the hostname
Configure /etc/hosts file
Remove old proxy configuration data
Create Subversion working copy
Bootstrap Ansible
"""
import datetime
import json
import logging
import os
import re
import shlex
import subprocess
import sys
import time

import argparse
import ipaddr
import requests

DNS_CONFIG_FILE = '/etc/resolv.conf'
NETWORK_INTERFACE_FILE = '/etc/sysconfig/network-scripts/ifcfg-eth0'
CHANGE_HOSTNAME_FILE = '/etc/sysconfig/network'
ETC_HOSTS_FILE = '/etc/hosts'
PROXY_SETTINGS_FILE = '/etc/profile.d/proxy.sh'
BACKOFFICE_ENDPOINT_NAMES = ('repo.phim.isyntax.net',
                             'phirepo.phim.isyntax.net',
                             'ntp.phim.isyntax.net',
                             'vigilant vigilant.phim.isyntax.net',
                             'sc.phim.isyntax.net',
                             'pypi.phim.isyntax.net')
PROXY_ENV_VARIABLES = ('http_proxy', 'https_proxy', 'no_proxy')
CRON_DAEMON_FILE = '/etc/sysconfig/crond'
SVN_INFO = dict()
SVN_INFO['username'] = 'phisvnuser'
SVN_INFO['password'] = 'st3nt0r'
SVN_INFO['url'] = 'sc.phim.isyntax.net/svn/ibc/sites/'
BACKOFFICE_CONNECTIVITY_URL = 'vigilant/health'
BACKOFFICE_NOTIFICATION_URL = 'vigilant/notification'
SITE_ID_REGEX = r'^[A-Z][A-Z0-9]{4}$'

SVN_WORKING_DIRECTORY = '/var/philips/views'
SVN_WORKING_COPY = os.path.join(SVN_WORKING_DIRECTORY, 'wc')

states = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')

logger = logging.getLogger('idm.initialization')
logger.setLevel(logging.INFO)

logging.Formatter.converter = time.gmtime
formatter = logging.Formatter(datefmt='%Y-%m-%dT%H:%M:%S',
                              fmt='%(asctime)s.%(msecs).06dZ - %(process)d - %(name)s - %(levelname)s - %(message)s')

handler_console = logging.StreamHandler()
handler_console.setFormatter(formatter)
logger.addHandler(handler_console)


# Functions
def check_arg(args=None):
    """
    Check the arguments passed
    """
    parser = argparse.ArgumentParser(description='Script to initialize On-site IDM node.')
    parser.add_argument('-i', '--ip',
                        help='the ip address of the On-site IDM node.',
                        required=True)
    parser.add_argument('-n', '--netmask',
                        help='the netmask of the On-site IDM node.',
                        required=True)
    parser.add_argument('-g', '--gateway',
                        help='gateway ip address',
                        required=True)
    parser.add_argument('-d', '--dns',
                        help='DNS server ip address(es)',
                        nargs='+',
                        required=True)
    parser.add_argument('-s', '--siteid',
                        help='Site ID of the customer.',
                        required=True)
    parser.add_argument('-e', '--endpoint',
                        help='ip address of the back office endpoint',
                        required=True)
    parser.add_argument('-t', '--noansible',
                        help='flag for preventing Ansible execution and back office notification if present.',
                        action='store_true',
                        required=False)
    parser.add_argument('-S', '--https',
                        help='flag for using https for back office endpoints if present.',
                        action='store_true',
                        required=False)
    parser.add_argument('-i6', '--ipv6',
                        help='the ipv6 address of the On-site IDM node.',
                        required=False)
    parser.add_argument('-n6', '--netmask6',
                        help='the netmask of the On-site IDM node in ipv6 format.',
                        required=False)
    parser.add_argument('-g6', '--gateway6',
                        help='gateway ipv6 address',
                        required=False)
    parser.add_argument('-d6', '--dns6',
                        help='DNS server ipv6 address(es)',
                        nargs='+',
                        required=False)

    results = parser.parse_args(args)
    return (
        results.ip,
        results.netmask,
        results.gateway,
        results.dns,
        results.siteid,
        results.endpoint,
        results.noansible,
        results.https,
        (
            results.ipv6,
            results.netmask6,
            results.gateway6,
            results.dns6
        )
    )


def is_valid_ip(ip_address):
    """
    Uses ipaddr module to determine if an IP Address is valid.  Works for both IPv4 and IPv6.  The
    function takes in either a string or an integer.  For integers less than 2^32 it will assume it is IPv4.
    There is a flag that can be passed to indicate whether IPv6 is intended for the integer, but this function
    does not utilize it.
    """
    try:
        ipaddr.IPAddress(ip_address)
    except ValueError:
        return False
    return True


def ip_address_version(ip_address):
    try:
        ret_obj = ipaddr.IPAddress(ip_address)
        return ret_obj.version
    except ValueError:
        return -1
    return -1



def is_valid_siteid(siteid):
    """
    SiteID is 5 characters long. First must be A-Z, the remaining can be A-Z or 0-9.
    This is from PRO4.08-01 Customer Site Naming Convention in Doc Control.
    """
    match = re.search(SITE_ID_REGEX, siteid)
    return match is not None


def is_valid_url(url, auth=None, verify=False):
    """
    Uses requests module to verify whether a URL can be contacted without error.  auth is a tuple containing
    the username and password of the website if required.  By default auth is set to None.  If the status is
    200, the function will return True and False otherwise.

    USAGE:
    is_valid_url('http://sc.phim.isyntax.net/svn/ibc', (username, password))
    """
    params = dict(verify=verify)
    if auth:
        params['auth'] = auth
    request = requests.get(url, **params)

    return request.status_code == requests.codes.ok


def verify_args(verify_ips, siteid):
    """
    Determine if all of the data received by the script is valid. If not exit with error status 1
    """
    logger.info('Ensuring that all of data passed to %s is valid.', os.path.basename(__file__))

    if not all(map(is_valid_ip, verify_ips)):
        logger.info('Invalid IP. Please check inputs')
        return False

    logger.info('Verifying SiteID %s is valid.', siteid)
    if not is_valid_siteid(siteid):
        logger.info('SiteID %s is NOT valid. Please verify the input to the script.', siteid)
        return False

    return True


def is_text_in_file(text_file, text):
    """
    Search file for text and return True if it is there.
    """
    logger.info('Verifying if %s is in %s', text, text_file)
    text_present = False
    try:
        with open(text_file, 'rU') as f:
            for line in f:
                # find returns position in string if found and -1 if not found
                if line.find(text) is not -1:
                    logger.info('%s is found in %s', text, text_file)
                    text_present = True
                    break
    except IOError:
        logger.info('Error accessing file %s', text_file)
        raise
    if text_present is False:
        logger.info('%s is NOT found in %s', text, text_file)
    return text_present


def is_last_line_terminated_with_newline(text_file):
    """
    Return True if the last line contains a \n
    """
    try:
        content = open(text_file, 'rU').read().splitlines(True)
        last_line = content[-1]
        return last_line.find('\n') is not -1
    except IOError:
        logger.info('Error opening file %s. Please investigate', text_file)
        raise


def append_line_to_file(text_file, line):
    """
    Adds a line to the end of a file
    """
    try:
        with open(text_file, 'a') as f:
            f.write(line)
    except IOError:
        logger.info('Error opening file %s. Please investigate', text_file)
        raise
    logger.info('Successfully updated %s file.', text_file)


def replace_line_in_file(filename, replace_this, newline):
    """
    find a string in file and replace the line with newline variable
    """
    try:
        with open(filename, 'rU') as f:
            content = f.read().splitlines()
            line_count = 0
            for line in content:
                # find returns character number if found and -1 if not
                if line.find(replace_this) is not -1:
                    content[line_count] = newline.strip('\n')
                    break
                line_count += 1
            content[-1] += '\n'
    except IOError:
        logger.info('Error opening file %s. Please investigate', filename)
        raise
    try:
        with open(filename, 'w') as f:
            f.write('\n'.join(content))
    except IOError:
        logger.info('Error writing to file %s. Please investigate', filename)
        raise


def replace_file_content(data, current_file):
    try:
        with open(current_file, 'w') as f:
            f.write(data)
    except IOError:
        logger.info('Error writing to file %s. Please investigate', current_file)
        raise


def configure_network(network_file, ip_address, netmask, gateway, ipv6_enabled=False):
    """
    Configure /etc/sysconfig/network-scripts/ifcfg-eth0 with network information
    setup strings for insertion into file.  This can be used for any valid network interface file
    not just the ifcfg-eth0 file.
    """
    onboot_line = 'ONBOOT=yes' + '\n'
    bootproto_line = 'BOOTPROTO=none' + '\n'
    if ipv6_enabled:
        ipinit_line = 'IPV6INIT=yes' + '\n'
        ipaddr_line = 'IPV6ADDR=' + ip_address + '\n'
        gateway_line = 'IPV6_DEFAULTGW=' + gateway + '\n'
        network_list = [onboot_line, bootproto_line, ipinit_line, ipaddr_line, gateway_line]
    else:
        ipaddr_line = 'IPADDR=' + ip_address + '\n'
        netmask_line = 'NETMASK=' + netmask + '\n'
        gateway_line = 'GATEWAY=' + gateway + '\n'
        network_list = [onboot_line, bootproto_line, ipaddr_line, netmask_line, gateway_line]

    logger.info('Configuring %s file with network information', network_file)
    for item in network_list:
        name = item.split('=')
        if is_text_in_file(network_file, name[0]):
            logger.info('Replacing line containing %s in %s', name[0], network_file)
            replace_line_in_file(network_file, name[0], item)
        else:
            logger.info('Appending line for %s setting to %s', name[0], network_file)
            if is_last_line_terminated_with_newline(network_file) is True:
                append_line_to_file(network_file, item)
            else:
                append_line_to_file(network_file, '\n' + item)


def update_dns(dns_file, dns_servers):
    """
    Update /etc/resolv.conf with the DNS Server IP
    :param dns_file:
    """
    logger.info('Updating %s with the DNS Server IP(s)' % dns_file)
    for server in dns_servers:
        newline = 'nameserver ' + server + '\n'
        if os.path.exists(dns_file):
            if is_text_in_file(dns_file, server) is True:
                logger.info('%s has already been added to %s', server, dns_file)
            else:
                # verify if the last in has a \n before updating
                if is_last_line_terminated_with_newline(dns_file):
                    append_line_to_file(dns_file, newline)
                else:
                    append_line_to_file(dns_file, '\n' + newline)
        else:
            # writing file with append option will create the file if it does not exist.
            logger.info('Creating %s and adding %s to it', dns_file, server)
            append_line_to_file(dns_file, newline)


def obtain_hostname(siteid):
    """
    Take in site ID and return hostname
    :param siteid:
    """
    return siteid.lower() + 'idm01'


def obtain_domainname(siteid):
    """
    Obtain FQDN from site ID and return full domain name
    :param siteid:
    :return:
    """
    return 'st' + siteid.lower() + '.isyntax.net'


def modify_sys_network(name, network_file, search_string='HOSTNAME', ipv6_enabled=False):
    """
    function that edits the /etc/sysconfig/network file with the system variables
    :param name:
    :param network_file:
    """
    newline = search_string + '=' + name + '\n'
    logger.info('Editing the %s file to change the %s.', network_file, search_string)
    # Don't edit file if the hostname is correct
    if not ipv6_enabled:
        if is_text_in_file(network_file, name):
            logger.info('%s has already been added to %s', name, network_file)
            return

    # Replace hostname line in the file with the appropriate hostname
    if is_text_in_file(network_file, search_string):
        logger.info('Replacing line containing %s in %s', search_string, network_file)
        replace_line_in_file(network_file, search_string, newline)
    else:
        # if for some reason there is no hostname line make one.
        logger.info('Appending %s to %s', newline, network_file)
        # verify if the last in has a \n before updating
        if not is_last_line_terminated_with_newline(network_file):
            newline = '\n' + newline
        append_line_to_file(network_file, newline)


def mod_probe_config():
    mod_probe_file = '/etc/modprobe.d/philips.conf'
    search_string = 'options ipv6 disable=1'
    newline = 'options ipv6 disable=0'
    if is_text_in_file(mod_probe_file, search_string):
        logger.info('Replacing line containing %s in %s', mod_probe_file, search_string)
        replace_line_in_file(mod_probe_file, search_string, newline)
    else:
        logger.info('Appending %s to %s', newline, mod_probe_file)
        if not is_last_line_terminated_with_newline(mod_probe_file):
            newline = '\n' + newline
        append_line_to_file(mod_probe_file, newline)


def build_host_file(ip_address, name, endpoint, endpoint_names):
    """
    create a host file from scratch
    """
    default_ipv4_line = '127.0.0.1\t\t localhost localhost.localdomain localhost4 localhost4.localdomain4 ' + name
    default_ipv6_line = '::1\t\t\t localhost localhost.localdomain localhost6 localhost6.localdomain6 ' + name
    hosts_line = ip_address + '\t\t' + name + ' ' + name.split('.')[0]
    hosts_file = [default_ipv4_line, default_ipv6_line, hosts_line]
    for service in endpoint_names:
        newline = endpoint + '\t\t' + service
        hosts_file.append(newline)
    # make sure last line has \n
    if hosts_file[-1].find('\n') is -1:
        hosts_file[-1] += '\n'
    content = '\n'.join(hosts_file)
    return content


def modify_host_file(ip_address, name, endpoint, hosts_file):
    """
    replace /etc/hosts file with one generated from scratch
    """
    logger.info('Generating and replacing %s', hosts_file)
    file_contents = build_host_file(ip_address, name, endpoint, BACKOFFICE_ENDPOINT_NAMES)
    replace_file_content(file_contents, hosts_file)
    logger.info('%s file has been updated.', hosts_file)


def remove_proxy(proxy_file):
    """
    Delete proxy.sh file and removes the environmental variable for the proxy if it is set as well.
    """
    logger.info('Removing proxy settings from the system')
    if os.path.exists(proxy_file):
        logger.info('%s file found.  Removing it from the system.', proxy_file)
        try:
            os.remove(proxy_file)
        except Exception as e:
            logger.exception(e)
            raise
    for proxy in PROXY_ENV_VARIABLES:
        if proxy in os.environ:
            logger.info('Removing proxy environmental variable %s.', proxy)
            try:
                del os.environ[proxy]
            except Exception as e:
                logger.exception(e)
                raise
    logger.info('Successfully removed proxy settings from the system')


def modify_crond_file(cron_file='/etc/sysconfig/crond'):
    """
    Set the CRONDARGS variable to prevent cron from sending email and to send the messages to the syslog.
    This function will overwrite the current crond file when it is run.
    """
    crond_contents = ['# Settings for the CRON daemon.',
                      '# CRONDARGS= :  any extra command-line startup arguments for crond',
                      "CRONDARGS='-s -m off'\n"]
    logger.info('Generating and replacing %s', cron_file)
    replace_file_content('\n'.join(crond_contents), cron_file)
    logger.info('%s file has been updated.', cron_file)


def execute_command(command, shell=False):
    """
    calls subprocess.check_call to execute commands with error handling.  function requires
    command and its arguments.  The command passes itself to subprocess as a list of strings separated
    by white space. Shlex is used to split the arguments to the command in such a manner that arguments with white
    space are handled appropriately.  This allows for more complex commands to be handled by this functions. Also, by
    default shell is set to False so that python will call the process directly.  If you are attempting to run
    multiple commands together you may want to set shell=True.  This will interact directly with the shell; however,
    it is considered a big security risk.

    MODULES REQUIRED:
    subprocess, shlex

    USAGE:
    execute_command(["ls", "-la"])
    """
    try:
        args = shlex.split(command)
        subprocess.check_call(args, shell=shell)
    except subprocess.CalledProcessError as e:
        logger.info("Error executing command.")
        logger.exception(e)
        raise
    except OSError as e:
        logger.info("Verify the program exists.")
        logger.exception(e)
        raise
    except Exception as e:
        logger.exception(e)
        raise


def restart_network():
    """
    restart network function
    """
    network_command = '/etc/rc.d/init.d/network restart'
    logger.info('Restarting the network with command %s', network_command)
    execute_command(network_command)
    logger.info('Successfully restarted the network.')


def bring_network_up(interface='eth0'):
    """
    Bring up a network interface using the ifup command.  It will assume eth0 is to be used if
    no interface is provided.  Also, no arguments will be assumed if they are not provided as well.

    USAGE:
    bring_network_up('eth0')
    """
    interface_cmd = '/etc/sysconfig/network-scripts/ifup ' + interface
    logger.info('Bringing up the network interface %s.', interface)
    execute_command(interface_cmd)
    logger.info('Successfully brought up the network interface %s.', interface)


def create_directory(directory):
    """
    Creates a directory in the file system.  Must provide the full path name.  Uses the os.makedirs
    functionality which will create all of the directories in the path if required.
    """
    if os.path.exists(directory) is True:
        logger.info('%s directory already exists, no need to recreate.', directory)
    else:
        try:
            os.makedirs(directory)
        except Exception as e:
            logger.exception(e)
            raise


def create_site_id_file(siteid, directory='/etc'):
    """
    Writes the SiteID File. Writes down every execution
    """
    newfile = os.path.join(directory, 'siteid')
    logger.info('Writing file %s.', newfile)
    replace_file_content(siteid + '\n', newfile)

def create_svn_local_directory():
    """
    This function will create the Subversion local repository on the system.
    It needs to do the following:
    -   Create the /var/philips/views directory.
    -   Go to the /var/philips/views directory.
    """

    logger.info('Creating the local Subversion repository.')
    logger.info('Creating the %s directory', SVN_WORKING_DIRECTORY)
    create_directory(SVN_WORKING_DIRECTORY)
    logger.info('Change current directory to %s', SVN_WORKING_DIRECTORY)
    try:
        os.chdir(SVN_WORKING_DIRECTORY)
    except Exception as e:
        logger.exception(e)
        raise


def bootstrap_ansible(siteid, protocol='http'):
    """
    This function will create an /etc/cron.d/ancible-pull file to bootstrap the ansible configuration for the node.
    It will start ansible at the next reboot.

    USAGE:
    bootstrap_ansible()
    This will run the ansible-pull command using the default location of the working copy.
    """


    logger.info('Bootstrapping ansible to the node.')
    svn_url = protocol + '://' + SVN_INFO['url'] + siteid

    logger.info('Verifying The SCM url %s is valid.', svn_url)
    if not is_valid_url(svn_url, (SVN_INFO['username'], SVN_INFO['password'])):
        logger.info('Error connection to url %s. Does the site exist in the backend', svn_url)
        return

    ansible_pull_command = "/usr/bin/ansible-pull -U " + svn_url + " -d " + SVN_WORKING_COPY \
                           + " --purge -f -m subversion -i localinv --checkout 'HEAD username=" \
                           + SVN_INFO['username'] + " password=" + SVN_INFO['password'] \
                           + "' common/ansible/local.yml"
    content = ['#Ansible: ansible pull on boot',
               '@reboot root ' + ansible_pull_command + ' 2>&1 > /tmp/ansible_pull.log\n']
    logger.info('Creating /etc/cron.d/ansible_pull file.')
    replace_file_content('\n'.join(content), '/etc/cron.d/ansible_pull')


def notify_backoffice(node_name, node_ip_address, siteid, protocol, verify=False):
    """
    This function will be used to post a message to the back office.  It needs the following information:

    USAGE:
    notify_backoffice('test', '172.16.4.74', 'IDMXX')

    :param node_name: hostname of the system
    :param node_ip_address: IP address of the system
    :param siteid: SiteID used by the system
    """

    connectivity_url = protocol + '://' + BACKOFFICE_CONNECTIVITY_URL
    if is_valid_url(connectivity_url):
        logger.info('Sending notification to the backoffice location %s.', connectivity_url)

        message = {'state': 'OK',
                   'service': 'Administrative__Philips__IntelligentDeviceManagement__Initialization__Status',
                   'timestamp': datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
                   'hostname': node_name,
                   'perfdata': None,
                   'siteid': siteid,
                   'hostaddress': node_ip_address,
                   'type': 'EVENT',
                   'payload': None}

        headers = {'content-type': 'application/json'}
        data = json.dumps(message)

        requests.post(url=protocol + '://' + BACKOFFICE_NOTIFICATION_URL, data=data, headers=headers, verify=verify)

    else:
        logger.info('Error connecting to the backoffice url %s', connectivity_url)
        raise Exception('Unable to connect to URL')


def main():
    ip_address, netmask, gateway, dnsservers, siteid, endpoint, no_ansible, https, ipv6_creds = check_arg()
    # Determine if all of the data received by the script is valid. If not exit with error status 1
    siteid = siteid.upper()
    # IP Addresses and Names for verification purposes.
    verify_ips = [ip_address, netmask, gateway]
    verify_ips.extend(dnsservers)

    state = 2
    try:
        if verify_args(verify_ips, siteid):
            logger.info('All of data passed to %s is valid.', os.path.basename(__file__))
            # Configure Networking
            configure_network(NETWORK_INTERFACE_FILE, ip_address, netmask, gateway)
            # Update DNS Information
            update_dns(DNS_CONFIG_FILE, dnsservers)
            # Obtaining system hostname and domain name
            logger.info('Determining the hostname and domain name from the site ID %s.', siteid)
            system_name = obtain_hostname(siteid)
            logger.info('The hostname of the system will be set to %s.', system_name)
            domain_name = obtain_domainname(siteid)
            logger.info('The domain name for %s will be set to %s.', system_name, domain_name)
            fqdn_system_name = system_name + '.' + domain_name
            logger.info('The FQDN of the system will be set to %s.', fqdn_system_name)
            # Changing the system hostname
            modify_sys_network(fqdn_system_name, CHANGE_HOSTNAME_FILE)
            # Updating host file with hostname, ip, and backoffice services
            modify_host_file(ip_address, fqdn_system_name, endpoint, ETC_HOSTS_FILE)
            # Set hostname in the RAM
            execute_command('hostname ' + fqdn_system_name)
            # Removing proxy settings from system
            remove_proxy(PROXY_SETTINGS_FILE)
            # Creating Site ID file in the /etc directory
            create_site_id_file(siteid)

            # Add ipv6 details on the VM
            ipv6_address, ipv6_netmask, ipv6_gateway, ipv6_dns = ipv6_creds
            if ip_address_version(ipv6_address) == 6:
                verify_ipv6s = [ipv6_gateway]
                if ipv6_dns:
                    verify_ipv6s.extend(ipv6_dns)
                if not verify_args(verify_ipv6s, siteid):
                    raise ValueError
                configure_network(NETWORK_INTERFACE_FILE, ipv6_address, ipv6_netmask, ipv6_gateway, ipv6_enabled=True)
                update_dns(DNS_CONFIG_FILE, ipv6_dns)
                modify_sys_network('yes', CHANGE_HOSTNAME_FILE, search_string='NETWORKING_IPV6', ipv6_enabled=True)
                mod_probe_config()

            # Bring up the network interface
            bring_network_up()
            restart_network()
            # Modify crond settings
            modify_crond_file(CRON_DAEMON_FILE)
            # Creating Subversion views directory and go to that location
            create_svn_local_directory()
            # Bootstrapping Ansible if a production system
            if not no_ansible:
                protocol = 'https' if https else 'http'
                bootstrap_ansible(siteid, protocol)
                notify_backoffice(system_name, ip_address, siteid, protocol)
                execute_command('sleep 30')
                execute_command('shutdown -r now')
            message = 'Successfully initialized Intelligent Device Management node %s.' % fqdn_system_name
            state = 0
        else:
            message = 'Invalid input into the script %s' % os.path.basename(__file__)
    except ValueError:
        message = 'Invalid input please investigate.'
    except IOError:
        message = 'Error accessing file.  Please investigate.'
    except OSError:
        message = 'Error accessing executable. Please investigate'
    except subprocess.CalledProcessError:
        message = 'Error executing program.'
    except Exception as e:
        message = 'Unknown error. %s' % e
        state = 3
    if state == 0:
        logger.info('%s STATUS - %s ', message, states[state])
    else:
        logger.exception('%s STATUS - %s ', message, states[state])
    sys.exit(state)


if __name__ == '__main__':
    main()
