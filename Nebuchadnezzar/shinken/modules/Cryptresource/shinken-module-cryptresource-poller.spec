# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      shinken-module-cryptresource-poller
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Shinken poller module to decrypt resources before passing them to plugins

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  shinken-receiver
Provides:  shinken-ws_passive_service_map-receiver

%description
Shinken web service passive service check with mapping module
Allows submitting service check results via web services
Can map ip address to hostname
Can map service names using a yaml configuration file


%prep
%setup


%build
echo OK


%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_sharedstatedir}/shinken/modules
%{__cp} -r %{_builddir}/%{buildsubdir}/cryptresource %{buildroot}%{_sharedstatedir}/shinken/modules/


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sharedstatedir}/shinken/modules/cryptresource


%changelog
* Fri Jun 03 2016 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
