from __future__ import absolute_import

from celery.utils.log import get_logger
from phim_onsite.celery import app
from phim_onsite.transmit import tx_data
from tbconfig import ANALYTICS_CLINICAL_QUEUE, ANALYTICS_DICOM_QUEUE, QUEUE_TRANSPORT, HEARTBEAT

logger = get_logger(__name__)


@app.task(bind=True, ignore_result=True,
          max_retries=QUEUE_TRANSPORT['RETRIES'], default_retry_delay=QUEUE_TRANSPORT['RETRY_DELAY'])
def transport_queue(self, data, queue_name):
    try:
        tx_data(QUEUE_TRANSPORT['ENDPOINT'], {'queue': queue_name, 'data': data}, QUEUE_TRANSPORT['KILLSWITCH'])
    except Exception as exc:
        raise self.retry(exc=exc)


@app.task(ignore_result=True)
def transport_clinical_queue(data):
    transport_queue.delay(data, ANALYTICS_CLINICAL_QUEUE)


@app.task(ignore_result=True)
def transport_dicom_queue(data):
    transport_queue.delay(data, ANALYTICS_DICOM_QUEUE)


@app.task(ignore_result=True)
def do_hb_msg(smtpto, smtpfrom, mailbody, url=HEARTBEAT['ENDPOINT']):
    data = {'message': mailbody, 'to': smtpto, 'from': smtpfrom}
    tx_data(url, data, HEARTBEAT['KILLSWITCH'], compress_payload=True, target_keys=['message'])
