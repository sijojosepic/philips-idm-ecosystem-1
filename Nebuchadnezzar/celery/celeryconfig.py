## Broker settings.

BROKER_URL = 'amqp://'

CELERY_RESULT_BACKEND = 'redis://'

# List of modules to import when celery starts.
CELERY_IMPORTS = ('phim_onsite.queuetask', 'phim_onsite.collect', 'phim_onsite.operate',
                  'phim_onsite.transmit', 'phim_onsite.transport', 'phim_onsite.shinken',)

# disable if not needed (best practice)
CELERY_DISABLE_RATE_LIMITS = True

# name accepted content (best practice)
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'yaml']


################################
# CELERYBEAT
################################
from celery.schedules import crontab
from datetime import timedelta
 
CELERYBEAT_SCHEDULE = {
#    'every-minute': {
#        'task': 'tasks.add',
#        'schedule': crontab(),
#        'args': (1,2),
#    },
}

