import unittest
from mock import MagicMock, patch, call


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        func.func_dict = {'rc': 'rc'}
        return func
    return fakeorator


class POTasksTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_po = MagicMock(name='phim_onsite')
            self.mock_po.celery.app.task = fakeorator_arg
            self.mock_taskbase = MagicMock(name='taskbase')
            self.mock_tbconfig = MagicMock(name='tbconfig')
            self.mock_celery = MagicMock(name='celery')
            self.mock_requests = MagicMock(name='requests')
            self.mock_phimutils = MagicMock(name='phimutils')
            self.mock_scanline = MagicMock(name='scanline')
            modules = {
                'tbconfig': self.mock_tbconfig,
                'taskbase': self.mock_taskbase,
                'taskbase.redistask': self.mock_taskbase.redistask,
                'phim_onsite.celery': self.mock_po.celery,
                'celery': self.mock_celery,
                'celery.canvas': self.mock_celery.canvas,
                'celery.utils': self.mock_celery.utils,
                'celery.utils.log': self.mock_celery.utils.log,
                'requests': self.mock_requests,
                'phimutils': self.mock_phimutils,
                'phimutils.stateredis': self.mock_phimutils.stateredis,
                'phimutils.message': self.mock_phimutils.message,
                'phimutils.timestamp': self.mock_phimutils.timestamp,
                'phimutils.resource': self.mock_phimutils.resource,
                'scanline': self.mock_scanline,
                'scanline.trinity': self.mock_scanline.trinity,
                'scanline.component': self.mock_scanline.component,
                'scanline.component.localhost': self.mock_scanline.localhost,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import phim_onsite.transmit
            self.transmit = phim_onsite.transmit
            self.transmit.PERFDATA = {'QUEUE': 'perfdata_queue'}
            self.transmit.DISCOVERY = {
                'ENDPOINT': 'ENDPOINT', 'KILLSWITCH': 'KILLSWITCH'}
            self.transmit.DISCOVERY_VERSION_KEYS = {'a': 'b', 'c': 'd'}
            self.transmit.STATE = {
                'REDIS_PREFIX': 'PREFIX', 'REDIS_SET': 'SET'}
            self.transmit.VERSION = {'ENDPOINT': 'enp'}
            self.transmit.PCM = {'REPO_PATH': 'REPO_PATH'}

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class GetPerfblobTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)

        self.msgs = []
        self.msgs.append(
            {
                'timestamp': '2013-10-16T09:15:54.353045Z',
                'hostname': 'host1',
                'hostaddress': '127.0.0.2',
                'service': 'Cat#Ven#Obj#Atr#Dim1',
                'perfdata': 'x=1m;1;2'
            }
        )
        self.msgs.append(
            {
                'timestamp': '2013-10-16T12:23:14.233540Z',
                'hostname': 'host2',
                'hostaddress': '127.0.0.3',
                'service': 'Cat#Ven#Obj#Atr#Dim2',
                'perfdata': 'y=31k;3;4'
            }
        )
        self.msgs.append(
            {
                'timestamp': '2013-11-16T02:44:61.468134Z',
                'hostname': 'host1',
                'hostaddress': '127.0.0.2',
                'service': 'Cat#Ven#Obj#Atr#Dim1',
                'perfdata': 'x=2m;1;2'
            }
        )

        self.conn = MagicMock(name='conn')
        self.queue = self.conn.SimpleQueue.return_value
        self.queue.__len__.return_value = 3

        self.transmit.msg2perfdict = MagicMock(
            name='msg2perfdict', side_effect=self.msgs)

    def test_get_perfblob_normal(self):
        self.assertEqual(
            self.transmit.get_perfblob(self.conn),
            [
                {
                    'timestamp': '2013-10-16T09:15:54.353045Z',
                    'hostname': 'host1',
                    'hostaddress': '127.0.0.2',
                    'service': 'Cat#Ven#Obj#Atr#Dim1',
                    'perfdata': 'x=1m;1;2'
                },
                {
                    'timestamp': '2013-10-16T12:23:14.233540Z',
                    'hostname': 'host2',
                    'hostaddress': '127.0.0.3',
                    'service': 'Cat#Ven#Obj#Atr#Dim2',
                    'perfdata': 'y=31k;3;4'
                },
                {
                    'timestamp': '2013-11-16T02:44:61.468134Z',
                    'hostname': 'host1',
                    'hostaddress': '127.0.0.2',
                    'service': 'Cat#Ven#Obj#Atr#Dim1',
                    'perfdata': 'x=2m;1;2'
                }
            ]
        )
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 3)

    def test_get_perfblob_index_error(self):
        self.transmit.msg2perfdict.side_effect = IndexError
        self.assertRaises(IndexError, self.transmit.get_perfblob, self.conn)
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')

    def test_get_perfblob_key_error(self):
        self.transmit.msg2perfdict.side_effect = KeyError
        self.assertEqual(self.transmit.get_perfblob(self.conn), [])
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 3)

    def test_get_perfblob_queue_empty_before_len_0(self):
        self.queue.get_nowait.side_effect = self.transmit.Empty
        self.assertEqual(self.transmit.get_perfblob(self.conn), [])
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 1)


class TxTrinityDiscoveryDataTestCase(POTasksTest.TestCase):

    def test_tx_trinity_discovery_data_monitor_flg_true(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1', 'monitor': True}
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_tx_discovery = [call(credentials=None, endpoint={
            'monitor': True, 'scanner': 'TestScanner', 'address': '1.0.0.1'}, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)
        _expected_call_process_end_config = [call(
            {'monitor': True, 'scanner': 'TestScanner', 'address': '1.0.0.1'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)

    def test_tx_trinity_discovery_data_monitor_flg_false(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1', 'monitor': False}
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'monitor': False, 'scanner': 'TestScanner', 'address': '1.0.0.1'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(credentials=None, endpoint={'monitor': False,
                                                                        'scanner': 'TestScanner', 'address': '1.0.0.1'}, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)

    def test_tx_trinity_discovery_data_without_monitor_flg(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1'}
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'scanner': 'TestScanner', 'address': '1.0.0.1'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(credentials=None, endpoint={
            'scanner': 'TestScanner', 'address': '1.0.0.1'}, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)

    def test_tx_trinity_discovery_data_verify_endpoint_expection(self):
        endpoint = {'scanner': 'TestScanner',
                               'address': '1.0.0.1'}
        mock_verify_endpoint = MagicMock(name='verify endpoint')
        mock_verify_endpoint.side_effect = RuntimeError
        self.transmit.verify_endpoint = mock_verify_endpoint
        response = self.transmit.tx_trinity_discovery_data(endpoint)
        self.assertEqual(
            response, (2, 'Using TestScanner on 1.0.0.1 Failed'))

    def test_tx_trinity_discovery_data_with_credentials(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1'}
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        credentials = {'u': 'u1', 'p': 'p1'}
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1", 'credentials': credentials}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'scanner': 'TestScanner', 'address': '1.0.0.1'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(credentials=credentials, endpoint={
            'scanner': 'TestScanner', 'address': '1.0.0.1'}, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)


class TxDiscoveryDataTestCase(POTasksTest.TestCase):
    def test_tx_discovery_empty_value(self):
        tx_data_mock = MagicMock(name='tx_data')
        self.transmit.tx_data = tx_data_mock
        self.transmit.tx_discovery()
        tx_data_mock.assert_called_once_with(
            'ENDPOINT', {'facts': None}, 'KILLSWITCH')

    def test_tx_discovery_non_empty_values(self):
        tx_data_mock = MagicMock(name='tx_data')
        self.transmit.tx_data = tx_data_mock
        self.transmit.tx_discovery(
            'hostname', 'service', 'facts', 'endpoint', 'credentials')
        out_put = {'facts': 'facts',
                   'credentials': 'credentials', 'endpoint': 'endpoint'}
        tx_data_mock.assert_called_once_with('ENDPOINT', out_put, 'KILLSWITCH')


class ResolveResourceTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)
        self.mock_resource = MagicMock(name='resource')

    def test_resolve_resource_dict_resource(self):
        item = {'$resource': '$USER56$'}
        self.mock_resource.get_resource.return_value = 'user1'
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, 'user1')
        self.mock_resource.get_resource.assert_called_once_with('$USER56$')

    def test_resolve_resource_dict_item_without_resource_key(self):
        item = {'item': '$USER56$'}
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, item)
        self.assertEqual(self.mock_resource.get_resource.call_count, 0)

    def test_resolve_resource_list_item(self):
        item = ['u1', '$USER45$']
        self.mock_resource.get_resource.side_effect = ('u1', 'u2')
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, ('u1', 'u2'))
        exp_call = [call('u1'), call('$USER45$')]
        self.assertEqual(
            self.mock_resource.get_resource.call_args_list, exp_call)

    def test_resolve_resource_str_item(self):
        item = 'user'
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, 'user')


class VerifyEndpointTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)

    def test_verify_endpoint_usr_pwd_length_match(self):
        endpoint = {'username': ['u1', 'u2']}
        endpoint['password'] = ['p1', 'p2']
        self.transmit.verify_endpoint(endpoint)

    def test_verify_endpoint_pwd_not_list(self):
        endpoint = {'username': ['u1', 'u2']}
        endpoint['password'] = 'pwd'
        self.assertRaises(
            RuntimeError, self.transmit.verify_endpoint, endpoint)

    def test_verify_endpoint_usr_not_list(self):
        endpoint = {'username': 'u1'}
        endpoint['password'] = ['p1', 'p2']
        self.assertRaises(
            RuntimeError, self.transmit.verify_endpoint, endpoint)

    def test_verify_endpoint_length_mismatch(self):
        endpoint = {'username': ['u1']}
        endpoint['password'] = ['p1', 'p2']
        self.assertRaises(
            RuntimeError, self.transmit.verify_endpoint, endpoint)


class VersionDiscoveryTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)

    def test_tx_version_discovery(self):
        mock_tx_data = MagicMock(name='tx_data')
        self.transmit.tx_data = mock_tx_data
        host_info = {"localhost": {"KeyOne": "ValueOne",
                                   "KeyTwo": None, "KeyThree": None, "KeyFour": None}}
        host_info['host1'] = {
            "KeyOne": None, "KeyTwo": "ValueTwo", "KeyThree": None, "KeyFour": None}
        self.mock_phimutils.stateredis.StateRedis.return_value.query.return_value = host_info
        self.transmit.tx_version_discovery()
        self.mock_phimutils.stateredis.StateRedis.assert_called_once_with(
            'rc', 'PREFIX', 'SET')
        exp_facts = {'localhost': {'Components': [{'version': 'ValueOne', 'name': 'KeyOne'}]},
                     'host1': {'Components': [{'version': 'ValueTwo', 'name': 'KeyTwo'}]}}
        mock_tx_data.assert_called_once_with('enp', data={'facts': exp_facts})
        self.mock_phimutils.stateredis.StateRedis.return_value.query.assert_called_once_with(
            key_map={'a': 'b', 'c': 'd'}, keys=['a', 'c'])

    def test_tx_pcm_discovery(self):
        mock_tx_data = MagicMock(name='tx_data')
        self.transmit.tx_data = mock_tx_data
        mock_os_walk = MagicMock(name='os.walk')
        mock_os_walk.return_value = [('/root', ['dir'], ('f1.xml', 'f2.xml'))]
        self.transmit.os.walk = mock_os_walk
        self.mock_scanline.localhost.LocalHost.return_value.get_attributes.return_value = {
            'a': 'b'}
        pcm_mock = MagicMock(name='pcm_obj')
        pcm_mock.get_download_status_and_size.return_value = ('Available', '0B')

        self.mock_phimutils.pcm.PCMPackage = MagicMock(name='PCMPackage')
        self.mock_phimutils.pcm.PCMPackage.return_value = pcm_mock

        self.transmit.tx_pcm_discovery()

        exp_facts = {'localhost': {'a': 'b', 'PCM': [{'version': self.mock_phimutils.pcm.PCMPackage().Version.replace(),
                                                      'name': self.mock_phimutils.pcm.PCMPackage().Name.__add__(),
                                                      'dependencies': self.mock_phimutils.pcm.PCMPackage().get_dependencies(),
                                                      'databags': self.mock_phimutils.pcm.PCMPackage().get_required_databags(),
                                                      'status': 'Available',
                                                      'fsize': '0B'},
                                                     {'version': self.mock_phimutils.pcm.PCMPackage().Version.replace(),
                                                      'name': self.mock_phimutils.pcm.PCMPackage().Name.__add__(),
                                                      'dependencies': self.mock_phimutils.pcm.PCMPackage().get_dependencies(),
                                                      'databags': self.mock_phimutils.pcm.PCMPackage().get_required_databags(),
                                                      'status': 'Available',
                                                      'fsize': '0B'
                                                      }]}}
        mock_tx_data.assert_called_once_with('enp', data={'facts': exp_facts})
        mock_os_walk.assert_called_once_with('REPO_PATH')


if __name__ == '__main__':
    unittest.main()
