#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import logging
import linecache
import json
import redis
from base64 import b64decode
from scanline.utilities.http import HTTPRequester

logger = logging.getLogger(__name__)

SITEID = ''
SITEID_FILE = '/etc/siteid'
VIGILANT_URL = 'http://vigilant/'
HOST_URL = 'pma/facts/{site_id}/hosts'
MODULE_URL = 'pma/facts/{site_id}/modules/ISP/keys/module_type'
FACT_URL = 'pma/facts/{site_id}/nodes'


def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_backoffice_data(fact_type='host'):
    host_url = VIGILANT_URL + HOST_URL.format(site_id=SITEID)
    module_url = VIGILANT_URL + MODULE_URL.format(site_id=SITEID)
    fact_url = VIGILANT_URL + FACT_URL.format(site_id=SITEID)
    fact_dict = {'host': host_url, 'module': module_url, 'fact': fact_url}
    http_obj = HTTPRequester()
    result = http_obj.suppressed_get(fact_dict[fact_type], headers={'X-Requested-With': "True"})
    return json.loads(result.content)

def get_node_info():
    global SITEID
    SITEID = get_siteid()
    outmsg = 'OK - UDM Nodes are available.'

    redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)
    redis_data = [key for key in redis_con.keys(pattern="*ISPACS*")]
    app_host = [key.split('#')[1] for key in redis_data if key.split('#')[-1] == '1024']
    if not redis_data:
        facts = get_backoffice_data('module')['result']
        app_host = [host['hostname'] for host in facts if host['ISP']['module_type'] == 1024]
    
    if not app_host:
        outmsg = 'UNKNOWN - UDM nodes are not available.'
        sys.exit(3)
    
    print outmsg


def main():
    get_node_info()


if __name__ == '__main__':
    main()
