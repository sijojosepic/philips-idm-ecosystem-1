import sys
import unittest
import requests
import datetime

from mock import MagicMock, patch


class CheckSqlMirrorTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'requests': self.mock_request,
            'argparse': self.mock_argparse,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_mirror_status
        self.module = check_sql_mirror_status
        self.current_server_timestamp_failover = [[(datetime.datetime(2018, 8, 21, 13, 34, 34, 483000),)], [(datetime.datetime(2018, 8, 21, 13, 34, 34, 643000),)]]
        self.current_server_timestamp_OK = [[(datetime.datetime(2018, 8, 21, 15, 34, 34, 483000),)], [(datetime.datetime(2018, 8, 21, 15, 34, 34, 643000),)]]
        self.failover_result = [(datetime.datetime(2018, 8, 21, 13, 32, 32, 360000), u'spid6s', u'The mirrored database "StentorOperation" is changing roles from "MIRROR" to "PRINCIPAL" because the mirroring session or availability group failed over due to failover from partner. This is an informational message only. No user action is required.'), (datetime.datetime(2018, 8, 21, 13, 32, 32, 460000), u'spid38s', u'The mirrored database "StentorOperation" is changing roles from "PRINCIPAL" to "MIRROR" because the mirroring session or availability group failed over due to manual failover. This is an informational message only. No user action is required.')]
        self.module.get_siteid = MagicMock(return_value="mocked")
        self.mock_db_hosts = MagicMock("DB_hosts")
        self.mock_state_result = MagicMock(name='state_result')
        self.mock_sync_result = MagicMock(name='sync_result')
        self.mock_db_hosts.return_value = ['db1', 'db2']

    def test_get_db_mirror_status_OK_1(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.mock_sync_result.return_value = [0, 0]
        self.assertEqual(self.module.get_formatted_data(self.current_server_timestamp_OK, self.mock_db_hosts.return_value, self.mock_state_result.return_value, self.mock_sync_result.return_value, self.failover_result, 102400), (0, 'OK - Database primary up on db1'))

    def test_get_db_mirror_status_OK_2(self):
        self.mock_state_result.return_value = ['Flag Error', 'OK']
        self.mock_sync_result.return_value = [0, 0]
        self.assertEqual(self.module.get_formatted_data(self.current_server_timestamp_OK, self.mock_db_hosts.return_value, self.mock_state_result.return_value, self.mock_sync_result.return_value, self.failover_result, 102400), (0, 'OK - Database primary up on db2'))

    def test_get_db_mirror_status_WARNING_1(self):
        self.mock_state_result.return_value = ['Flag Error', 'OK']
        self.mock_sync_result.return_value = [1, 0]
        self.assertEqual(self.module.get_formatted_data(self.current_server_timestamp_OK, self.mock_db_hosts.return_value, self.mock_state_result.return_value, self.mock_sync_result.return_value, self.failover_result, 102400), (2, 'CRITICAL: Data synchronization is slow at db1 - Backlog reached threshold of 100MB.'))

    def test_get_db_mirror_status_WARNING_2(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.mock_sync_result.return_value = [0, 1]
        self.assertEqual(self.module.get_formatted_data(self.current_server_timestamp_OK, self.mock_db_hosts.return_value, self.mock_state_result.return_value, self.mock_sync_result.return_value, self.failover_result, 102400), (2, 'CRITICAL: Data synchronization is slow at db2 - Backlog reached threshold of 100MB.'))

    def test_get_db_mirror_status_WARNING_3(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.mock_sync_result.return_value = [1, 1]
        self.assertEqual(self.module.get_formatted_data(self.current_server_timestamp_OK, self.mock_db_hosts.return_value, self.mock_state_result.return_value, self.mock_sync_result.return_value, self.failover_result, 102400), (2, "CRITICAL: Data synchronization is slow at ['db1', 'db2'] - Backlog reached threshold of 100MB."))

    def test_get_db_mirror_status_CRITICAL(self):
        self.mock_state_result.return_value = ['Flag Error', 'Flag Error']
        self.mock_sync_result.return_value = [0, 0]
        self.assertEqual(self.module.get_formatted_data(self.current_server_timestamp_OK, self.mock_db_hosts.return_value, self.mock_state_result.return_value, self.mock_sync_result.return_value, self.failover_result, 102400), (2, 'CRITICAL - Splitbrain State'))

    def test_get_db_mirror_status_CRITICAL_Failover_1(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.mock_sync_result.return_value = [0, 0]
        self.assertEqual(self.module.get_formatted_data(self.current_server_timestamp_failover, self.mock_db_hosts.return_value, self.mock_state_result.return_value, self.mock_sync_result.return_value, self.failover_result, 102400), (1, 'WARNING - Failover happen on db2 and Service up on db1'))

    def test_get_db_mirror_status_CRITICAL_Failover_2(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.mock_sync_result.return_value = [0, 0]
        self.assertEqual(self.module.get_formatted_data(self.current_server_timestamp_failover, self.mock_db_hosts.return_value, self.mock_state_result.return_value, self.mock_sync_result.return_value, self.failover_result[::-1], 102400), (1, 'WARNING - Failover happen on db1 and Service up on db2'))

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()