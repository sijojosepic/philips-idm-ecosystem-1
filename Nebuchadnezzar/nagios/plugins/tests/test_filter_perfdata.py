import sys
import unittest
from mock import MagicMock

sys.modules['phimutils.perfdata'] = MagicMock(name='phimutils.perfdata')
perfdict = [{'uom': 'B', 'value': '24576', 'label': 'Idle WorkingSetPrivate'},
            {'uom': 'B', 'value': '24576', 'label': 'Idle WorkingSet'},
            {'uom': 'B', 'value': '57344', 'label': 'System WorkingSetPrivate'},
            {'uom': 'B', 'value': '307200', 'label': 'System WorkingSet'},
            {'uom': 'B', 'value': '507904', 'label': 'smss WorkingSetPrivate'},
            {'uom': 'B', 'value': '1261568', 'label': 'smss WorkingSet'},
            {'uom': 'B', 'value': '2961408', 'label': 'csrss WorkingSetPrivate'},
            {'uom': 'B', 'value': '5820416', 'label': 'csrss WorkingSet'},
            {'uom': 'B', 'value': '1400832', 'label': 'wininit WorkingSetPrivate'},
            {'uom': 'B', 'value': '4632576', 'label': 'wininit WorkingSet'},
            {'uom': 'B', 'value': '5173248', 'label': 'services WorkingSetPrivate'},
            {'uom': 'B', 'value': '13422592', 'label': 'services WorkingSet'},
            {'uom': 'B', 'value': '16375808', 'label': 'lsass WorkingSetPrivate'},
            {'uom': 'B', 'value': '25620480', 'label': 'lsass WorkingSet'},
            {'uom': 'B', 'value': '2580480', 'label': 'lsm WorkingSetPrivate'},
            {'uom': 'B', 'value': '6893568', 'label': 'lsm WorkingSet'},
            {'uom': 'B', 'value': '5238784', 'label': 'svchost WorkingSetPrivate'},
            {'uom': 'B', 'value': '12005376', 'label': 'svchost WorkingSet'},
            {'uom': 'B', 'value': '6197248', 'label': 'svchost#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '11264000', 'label': 'svchost#1 WorkingSet'},
            {'uom': 'B', 'value': '15495168', 'label': 'svchost#2 WorkingSetPrivate'},
            {'uom': 'B', 'value': '22413312', 'label': 'svchost#2 WorkingSet'},
            {'uom': 'B', 'value': '38428672', 'label': 'svchost#3 WorkingSetPrivate'},
            {'uom': 'B', 'value': '57221120', 'label': 'svchost#3 WorkingSet'},
            {'uom': 'B', 'value': '7569408', 'label': 'svchost#4 WorkingSetPrivate'},
            {'uom': 'B', 'value': '16076800', 'label': 'svchost#4 WorkingSet'},
            {'uom': 'B', 'value': '8753152', 'label': 'svchost#5 WorkingSetPrivate'},
            {'uom': 'B', 'value': '17002496', 'label': 'svchost#5 WorkingSet'},
            {'uom': 'B', 'value': '8372224', 'label': 'svchost#6 WorkingSetPrivate'},
            {'uom': 'B', 'value': '18190336', 'label': 'svchost#6 WorkingSet'},
            {'uom': 'B', 'value': '5210112', 'label': 'svchost#7 WorkingSetPrivate'},
            {'uom': 'B', 'value': '12029952', 'label': 'svchost#7 WorkingSet'},
            {'uom': 'B', 'value': '6184960', 'label': 'spoolsv WorkingSetPrivate'},
            {'uom': 'B', 'value': '14970880', 'label': 'spoolsv WorkingSet'},
            {'uom': 'B', 'value': '4460544', 'label': 'svchost#8 WorkingSetPrivate'},
            {'uom': 'B', 'value': '10010624', 'label': 'svchost#8 WorkingSet'},
            {'uom': 'B', 'value': '6959104', 'label': 'FrameworkService WorkingSetPrivate'},
            {'uom': 'B', 'value': '14467072', 'label': 'FrameworkService WorkingSet'},
            {'uom': 'B', 'value': '3960832', 'label': 'mqsvc WorkingSetPrivate'},
            {'uom': 'B', 'value': '10305536', 'label': 'mqsvc WorkingSet'},
            {'uom': 'B', 'value': '324419584', 'label': 'sqlservr WorkingSetPrivate'},
            {'uom': 'B', 'value': '356052992', 'label': 'sqlservr WorkingSet'},
            {'uom': 'B', 'value': '991232', 'label': 'svchost#9 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2945024', 'label': 'svchost#9 WorkingSet'},
            {'uom': 'B', 'value': '1929216', 'label': 'sqlwriter WorkingSetPrivate'},
            {'uom': 'B', 'value': '6635520', 'label': 'sqlwriter WorkingSet'},
            {'uom': 'B', 'value': '14524416', 'label': 'vmtoolsd WorkingSetPrivate'},
            {'uom': 'B', 'value': '24154112', 'label': 'vmtoolsd WorkingSet'},
            {'uom': 'B', 'value': '29863936', 'label': 'WmiPrvSE WorkingSetPrivate'},
            {'uom': 'B', 'value': '38232064', 'label': 'WmiPrvSE WorkingSet'},
            {'uom': 'B', 'value': '7430144', 'label': 'WmiPrvSE#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '13856768', 'label': 'WmiPrvSE#1 WorkingSet'},
            {'uom': 'B', 'value': '2936832', 'label': 'svchost#10 WorkingSetPrivate'},
            {'uom': 'B', 'value': '9322496', 'label': 'svchost#10 WorkingSet'},
            {'uom': 'B', 'value': '1695744', 'label': 'svchost#11 WorkingSetPrivate'},
            {'uom': 'B', 'value': '5730304', 'label': 'svchost#11 WorkingSet'},
            {'uom': 'B', 'value': '3801088', 'label': 'dllhost WorkingSetPrivate'},
            {'uom': 'B', 'value': '11603968', 'label': 'dllhost WorkingSet'},
            {'uom': 'B', 'value': '3178496', 'label': 'msdtc WorkingSetPrivate'},
            {'uom': 'B', 'value': '8163328', 'label': 'msdtc WorkingSet'},
            {'uom': 'B', 'value': '9039872', 'label': 'WmiPrvSE#2 WorkingSetPrivate'},
            {'uom': 'B', 'value': '14368768', 'label': 'WmiPrvSE#2 WorkingSet'},
            {'uom': 'B', 'value': '1486848', 'label': 'svchost#12 WorkingSetPrivate'},
            {'uom': 'B', 'value': '4743168', 'label': 'svchost#12 WorkingSet'},
            {'uom': 'B', 'value': '2940928', 'label': 'sppsvc WorkingSetPrivate'},
            {'uom': 'B', 'value': '9560064', 'label': 'sppsvc WorkingSet'},
            {'uom': 'B', 'value': '2142208', 'label': 'csrss#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '6656000', 'label': 'csrss#1 WorkingSet'},
            {'uom': 'B', 'value': '1703936', 'label': 'winlogon WorkingSetPrivate'},
            {'uom': 'B', 'value': '5332992', 'label': 'winlogon WorkingSet'},
            {'uom': 'B', 'value': '1941504', 'label': 'taskhost WorkingSetPrivate'},
            {'uom': 'B', 'value': '6385664', 'label': 'taskhost WorkingSet'},
            {'uom': 'B', 'value': '1437696', 'label': 'dwm WorkingSetPrivate'},
            {'uom': 'B', 'value': '4804608', 'label': 'dwm WorkingSet'},
            {'uom': 'B', 'value': '22908928', 'label': 'explorer WorkingSetPrivate'},
            {'uom': 'B', 'value': '49225728', 'label': 'explorer WorkingSet'},
            {'uom': 'B', 'value': '7311360', 'label': 'VMwareTray WorkingSetPrivate'},
            {'uom': 'B', 'value': '10973184', 'label': 'VMwareTray WorkingSet'},
            {'uom': 'B', 'value': '8859648', 'label': 'vmtoolsd#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '16633856', 'label': 'vmtoolsd#1 WorkingSet'},
            {'uom': 'B', 'value': '4747264', 'label': 'mmc WorkingSetPrivate'},
            {'uom': 'B', 'value': '20799488', 'label': 'mmc WorkingSet'},
            {'uom': 'B', 'value': '995328', 'label': 'cmd WorkingSetPrivate'},
            {'uom': 'B', 'value': '2940928', 'label': 'cmd WorkingSet'},
            {'uom': 'B', 'value': '1277952', 'label': 'conhost WorkingSetPrivate'},
            {'uom': 'B', 'value': '4444160', 'label': 'conhost WorkingSet'},
            {'uom': 'B', 'value': '6672384', 'label': 'svchost#13 WorkingSetPrivate'},
            {'uom': 'B', 'value': '12402688', 'label': 'svchost#13 WorkingSet'},
            {'uom': 'B', 'value': '13553664', 'label': 'iSiteMonitor WorkingSetPrivate'},
            {'uom': 'B', 'value': '33218560', 'label': 'iSiteMonitor WorkingSet'},
            {'uom': 'B', 'value': '32501760', 'label': 'Qrscp WorkingSetPrivate'},
            {'uom': 'B', 'value': '67190784', 'label': 'Qrscp WorkingSet'},
            {'uom': 'B', 'value': '1040384', 'label': 'conhost#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2859008', 'label': 'conhost#1 WorkingSet'},
            {'uom': 'B', 'value': '22134784', 'label': 'QueueMonitor WorkingSetPrivate'},
            {'uom': 'B', 'value': '48742400', 'label': 'QueueMonitor WorkingSet'},
            {'uom': 'B', 'value': '1044480', 'label': 'conhost#2 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2867200', 'label': 'conhost#2 WorkingSet'},
            {'uom': 'B', 'value': '20811776', 'label': 'iSiteServer WorkingSetPrivate'},
            {'uom': 'B', 'value': '51515392', 'label': 'iSiteServer WorkingSet'},
            {'uom': 'B', 'value': '1167360', 'label': 'conhost#3 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2990080', 'label': 'conhost#3 WorkingSet'},
            {'uom': 'B', 'value': '14692352', 'label': 'TaskManager WorkingSetPrivate'},
            {'uom': 'B', 'value': '37646336', 'label': 'TaskManager WorkingSet'},
            {'uom': 'B', 'value': '18944000', 'label': 'NotificationService WorkingSetPrivate'},
            {'uom': 'B', 'value': '45109248', 'label': 'NotificationService WorkingSet'},
            {'uom': 'B', 'value': '1044480', 'label': 'conhost#4 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2867200', 'label': 'conhost#4 WorkingSet'},
            {'uom': 'B', 'value': '1163264', 'label': 'conhost#5 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2965504', 'label': 'conhost#5 WorkingSet'},
            {'uom': 'B', 'value': '17866752', 'label': 'RemoteEventLogMonitor WorkingSetPrivate'},
            {'uom': 'B', 'value': '42422272', 'label': 'RemoteEventLogMonitor WorkingSet'},
            {'uom': 'B', 'value': '1040384', 'label': 'conhost#6 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2867200', 'label': 'conhost#6 WorkingSet'},
            {'uom': 'B', 'value': '13578240', 'label': 'LoggingService WorkingSetPrivate'},
            {'uom': 'B', 'value': '38830080', 'label': 'LoggingService WorkingSet'},
            {'uom': 'B', 'value': '1040384', 'label': 'conhost#7 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2867200', 'label': 'conhost#7 WorkingSet'},
            {'uom': 'B', 'value': '3940352', 'label': 'notepad WorkingSetPrivate'},
            {'uom': 'B', 'value': '8425472', 'label': 'notepad WorkingSet'},
            {'uom': 'B', 'value': '1896448', 'label': 'csrss#2 WorkingSetPrivate'},
            {'uom': 'B', 'value': '5509120', 'label': 'csrss#2 WorkingSet'},
            {'uom': 'B', 'value': '1650688', 'label': 'winlogon#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '5156864', 'label': 'winlogon#1 WorkingSet'},
            {'uom': 'B', 'value': '1933312', 'label': 'taskhost#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '6402048', 'label': 'taskhost#1 WorkingSet'},
            {'uom': 'B', 'value': '1826816', 'label': 'rdpclip WorkingSetPrivate'},
            {'uom': 'B', 'value': '6627328', 'label': 'rdpclip WorkingSet'},
            {'uom': 'B', 'value': '1449984', 'label': 'dwm#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '4833280', 'label': 'dwm#1 WorkingSet'},
            {'uom': 'B', 'value': '17100800', 'label': 'explorer#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '39993344', 'label': 'explorer#1 WorkingSet'},
            {'uom': 'B', 'value': '3694592', 'label': 'VMwareTray#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '7380992', 'label': 'VMwareTray#1 WorkingSet'},
            {'uom': 'B', 'value': '15855616', 'label': 'mmc#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '27791360', 'label': 'mmc#1 WorkingSet'},
            {'uom': 'B', 'value': '2834432', 'label': 'vds WorkingSetPrivate'},
            {'uom': 'B', 'value': '9072640', 'label': 'vds WorkingSet'},
            {'uom': 'B', 'value': '8630272', 'label': 'TrustedInstaller WorkingSetPrivate'},
            {'uom': 'B', 'value': '14655488', 'label': 'TrustedInstaller WorkingSet'},
            {'uom': 'B', 'value': '2387968', 'label': 'slui WorkingSetPrivate'},
            {'uom': 'B', 'value': '8331264', 'label': 'slui WorkingSet'},
            {'uom': 'B', 'value': '774144', 'label': 'naPrdMgr WorkingSetPrivate'},
            {'uom': 'B', 'value': '1298432', 'label': 'naPrdMgr WorkingSet'},
            {'uom': 'B', 'value': '26476544', 'label': 'DmwlApp WorkingSetPrivate'},
            {'uom': 'B', 'value': '52486144', 'label': 'DmwlApp WorkingSet'},
            {'uom': 'B', 'value': '1052672', 'label': 'conhost#8 WorkingSetPrivate'},
            {'uom': 'B', 'value': '2875392', 'label': 'conhost#8 WorkingSet'},
            {'uom': 'B', 'value': '54521856', 'label': 'StorageService WorkingSetPrivate'},
            {'uom': 'B', 'value': '98381824', 'label': 'StorageService WorkingSet'},
            {'uom': 'B', 'value': '1200128', 'label': 'conhost#9 WorkingSetPrivate'},
            {'uom': 'B', 'value': '3026944', 'label': 'conhost#9 WorkingSet'},
            {'uom': 'B', 'value': '30052352', 'label': 'w3wp WorkingSetPrivate'},
            {'uom': 'B', 'value': '66572288', 'label': 'w3wp WorkingSet'},
            {'uom': 'B', 'value': '42061824', 'label': 'w3wp#1 WorkingSetPrivate'},
            {'uom': 'B', 'value': '72597504', 'label': 'w3wp#1 WorkingSet'},
            {'uom': 'B', 'value': '72220672', 'label': 'w3wp#2 WorkingSetPrivate'},
            {'uom': 'B', 'value': '115138560', 'label': 'w3wp#2 WorkingSet'},
            {'uom': 'B', 'value': '27996160', 'label': 'w3wp#3 WorkingSetPrivate'},
            {'uom': 'B', 'value': '59289600', 'label': 'w3wp#3 WorkingSet'},
            {'uom': 'B', 'value': '75247616', 'label': 'w3wp#4 WorkingSetPrivate'},
            {'uom': 'B', 'value': '117960704', 'label': 'w3wp#4 WorkingSet'},
            {'uom': 'B', 'value': '180862976', 'label': 'w3wp#5 WorkingSetPrivate'},
            {'uom': 'B', 'value': '222461952', 'label': 'w3wp#5 WorkingSet'},
            {'uom': 'B', 'value': '1798144', 'label': 'unsecapp WorkingSetPrivate'},
            {'uom': 'B', 'value': '5406720', 'label': 'unsecapp WorkingSet'},
            {'uom': 'B', 'value': '6758400', 'label': 'w3wp#6 WorkingSetPrivate'},
            {'uom': 'B', 'value': '17756160', 'label': 'w3wp#6 WorkingSet'},
            {'uom': 'B', 'value': '1355997184', 'label': '_Total WorkingSetPrivate'},
            {'uom': 'B', 'value': '2284298240', 'label': '_Total WorkingSet'}]

#sys.modules['phimutils.perfdata'].perfdata2dict.return_value = perfdict
#sys.modules['phimutils.perfdata'].raw_metric.side_effect = lambda **kwargs: kwargs
sys.modules['phimutils'] = MagicMock(name='phimutils')

import filter_perfdata


class FilterPerfdataFilteredMetricsTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        # This does not actually get used, but its the string where the dictionary comes from
        #self.perfdata = "'Idle WorkingSetPrivate'=24576B; 'Idle WorkingSet'=24576B; 'System WorkingSetPrivate'=57344B; 'System WorkingSet'=307200B; 'smss WorkingSetPrivate'=507904B; 'smss WorkingSet'=1261568B; 'csrss WorkingSetPrivate'=2961408B; 'csrss WorkingSet'=5820416B; 'wininit WorkingSetPrivate'=1400832B; 'wininit WorkingSet'=4632576B; 'services WorkingSetPrivate'=5173248B; 'services WorkingSet'=13422592B; 'lsass WorkingSetPrivate'=16375808B; 'lsass WorkingSet'=25620480B; 'lsm WorkingSetPrivate'=2580480B; 'lsm WorkingSet'=6893568B; 'svchost WorkingSetPrivate'=5238784B; 'svchost WorkingSet'=12005376B; 'svchost#1 WorkingSetPrivate'=6197248B; 'svchost#1 WorkingSet'=11264000B; 'svchost#2 WorkingSetPrivate'=15495168B; 'svchost#2 WorkingSet'=22413312B; 'svchost#3 WorkingSetPrivate'=38428672B; 'svchost#3 WorkingSet'=57221120B; 'svchost#4 WorkingSetPrivate'=7569408B; 'svchost#4 WorkingSet'=16076800B; 'svchost#5 WorkingSetPrivate'=8753152B; 'svchost#5 WorkingSet'=17002496B; 'svchost#6 WorkingSetPrivate'=8372224B; 'svchost#6 WorkingSet'=18190336B; 'svchost#7 WorkingSetPrivate'=5210112B; 'svchost#7 WorkingSet'=12029952B; 'spoolsv WorkingSetPrivate'=6184960B; 'spoolsv WorkingSet'=14970880B; 'svchost#8 WorkingSetPrivate'=4460544B; 'svchost#8 WorkingSet'=10010624B; 'FrameworkService WorkingSetPrivate'=6959104B; 'FrameworkService WorkingSet'=14467072B; 'mqsvc WorkingSetPrivate'=3960832B; 'mqsvc WorkingSet'=10305536B; 'sqlservr WorkingSetPrivate'=324419584B; 'sqlservr WorkingSet'=356052992B; 'svchost#9 WorkingSetPrivate'=991232B; 'svchost#9 WorkingSet'=2945024B; 'sqlwriter WorkingSetPrivate'=1929216B; 'sqlwriter WorkingSet'=6635520B; 'vmtoolsd WorkingSetPrivate'=14524416B; 'vmtoolsd WorkingSet'=24154112B; 'WmiPrvSE WorkingSetPrivate'=29863936B; 'WmiPrvSE WorkingSet'=38232064B; 'WmiPrvSE#1 WorkingSetPrivate'=7430144B; 'WmiPrvSE#1 WorkingSet'=13856768B; 'svchost#10 WorkingSetPrivate'=2936832B; 'svchost#10 WorkingSet'=9322496B; 'svchost#11 WorkingSetPrivate'=1695744B; 'svchost#11 WorkingSet'=5730304B; 'dllhost WorkingSetPrivate'=3801088B; 'dllhost WorkingSet'=11603968B; 'msdtc WorkingSetPrivate'=3178496B; 'msdtc WorkingSet'=8163328B; 'WmiPrvSE#2 WorkingSetPrivate'=9039872B; 'WmiPrvSE#2 WorkingSet'=14368768B; 'svchost#12 WorkingSetPrivate'=1486848B; 'svchost#12 WorkingSet'=4743168B; 'sppsvc WorkingSetPrivate'=2940928B; 'sppsvc WorkingSet'=9560064B; 'csrss#1 WorkingSetPrivate'=2142208B; 'csrss#1 WorkingSet'=6656000B; 'winlogon WorkingSetPrivate'=1703936B; 'winlogon WorkingSet'=5332992B; 'taskhost WorkingSetPrivate'=1941504B; 'taskhost WorkingSet'=6385664B; 'dwm WorkingSetPrivate'=1437696B; 'dwm WorkingSet'=4804608B; 'explorer WorkingSetPrivate'=22908928B; 'explorer WorkingSet'=49225728B; 'VMwareTray WorkingSetPrivate'=7311360B; 'VMwareTray WorkingSet'=10973184B; 'vmtoolsd#1 WorkingSetPrivate'=8859648B; 'vmtoolsd#1 WorkingSet'=16633856B; 'mmc WorkingSetPrivate'=4747264B; 'mmc WorkingSet'=20799488B; 'cmd WorkingSetPrivate'=995328B; 'cmd WorkingSet'=2940928B; 'conhost WorkingSetPrivate'=1277952B; 'conhost WorkingSet'=4444160B; 'svchost#13 WorkingSetPrivate'=6672384B; 'svchost#13 WorkingSet'=12402688B; 'iSiteMonitor WorkingSetPrivate'=13553664B; 'iSiteMonitor WorkingSet'=33218560B; 'Qrscp WorkingSetPrivate'=32501760B; 'Qrscp WorkingSet'=67190784B; 'conhost#1 WorkingSetPrivate'=1040384B; 'conhost#1 WorkingSet'=2859008B; 'QueueMonitor WorkingSetPrivate'=22134784B; 'QueueMonitor WorkingSet'=48742400B; 'conhost#2 WorkingSetPrivate'=1044480B; 'conhost#2 WorkingSet'=2867200B; 'iSiteServer WorkingSetPrivate'=20811776B; 'iSiteServer WorkingSet'=51515392B; 'conhost#3 WorkingSetPrivate'=1167360B; 'conhost#3 WorkingSet'=2990080B; 'TaskManager WorkingSetPrivate'=14692352B; 'TaskManager WorkingSet'=37646336B; 'NotificationService WorkingSetPrivate'=18944000B; 'NotificationService WorkingSet'=45109248B; 'conhost#4 WorkingSetPrivate'=1044480B; 'conhost#4 WorkingSet'=2867200B; 'conhost#5 WorkingSetPrivate'=1163264B; 'conhost#5 WorkingSet'=2965504B; 'RemoteEventLogMonitor WorkingSetPrivate'=17866752B; 'RemoteEventLogMonitor WorkingSet'=42422272B; 'conhost#6 WorkingSetPrivate'=1040384B; 'conhost#6 WorkingSet'=2867200B; 'LoggingService WorkingSetPrivate'=13578240B; 'LoggingService WorkingSet'=38830080B; 'conhost#7 WorkingSetPrivate'=1040384B; 'conhost#7 WorkingSet'=2867200B; 'notepad WorkingSetPrivate'=3940352B; 'notepad WorkingSet'=8425472B; 'csrss#2 WorkingSetPrivate'=1896448B; 'csrss#2 WorkingSet'=5509120B; 'winlogon#1 WorkingSetPrivate'=1650688B; 'winlogon#1 WorkingSet'=5156864B; 'taskhost#1 WorkingSetPrivate'=1933312B; 'taskhost#1 WorkingSet'=6402048B; 'rdpclip WorkingSetPrivate'=1826816B; 'rdpclip WorkingSet'=6627328B; 'dwm#1 WorkingSetPrivate'=1449984B; 'dwm#1 WorkingSet'=4833280B; 'explorer#1 WorkingSetPrivate'=17100800B; 'explorer#1 WorkingSet'=39993344B; 'VMwareTray#1 WorkingSetPrivate'=3694592B; 'VMwareTray#1 WorkingSet'=7380992B; 'mmc#1 WorkingSetPrivate'=15855616B; 'mmc#1 WorkingSet'=27791360B; 'vds WorkingSetPrivate'=2834432B; 'vds WorkingSet'=9072640B; 'TrustedInstaller WorkingSetPrivate'=8630272B; 'TrustedInstaller WorkingSet'=14655488B; 'slui WorkingSetPrivate'=2387968B; 'slui WorkingSet'=8331264B; 'naPrdMgr WorkingSetPrivate'=774144B; 'naPrdMgr WorkingSet'=1298432B; 'DmwlApp WorkingSetPrivate'=26476544B; 'DmwlApp WorkingSet'=52486144B; 'conhost#8 WorkingSetPrivate'=1052672B; 'conhost#8 WorkingSet'=2875392B; 'StorageService WorkingSetPrivate'=54521856B; 'StorageService WorkingSet'=98381824B; 'conhost#9 WorkingSetPrivate'=1200128B; 'conhost#9 WorkingSet'=3026944B; 'w3wp WorkingSetPrivate'=30052352B; 'w3wp WorkingSet'=66572288B; 'w3wp#1 WorkingSetPrivate'=42061824B; 'w3wp#1 WorkingSet'=72597504B; 'w3wp#2 WorkingSetPrivate'=72220672B; 'w3wp#2 WorkingSet'=115138560B; 'w3wp#3 WorkingSetPrivate'=27996160B; 'w3wp#3 WorkingSet'=59289600B; 'w3wp#4 WorkingSetPrivate'=75247616B; 'w3wp#4 WorkingSet'=117960704B; 'w3wp#5 WorkingSetPrivate'=180862976B; 'w3wp#5 WorkingSet'=222461952B; 'unsecapp WorkingSetPrivate'=1798144B; 'unsecapp WorkingSet'=5406720B; 'w3wp#6 WorkingSetPrivate'=6758400B; 'w3wp#6 WorkingSet'=17756160B; '_Total WorkingSetPrivate'=1355997184B; '_Total WorkingSet'=2284298240B;"
        self.perfdict = perfdict

    def test_valid_one(self):
        self.assertEqual(filter_perfdata.filtered_metrics(['Idle WorkingSet'], self.perfdict),
                         [{'uom': 'B', 'value': '24576', 'label': 'Idle WorkingSet'}])

    def test_empty(self):
        self.assertEqual(filter_perfdata.filtered_metrics([], self.perfdict), [])

    def test_valid_two(self):
        self.assertEqual(
            filter_perfdata.filtered_metrics(['Idle WorkingSet', 'DmwlApp WorkingSetPrivate'], self.perfdict),
            [
                {'uom': 'B', 'value': '24576', 'label': 'Idle WorkingSet'},
                {'uom': 'B', 'value': '26476544', 'label': 'DmwlApp WorkingSetPrivate'}
            ]
        )

    def test_valid_one_not_found_one(self):
        self.assertEqual(
            filter_perfdata.filtered_metrics(['SuperTurbo99 WorkingSet', 'DmwlApp WorkingSetPrivate'], self.perfdict),
            [{'uom': 'B', 'value': '26476544', 'label': 'DmwlApp WorkingSetPrivate'}]
        )


class FilterPerfdataFormatMetric(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)

    def test_metric_one(self):
        self.assertEqual(filter_perfdata.format_metric({'uom': 'B', 'value': '24576', 'label': 'Idle WorkingSet'}),
                         "'Idle WorkingSet' = 24576B")

    def test_metric_two(self):
        self.assertEqual(filter_perfdata.format_metric({'value': '5588', 'label': 'awesomeness'}),
                         "'awesomeness' = 5588")


if __name__ == '__main__':
    unittest.main()