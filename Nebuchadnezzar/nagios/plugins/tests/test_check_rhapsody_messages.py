import unittest
import requests
from mock import MagicMock, patch


class CheckRhapsodyMessagesTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request,
        }
        self.mock_scanline = MagicMock(name='scanline')
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.http_patch = patch('scanline.utilities.http.HTTPRequester')
        self.http_patcher = self.http_patch.start()
        import check_rhapsody_messages
        self.check_rhapsody_messages = check_rhapsody_messages
        from check_rhapsody_messages import get_message_count
        from check_rhapsody_messages import get_version
        self.get_message_count = get_message_count
        self.get_version = get_version

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_message_count_ok(self):
        self.http_patcher.return_value.suppressed_get_parsed.return_value =\
            'Queue Size:2'
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 5, 10),
                         (0, 'Total message(s) found in the queue: 2'))

    def test_get_message_count_critical(self):
        self.http_patcher.return_value.suppressed_get_parsed.return_value =\
            'Queue Size:2'
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 0, 0),
                         (2, 'Total message(s) found in the queue: 2'))

    def test_get_message_count_warning(self):
        self.http_patcher.return_value.suppressed_get_parsed.return_value =\
            'Queue Size:2'
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 2, 5),
                         (1, 'Total message(s) found in the queue: 2'))

    def test_get_message_count_indexerror(self):
        self.http_patcher.return_value.suppressed_get_parsed.side_effect =\
            IndexError
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 0, 0),
                         (2, 'Could not fetch attribute.'))

    def test_get_message_count_exception(self):
        self.http_patcher.return_value.suppressed_get_parsed.side_effect =\
            requests.exceptions.ConnectionError
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 0, 0),
                         (2, 'Could not connect to server.'))

    @patch('check_rhapsody_messages.requests.get')
    def test_get_version_200(self, mock_get):
        mock_get.return_value.status_code = 200
        version_json = '{"data": {"uptime": "P0Y0M3DT7H10M33.061S",' \
                       '"version": "6.2.2", "name": "Rhapsody"}}'
        mock_get.return_value.content = version_json
        self.assertEqual(self.get_version('localhost', 'guest', 'guest', 8544),
                         (0, 'Rhapsody version is: 6.2.2'))

    @patch('check_rhapsody_messages.requests.get')
    def test_get_version_404(self, mock_get):
        mock_get.return_value.status_code = 404
        self.assertEqual(self.get_version('localhost', 'guest', 'guest', 8544),
                         (2, 'Could not fetch version, status code: 404'))

    @patch('check_rhapsody_messages.requests.get')
    def test_get_version_exception(self, mock_get):
        mock_get.side_effect = Exception
        self.assertEqual(self.get_version('localhost', 'guest', 'guest', 8544),
                         (2, 'Could not connect to server.'))


if __name__ == '__main__':
    unittest.main()
