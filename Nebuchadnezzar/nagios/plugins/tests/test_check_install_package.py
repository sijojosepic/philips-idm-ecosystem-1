import unittest
import mock
from mock import MagicMock, patch, Mock
from StringIO import StringIO


class CheckInstallPackageTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'requests': self.mock_request,
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.installing_package_status': self.mock_scanline.utilities.installing_package_status,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_install_package
        self.module = check_install_package
        self.sys_mock = MagicMock(name = 'sys')
        self.module.sys = self.sys_mock
        
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @mock.patch("__builtin__.open", create=True)
    def test_read_files(self, mock_open):
    	download_content = 'download_script'
    	install_content = 'install_script'
    	mock_open.side_effect = [
            mock.mock_open(read_data="Data1").return_value,
            mock.mock_open(read_data="Data2").return_value
        ]
        self.assertEqual(self.module.read_files('download_script', 'install_script'),('Data1', 'Data2'))

    
    def test_upload_ps_scripts(self): 
        mock_conn = MagicMock(name = 'conn')
    	self.module.upload_ps_scripts(mock_conn, 'download_content', 'install_content', 'download_path',
                      '/usr/lib/download_script', '/usr/lib/install_script')
    	self.assertEqual(mock_conn.run_ps.call_count,3)

    def test_upload_ps_scripts_no_installcontent(self): 
        mock_conn = MagicMock(name = 'conn')
    	self.module.upload_ps_scripts(mock_conn, 'download_content', '', 'download_path',
                      '/usr/lib/download_script', '/usr/lib/install_script')
    	self.assertEqual(mock_conn.run_ps.call_count,2)

    def test_upload_ps_scripts_no_downloadcontent(self): 
        mock_conn = MagicMock(name = 'conn')
    	self.module.upload_ps_scripts(mock_conn, '', 'install_content', 'download_path',
                      '/usr/lib/download_script', '/usr/lib/install_script')
    	self.assertEqual(mock_conn.run_ps.call_count,2)

    def test_upload_ps_scripts_no_content(self): 
        mock_conn = MagicMock(name = 'conn')
    	self.module.upload_ps_scripts(mock_conn, '', '', 'download_path',
                      '/usr/lib/download_script', '/usr/lib/install_script')
    	self.assertEqual(mock_conn.run_ps.call_count,1)

    def test_download_package(self):
    	mock_conn = MagicMock(name = 'conn')
    	self.module.download_package(mock_conn, 'http://132.32.33.2/pcm/pcm.1.1.msi', 's:\pcm',
                      '/usr/lib/download_script')
    	self.assertEqual(mock_conn.run_ps.call_count,1)

    @patch('sys.stdout', new_callable = StringIO)
    def test_download_package_exception(self, std_out):
    	self.module.download_package('conn', 'http://132.32.33.2/pcm/pcm.1.1.msi', 's:\pcm',
                      '/usr/lib/download_script')
    	self.assertEqual(std_out.getvalue(), 'CRITICAL: Could not download package.\n')
    	self.sys_mock.exit.assert_called_once_with(2)


    def test_install_package(self):
    	mock_conn = MagicMock(name = 'conn')
    	self.module.install_package(mock_conn, 'username', 'password', 'domain', 'hostaddress', 'http://132.32.33.2/pcm/pcm.1.1.msi',
                's:\pcm', '/usr/lib/install_script', 'pcmlistenerservice')
    	self.assertEqual(mock_conn.run_ps.call_count,1)

    def test_delete_partial_files(self):
    	mock_conn = MagicMock(name = 'conn')
    	self.module.delete_partial_files(mock_conn,'s:\pcm')
    	self.assertEqual(mock_conn.run_ps.call_count,1)


if __name__ == '__main__':
    unittest.main()