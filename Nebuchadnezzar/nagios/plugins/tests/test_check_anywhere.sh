plugin="check_anywhere.py"
plugindir="/home/nagiost/AnywhereMonitoring/"
webroot="/var/www/html/"
webhost="localhost"
directory="any"
fakehost="fakehost"

[[ $1 ]] && plugindir=$1

#GLOBAL
SUMRESULT=0

cleanup(){
    rm -rf ${webroot}/${directory}
}

setuptestdir(){
    mkdir -p ${webroot}/${directory}
    
    appvgen="    <h2 id=\"APP_VERSION\">
1.0.24.0
    </h2>"
    
    appvlong="    <h2 data-id=\"none\" class=\"APP_VERSION \">
1.0.24.0
    </h2>
    <h2 data-itutu=\"none\" id=\"APP_VERSION\" class=\"APP_VERSION \">
1.0.24.0
    </h2>"
    
    read -r -d '' htmldoctop <<EOF1
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>ISP Anywhere Version Information</title>
  </head>
  <body>
    <h1><strong>ISP Anywhere Version Information Page</strong></h1>
    <h1>ISP Anywherere Version  : </h1>
EOF1

    read -r -d '' htmldocbot <<EOF2
    <h1>ISP PACS Minimum Backend Version  : </h1><h2 id="APP_BACKEND_MINVERSION">4.4.229.0</h2>
  </body>
</html>
EOF2
    
    # bad file, empty
    echo "" > ${webroot}${directory}/version0.html
    
    #fileversion#=${webdir}${directory}/version#.html
    read $(echo fileversion{1..6}) <<<$(echo ${webroot}${directory}/version{1..6}.html)
    
    # good file, well behaved
    echo "$htmldoctop"  > $fileversion1
    echo "$appvgen"    >> $fileversion1
    echo "$htmldocbot" >> $fileversion1

    # string in incorrect format
    sed "s/1.0.24.0/1.0.24.xxx/g" $fileversion1 > $fileversion2
    
    # good file, well behaved, larger numbers
    sed "s/1.0.24.0/4545.34505435.245304.53453490/g" $fileversion1 > $fileversion3
    
    # bad file, missing version
    echo "$htmldoctop"  > $fileversion4
    echo "$htmldocbot" >> $fileversion4
    
    # good file, longer node
    echo "$htmldoctop"  > $fileversion5
    echo "$appvlong"   >> $fileversion5
    echo "$htmldocbot" >> $fileversion5
    
    # good file, different format and node
    cat <<EOF3 > $fileversion6
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>ISP Anywhere Version Information</title>
  </head>
  <body>
    <h1><strong>ISP Anywhere Version Information Page</strong></h1>
    <p>This is a paragraph</p>
    <dl>
        <dt>ISP Anywherere Version:</dt><dd data-itutu="none" id="APP_VERSION" class="APP_LOOK ">2.1.2554.2    </dd>
        <dt>Some other thing Version:</dt><dd data-id="none" class="APP_VERSION ">1.0.12.1</dd>
    <dl>
    <h1>ISP PACS Minimum Backend Version  : </h1><h2 id="APP_BACKEND_MINVERSION">4.3.229.0</h2>
  </body>
</html>
EOF3
}

tester() # TESTNAME TESTCMD EXPECTEDEXIT EXPECTEDOUT
{
    echo
    _EXIT=0
    echo "$1"
    echo "$2"
    _OUT_TEXT=$(eval "$2")
    _OUT_EXIT=$?
    if [[ "$4" ]]
    then
        echo "Expected:"
        echo "$4"
        echo "Output:"
        echo "$_OUT_TEXT"
        if [[ $4 == $_OUT_TEXT ]] 
        then 
            echo "Expected output match - [SUCCESS]"  
        else
            echo "Expected output NOT matched - [FAIL]"
            ((_EXIT++))
        fi
    fi
    if [[ $3 == $_OUT_EXIT ]]
    then
        echo "Expected exit match - [SUCCESS]"
    else
        echo "Expected exit NOT matched - [FAIL]"
        ((_EXIT++))
    fi
    echo
    return $_EXIT
}

# BEGIN SETUP TESTS

testname[0]="Test non existent file"
testtext[0]="${plugindir}${plugin} -H ${webhost} -u \"/${directory}/version0.html\""
testexit[0]=2
test_out[0]="CRITICAL - APP_VERSION not found"

testname[1]="Test correct file in current format"
testtext[1]="${plugindir}${plugin} -H ${webhost} -u \"/${directory}/version1.html\""
testexit[1]=0
test_out[1]="OK - Found ++I# Version = 1.0.24.0"

testname[2]="Test incorrect string format"
testtext[2]="${plugindir}${plugin} -H ${webhost} -u \"/${directory}/version2.html\""
testexit[2]=2
test_out[2]="CRITICAL - Version not found or in incorrect format"

testname[3]="Test correct file with large numbers in current format"
testtext[3]="${plugindir}${plugin} -H ${webhost} -u \"/${directory}/version3.html\""
testexit[3]=0
test_out[3]="OK - Found ++I# Version = 4545.34505435.245304.53453490"

testname[4]="Test file missing id=APP_VERSION section"
testtext[4]="${plugindir}${plugin} -H ${webhost} -u \"/${directory}/version4.html\""
testexit[4]=2
test_out[4]="CRITICAL - APP_VERSION not found"

testname[5]="Test correct format with longer node"
testtext[5]="${plugindir}${plugin} -H ${webhost} -u \"/${directory}/version5.html\""
testexit[5]=0
test_out[5]="OK - Found ++I# Version = 1.0.24.0"

testname[6]="Test file with version in different node"
testtext[6]="${plugindir}${plugin} -H ${webhost} -u \"/${directory}/version6.html\""
testexit[6]=0
test_out[6]="OK - Found ++I# Version = 2.1.2554.2"

testname[7]="Test non existent host"
testtext[7]="${plugindir}${plugin} -H ${fakehost} -u \"/${directory}/version.html\""
testexit[7]=2
test_out[7]="CRITICAL - Address 'https://fakehost//any/version.html' could not be read"

testname[8]="Test non existent host over http"
testtext[8]="${plugindir}${plugin} -H ${fakehost} -u \"/${directory}/version.html\" -n"
testexit[8]=2
test_out[8]="CRITICAL - Address 'http://fakehost//any/version.html' could not be read"

testname[9]="Test missing parameters"
testtext[9]="${plugindir}${plugin} -H"
testexit[9]=2
test_out[9]=""

# END SETUP TESTS

# BEGIN MAIN
setuptestdir

for (( i=0; i<${#testname[@]}; i++ ))
do
    tester "${testname[$i]}" "${testtext[$i]}" "${testexit[$i]}" "${test_out[$i]}"
    SUMRESULT=$((SUMRESULT+$?))
done

cleanup

exit $SUMRESULT

# END MAIN