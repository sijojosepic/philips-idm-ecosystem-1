import unittest

from mock import MagicMock, patch


class CheckElasticsearchKibanaStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'requests': self.mock_request,
            'requests.auth': self.mock_request.auth,
            'requests.exceptions': self.mock_request.exceptions,
            'argparse': self.mock_argparse,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_elasticsearch_kibana_status
        self.check_elasticsearch_kibana_status = check_elasticsearch_kibana_status
        from check_elasticsearch_kibana_status import get_elastic_version
        from check_elasticsearch_kibana_status import get_kibana_status
        from check_elasticsearch_kibana_status import get_elastic_health
        from check_elasticsearch_kibana_status import get_json_resp
        self.get_elastic_version = get_elastic_version
        self.get_kibana_status = get_kibana_status
        self.get_elastic_health = get_elastic_health
        self.get_json_resp = get_json_resp

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_elasticsearch_ok_status(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'cluster_name': u'elasticsearch', u'version': {u'number': u'6.2.2'}};
        self.mock_request.get.return_value = resp_mock

        resp = self.get_elastic_version('127.0.0.1', 'https', 'elastic', 'changeme')
        self.mock_request.get.assert_called_once_with(
            'https://127.0.0.1:9200', headers={'accept': 'application/v1+json'}, auth=self.mock_request.auth.HTTPBasicAuth('elastic', 'changeme'), verify=False, timeout=10)
        resp_mock.raise_for_status.assert_called_once_with()
        self.assertEqual(resp, (0, "OK - Elasticsearch is running; version:6.2.2"))

    def test_elasticsearch_critical_status(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'cluster_name': u'elasticsearch', u'version': {u'number': u'6.2.2'}};
        self.mock_request.get.return_value = resp_mock
        resp_mock.raise_for_status.side_effect = Exception('Exception')

        resp = self.get_elastic_version('127.0.0.1', 'https', 'elastic', 'changeme')
        self.mock_request.get.assert_called_once_with(
            'https://127.0.0.1:9200', headers={'accept': 'application/v1+json'}, auth=self.mock_request.auth.HTTPBasicAuth('elastic', 'changeme'), verify=False, timeout=10)
        self.assertEqual(resp, (2, 'CRITICAL - Elasticsearch is not running'))

    def test_kibana_ok_status(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'cluster_name': u'kibana', u'version': {u'number': u'6.2.2'}};
        self.mock_request.get.return_value = resp_mock
        resp = self.get_kibana_status('127.0.0.1', 'https', 'elastic', 'changeme')
        self.mock_request.get.assert_called_once_with(
            'https://127.0.0.1:5601/api/status', headers={'accept': 'application/v1+json'}, auth=self.mock_request.auth.HTTPBasicAuth('elastic', 'changeme'), verify=False, timeout=10)
        resp_mock.raise_for_status.assert_called_once_with()
        self.assertEqual(resp, (0, "OK - Kibana is running; version:6.2.2"))

    def test_elasticsearch_health(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'cluster_name': u'elasticsearch',
            u'status': u'yellow', u'timed_out': u'false', u'number_of_nodes': 1,
            u'number_of_data_nodes': 1, u'active_primary_shards': 100, u'active_shards': 100,
            u'relocating_shards': 0, u'initializing_shards': 0, u'unassigned_shards': 0,
            u'delayed_unassigned_shards': 0, u'active_shards_percent_as_number': 52.27};
        self.mock_request.get.return_value = resp_mock

        resp = self.get_elastic_health('127.0.0.1', 'https', 'elastic', 'changeme')
        self.mock_request.get.assert_called_once_with(
            'https://127.0.0.1:9200/_cluster/health', headers={'accept': 'application/v1+json'}, auth=self.mock_request.auth.HTTPBasicAuth('elastic', 'changeme'), verify=False, timeout=10)
        resp_mock.raise_for_status.assert_called_once_with()
        self.assertEqual(resp, (1, "WARNING - status:yellow; timed_out:false; number_of_nodes:1; \
number_of_data_nodes:1 active_primary_shards:100 active_shards:100 relocating_shards:0 \
initializing_shards:0 unassigned_shards:0 active_shards_percent:52.27 | \
'active_primary'=100; 'active'=100; 'relocating'=0; 'init'=0; 'unass'=0"))

    def test_json_resp_https_status(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'cluster_name': u'kibana', u'version': {u'number': u'6.2.2'}};
        self.mock_request.get.return_value = resp_mock
        #resp_mock.raise_for_status.side_effect = Exception('Exception')

        resp = self.get_json_resp('https://127.0.0.1:5601', '/api/status', 'elastic', 'changeme')
        self.mock_request.get.assert_called_once_with(
            'https://127.0.0.1:5601/api/status', headers={'accept': 'application/v1+json'}, auth=self.mock_request.auth.HTTPBasicAuth('elastic', 'changeme'), verify=False, timeout=10)
        self.assertEqual(resp.json(), {u'cluster_name': u'kibana', u'version': {u'number': u'6.2.2'}})

    def test_json_resp_https_fail(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'cluster_name': u'kibana', u'version': {u'number': u'6.2.2'}};
        self.mock_request.get.return_value = resp_mock
        resp_mock.raise_for_status.side_effect = Exception('SSLError')

        resp = self.get_json_resp('https://127.0.0.1:5601', '/api/status', 'elastic', 'changeme')
        self.mock_request.get.assert_called_once_with(
            'https://127.0.0.1:5601/api/status', headers={'accept': 'application/v1+json'}, auth=self.mock_request.auth.HTTPBasicAuth('elastic', 'changeme'), verify=False, timeout=10)
        self.assertEqual(resp, None)

    def test_json_resp_http_status(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'cluster_name': u'kibana', u'version': {u'number': u'6.2.2'}};
        self.mock_request.get.return_value = resp_mock
        resp_mock.raise_for_status.side_effect = Exception('Exception')

        resp = self.get_json_resp('https://127.0.0.1:5601', '/api/status', 'elastic', 'changeme')
        self.mock_request.get.assert_called_once_with(
            'https://127.0.0.1:5601/api/status', headers={'accept': 'application/v1+json'}, auth=self.mock_request.auth.HTTPBasicAuth('elastic', 'changeme'), verify=False, timeout=10)
        self.assertEqual(resp, None)


if __name__ == '__main__':
    unittest.main()
