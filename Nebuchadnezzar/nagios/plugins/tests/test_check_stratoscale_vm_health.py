'''
created by Vijender.singh@philips.com on 8/5/18
'''
import unittest
import json
from argparse import Namespace
from mock import MagicMock, patch, Mock


class TestCheckStratoscaleVmHealth(unittest.TestCase):
    CRITICAL_STATUS = 2
    WARNING_STATUS = 1
    OK_STATUS = 0
    EXPECTED_CPU_USAGE = 30
    EXPECTED_RAM_USAGE = 83
    EXPECTED_STORAGE_USAGE = 94

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        modules = {
            'requests': self.mock_requests
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'winrm': self.mock_winrm,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_stratoscale_vm_health

        self.check_stratoscale_vm_health = check_stratoscale_vm_health
        self.domainname = 'cloud_admin'
        self.username = 'admin'
        self.password = 'admin'
        self.hostname = '130.147.86.215'
        self.vmid = 'a4ebd786-0109-4aa5-a975-140915da5dbd'
        self.warningthreshold = 80
        self.criticalthreshold = 90
        self.resourceType = 'CPU'
        self.floatingip = '130.147.86.161'
        self.ostype = 'LINUX'
        self.vmusername = 'root'
        self.vmpassword = 'ph!lt0r@st3nt0r'

        self.vm_info_response_stub = {
            "network__rx_kbps__of__vm__in__kbps": [["a4ebd786-0109-4aa5-a975-140915da5dbd", 2607.9749],
                                                   ["fe081659-4acb-4cf0-8925-47b6cf69cd65", 2547.3064]],
            "cpu__used__of__vm__in__percent": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 46.306805],
                                               ["a4ebd786-0109-4aa5-a975-140915da5dbd", 90.031555]],
            "network__tx_kbps__of__vm__in__kbps": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 2666.5305],
                                                   ["a4ebd786-0109-4aa5-a975-140915da5dbd", 2078.196]],
            "memory__used_by_guest_os__of__vm__in__percent": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 75.246735],
                                                              ["a4ebd786-0109-4aa5-a975-140915da5dbd", 44.29414]]}

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('argparse.ArgumentParser.parse_args')
    def test_check_arg(self, mock_argparse):
        mock_argparse.return_value = Namespace(hostname=self.hostname, domainname=self.domainname,
                                               username=self.username, password=self.password,
                                               vmid=self.vmid,
                                               warningthreshold=self.warningthreshold,
                                               criticalthreshold=self.criticalthreshold,
                                               resourceType=self.resourceType,
                                               floatingip=self.floatingip,
                                               ostype=self.ostype,
                                               vmusername=self.vmusername,
                                               vmpassword=self.vmpassword)
        results = self.check_stratoscale_vm_health.check_arg()
        assert results == (
            self.hostname, self.domainname, self.username, self.password, self.vmid, self.warningthreshold,
            self.criticalthreshold, self.resourceType, self.floatingip, self.ostype, self.vmusername,
            self.vmpassword)

    def test_get_token_payload(self):
        expected_body_val = json.dumps({'auth': {'identity': {'methods': ['password'], 'password': {
            'user': {'name': 'admin', 'password': 'admin', 'domain': {'name': 'cloud_admin'}}}},
                                                 'scope': {'domain': {'name': 'cloud_admin'}}}})
        expected_header = {'content-type': 'application/json'}
        result = self.check_stratoscale_vm_health.get_token_payload(self.domainname, self.username, self.password)
        assert expected_header == result.headers
        assert expected_body_val == result.body

    def test_get_token(self):
        mock_headers = {'x-subject-token': 'SomeBase64String'}
        self.mock_requests.post.return_value = Mock(ok=True, headers=mock_headers, status_code=200)
        token_payload = self.check_stratoscale_vm_health.get_token_payload(self.domainname, self.username,
                                                                           self.password)
        token = self.check_stratoscale_vm_health.get_token(self.hostname, token_payload)
        self.assertEqual(token, 'SomeBase64String')

    def test_get_product_id(self):
        stub_productid_json_response = [
            {
                "is_domain": "false",
                "name": "default",
                "enabled": "true",
                "domain_name": "cloud_admin",
                "id": "d134786daf55448d8c02b97601aa38e8",
                "parent_id": "null",
                "domain_id": "default",
                "description": "Default project"
            }
        ]
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = stub_productid_json_response
        product_id = self.check_stratoscale_vm_health.get_product_id(self.hostname, 'SomeBase64String', self.domainname)
        self.assertEqual(product_id, 'd134786daf55448d8c02b97601aa38e8')

    def test_get_product_token_payload(self):
        expected_body_val = json.dumps({
            'auth': {'identity': {'methods': ['token'], 'token': {'id': 'SomeBase64String'}},
                     'scope': {'project': {'id': 'd134786daf55448d8c02b97601aa38e8'}}}})
        expected_header = {'content-type': 'application/json'}
        result = self.check_stratoscale_vm_health.get_product_token_payload('SomeBase64String',
                                                                            'd134786daf55448d8c02b97601aa38e8')
        assert expected_header == result.headers
        assert expected_body_val == result.body

    def test_get_stratoscale_vms_info(self):
        stub_vm_info_response = {
            "network__rx_kbps__of__vm__in__kbps": [["a4ebd786-0109-4aa5-a975-140915da5dbd", 2607.9749],
                                                   ["fe081659-4acb-4cf0-8925-47b6cf69cd65", 2547.3064]],
            "cpu__used__of__vm__in__percent": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 46.306805],
                                               ["a4ebd786-0109-4aa5-a975-140915da5dbd", 90.031555]],
            "network__tx_kbps__of__vm__in__kbps": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 2666.5305],
                                                   ["a4ebd786-0109-4aa5-a975-140915da5dbd", 2078.196]],
            "memory__used_by_guest_os__of__vm__in__percent": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 75.246735],
                                                              ["a4ebd786-0109-4aa5-a975-140915da5dbd", 44.29414]]}

        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = stub_vm_info_response
        result = self.check_stratoscale_vm_health.get_stratoscale_vms_info(self.hostname, 'SomeBase64String')
        assert result == self.vm_info_response_stub

    def test_get_memory_status(self):
        status, message = self.check_stratoscale_vm_health.get_memory_status(self.vm_info_response_stub, self.vmid,
                                                                             self.warningthreshold,
                                                                             self.criticalthreshold)
        self.assertEqual(status, 0)

    def test_get_cpu_status(self):
        status, message = self.check_stratoscale_vm_health.get_cpu_status(self.vm_info_response_stub, self.vmid,
                                                                          self.warningthreshold,
                                                                          self.criticalthreshold)
        self.assertEqual(status, 2)

    def test_get_network_tx(self):
        status, message = self.check_stratoscale_vm_health.get_network_tx(self.vm_info_response_stub, self.vmid,
                                                                          self.warningthreshold,
                                                                          self.criticalthreshold)
        self.assertEqual(status, 0)

    def test_get_network_rx(self):
        status, message = self.check_stratoscale_vm_health.get_network_rx(self.vm_info_response_stub, self.vmid,
                                                                          self.warningthreshold,
                                                                          self.criticalthreshold)
        self.assertEqual(status, 0)

    def test_get_storage_status_without_ostag(self):
        status, message = self.check_stratoscale_vm_health.get_storage_status('None', self.floatingip,
                                                                              self.vmusername,
                                                                              self.vmpassword,
                                                                              self.warningthreshold,
                                                                              self.criticalthreshold)
        self.assertEqual(status, 3)

    def test_get_storage_status_without_fip(self):
        status, message = self.check_stratoscale_vm_health.get_storage_status(self.ostype, 'None', self.vmusername,
                                                                              self.vmpassword,
                                                                              self.warningthreshold,
                                                                              self.criticalthreshold)
        self.assertEqual(status, 3)

    def test_linux_storage_status(self):
        mock_obj = Mock()
        attrs = {'self.check_stratoscale_vm_health.get_linux_storage_usage.return_value': (
        16, 'Drives:DATA = 16% SYSTEM = 0%')}
        mock_obj.configure_mock(**attrs)
        percent, message = mock_obj.self.check_stratoscale_vm_health.get_linux_storage_usage(self.floatingip,
                                                                                             self.vmusername,
                                                                                             self.vmpassword)
        self.assertEqual(percent, 16)


if __name__ == '__main__':
    unittest.main()

