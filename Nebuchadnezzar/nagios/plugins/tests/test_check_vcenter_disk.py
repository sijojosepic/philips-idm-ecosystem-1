import unittest
from mock import MagicMock, patch


class TestDisk:
    def __init__(self, diskpath, capacity, freeSpace):
        self.diskPath = diskpath
        self.capacity = capacity
        self.freeSpace = freeSpace


class VcenterDiskTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_pyVim = MagicMock(name='pyVim')
        self.mock_pyVmomi = MagicMock(name='pyVmomi')
        self.mock_atexit = MagicMock(name='atexit')
        self.mock_sys = MagicMock(name='sys')
        modules = {
            'argparse': self.mock_argparse,
            'pyVim': self.mock_pyVim,
            'pyVmomi': self.mock_pyVmomi,
            'atexit': self.mock_atexit,
            'sys': self.mock_sys
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_vcenter_disk
        self.module = check_vcenter_disk

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_size_fmt_bytes(self):
        self.assertEqual(self.module.sizeof_fmt(1000), '1000.0bytes')

    def test_size_fmt_KB(self):
        self.assertEqual(self.module.sizeof_fmt(2000), '2.0KB')

    def test_size_fmt_MB(self):
        self.assertEqual(self.module.sizeof_fmt(1048576), '1.0MB')

    def test_size_fmt_GB(self):
        self.assertEqual(self.module.sizeof_fmt(1073741824), '1.0GB')

    def test_size_fmt_TB(self):
        self.assertEqual(self.module.sizeof_fmt(1024*1024*1024*1024), '1.0TB')

    def test_calculate_percentage(self):
        self.assertEqual(self.module.calculate_percentage(88888888, 8888888888), '99.00')

    def test_server_connect(self):
        connection = self.mock_pyVim.connect.SmartConnect.return_value
        disconnect = self.mock_pyVim.connect.Disconnect
        self.assertEqual(self.module.server_connect('10.220.3.1', 'tester', 'CryptThis'), connection)
        self.mock_pyVim.connect.SmartConnect.assert_called_once_with(host='10.220.3.1', user='tester', pwd='CryptThis')
        self.mock_atexit.register.assert_called_once_with(disconnect, connection)

    def test_disk_info(self):
        disks = iter([TestDisk('S:\\', 8586784768L, 1462870016L)])
        self.assertEqual(self.module.disk_info(disks, '80', '90'),
                         (['82.96'], 'Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n',
                          "'S:\\'=82.96%;80;90;\n"))

    def test_check_threshold(self):
        self.assertEqual(self.module.check_threshold(['50.66', '55.77', '60.11'], '61', '80', critical=False), False)
        self.assertEqual(self.module.check_threshold(['50.66', '55.77', '60.11'], '60', '80'), True)

    def test_get_status_warning(self):
        self.assertEqual(self.module.get_status(['50.66', '55.77', '62.11'],
                                                'Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB,'
                                                ' Percentage used= 82.96\n', 'S:\\=82.96%;80;90;', '61', '80'),
                         (1, 'WARNING: Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n '
                             '| S:\\=82.96%;80;90;'))

    def test_get_status_critical(self):
        self.assertEqual(self.module.get_status(['50.66', '55.77', '82.11'],
                                                'Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB,'
                                                ' Percentage used= 82.96\n', 'S:\\=82.96%;80;90;', '61', '80'),
                         (2, 'CRITICAL: Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n '
                             '| S:\\=82.96%;80;90;'))

    def test_get_status_ok(self):
        self.assertEqual(self.module.get_status(['50.66', '55.77', '62.11'],
                                                'Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB,'
                                                ' Percentage used= 82.96\n', 'S:\\=82.96%;80;90;', '63', '80'),
                         (0, 'OK: Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n '
                             '| S:\\=82.96%;80;90;'))


if __name__ == '__main__':
    unittest.main()
