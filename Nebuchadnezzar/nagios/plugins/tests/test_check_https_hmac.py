import unittest
from StringIO import StringIO

from mock import MagicMock, patch


class CheckHttpsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_utilities = MagicMock(name='utilities')
        modules = {
            'requests': self.mock_request,
            'argparse': self.mock_argparse,
            'scanline.utilities': self.mock_utilities,
            'scanline.utilities.token_mgr': self.mock_utilities.token_mgr,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_https_hmac
        self.module = check_https_hmac

    def test_get_url(self):
        url = self.module.get_url('host', '/service_url', 'protocol')
        self.assertEqual(url, 'protocol://host/service_url')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_hmac(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.get_url = mock_get_url
        mock_auth_resp = MagicMock(name='get_authenticated_response')
        mock_response = MagicMock(name='response_mock')
        mock_response.status_code = 200
        mock_response.content = 'OK'
        mock_auth_resp.return_value = [mock_response]
        self.mock_utilities.token_mgr.get_authenticated_response = mock_auth_resp
        self.module.main('host', 'service_url', 'protocol', True)
        self.sys_mock.exit.assert_called_once_with(0)
        mock_auth_resp.assert_called_once_with('url')
        mock_get_url.assert_called_once_with('host', 'service_url', 'protocol')
        self.assertEqual(std_out.getvalue(), 'OK - 200 :OK\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_non_hmac(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.get_url = mock_get_url
        mock_response = MagicMock(name='response_mock')
        mock_response.status_code = 200
        mock_response.content = 'OK'
        self.mock_request.get.return_value = mock_response
        self.module.main('host', 'service_url', 'protocol', '')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_request.get.assert_called_once_with('url', timeout=5, verify=False)
        mock_get_url.assert_called_once_with('host', 'service_url', 'protocol')
        self.assertEqual(std_out.getvalue(), 'OK - 200 :OK\n')


    @patch('sys.stdout', new_callable=StringIO)
    def test_main_no_respone(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.get_url = mock_get_url
        mock_response = MagicMock(name='response_mock')
        mock_response.status_code = 200
        mock_response.content = 'OK'
        self.mock_request.get.return_value = mock_response
        self.module.main('host', 'service_url', 'protocol', '')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_request.get.assert_called_once_with('url', timeout=5, verify=False)
        mock_get_url.assert_called_once_with('host', 'service_url', 'protocol')
        self.assertEqual(std_out.getvalue(), 'OK - 200 :OK\n')


    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.side_effect = Exception('exe')
        self.module.get_url = mock_get_url
        mock_response = MagicMock(name='response_mock')
        mock_response.status_code = 200
        mock_response.content = 'OK'
        self.mock_request.get.return_value = mock_response
        self.module.main('host', 'service_url', 'protocol', '')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Exception - exe\n')

if __name__ == '__main__':
    unittest.main()
