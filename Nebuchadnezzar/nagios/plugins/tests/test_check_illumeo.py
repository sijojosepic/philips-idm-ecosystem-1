import unittest
from mock import MagicMock, patch, Mock


class CheckIllumeoStatusTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_redis = MagicMock(name='redis')
        modules = {
            'requests': self.mock_request,
            'redis': self.mock_redis
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_illumeo
        self.check_illumeo = check_illumeo
        from check_illumeo import get_illumeo_status_from_restapi, get_illumeo_status
        self.get_illumeo_status_from_restapi = get_illumeo_status_from_restapi
        self.get_illumeo_status = get_illumeo_status


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_illumeo_status_from_restapi_OK(self,):
        self.check_illumeo.get_siteid=Mock(return_value="mocked")
        mock_get = MagicMock(name='get_request')
        json_val = {'result': [{'module_type': 'I4Processing'}]}
        mock_get.json.return_value = json_val
        self.mock_request.get.return_value = mock_get
        noncore_nodes = ['Rviewer','Eviewer','I4Processing']
        status_message = 'Either one {0} is available'.format(','.join(noncore_nodes))
        self.assertEqual(self.get_illumeo_status_from_restapi('http'), (0, status_message))

    def test_get_illumeo_status__from_restapi_UNKNOWN(self,):
        self.check_illumeo.get_siteid=Mock(return_value="mocked")
        mock_get = MagicMock(name='get_request')
        json_val = {'result': [{'module_type': 128}]}
        mock_get.json.return_value = json_val
        self.mock_request.get.return_value = mock_get
        noncore_nodes = ['Rviewer','Eviewer','I4Processing']
        status_message = ','.join(noncore_nodes) + " not available"
        self.assertEqual(self.get_illumeo_status_from_restapi('http'), (3, status_message))

    def test_get_illumeo_status_OK(self,):
        mock_redis = MagicMock(return_value=None)
        mock_redis.keys.return_value = ['#ilu03pc1.ilu03.isyntax.net#ISPACS#I4Processing']
        self.mock_redis.StrictRedis.return_value = mock_redis
        noncore_nodes = ['Rviewer','Eviewer','I4Processing']
        status_message = 'Either one {0} is available'.format(','.join(noncore_nodes))
        self.assertEqual(self.get_illumeo_status(), (0, status_message))


if __name__ == '__main__':
    unittest.main()
