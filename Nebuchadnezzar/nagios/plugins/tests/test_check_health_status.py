from StringIO import StringIO

import sys
import unittest
import requests

from mock import MagicMock, patch
from scanline.utilities import token_mgr


class HealthStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'requests': self.mock_request,
            'argparse': self.mock_argparse,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_health_status
        self.module = check_health_status

    def test_get_url(self):
        url = self.module.get_url('host', '/service_url', 'protocol')
        self.assertEqual(url, 'protocol://host/service_url')


    def test_process_output_ok_status(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status": "OK",
                                           "data": {
                                               "message": "Cumulative Calls : 0 , Success Calls : 0, Failed Calls : 0"
                                           }
                                           }
        expected_response = (
            0, 'OK - Cumulative Calls : 0 , Success Calls : 0, Failed Calls : 0| service status=0;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_process_output_critical_status(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status": "CRITICAL",
                                           "data": {
                                               "message": "Error"
                                           }
                                           }
        expected_response = (2, 'CRITICAL - Error| service status=2;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))


    def test_process_output_proper_response(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status": "CRITICAL",
                                           "data": {
                                               "message": "Error"
                                           }
                                           }
        expected_response = (2, 'CRITICAL - Error| service status=2;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_process_output_invalid_status(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status": "INVALID",
                                           "data": {
                                               "message": "Error"
                                           }
                                           }
        expected_response = (
            2, 'CRITICAL : Invalid status `INVALID` received, status should be any of `OK,WARNING,CRITICAL,UNKNOWN`| service status=2;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_process_output_verify_json_false(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status1": "INVALID",
                                           "data": {
                                               "message": "Error"
                                           }
                                           }
        mock_verify_response = MagicMock(name='verify_response')
        mock_verify_response.return_value = False
        self.module.mock_verify_response = mock_verify_response
        expected_response = (2, "CRITICAL : Improper JSON response, {'data': {'message': 'Error'}, 'status1': 'INVALID'}| service status=2;1;2;0;2")
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_health_status(self):
        mock_resp = MagicMock(name='repsonse')
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 'h_s'
        self.module.process_output = mock_process_output
        self.mock_request.get.return_value = mock_resp
        self.assertEqual(self.module.health_status('url', False, False), 'h_s')
        mock_process_output.assert_called_once_with(mock_resp, 'url')

    def test_health_status_verify_arg(self):
        mock_resp = MagicMock(name='repsonse')
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 'h_s'
        self.module.process_output = mock_process_output
        self.mock_request.get.return_value = mock_resp
        self.assertEqual(self.module.health_status('url', False, False), 'h_s')
        mock_process_output.assert_called_once_with(mock_resp, 'url')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_health_status = MagicMock(name='health_status')
        mock_health_status.return_value = 1, 'Msg'
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.health_status = mock_health_status
        self.module.get_url = mock_get_url
        self.module.main('host', 'url', 'protocol', False)
        mock_health_status.assert_called_once_with('url', False)
        mock_get_url.assert_called_once_with('host', 'url', 'protocol')
        self.sys_mock.exit.assert_called_once_with(1)
        self.assertEqual(std_out.getvalue(), 'Msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_request_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_health_status = MagicMock(name='health_status')
        mock_health_status.side_effect = requests.exceptions.RequestException(
            'Request Error')
        self.module.health_status = mock_health_status
        self.module.main('host', 'url', 'protocol', False)
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         'CRITICAL : Request Error - Request Error| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_health_status = MagicMock(name='health_status')
        mock_health_status.side_effect = Exception('Exception')
        self.module.health_status = mock_health_status
        self.module.main('host', 'url', 'protocol', False)
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Error - Exception| service status=2;1;2;0;2\n')

    def test_search_response_status(self):
        json_data = {"status": "CRITICAL", "data": {"message": "Error"}}
        self.assertEqual(self.module.search_response(json_data, 'status'), 'CRITICAL')
    
    def test_search_response_message(self):
        json_data = {"status": "CRITICAL", "data": {"message": "Error"}}
        self.assertEqual(self.module.search_response(json_data, 'message'), 'Error')

    def test_search_response_wrong(self):
        json_data = {"status": "CRITICAL", "data": {"message": "Error"}}
        self.assertEqual(self.module.search_response(json_data, 'stat'), None)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()

