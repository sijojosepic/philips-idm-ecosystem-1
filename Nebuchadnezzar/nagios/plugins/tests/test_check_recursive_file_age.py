from StringIO import StringIO

import unittest
from mock import MagicMock, patch

import requests
import winrm


class RecursiveFileAgeTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_winrm = MagicMock(name='winrm')
        modules = {
            'requests': self.mock_request,
            'requests.exceptions': self.mock_request.exceptions,
            'winrm': self.mock_winrm,
            'winrm.exceptions': self.mock_winrm.exceptions,
            'argparse': self.mock_argparse,
            'scanline.utilities.win_rm': self.mock_winrm.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_recursive_file_age
        self.module = check_recursive_file_age

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_path(self):
        self.assertEqual(self.module.get_path('d', 'p'), 'd:\\p\\')

    def test_get_path_endswith(self):
        self.assertEqual(self.module.get_path('d', 'p\\'), 'd:\\p\\')

    def test_ps_script(self):
        self.assertEqual(self.module.ps_script(
            'p', 30), 'Get-childitem -path p  -Recurse| Where {!$_.PsIsContainer} | Where-object {($_.LastWriteTime -lt (Get-Date).AddMinutes(-30))}| % {\n $dir=$_.directory\n $fullname=$_.FullName\n Write-Output $dir`t$fullname`t`t`t\n }')

    def test_format_error_pathnotfound(self):
        std_err = '\r\n\r\n    PathNotFound'
        exp_result = 'WARNING: p PathNotFound'
        self.assertEqual(self.module.format_err(std_err, 'p'), exp_result)

    def test_format_error(self):
        self.assertEqual(self.module.format_err('abcd', 'p'), 'abcd')

    def test_compose_status_msg_ok(self):
        directory_name = ''
        exp_result = (0, 'OK: Found 0 file, aged more than 30 minutes \n')
        self.assertEqual(self.module.compose_status_msg(
            directory_name, 0, 30), exp_result)

    def test_compose_status_msg_filecount_one(self):
        directory_name = 'a1'
        exp_result = (2, 'CRITICAL: Found 1 file, aged more than 30 minutes \na\n1')
        self.assertEqual(self.module.compose_status_msg(
            directory_name, 1, 30), exp_result)

    def test_compose_status_msg_filecount_more(self):
        directory_name = 'a1', 'a2'
        exp_result = (2, 'CRITICAL: Found 2 files, aged more than 30 minutes \na1\na2')
        self.assertEqual(self.module.compose_status_msg(
            directory_name, 2, 30), exp_result)

    def test_process_output_ok(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = 'directory_name\tfilenames\t\t\t'
        mock_compose_status_msg = MagicMock(name='compose_status_msg')
        mock_compose_status_msg.return_value = 'status', 'msg'
        mock_format_err = MagicMock(name='format_err')
        mock_format_err.return_value = 'err'
        self.module.compose_status_msg = mock_compose_status_msg
        self.module.format_err = mock_format_err
        result = self.module.process_output(mock_out_put, 'p', 30)
        self.assertEqual(result, ('status', 'msg'))
        mock_compose_status_msg.assert_called_once_with(set(['directory_name']), 1, 30)
        self.assertEqual(mock_format_err.call_count, 0)

    def test_process_output_not_ok(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 1
        mock_out_put.std_err = 'std_err'
        mock_compose_status_msg = MagicMock(name='compose_status_msg')
        mock_format_err = MagicMock(name='format_err')
        mock_format_err.return_value = 'err'
        self.module.compose_status_msg = mock_compose_status_msg
        self.module.format_err = mock_format_err
        result = self.module.process_output(mock_out_put, 'p', 30)
        self.assertEqual(result, (1, 'err'))
        mock_format_err.assert_called_once_with('std_err', 'p')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_request_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = requests.exceptions.RequestException()
        self.module.main('host', 'user', 'passwd',
                         'threshold', 'drive', 'path')
        self.sys_mock.exit.assert_called_once_with(2)

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = winrm.exceptions.AuthenticationError()
        self.module.main('host', 'user', 'passwd',
                         'threshold', 'drive', 'path')
        self.sys_mock.exit.assert_called_once_with(2)

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = Exception('e')
        self.module.main('host', 'user', 'passwd',
                         'threshold', 'drive', 'path')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         'CRITICAL : Exception e\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = 'out_put'
        self.mock_winrm.utilities.WinRM.return_value = mock_WinRM
        mock_ps_script = MagicMock(name='ps_script')
        mock_ps_script.return_value = 'script'
        self.module.ps_script = mock_ps_script
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 0, 'msg'
        self.module.process_output = mock_process_output
        mock_get_path = MagicMock(name='get_path')
        mock_get_path.return_value = 'path'
        self.module.get_path = mock_get_path
        self.module.main('host', 'user', 'passwd',
                         'threshold', 'drive', 'path')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_winrm.utilities.WinRM.assert_called_once_with(
            'host', 'user', 'passwd')
        mock_WinRM.execute_ps_script.assert_called_once_with('script')
        mock_get_path.assert_called_once_with('drive', 'path')
        mock_ps_script.assert_called_once_with('path', 'threshold')
        mock_process_output.assert_called_once_with(
            'out_put', 'path', 'threshold')
        self.assertEqual(std_out.getvalue(),
                         'msg\n')


if __name__ == '__main__':
    unittest.main()


