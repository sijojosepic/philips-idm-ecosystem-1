>>> import hb_nagios
>>> import minimock

>>> import tempfile

>>> minimock.mock('hb_nagios.sys.exit')

>>> temp_file2 = tempfile.NamedTemporaryFile()
>>> temp_file2.write("""\
... <?xml version="1.0" encoding="UTF-8"?>
... <heartbeat>
...     <service>
...         <description>Switch available ports</description>
...         <errorcode>9116010101</errorcode>
...         <errortype>CE_network</errortype>
...     </service>
...     <service>
...         <description>Switch CPU usage</description>
...         <errorcode>9116010102</errorcode>
...         <errortype>CE_network</errortype>
...     </service>
... </heartbeat>
... """)
>>> temp_file2.seek(0)

>>> doc = hb_nagios.libxml2.parseFile(temp_file2.name)

>>> temp_file2.close()

>>> hb_nagios.find_errcode('Switch available ports', doc)
('9116010101', 'CE_network')


>>> hb_nagios.find_errcode('Not described failure', doc)
Traceback (most recent call last):
IndexError: list index out of range



>>> minimock.mock('hb_nagios.smtplib.SMTP', returns=minimock.Mock('smtp_connection'))
>>> msg = hb_nagios.create_msg('site04', 'somehost01', '10.4.4.1', hb_nagios.custom_timestamp('1375314581'), 'Super Error', '4366657', 'som tin wong')
>>> hb_nagios.send_message(msg, 'test@fakehost.example.com', 'someone@fakehost.example.com', 'superfake.example.com')
Called hb_nagios.smtplib.SMTP('superfake.example.com')
Called smtp_connection.sendmail(
    'test@fakehost.example.com',
    'someone@fakehost.example.com',
    'Content-Type: text/plain; charset="us-ascii"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\nFrom: test@fakehost.example.com\nTo: someone@fakehost.example.com\n\n?<?xml version="1.0" encoding="utf-8"?>\n<MESSAGE_ROOT>\n  <VERSION>1.0</VERSION>\n  <CATEGORY>LM_NT_EVENT</CATEGORY>\n  <COMPONENT>RemoteEventLogMonitor</COMPONENT>\n  <SITE_NAME>site04</SITE_NAME>\n  <LOCAL_HOST>somehost01</LOCAL_HOST>\n  <CLUSTER>NO</CLUSTER>\n  <FILTER_CONFIG_VERSION>1.0.1.0</FILTER_CONFIG_VERSION>\n  <LOCATION>Main Location</LOCATION>\n  <BODY>\n    <REPORT>\n      <SITE_NAME>site04</SITE_NAME>\n      <LOCAL_HOST>somehost01</LOCAL_HOST>\n      <CLUSTER>NO</CLUSTER>\n      <LOGMESSAGE>\n        <ORIGINATING_HOST>somehost01</ORIGINATING_HOST>\n        <ORIGINATING_HOST_IPS> 10.4.4.1 </ORIGINATING_HOST_IPS>\n        <TIMESTAMP>07/31/2013 16:49:41.000PM</TIMESTAMP>\n        <PRIORITY>LM_NT_EVENT</PRIORITY>\n        <PRIORITY_CODE>12345</PRIORITY_CODE>\n        <THREAD_ID>1</THREAD_ID>\n        <MESSAGE_BODY>Application log:\nSuper Error [somehost01] generated error Event 4366657: som tin wong \n\n</MESSAGE_BODY>\n      </LOGMESSAGE>\n      <SYSTEM_INFO>Module Info: somehost01 10.4.4.1</SYSTEM_INFO>\n    </REPORT>\n  </BODY>\n</MESSAGE_ROOT>')
Called smtp_connection.quit()


