import unittest
from mock import MagicMock, patch


class CheckSHDBClientStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_winrm = MagicMock(name='winrm')
        modules = {
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
            'winrm': self.mock_winrm,
            'winrm.exceptions': self.mock_winrm.exceptions,            
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_shdb_client_status
        self.check_shdb_client_status = check_shdb_client_status
        from check_shdb_client_status import get_process_status
        self.get_process_status = get_process_status


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_process_ok_status(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_cmd.return_value.std_out = '\r\nfilebeat.exe'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.get_process_status('192.168.59.45', 'user', 'pass', 'filebeat.exe',''),\
                         (0, 'OK - filebeat.exe is running')
                        )

    def test_process_critical_status(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_cmd.return_value.std_out = '\r\nfileseat.exe'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.get_process_status('192.168.59.45', 'user', 'pass', 'filebeat.exe',''),\
                         (2, 'CRITICAL - filebeat.exe is not running')
                        )

    def test_process_status_exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        self.assertEqual(self.get_process_status('192.168.59.45', 'user', 'pass', 'filebeat.exe',''),\
                         (2, 'CRITICAL : Exception e')
                        )


if __name__ == '__main__':
    unittest.main()
