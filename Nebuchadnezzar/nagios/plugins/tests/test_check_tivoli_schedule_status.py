from StringIO import StringIO
import unittest
from mock import MagicMock, patch, call
import requests
import winrm


class TivoliScheduleStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_winrm = MagicMock(name='winrm')
        modules = {
            'requests': self.mock_request,
            'requests.exceptions': self.mock_request.exceptions,
            'winrm': self.mock_winrm,
            'winrm.exceptions': self.mock_winrm.exceptions,
            'argparse': self.mock_argparse,
            'scanline.utilities.win_rm': self.mock_winrm.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_tivoli_schedule_status
        self.module = check_tivoli_schedule_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_ps_script(self):
        self.assertEqual(self.module.ps_script('Administrator', 'XYZ'), "cd 'C:\\Program Files\\Tivoli\\TSM\\baclient\\' | dsmadmc.exe -ID=Administrator -PASSWORD=XYZ -DISPLAY=LIST 'QUERY EVENT * * BEGINDATE=TODAY-32 ENDDATE=TODAY FORMAT=DETAILED'")

    def test_query_sch(self):
        self.assertEqual(self.module.query_sch('Administrator', 'XYZ'), "cd 'C:\\Program Files\\Tivoli\\TSM\\baclient\\' | dsmadmc.exe -ID=Administrator -PASSWORD=XYZ -DISPLAY=LIST 'query schedule format=detailed'")

    def test_format_query_sch(self):
        schedule_list = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n   Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2\r\n'''
        self.assertEqual(self.module.format_query_sch(schedule_list), ['INCR_PHIL0_PROD_ISPORTAL3AP2'])

    def test_process_output_exception(self):
        mock_out_put = MagicMock(name='output')
        mock_out_put.std_out = 'unable to establish session with server.'
        schedule_name = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        result = self.module.process_output(mock_out_put, schedule_name)
        self.assertEqual(result, (2, 'CRITICAL: Unable to establish session with Tivoli Server'))

    def test_process_output_ok(self):
        mock_out_put = MagicMock(name='output')
        mock_format_output = MagicMock(name='format_output')
        mock_format_output.return_value = 'status', 'msg'
        self.module.format_output = mock_format_output
        schedule_name = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        result = self.module.process_output(mock_out_put, schedule_name)
        self.assertEqual(result, ('status', 'msg'))

    def test_format_output_ok(self):
        in_put = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n     Schedule Name: INCR_PHIL0_PROD_DWPAP1\r\n         Node Name: ISEECU1-0\r\n   Scheduled Start: 07/11/2018 01:30:00\r\n      Actual Start: 07/11/2018 01:38:54\r\n         Completed: 07/11/2018 01:39:01\r\n            Status: Completed\r\n            Result: 0\r\n            Reason: All operations completed successfully.\r\n\r\n'''
        in_put1 = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        self.assertEqual(self.module.format_output(in_put, in_put1), (0, 'OK : All backup scheduled jobs completed successfully'))

    def test_format_output_missed(self):
        in_put = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n     Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2\r\n         Node Name: ISEECU1-0\r\n   Scheduled Start: 07/15/2018 01:30:00\r\n      Actual Start: \r\n         Completed: 07/15/2018 02:30:00\r\n            Status: Missed\r\n            Result: \r\n            Reason: \r\n\r\n'''
        in_put1 = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        exp_result = "CRITICAL: Failed Jobs 1\n('Policy Domain Name: STANDARD', 'Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2', 'Node Name: ISEECU1-0', 'Scheduled Start: 07/15/2018 01:30:00', 'Actual Start:', 'Completed: 07/15/2018 02:30:00', 'Status: Missed', 'Result:', 'Reason:')"
        self.assertEqual(self.module.format_output(in_put, in_put1), (2, exp_result))

    def test_format_output_warning(self):
        in_put = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n     Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2\r\n         Node Name: ISEECU1-0\r\n   Scheduled Start: 07/15/2018 01:30:00\r\n      Actual Start: \r\n         Completed: \r\n            Status: In Progress \r\n            Result: \r\n            Reason: \r\n\r\n'''
        in_put1 = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        exp_result = "WARNING: Warning Jobs 1\n('Policy Domain Name: STANDARD', 'Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2', 'Node Name: ISEECU1-0', 'Scheduled Start: 07/15/2018 01:30:00', 'Actual Start:', 'Completed:', 'Status: In Progress', 'Result:', 'Reason:')"
        self.assertEqual(self.module.format_output(in_put, in_put1), (1, exp_result))

    def test_format_output_uncertain(self):
        in_put = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n     Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2\r\n         Node Name: ISEECU1-0\r\n   Scheduled Start: 07/15/2018 01:30:00\r\n      Actual Start: \r\n         Completed: \r\n            Status: Uncertain \r\n            Result: \r\n            Reason: \r\n\r\n'''
        in_put1 = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        exp_result = "CRITICAL: Failed Jobs 1\n('Policy Domain Name: STANDARD', 'Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2', 'Node Name: ISEECU1-0', 'Scheduled Start: 07/15/2018 01:30:00', 'Actual Start:', 'Completed:', 'Status: Uncertain', 'Result:', 'Reason:')"
        self.assertEqual(self.module.format_output(in_put, in_put1), (2, exp_result))

    def test_format_output_severed(self):
        in_put = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n     Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2\r\n         Node Name: ISEECU1-0\r\n   Scheduled Start: 07/15/2018 01:30:00\r\n      Actual Start: \r\n         Completed: \r\n            Status: Severed \r\n            Result: \r\n            Reason: \r\n\r\n'''
        in_put1 = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        exp_result = "CRITICAL: Failed Jobs 1\n('Policy Domain Name: STANDARD', 'Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2', 'Node Name: ISEECU1-0', 'Scheduled Start: 07/15/2018 01:30:00', 'Actual Start:', 'Completed:', 'Status: Severed', 'Result:', 'Reason:')"
        self.assertEqual(self.module.format_output(in_put, in_put1), (2, exp_result))

    def test_format_output_failed(self):
        in_put = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n     Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2\r\n         Node Name: ISEECU1-0\r\n   Scheduled Start: 07/15/2018 01:30:00\r\n      Actual Start: 07/15/2018 01:35:00\r\n         Completed: 07/15/2018 01:35:00\r\n            Status: Failed \r\n            Result: \r\n            Reason: \r\n\r\n'''
        in_put1 = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        exp_result = "CRITICAL: Failed Jobs 1\n('Policy Domain Name: STANDARD', 'Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2', 'Node Name: ISEECU1-0', 'Scheduled Start: 07/15/2018 01:30:00', 'Actual Start: 07/15/2018 01:35:00', 'Completed: 07/15/2018 01:35:00', 'Status: Failed', 'Result:', 'Reason:')"
        self.assertEqual(self.module.format_output(in_put, in_put1), (2, exp_result))

    def test_format_output_pending(self):
        in_put = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n     Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2\r\n         Node Name: ISEECU1-0\r\n   Scheduled Start: 07/15/2018 01:30:00\r\n      Actual Start: \r\n         Completed: \r\n            Status: Pending \r\n            Result: \r\n            Reason: \r\n\r\n'''
        in_put1 = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        exp_result = "CRITICAL: Failed Jobs 1\n('Policy Domain Name: STANDARD', 'Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2', 'Node Name: ISEECU1-0', 'Scheduled Start: 07/15/2018 01:30:00', 'Actual Start:', 'Completed:', 'Status: Pending', 'Result:', 'Reason:')"
        self.assertEqual(self.module.format_output(in_put, in_put1), (2, exp_result))

    def test_format_output_restarted(self):
        in_put = '''\r\n\r\nPolicy Domain Name: STANDARD\r\n     Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2\r\n         Node Name: ISEECU1-0\r\n   Scheduled Start: 07/15/2018 01:30:00\r\n      Actual Start: \r\n         Completed: \r\n            Status: Restarted \r\n            Result: \r\n            Reason: \r\n\r\n'''
        in_put1 = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        exp_result = "CRITICAL: Failed Jobs 1\n('Policy Domain Name: STANDARD', 'Schedule Name: INCR_PHIL0_PROD_ISPORTAL3AP2', 'Node Name: ISEECU1-0', 'Scheduled Start: 07/15/2018 01:30:00', 'Actual Start:', 'Completed:', 'Status: Restarted', 'Result:', 'Reason:')"
        self.assertEqual(self.module.format_output(in_put, in_put1), (2, exp_result))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_request_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = requests.exceptions.RequestException()
        self.module.main('host', 'username', 'password', 'tivoliuser', 'tivolipassword')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception \n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = TypeError('takes exactly 2')
        self.module.main('host', 'username', 'password', 'tivoliuser', 'tivolipassword')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Issue in connecting to Tivoli client')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = TypeError('e')
        self.module.main('host', 'username', 'password', 'tivoliuser', 'tivolipassword')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Typeerror(May be Issue in connecting to Tivoli client)')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = winrm.exceptions.AuthenticationError()
        self.module.main('host', 'username', 'password', 'tivoliuser', 'tivolipassword')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception \n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = Exception('e')
        self.module.main('host', 'username', 'password', 'tivoliuser', 'tivolipassword')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception e\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_ps_output = MagicMock(name='psscript_output')
        mock_ps_output.std_err = "the term 'dsmadmc.exe' is not recognized"
        mock_WinRM.execute_ps_script.return_value = mock_ps_output
        self.mock_winrm.utilities.WinRM.return_value = mock_WinRM
        mock_query_sch = MagicMock(name='query_sch')
        mock_query_sch.return_value = 'script'
        self.module.query_sch = mock_query_sch
        self.module.main('host', 'username', 'password', 'tivoliuser', 'tivolipassword')
        mock_query_sch.assert_called_once_with('tivoliuser', 'tivolipassword')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),  'CRITICAL : dsmadmc.exe is not available or the path(C:\\Program Files'
                                              '\\Tivoli\\TSM\\Baclient)is not set in Environment variables of Utility '
                                              'server\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_ps_output = MagicMock(name='psscript_output')
        mock_ps_output.std_out = ''
        mock_WinRM.execute_ps_script.return_value = mock_ps_output
        self.mock_winrm.utilities.WinRM.return_value = mock_WinRM
        mock_query_sch = MagicMock(name='query_sch')
        mock_query_sch.return_value = 'script'
        self.module.query_sch = mock_query_sch
        self.module.main('host', 'username', 'password', 'tivoliuser', 'tivolipassword')
        mock_query_sch.assert_called_once_with('tivoliuser', 'tivolipassword')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),  'CRITICAL : Empty response from Tivoli client\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_ps_output = MagicMock(name='psscript_output')
        mock_ps_output.std_out = 'out_put'
        mock_WinRM.execute_ps_script.return_value = mock_ps_output
        self.mock_winrm.utilities.WinRM.return_value = mock_WinRM
        mock_query_sch = MagicMock(name='query_sch')
        mock_query_sch.return_value = 'script'
        self.module.query_sch = mock_query_sch
        mock_format_query_sch = MagicMock(name='format_query_sch')
        mock_format_query_sch.return_value = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        self.module.format_query_sch = mock_format_query_sch
        mock_ps_script = MagicMock(name='ps_script')
        mock_ps_script.return_value = 'script'
        self.module.ps_script = mock_ps_script
        schedule_name = ['INCR_PHIL0_PROD_ISPORTAL3AP2', 'INCR_PHIL0_PROD_ISPORTAL3AP1']
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 0, 'msg'
        self.module.process_output = mock_process_output
        self.module.main('host', 'username', 'password', 'tivoliuser', 'tivolipassword')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_winrm.utilities.WinRM.assert_called_once_with('host', 'username', 'password')
        expected_call = [call('script'), call('script')]
        self.assertEqual(mock_WinRM.execute_ps_script.call_args_list, expected_call)
        mock_query_sch.assert_called_once_with('tivoliuser', 'tivolipassword')
        mock_format_query_sch.assert_called_once_with('out_put')
        mock_ps_script.assert_called_once_with('tivoliuser', 'tivolipassword')
        mock_process_output.assert_called_once_with(mock_ps_output, schedule_name)
        self.assertEqual(std_out.getvalue(), 'msg\n')


if __name__ == '__main__':
    unittest.main()
