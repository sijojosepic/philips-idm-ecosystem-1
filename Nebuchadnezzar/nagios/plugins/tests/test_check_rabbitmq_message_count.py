import unittest
import requests
from mock import MagicMock, patch, Mock


class CheckRabbitmqDataTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rabbitmq_message_count
        self.check_rabbitmq_message_count = check_rabbitmq_message_count
        from check_rabbitmq_message_count import get_queue_message_count
        self.get_queue_message_count = get_queue_message_count

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_message_count_OK(self,):
        mock_get = MagicMock(name='get_request')
        json_val = {'messages': 15}
        mock_get.json.return_value = json_val
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.get_queue_message_count('localhost','1567','guest','guest','I4PreProcessing',30,60), 
            (0,'Queue message count 15|message count=15;30;60;1;100'))

    def test_get_message_count_WARNING(self,):
        mock_get = MagicMock(name='get_request')
        json_val = {'messages': 31}
        mock_get.json.return_value = json_val
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.get_queue_message_count('localhost','1567','guest','guest','I4PreProcessing',30,60), 
            (1,'Queue message count 31|message count=31;30;60;1;100'))

    def test_get_message_count_CRITICAL(self,):
        mock_get = MagicMock(name='get_request')
        json_val = {'messages': 61}
        mock_get.json.return_value = json_val
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.get_queue_message_count('localhost','1567','guest','guest','I4PreProcessing',30,60), 
            (2,'Queue message count 61|message count=61;30;60;1;100'))

if __name__ == '__main__':
    unittest.main()