import sys
import unittest
import mock

from mock import MagicMock, patch, Mock

class CheckSqlFailoverTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_always_on_failover_status
        self.module = check_sql_always_on_failover_status

    
    def test_get_dbnodes(self):
    	self.module.get_dbnodes('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb')
        self.mock_pymssql.connect.assert_called_once_with(
            server='zvm01_cluster.zvm01.isyntax.net', user='phiadmin', password='philtor', database='msdb', login_timeout = 20)

    def test_get_dbnodes_exception(self):
    	self.mock_pymssql.connect.side_effect = Exception('e')
    	self.assertEqual(self.module.get_dbnodes('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb'), 
    		False)

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=False)
    def test_get_failover_status(self, mock_dbnodes):
    	self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2)
        self.mock_pymssql.connect.assert_called_once_with(
            server='zvm01_cluster.zvm01.isyntax.net', user='phiadmin', password='philtor', database='msdb', login_timeout = 10)

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_with_nodes(self, mock_dbnodes):
    	self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2)
    	self.assertEqual(self.mock_pymssql.connect.call_count,3)

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_exception(self, mock_dbnodes):
    	self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2),
        	(2, "CRITICAL - Connection error with ['db1', 'db2', 'zvm01_cluster.zvm01.isyntax.net']. "))
    
    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_warning(self, mock_dbnodes):
    	self.module.FAILOVER_COUNT = ['log1']
    	self.module.validate_query = Mock(return_value=[])
    	self.assertEqual(self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2),
        	(1, 'WARNING - 1 DB failover occurred in last 6 hours.'))

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_critical(self, mock_dbnodes):
    	self.module.FAILOVER_COUNT = ['log1', 'log2']
    	self.module.validate_query = Mock(return_value=[])
    	self.assertEqual(self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2),
        	(2, 'CRITICAL - Database failover occurred 2 times in last 6 hours.'))

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_ok(self, mock_dbnodes):
    	self.module.FAILOVER_COUNT = []
    	self.module.validate_query = Mock(return_value=[])
    	self.assertEqual(self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2),
        	(0, 'OK - There is no database failover occured in last 6 hours.'))
    	
    @mock.patch('check_sql_always_on_failover_status.pymssql.connect.cursor')
    def test_validate_query(self, mock_cursor):
        self.module.FAILOVER_COUNT = []
        self.module.validate_query(mock_cursor, 6)
        self.assertEqual(mock_cursor.execute.call_count, 4)
 
    @mock.patch('check_sql_always_on_failover_status.pymssql.connect.cursor')
    def test_validate_query_FAILCOUNT(self, mock_cursor):
        self.module.FAILOVER_COUNT = []
        mock_cursor.fetchall.return_value = ["(datetime.datetime(2018, 10, 20, 2, 49, 11, 90000), u'spid22s'"]
        self.module.validate_query(mock_cursor, 6)
        self.assertEqual(len(self.module.FAILOVER_COUNT), 4)

    @mock.patch('check_sql_always_on_failover_status.pymssql.connect.cursor')
    def test_validate_query_exception(self, mock_cursor):
    	self.module.FAILOVER_COUNT = []
        mock_cursor.execute.side_effect = Exception('e')
        self.module.validate_query(mock_cursor, 6)
        self.assertEqual(len(self.module.FAILOVER_COUNT), 0)

        
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop() 

if __name__ == '__main__':
    unittest.main()