import unittest
import requests
from mock import MagicMock, patch


class CheckRhapsodyCommpointTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request,
        }
        self.mock_scanline = MagicMock(name='scanline')
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.http_patch = patch('scanline.utilities.http.HTTPRequester')
        self.http_patcher = self.http_patch.start()
        import check_rhapsody_commpoint
        self.check_rhapsody_commpoint = check_rhapsody_commpoint
        from check_rhapsody_commpoint import get_queue_messages
        from check_rhapsody_commpoint import fetch_component_id
        from check_rhapsody_commpoint import get_output_state
        self.get_queue_messages = get_queue_messages
        self.fetch_component_id = fetch_component_id
        self.get_output_state = get_output_state

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_output_state0(self):
        self.assertEqual(self.get_output_state(10, 12, 20, 40), 0)

    def test_get_output_state1(self):
        self.assertEqual(self.get_output_state(10, 12, 5, 40), 1)

    def test_get_output_state2(self):
        self.assertEqual(self.get_output_state(10, 12, 2, 10), 2)

    @patch('check_rhapsody_commpoint.requests.post')
    def test_fetch_component_id_200(self, mock_post):
        mock_post.return_value.status_code = 200
        mock_post.return_value.content = 153
        self.assertEqual(self.fetch_component_id('127.0.0.1', 8544, 'guest',
                                                 'home/user/hl7web'),
                         (200, 153))

    @patch('check_rhapsody_commpoint.requests.post')
    def test_fetch_component_id_404(self, mock_post):
        mock_post.return_value.status_code = 404
        self.assertEqual(self.fetch_component_id('127.0.0.1', 8544, 'guest',
                                                 'home/user/hl7web'),
                         (404, None))

    @patch('check_rhapsody_commpoint.requests.post')
    def test_fetch_component_id_exception(self, mock_post):
        mock_post.side_effect = Exception
        self.assertEqual(self.fetch_component_id('127.0.0.1', 8544, 'guest',
                                                 'home/user/hl7web'), None)

    @patch('check_rhapsody_commpoint.requests.get')
    @patch('check_rhapsody_commpoint.requests.post')
    def test_get_queue_messages_200(self, mock_get, mock_post):
        mock_post.return_value.status_code = 200
        mock_get.return_value.status_code = 200
        content_val = '{"data": {"inQueueSize": 10, "outQueueSize": 20}}'
        mock_post.return_value.content = content_val
        self.assertEqual(self.get_queue_messages('127.0.0.1', 8544, 'guest',
                                                 'guest', 'home/user/hl7web',
                                                 5, 10),
                         ( {'home/user/hl7web': {'status': 2, 'msg': 'home/user/hl7web : Message(s) found in inQueue = 10, outQueue = 20'}}))

    @patch('check_rhapsody_commpoint.requests.get')
    @patch('check_rhapsody_commpoint.requests.post')
    def test_get_queue_messages_404(self, mock_get, mock_post):
        mock_post.return_value.status_code = 404
        mock_get.return_value.status_code = 404
        self.assertEqual(self.get_queue_messages('127.0.0.1', 8544, 'guest',
                                                 'guest', 'home/user/hl7web',
                                                 5, 10),
                         ({'home/user/hl7web': {'status': 2, 'msg': 'Could not fetch attributes for home/user/hl7web, status code: 404 \n'}}))

    @patch('check_rhapsody_commpoint.requests.get')
    @patch('check_rhapsody_commpoint.requests.post')
    def test_get_queue_messages_exception(self, mock_get, mock_post):
        mock_post.side_effect = Exception
        mock_get.side_effect = Exception
        self.assertEqual(self.get_queue_messages('127.0.0.1', 8544, 'guest',
                                                 'guest', 'home/user/hl7web',
                                                 5, 10),
                         ({'home/user/hl7web': {'status': 2, 'msg': 'Could not connect to Raphsody end point 127.0.0.1 \n'}}))


if __name__ == '__main__':
    unittest.main()
