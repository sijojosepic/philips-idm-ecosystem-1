import unittest
import requests
from mock import MagicMock, patch, Mock


class CheckRabbitmqDataTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rhapsody_aliveness
        self.check_rhapsody_aliveness = check_rhapsody_aliveness
        from check_rhapsody_aliveness import check_aliveness
        self.check_aliveness = check_aliveness

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_aliveness_OK(self,):
        mock_get = MagicMock(name='get_request')
        mock_get.status_code = 200
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.check_aliveness('localhost','8544','guest','guest','https'), 
            (0, 'Ok! Rhapsody Alive'))

    def test_check_aliveness_credential_CRITICAL(self,):
        mock_get = MagicMock(name='get_request')
        mock_get.status_code = 401
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.check_aliveness('localhost','8544','guest','guest','https'), 
            (2, 'CRITICAL: Rhapsody Username or Password is not valid'))

    def test_check_aliveness_CRITICAL(self,):
        mock_get = MagicMock(name='get_request')
        mock_get.status_code = 404
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.check_aliveness('localhost','8544','guest','guest','https'), 
            (2, 'CRITICAL: Rhapsody server might receive too many requests or is not accessible'))

    def test_check_aliveness_exception(self,):
        self.mock_request.get.side_effect = Exception()
        self.assertEqual(self.check_aliveness('localhost','8544','guest','guest','https'), 
            (2, 'CRITICAL: Rhapsody is not accessible/down'))


if __name__ == '__main__':
    unittest.main()