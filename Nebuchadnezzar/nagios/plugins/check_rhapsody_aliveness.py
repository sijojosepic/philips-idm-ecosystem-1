#!/usr/bin/env python
import sys
import json
import argparse
import requests

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to get Rhapsody aliveness')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The name of the Rhapsody server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the Rhapsody server')
    parser.add_argument('-u', '--rhapsodyusername', required=True,
                        help='The username of the Rhapsody server')
    parser.add_argument('-p', '--rhapsodypassword', required=True,
                        help='The password of the Rahpsody server')
    parser.add_argument('-pr', '--protocol', required=False, default='https',
                        help='The protocol for rhapsody server')
    results = parser.parse_args(args)
    return (
        results.hostname,
        results.port,
        results.rhapsodyusername,
        results.rhapsodypassword,
        results.protocol)

def check_aliveness(hostname, port, username, password, protocol):
    status, msg = CRITICAL, ''
    try:
        url = '{0}://{1}:{2}/api/alerts/active'.format(protocol, hostname, port)
        auth = (username, password)
        response = requests.get(url,auth=auth,verify=False, timeout=10)
        if response.status_code == 401:
            return 2, 'CRITICAL: Rhapsody Username or Password is not valid'
        elif response.status_code < 200 or response.status_code > 400:
            return 2, 'CRITICAL: Rhapsody server might receive too many requests or is not accessible'
        else:
            return 0, 'Ok! Rhapsody Alive'
    except:
        return 2, 'CRITICAL: Rhapsody is not accessible/down'

def main():
    status, msg = check_aliveness(*check_arg())
    print msg
    exit(status)


if __name__ == '__main__':
    main()
