#!/usr/bin/env python
from __future__ import print_function
import argparse
import sys
from scanline.utilities.win_rm import WinRM
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
from requests.exceptions import RequestException


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a command deciding whether to run it')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server')
    parser.add_argument('-w', '--warning', required=True, help='CPU warning threshold flag')
    parser.add_argument('-c', '--critical', required=True, help='CPU critical threshold flag')
    results = parser.parse_args(args)
    return results.hostname, results.username, results.password, results.warning, results.critical


def get_status(cpu_usage, cpu_fact, perf_msg, cpu_warn, cpu_crit):
    """Pass the appropriate output as per threshold"""
    msg = 'OK: {0} | {1}'.format(cpu_fact, perf_msg)
    state = 0
    if cpu_usage >= int(cpu_crit):
        msg = 'CRITICAL: {0} | {1}'.format(cpu_fact, perf_msg)
        state = 2
    elif cpu_usage >= int(cpu_warn):
        msg = 'WARNING: {0} | {1}'.format(cpu_fact, perf_msg)
        state = 1
    return state, msg


def cpu_check(hostname, username, password, cpu_warn, cpu_crit):
    """Get the CPU utilization and process details"""
    try:
        ps_command = """(Get-Counter -ErrorAction SilentlyContinue '\Process(*)\% Processor Time').CounterSamples|
         Sort-Object -Property cookedvalue -Descending| Select-Object -First 5 -Property InstanceName, 
         @{Name="CPU";Expression={[Decimal]::Round(($_.CookedValue/$env:NUMBER_OF_PROCESSORS), 2)}}"""
        win_rm = WinRM(hostname, username, password)
        out_put = win_rm.execute_ps_script(ps_command)
        pro_list = out_put.std_out.split()[5:]
        try:
            idle_index = pro_list.index("idle")
            cpu_usage = float(pro_list[0])-float(pro_list[idle_index+1])
            pro_list.pop(idle_index+1)
            pro_list.pop(idle_index)
        except ValueError:  # ValueError - No data for CPU "idle" state, CPU utilization is 100%
            cpu_usage = pro_list[0]
        cpu_fact = 'Total CPU usage - {usage}%, Top 3 processes: {p1} - {data1}%, {p2} - {data2}%, {p3} - {data3}%'\
            .format(usage=cpu_usage, p1=pro_list[1], data1=pro_list[2], p2=pro_list[3], data2=pro_list[4],
                    p3=pro_list[5], data3=pro_list[6])
        perf_msg = "'Avg CPU Utilisation'={0}%;{1};{2};\n".format(cpu_usage, cpu_warn, cpu_crit)
        state, msg = get_status(cpu_usage, cpu_fact, perf_msg, cpu_warn, cpu_crit)
    except RequestException as e:
        state, msg = 2, 'CRITICAL : Request Error {0}'.format(e)
    except (AuthenticationError, WinRMOperationTimeoutError,
            WinRMTransportError, WinRMError) as e:
        state, msg = 2,  'CRITICAL : WinRM Error {0}'.format(e)
    except Exception as e:
        state, msg = 2,  'CRITICAL : Exception {0}'.format(e)
    return state, msg


def main():
    state, output = cpu_check(*check_arg())
    print(output, end='')
    sys.exit(state)


if __name__ == '__main__':
    main()
