#!/usr/bin/env python
# This script gets the elasticsearch and kibana version along with elasticsearch perf status
# if the version is retrieved, displays it with status OK
# else the status displayed is CRITICAL

import requests
import argparse
from requests.auth import HTTPBasicAuth

def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to check the status of elasticsearch and kibana')
    parser.add_argument('-T', '--type', required=False, help='type of check esstatus|eshealth|kibanastatus', default='esstatus')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server')
    parser.add_argument('-U', '--username', required=False, help='elastic user', default='elastic')
    parser.add_argument('-S', '--password', required=False, help='elastic password', default='changeme')
    results = parser.parse_args(args)

    return (results.hostname, results.type, results.username, results.password)

def get_json_resp(baseuri, path, username, password):
    uri = '{baseuri}{path}'.format(baseuri=baseuri, path=path)
    try:
        headers = {
          'accept': 'application/v1+json',
        }
        res = requests.get(uri, headers=headers, auth=HTTPBasicAuth(username, password), verify=False, timeout=10)
        res.raise_for_status()
        return res
    except Exception as e:
        return None

def get_elastic_health(hostname, proto, username, password):
    """get elasticsearch details"""
    if not hostname:
        return 2, 'Could not get due to missing arguments'
    res = get_json_resp('{proto}://{hostname}:9200'.format(proto=proto, hostname=hostname), '/_cluster/health', username, password)
    if res:
        jsonresp = res.json() 
        status = jsonresp.get('status')
        timed_out = jsonresp.get('timed_out')
        number_of_nodes = jsonresp.get('number_of_nodes')
        number_of_data_nodes = jsonresp.get('number_of_data_nodes')
        active_primary_shards = jsonresp.get('active_primary_shards')
        active_shards = jsonresp.get('active_shards')
        relocating_shards = jsonresp.get('relocating_shards')
        initializing_shards = jsonresp.get('initializing_shards')
        unassigned_shards = jsonresp.get('unassigned_shards')
        active_shards_percent = jsonresp.get('active_shards_percent_as_number')
        outmsg = "status:{status}; timed_out:{timed_out}; \
number_of_nodes:{number_of_nodes}; \
number_of_data_nodes:{number_of_data_nodes} \
active_primary_shards:{active_primary_shards} \
active_shards:{active_shards} \
relocating_shards:{relocating_shards} \
initializing_shards:{initializing_shards} \
unassigned_shards:{unassigned_shards} \
active_shards_percent:{active_shards_percent} | \
'active_primary'={active_primary_shards}; \
'active'={active_shards}; 'relocating'={relocating_shards}; \
'init'={initializing_shards}; 'unass'={unassigned_shards}".format(
            status=status, timed_out=timed_out,
            number_of_nodes=number_of_nodes,
            number_of_data_nodes=number_of_data_nodes,
            active_primary_shards=active_primary_shards,
            active_shards=active_shards, relocating_shards=relocating_shards,
            initializing_shards=initializing_shards,
            unassigned_shards=unassigned_shards,
            active_shards_percent=active_shards_percent)
        if status == 'red':
            state = 2
            outmsg = 'CRITICAL - ' + outmsg
        elif status == 'yellow':
            state = 1
            outmsg = 'WARNING - ' + outmsg
        else:
            state = 0
            outmsg = 'OK - ' + outmsg
    else:
        outmsg = 'CRITICAL - Elasticsearch is not running'
        state = 2
    return state, outmsg

def get_kibana_status(hostname, proto, username, password):
    """get kibana details"""
    if not hostname:
        return 2, 'Could not get due to missing arguments'
    res = get_json_resp('{proto}://{hostname}:5601'.format(proto=proto, hostname=hostname), '/api/status', username, password)
    if res:
        version = res.json().get('version').get('number')
        status = 0
        outmsg = 'OK - Kibana is running; version:{version}'.format(version=version)
    else:
        outmsg = 'CRITICAL - Kibana is not running'
        status = 2
    return status, outmsg

def get_elastic_version(hostname, proto, username, password):
    """get elasticsearch details"""
    if not hostname:
        return 2, 'Could not get due to missing arguments'
    res = get_json_resp('{proto}://{hostname}:9200'.format(proto=proto, hostname=hostname), '', username, password)
    if res:
        version = res.json().get('version').get('number')
        status = 0
        outmsg = 'OK - Elasticsearch is running; version:{version}'.format(version=version)
    else:
        outmsg = 'CRITICAL - Elasticsearch is not running'
        status = 2
    return status, outmsg

def get_service_status(hostname, type, username, password):
    status = 0
    outmsg = 'OK - Unknown check type'
    proto = 'https'
    res = get_json_resp('https://{hostname}:9200'.format(hostname=hostname), '', username, password)
    if res == None:
        proto = 'http'
    if type == 'esstatus':
        status, outmsg = get_elastic_version(hostname, proto, username, password)
    elif type == 'eshealth':
        status, outmsg = get_elastic_health(hostname, proto, username, password)
    elif type == 'kibanastatus':
        status, outmsg = get_kibana_status(hostname, proto, username, password)
    return status, outmsg

def main():
    status, outmsg = get_service_status(*check_arg())
    print(outmsg)
    exit(status)


if __name__ == '__main__':
    main()
