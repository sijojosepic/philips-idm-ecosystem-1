#!/usr/bin/env python

import argparse
import sys
import requests
from requests.auth import HTTPBasicAuth

URL_TEMPLATE = {
    'SYSTEM': 'https://{hostname}/redfish/v1/systems/1',
    'THERMAL': 'https://{hostname}/redfish/v1/Chassis/1/Thermal/',
    'MEMORY': 'https://{hostname}/redfish/v1/Systems/1/Memory/',
    'VOLTAGE': 'https://{hostname}/redfish/v1/Chassis/1/Power/',
    'BATTERY': 'https://{hostname}/redfish/v1/Chassis/1/',
    'MEMORY_SLOT': 'https://{hostname}{memory_slot_url}'
}

STATUSES = {'OK': 0, 'WARNING': 1, 'CRITICAL': 2}

HARDWARE_DEVICE_URL_DICT = {'FAN': 'Oem.Hpe.AggregateHealthStatus.Temperatures.Status.Health',
                            'BATTERY': 'Oem.Hpe.AggregateHealthStatus.SmartStorageBattery.Status.Health',
                            'STORAGE': 'Oem.Hpe.AggregateHealthStatus.Storage.Status.Health',
                            'TEMPERATURE': 'Oem.Hpe.AggregateHealthStatus.Temperatures.Status.Health',
                            'MEMORY': 'Oem.Hpe.AggregateHealthStatus.Memory.Status.Health',
                            'NETWORK': 'Oem.Hpe.AggregateHealthStatus.Network.Status.Health',
                            'VOLTAGE': 'Oem.Hpe.AggregateHealthStatus.PowerSupplies.Status.Health'
                            }

ILO_STATES = {
    'Absent': 'This function or resource is not present or not detected.',
    'Deferring': 'The element will not process any commands but will queue new requests.',
    'Disabled': 'This function or resource has been disabled.',
    'Enabled': 'This function or resource has been enabled.',
    'InTest': 'This function or resource is undergoing testing.',
    'Quiesced': 'The element is enabled but only processes a restricted set of commands.',
    'StandbyOffline': 'This function or resource is enabled, but awaiting an external action to activate it.',
    'StandbySpare': 'This function or resource is part of a redundancy set and is awaiting a failover or other external action to activate it.',
    'Starting': 'This function or resource is starting.',
    'UnavailableOffline': 'This function or resource is present but cannot be used.',
    'Updating': 'The element is updating and may be unavailable or degraded.'}


def get_url(hostname, device):
    return URL_TEMPLATE[device].format(hostname=hostname)


def get_memory_slot_url(hostname, memory_slot_url):
    return URL_TEMPLATE['MEMORY_SLOT'].format(hostname=hostname, memory_slot_url=memory_slot_url)


def get_ilo_response(url, username, password):
    headers = {'Content-Type': 'application/json'}
    response_obj = requests.get(url,
                                verify=False,
                                auth=HTTPBasicAuth(username, password),
                                allow_redirects=True,
                                headers=headers)
    response = response_obj.json()
    return response


def get_system_aggregate_status(response, hardware_device):
    device_url_parameters = HARDWARE_DEVICE_URL_DICT[hardware_device].split('.')
    value = response
    for parameter in device_url_parameters:
        value = value[parameter]
    return value


def get_temperature_status(hostname, username, password):
    url = get_url(hostname, 'THERMAL')
    response = get_ilo_response(url, username, password)

    for temperature in response['Temperatures']:
        if 'Status' in temperature and 'Health' in temperature['Status']:
            status_health = temperature['Status']['Health']
            #status_health = 'CRITICAL'
            if status_health.upper() != 'OK':
                temperature_name = temperature['Name'] if 'Name' in temperature else ''
                temperature_reading_celsius = temperature['ReadingCelsius'] if 'ReadingCelsius' in temperature else ''
                status_state = temperature['Status']['State'] if 'State' in temperature['Status'] else ''
                message = 'Name: {0}, Reading Celcius: {1},State: {2}({3})'.format(temperature_name,
                                                                                   temperature_reading_celsius,
                                                                                   status_state,
                                                                                   ILO_STATES[status_state])
                return message
    return ''


def get_fan_status(hostname, username, password):
    url = get_url(hostname, 'THERMAL')
    response = get_ilo_response(url, username, password)

    for fan in response['Fans']:
        if 'Status' in fan and 'Health' in fan['Status']:
            status_health = fan['Status']['Health']
            #status_health = 'CRITICAL'
            if status_health.upper() != 'OK':
                fan_name = fan['Name'] if 'Name' in fan else ''
                fan_reading = fan['Reading'] if 'Reading' in fan else ''
                status_state = fan['Status']['State'] if 'State' in fan['Status'] else ''
                message = 'Name: {0}, Reading Percent: {1}, State: {2}({3})'.format(fan_name, fan_reading, status_state,
                                                                                    ILO_STATES[status_state])
                return message
    return ''


def get_memory_status(hostname, username, password):
    url = get_url(hostname, 'MEMORY')
    response = get_ilo_response(url, username, password)

    for memory_slot in response['Members']:
        memory_slot_url = memory_slot['@odata.id']
        url = get_memory_slot_url(hostname, memory_slot_url)
        slot_rsp = get_ilo_response(url, username, password)

        if 'Status' in slot_rsp and 'Health' in slot_rsp['Status']:
            status_health = slot_rsp['Status']['Health']
            #status_health = 'CRITICAL'
            if status_health.upper() != 'OK':
                status_state = slot_rsp['Status']['State'] if 'State' in slot_rsp['Status'] else ''
                if 'MemoryLocation' in slot_rsp:
                    slot_loc = slot_rsp['MemoryLocation']
                    slot_location = slot_loc['Slot'] if 'Slot' in slot_loc else ''
                    slot_controller = slot_loc['MemoryController'] if 'MemoryController' in slot_loc else ''
                    slot_channel = slot_loc['Channel'] if 'Channel' in slot_loc else ''
                    slot_socket = slot_loc['Socket'] if 'Socket' in slot_loc else ''
                    message = 'Slot: {0}, Controller: {1}, Channel: {2}, Socket: {3}, State: {4}({5})'.format(
                        slot_location, slot_controller, slot_channel, slot_socket,
                        status_state, ILO_STATES[status_state])
                    return message
    return ''


def get_battery_status(hostname, username, password):
    url = get_url(hostname, 'BATTERY')
    response = get_ilo_response(url, username, password)

    if 'Oem' in response and 'Hpe' in response['Oem'] and 'SmartStorageBattery' in response['Oem']['Hpe']:
        smart_storage_battery_list = response['Oem']['Hpe']['SmartStorageBattery']
        for smart_storage_battery in smart_storage_battery_list:
            if 'Status' in smart_storage_battery and 'Health' in smart_storage_battery['Status']:
                status_health = smart_storage_battery['Status']['Health']
                #status_health = 'CRITICAL'
                if status_health.upper() != 'OK':
                    status_state = smart_storage_battery['Status']['State'] if 'State' in smart_storage_battery[
                        'Status'] else ''
                    spare_part_number = smart_storage_battery[
                        'SparePartNumber'] if 'SparePartNumber' in smart_storage_battery else ''
                    serial_number = smart_storage_battery[
                        'SerialNumber'] if 'SerialNumber' in smart_storage_battery else ''
                    charge_level_percent = smart_storage_battery[
                        'ChargeLevelPercent'] if 'ChargeLevelPercent' in smart_storage_battery else ''
                    model = smart_storage_battery['Model'] if 'Model' in smart_storage_battery else ''
                    battery_msg = 'Spare Part No: {0}, Serial No.: {1}, Charge Level %: {2},Model: {3}, State: {4}({5})'
                    message = battery_msg.format(spare_part_number, serial_number,
                                                 str(charge_level_percent), model, status_state,
                                                 ILO_STATES[status_state])
                    return message
        return ''


def get_power_status(hostname, username, password):
    url = get_url(hostname, 'VOLTAGE')
    response = get_ilo_response(url, username, password)

    for power_supply in response['PowerSupplies']:
        serial_number = power_supply['SerialNumber'] if 'SerialNumber' in power_supply else ''
        spare_part_number = power_supply['SparePartNumber'] if 'SparePartNumber' in power_supply else ''
        if 'Status' in power_supply and 'Health' in power_supply['Status']:
            status_health = power_supply['Status']['Health']
            #status_health = 'CRITICAL'
            if status_health.upper() != 'OK':
                status_state = power_supply['Status']['State'] if 'State' in power_supply['Status'] else ''
                bay_number = ''
                if 'Oem' in power_supply and 'Hpe' in power_supply['Oem'] and 'BayNumber' in power_supply['Oem']['Hpe']:
                    bay_number = power_supply['Oem']['Hpe']['BayNumber']
                message = 'Serial Number: {0}, Spare Part Number: {1}, Bay Number: {2}, State: {3}({4})'.format(
                    serial_number, spare_part_number, bay_number, status_state, ILO_STATES[status_state])
                return message
    return ''


def get_hardware_device_status(hostname, username, password, hardware_device):
    try:
        url = get_url(hostname, 'SYSTEM')
        response = get_ilo_response(url, username, password)

        status = get_system_aggregate_status(response, hardware_device)
        #status = 'CRITICAL'
        FUNCTION_TEMPLATE = {
            'TEMPERATURE': get_temperature_status,
            'FAN': get_fan_status,
            'VOLTAGE': get_power_status,
            'BATTERY': get_battery_status,
            'MEMORY': get_memory_status,
        }
        message = 'OK'
        if status.upper() != 'OK':
            message = FUNCTION_TEMPLATE[hardware_device.upper()](hostname, username, password)
        return STATUSES[status], message
    except Exception as e:
        return 2, '%s' % str(e)


def check_args(args=None):
    try:
        parser = argparse.ArgumentParser(description='Script to HSoP server hardware health details.')
        parser.add_argument('-H', '--hostname', required=True, help='The host name of the server')
        parser.add_argument('-U', '--username', required=True, help='The user name of the server')
        parser.add_argument('-P', '--password', required=True, help='The password for the server')
        parser.add_argument('-D', '--hardware_device', required=True, help='The Hardware Device to monitor.')
        results = parser.parse_args(args)
        if results.hardware_device not in HARDWARE_DEVICE_URL_DICT.keys():
            raise
        return (results.hostname, results.username, results.password, results.hardware_device)
    except Exception as e:
        return 2, 'Invalid parameters in command'


def main():
    status, message = get_hardware_device_status(*check_args())
    print message
    sys.exit(status)


if __name__ == '__main__':
    main()
