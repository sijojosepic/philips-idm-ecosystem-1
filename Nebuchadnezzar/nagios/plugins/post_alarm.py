#!/usr/bin/env python
import linecache
import json
import sys
import argparse
import requests
from phimutils.collection import DictNoEmpty
from phimutils.message import Message
from phimutils.argument import from_json
PAYLOAD_HELP = 'A label and JSON string to be used as part of the payload'
STATE_CHOICES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


class JSONAttributeAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not (len(values) == 2):
            raise argparse.ArgumentError(
                self, '%s takes 2 values, %d given' % (option_string, len(values)))
        label, json_string = values
        try:
            value = from_json(json_string)
        except argparse.ArgumentTypeError:
            raise argparse.ArgumentError(
                self, '%s takes valid JSON, %s given' % (option_string, json_string))
        destination = getattr(namespace, self.dest) or {}
        destination[label] = value
        setattr(namespace, self.dest, destination)


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to send a message via HTTP post')
    parser.add_argument('-u', '--url', required=True,
                        help='The URL address to post to')
    parser.add_argument('-H', '--op_console_host', required=True,
                        help='The address of the keystone server')
    parser.add_argument('-a', '--hostname', required=True,
                        help='The name of the keystone server')
    parser.add_argument('-n', '--op_console_username',
                        help='The username of the keystone server', required=True)
    parser.add_argument('-w', '--op_console_password',
                        help='The  password of the keystone server', required=True)
    parser.add_argument('-e', '--op_console_tenant',
                        help='The  tenant of the keystone server', required=True)
    parser.add_argument('-d', '--description', required=True,
                        help='The description of the entity for the message')
    parser.add_argument('-t', '--timestamp',
                        help='The timestamp for the message, expected in UNIX epoch time')
    parser.add_argument('-o', '--output',
                        help='The output for the message')
    parser.add_argument('-T', '--notificationtype',
                        help='The notification type for the message')
    parser.add_argument('-s', '--state', choices=STATE_CHOICES,
                        help='The state for the message')
    parser.add_argument('-p', '--perfdata',
                        help='The performance data type for the message')
    parser.add_argument('-P', '--payload',
                        action=JSONAttributeAction, help=PAYLOAD_HELP, nargs=2)
    parser.add_argument('-k', '--insecure',
                        help='do not verify ssl', action='store_true')
    parser.add_argument('-K', '--keystone_port',
                        help='The port of the keystone URL.')
    parser.add_argument('-A', '--alarm_port',
                        help='The port of the alarm URL.')
    parser.add_argument('-v', '--op_console_version',
                        help='The performance data type for the message')
    parser.add_argument('-S', '--shinken_alarm_state',
                        help='The performance data type for the message')
    parser.add_argument('-D', '--dashboard_port',
                        help='The port of the dashboard URL.')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--siteid_file',
                        help='A file containing the site id', default='/etc/siteid')
    group.add_argument('--siteid', help='The site id')
    results = parser.parse_args(args)

    return (
        results.url,
        results.hostname,
        results.op_console_host,
        results.op_console_username,
        results.op_console_password,
        results.op_console_tenant,
        results.description,
        results.notificationtype,
        results.state,
        results.timestamp,
        results.output,
        results.perfdata,
        results.siteid,
        results.siteid_file,
        results.payload,
        results.insecure,
        results.keystone_port,
        results.alarm_port,
        results.op_console_version,
        results.shinken_alarm_state,
        results.dashboard_port)


def get_keystone_token(op_console_host, op_console_username, op_console_password,
                       op_console_tenant, keystone_port, op_console_version):
    keystone_url = "https://{0}:{1}/{2}/tokens".format(op_console_host,
                                                      keystone_port,
                                                      op_console_version)
    payload = {'auth': {'passwordCredentials': {'username': op_console_username,
                                                'password': op_console_password},
                        'tenantName': op_console_tenant}}
    headers = {'content-type': "application/json", 'cache-control': "no-cache"}
    response = requests.request("POST", keystone_url, data=json.dumps(payload),
                                headers=headers, verify=False)
    keystone_token = json.loads(response.text)['access']['token']['id']
    return keystone_token


def get_alarm_json(op_console_host, keystone_token, alarm_port,
                   op_console_version, shinken_alarm_state):
    alarm_url = "https://{0}:{1}/{2}/alarms/".format(op_console_host,
                                                     alarm_port,
                                                     op_console_version)
    querystring = {"state": "ALARM"}
    headers = {'x-auth-token': keystone_token,
               'accept': "application/json", 'cache-control': "no-cache"}
    response = requests.request("GET", alarm_url, headers=headers,
                                params=querystring, verify=False)
    if shinken_alarm_state == '1':
        alarms = filter(lambda alarm: alarm['alarm_definition']['severity'] == 'MEDIUM' or
                                      alarm['alarm_definition']['severity'] == 'LOW',
                                      json.loads(response.text)['elements'])
        return alarms
    elif shinken_alarm_state == '2':
        val = json.loads(response.text)['elements']
        alarms = filter(lambda alarm: alarm['alarm_definition']['severity'] == 'CRITICAL' or
                                      alarm['alarm_definition']['severity'] == 'HIGH', val)
        return alarms


def get_alarm_count(alarms):
    return len(alarms)


def get_alarm_id_description(alarms):
    return dict((alarm['id'], alarm['alarm_definition']['name']) for alarm in alarms)


def post_notification(
        url,
        hostname,
        op_console_host,
        op_console_username,
        op_console_password,
        op_console_tenant,
        description,
        notificationtype,
        state,
        timestamp,
        output,
        perfdata,
        siteid,
        siteid_file,
        payload=None,
        insecure=False,
        keystone_port=None,
        alarm_port=None,
        op_console_version=None,
        shinken_alarm_state=None,
        dashboard_port=None):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
        if not siteid:
            return 2, 'But could not post due to missing siteid'
    try:
        keystone_token = get_keystone_token(op_console_host,
                                            op_console_username,
                                            op_console_password,
                                            op_console_tenant,
                                            keystone_port,
                                            op_console_version)
        alarms = get_alarm_json(op_console_host,
                                keystone_token,
                                alarm_port,
                                op_console_version,
                                shinken_alarm_state)
        alarm_detail = {"counts": get_alarm_count(alarms),
                   "description": get_alarm_id_description(alarms)}
        payload = DictNoEmpty(payload or {}, output=output)
    except Exception:
        return 2, "Could not fetch alarm(s) due to request error"

    msg = Message(
        siteid=siteid,
        epoch_time=timestamp,
        hostname=hostname,
        hostaddress=op_console_host,
        service=description,
        type=notificationtype,
        state=state,
        payload=payload,
        perfdata=perfdata
    )

    dashboard_url = "https://{0}:{1}/#/general/dashboard_alarms_summary" \
                    "".format(op_console_host, dashboard_port)
    try:
        msg.type = 'RECOVERY'
        output_details = 'Everything looks good. Ops Console URL: {0}'.format(dashboard_url)
        msg.output = (payload.update({'output':output_details}))
        if alarm_detail['counts'] > 0:
            msg.type = 'PROBLEM'
            if int(shinken_alarm_state) == 2:
                output_details = '{0} active alarm(s) found. Ops Console URL: ' \
                                 '{1} Alarm list: {2}'.format(alarm_detail['counts'],
                                                              dashboard_url,
                                                              alarm_detail['description'])
                msg.state = 'CRITICAL'
                msg.output = (payload.update({'output':output_details}))
            elif int(shinken_alarm_state) == 1:
                output_details = '{0} active alarm(s) found. Ops Console URL: ' \
                                 '{1} Alarm list: {2}'.format(alarm_detail['counts'],
                                                              dashboard_url,
                                                              alarm_detail['description'])
                msg.state = 'WARNING'
                msg.output = (payload.update({'output':output_details}))
        r = requests.post(url, json=msg.to_dict(), verify=not insecure)
        r.raise_for_status()
        if alarm_detail['counts'] > 0:
            return (int(shinken_alarm_state), output_details)
        else:
            return 0, output_details
    except requests.exceptions.RequestException:
        return 2, 'But could not post due to request error'


def main():
    state, msg = post_notification(*check_arg())
    print msg
    sys.exit(state)


if __name__ == '__main__':
    main()
