#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import requests
import argparse
from scanline.utilities.token_mgr import get_formatted_token


PAYLOAD_PART_HELP = 'A label and JSON string to be used as part of the payload'
STATE_CHOICES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-u', '--argument', required=True, help='argument to post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-O', '--options', required=False, help='argument to post', default='GET')
    results = parser.parse_args(args)

    return (results.hostname, results.argument, results.options)


def get_http_with_auth(hostname, argument, options='GET'):
    """Send a get via http in with auth header"""
    if not argument:
        return 2, 'Could not post due to missing arguments'
    uri = '{http}://{hostname}{arg}'.format(http='https', hostname=hostname, arg=argument)
    token = get_formatted_token()
    try:
        headers = {
        'accept': 'application/v1+json',
        'authorization': get_formatted_token(),
        }
        res = requests.get(uri, headers=headers, verify=False)
        if options=='options':
            res = requests.options(uri, headers=headers, verify=False)

        res.raise_for_status()
        status = 0
        outmsg = '{res} - Service is up'.format(res=res.status_code)
    except Exception:
        outmsg = 'could not post due to request error'
        status = 2
    return status, outmsg


def main():
    state, msg = get_http_with_auth(*check_arg())
    print(msg)
    exit(state)


if __name__ == '__main__':
    main()
