#!/usr/bin/env python
from pyVim import connect
from pyVmomi import vim
import argparse
import atexit
import sys


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to get vCenter disk usage information')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of the vCenter server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the vCenter server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the vCenter server')
    parser.add_argument('-w', '--warning_val', required=True,
                        help='The warning threshold for the disk usage')
    parser.add_argument('-c', '--critical_val',required=True,
                        help='The critical threshold for the disk usage')
    results = parser.parse_args(args)

    return (results.hostname,
            results.username,
            results.password,
            results.warning_val,
            results.critical_val)


def sizeof_fmt(num):
    for item in ['bytes', 'KB', 'MB', 'GB']:
        if num < 1024.0:
            return "%3.1f%s" % (num, item)
        num /= 1024.0
    return "%3.1f%s" % (num, 'TB')


def server_connect(hostname, username, password):
    try:
        connection = connect.SmartConnect(host=hostname, user=username, pwd=password)
        atexit.register(connect.Disconnect, connection)
        return connection
    except IOError:
        return 2,  'Could not connect to vCenter server.'
    except vim.fault.InvalidLogin:
        return 2, 'Cannot complete login due to an incorrect user name or password.'


def calculate_percentage(free_space, capacity):
    res = 100.00 - (float(free_space)/float(capacity))*100
    return format(res, '.2f')


def disk_info(disks, warning_val, critical_val):
    disk_fact = ''
    perf_msg = ''
    perct_lst = []
    for disk in disks:
        percent_used = calculate_percentage(disk.freeSpace, disk.capacity)
        disk_fact += 'Path= ' + disk.diskPath + ', ' + 'Capacity= ' + sizeof_fmt(disk.capacity) + ', ' + 'Free space= '\
                     + sizeof_fmt(disk.freeSpace) + ', ' + 'Percentage used= ' + percent_used + '\n'
        perf_msg += "'{0}'={1}%;{2};{3};\n".format(disk.diskPath, percent_used, warning_val, critical_val)
        perct_lst.append(percent_used)
    return perct_lst, disk_fact, perf_msg


def check_threshold(percent_used, check_val, other_val=0, critical=True):
    res = False
    for i in percent_used:
            if float(i) >= float(check_val) and float(i) < float(other_val):
                res = True
    if critical:
        for i in percent_used:
            if float(i) >= float(check_val):
                res = True
    return res


def get_status(percent_used, disk_fact, perf_msg, warning_val, critical_val):
    state = 0
    msg = 'OK: {0} | {1}'.format(disk_fact, perf_msg)
    if check_threshold(percent_used, critical_val):
        msg = 'CRITICAL: {0} | {1}'.format(disk_fact, perf_msg)
        state, msg = 2, msg
    elif check_threshold(percent_used, warning_val, other_val=critical_val, critical=False):
        msg = 'WARNING: {0} | {1}'.format(disk_fact, perf_msg)
        state, msg = 1, msg
    return state, msg


def main(hostname, username, password, warning_val, critical_val):
    try:
        server_instance = server_connect(hostname, username, password)
        virtual_machine_instance = server_instance.content.searchIndex.FindByIp(None, hostname, True)
        disks = virtual_machine_instance.guest.disk
        percent_used, disk_fact, perf_msg = disk_info(disks, warning_val, critical_val)
        state, msg = get_status(percent_used, disk_fact, perf_msg, warning_val, critical_val)
        print msg
        sys.exit(state)
    except Exception:
        print 'Cannot complete login due to an incorrect user name or password or Could not connect to vCenter server.'
        sys.exit(2)


if __name__ == '__main__':
    main(*check_arg())
