#!/usr/bin/env python
import argparse
import sys

import requests

from scanline.utilities import token_mgr


URL_TEMPLATE = '{protocol}://{host}{service_url}'
OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def parse_cmd_args(args=None):
    parser = argparse.ArgumentParser(
        description='Check the Health of an Application over HTTP')
    parser.add_argument('-H', '--host', required=True,
                        help='The hostname of the server')
    parser.add_argument('-u', '--service_url', required=True,
                        help='The Service URL')
    parser.add_argument('-p', '--protocol', default='https',
                        help='The Request protocol HTTP vs HTTPS, defaults to HTTPS')
    parser.add_argument('-c', '--hmac', default=True,
                        help='The rest service if use HMAC authentication')
    results = parser.parse_args(args)
    return (results.host, results.service_url, results.protocol, results.hmac)


def get_url(host, service_url, protocol):
    return URL_TEMPLATE.format(protocol=protocol, host=host, service_url=service_url)


def main(host, service_url, protocol, hmac, verify=False):
    try:
        if hmac:
            response = token_mgr.get_authenticated_response(
                get_url(host, service_url, protocol))[0]
        else:
            response = requests.get(
                get_url(host, service_url, protocol), verify=verify, timeout=5)
        if response:
            print 'OK - {0} :{1}'.format(response.status_code, response.content)
            sys.exit(OK)
        else:
            print 'CRITICAL - Non 200 response status'
            sys.exit(CRITICAL)
    except Exception as e:
        print 'CRITICAL - Exception -', str(e)
        sys.exit(CRITICAL)


if __name__ == '__main__':
    main(*parse_cmd_args())
