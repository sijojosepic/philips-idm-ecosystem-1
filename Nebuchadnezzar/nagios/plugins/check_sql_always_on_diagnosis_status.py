#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import logging
import pymssql
import datetime
from scanline.utilities import dbutils

logger = logging.getLogger(__name__)

QUERY_GET_LOG = """
     EXEC master.dbo.xp_readerrorlog {log_file}, 1, {query_string}, NULL, {start_date}, {end_date}, N'desc'
     SELECT 'FLAG'"""


class SQLHandler:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    def execute(self, query):
        return self.cursor.execute(query)


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    parser.add_argument('-T', '--logsnap', required=True, help='Log Snap duration', default=6)
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.logsnap)

def get_connection(hostname, username, password, database):
    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database, login_timeout=20)
        return conn
    except Exception as e:
        return False

def get_diagnosis_status(hostname, username, password, database, logsnap):
    outmsg = ''
    conn = get_connection(hostname, username, password, database)
    if not conn:
        node = dbutils.get_cluster_primary_db(hostname, username, password, database)
        conn = get_connection(node, username, password, database)
    if not conn:
        status, outmsg = 2, 'CRITICAL: Connection error.'
        return status, outmsg
    cursor = conn.cursor()
    for log_file_count in range(3):
        cursor.execute(
            QUERY_GET_LOG.format(log_file=log_file_count, query_string="N'Error: 19407'", start_date='NULL', end_date='NULL'))
        result = cursor.fetchone()
        if result[0] != 'FLAG':
            start_date = result[0] - datetime.timedelta(seconds=int(logsnap)/2)
            end_date = result[0] + datetime.timedelta(seconds=int(logsnap)/2)
            start_date = "'" + str(start_date.replace(microsecond=0)) + "'"
            end_date = "'" + str(end_date.replace(microsecond=0)) + "'"
            cursor.execute(
                QUERY_GET_LOG.format(log_file=log_file_count, query_string='NULL', start_date=start_date, end_date=end_date))
            log_result = cursor.fetchall()
            if log_result:
                outmsg = "WARNING: Error : 19407\n"
                for log in log_result:
                    outmsg = outmsg + '{log_datetime} : {description}\n'.format(log_datetime=log[0], description=log[2].strip())
                return 1, outmsg + '\n For more information, please check SQL Server Logs.'

    status, outmsg = 0, "OK: Internally SQL Server is healthy."
    return status, outmsg


def main():
    state, msg = get_diagnosis_status(*check_arg())
    print(msg)
    exit(state)


if __name__ == '__main__':
    main()