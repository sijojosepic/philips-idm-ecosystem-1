#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import logging
import pymssql
from scanline.utilities import dbutils

logger = logging.getLogger(__name__)

QUERY_SYNC_HEALTH = """SELECT adc.database_name 
                    FROM sys.dm_hadr_database_replica_states AS drs
                    INNER JOIN sys.availability_databases_cluster AS adc 
                    ON drs.group_id = adc.group_id AND 
                    drs.group_database_id = adc.group_database_id
                    INNER JOIN sys.availability_groups AS ag
                    ON ag.group_id = drs.group_id
                    INNER JOIN sys.availability_replicas AS ar 
                    ON drs.group_id = ar.group_id AND 
                    drs.replica_id = ar.replica_id
                    where synchronization_health=0;"""

QUERY_SYNC_BACKLOG = """SELECT ar.replica_server_name, 
                     adc.database_name, ag.name AS ag_name, drs.is_local, 
                     --drs.is_primary_replica, drs.synchronization_state_desc, drs.is_commit_participant, 
                     drs.synchronization_health_desc,  drs.log_send_queue_size 
                     FROM sys.dm_hadr_database_replica_states AS drs
                     INNER JOIN sys.availability_databases_cluster AS adc 
                     ON drs.group_id = adc.group_id AND drs.group_database_id = adc.group_database_id
                     INNER JOIN sys.availability_groups AS ag
                     ON ag.group_id = drs.group_id
                     INNER JOIN sys.availability_replicas AS ar 
                     ON drs.group_id = ar.group_id AND  drs.replica_id = ar.replica_id
                     where log_send_queue_size >{counterval}
                     ORDER BY 
                     ag.name, 
                     ar.replica_server_name, 
                     adc.database_name;"""


class SQLHandler:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    def execute(self, query):
        return self.cursor.execute(query)


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    parser.add_argument('-V', '--counterval', required=False, help='', default=102400)

    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.counterval)


def get_connection(hostname, username, password, database):
    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database, login_timeout=20)
        return conn
    except Exception as e:
        return False


def get_synchronization_status(hostname, username, password, database, counterval):
    status, outmsg = 0, ''
    conn = get_connection(hostname, username, password, database)
    if not conn:
        node = dbutils.get_cluster_primary_db(hostname, username, password, database)
        conn = get_connection(node, username, password, database)
    if not conn:
        status, outmsg = 2, 'CRITICAL: Connection error.'
        return status, outmsg
    cursor = conn.cursor()
    cursor.execute(QUERY_SYNC_HEALTH)
    sync_health = cursor.fetchall()
    cursor.execute(QUERY_SYNC_BACKLOG.format(counterval=counterval))
    sync_backlog = cursor.fetchall()
    if sync_health:
        status, outmsg = 2, 'CRITICAL: Problem in database synchronization. Unhealthy databases are {0}.'.format(
            ', '.join(i[0] for i in set(sync_health)))
        return status, outmsg
    if sync_backlog:
        status, outmsg = 2, 'CRITICAL: Database synchronization backlog threshold reached.'
        return status, outmsg

    status, outmsg = 0, 'OK: Synchronization is healthy.'
    return status, outmsg


def main():
    state, msg = get_synchronization_status(*check_arg())
    print(msg)
    exit(state)


if __name__ == '__main__':
    main()
