#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import redis
import json
import logging
import linecache
import pymssql
import datetime
from scanline.utilities.http import HTTPRequester

logger = logging.getLogger(__name__)

SYNC_QUERY = """select instance_name,cntr_value from sys.dm_os_performance_counters PC
      where object_name like '%database mirroring%'
      and counter_name in ('log send queue kb','Redo Queue Kb')
      and instance_name in (select db_name(dbm.database_id) from master.sys.database_mirroring dbm 
      where dbm.mirroring_role_desc in ('Principal','Mirror'))
      and PC.cntr_value >={counterval}"""

DB_STATE_QUERY = """DECLARE @dbcount int;
     DECLARE @dbcountgood int;
     SET @dbcount = (select count(*) from master.sys.database_mirroring where mirroring_partner_instance <> 'NULL');
     SET @dbcountgood = (select count(*) from master.sys.database_mirroring where mirroring_role = '1');
     IF (@dbcount <> @dbcountgood)
     Select 'Flag Error'
     ELSE Select 'OK'"""

FAILOVER_QUERY = """DECLARE @dbdate date;
     SET @dbdate = CURRENT_TIMESTAMP-1;
     EXEC master.dbo.xp_readerrorlog 0, 1, N'"Audit" is changing roles', NULL, @dbdate, NULL, N'desc'
     SELECT 'FLAG'"""

SITEID_FILE = '/etc/siteid'
TOKEN = ''
SITEID = ''
VIGILANT_URL = 'http://vigilant/'
HOST_URL = 'pma/facts/{site_id}/hosts'
MODULE_URL_DEFAULT = 'pma/facts/{site_id}/modules/ISP/keys/module_type'
FACT_URL = 'pma/facts/{site_id}/nodes'


class SQLHandler:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    def execute(self, query):
        return self.cursor.execute(query)


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='argument to post')
    parser.add_argument('-P', '--password', required=True, help='argument to post', default=2)
    parser.add_argument('-D', '--database', required=True, help='argument to post', default='')
    parser.add_argument('-V', '--counterval', required=False, help='', default=102400)
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.counterval)


def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_backoffice_data(fact_type='host', MODULE_URL=MODULE_URL_DEFAULT):
    host_url = VIGILANT_URL + HOST_URL.format(site_id=SITEID)
    module_url = VIGILANT_URL + MODULE_URL.format(site_id=SITEID)
    fact_url = VIGILANT_URL + FACT_URL.format(site_id=SITEID)
    fact_dict = {'host': host_url, 'module': module_url, 'fact': fact_url}
    http_obj = HTTPRequester()
    result = http_obj.suppressed_get(fact_dict[fact_type], headers={'X-Requested-With': "True"})
    return json.loads(result.content)


def get_db_hosts():
    redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)
    db_host = [key.split('#')[1] for key in redis_con.keys(pattern="*ISPACS*") if key.split('#')[-1] == 'Database']
    if not db_host:
        facts = get_backoffice_data('module')['result']
        db_host = [host['hostname'] for host in facts if host['ISP']['module_type'] == 'Database']
        if not db_host:
            facts = get_backoffice_data(fact_type='module', MODULE_URL='pma/facts/{site_id}/modules/Database')['result']
            db_host = [str(host['hostname']) for host in facts]
    return db_host


def get_formatted_data(current_server_timestamp, db_hosts, state_result, sync_result, failover_result, counterval):
    ok_msg = 'OK - Database primary up on {DB_node}'
    critical_msg = 'CRITICAL: Data synchronization is slow at {node} - Backlog reached threshold of {counterval}MB.'
    failover_node_msg = '"PRINCIPAL" to "MIRROR"'
    takeover_node_msg = '"MIRROR" to "PRINCIPAL"'
    if state_result[0] == 'Flag Error' and state_result[1] == 'Flag Error':
        status, outmsg = 2, 'CRITICAL - Splitbrain State'
    else:
        status, outmsg = 0, ok_msg.format(DB_node=db_hosts[state_result.index('OK')])
        if failover_result[0][0] !='FLAG':
            if failover_node_msg in failover_result[0][2] and takeover_node_msg in failover_result[1][2]:
                if failover_result[0][0] > current_server_timestamp[0][0][0] - datetime.timedelta(minutes=6):
                    status, outmsg = 1, 'WARNING - Failover happen on {fnode} and Service up on {dnode}'.format(
                        fnode=db_hosts[0], dnode=db_hosts[1])
            if failover_node_msg in failover_result[1][2] and takeover_node_msg in failover_result[0][2]:
                if (failover_result[1][0]) > (current_server_timestamp[1][0][0] - datetime.timedelta(minutes=6)):
                    status, outmsg = 1, 'WARNING - Failover happen on {fnode} and Service up on {dnode}'.format(
                        fnode=db_hosts[1], dnode=db_hosts[0])
    if not status:
        if sync_result[0]:
            status, outmsg = 2, critical_msg.format(node=db_hosts[0], counterval=int(counterval)/1024)
        if sync_result[1]:
            status, outmsg = 2, critical_msg.format(node=db_hosts[1], counterval=int(counterval)/1024)
        if sync_result[0] and sync_result[1]:
            status, outmsg = 2, critical_msg.format(node=db_hosts, counterval=int(counterval)/1024)
    return status, outmsg


def get_db_mirror_status(hostname, username, password, database, counterval):
    global TOKEN
    global SITEID
    SITEID = get_siteid()
    state_result = []
    sync_result = []
    failover_result = []
    current_server_timestamp = []
    all_db_hosts = get_db_hosts()
    db_hosts = filter(lambda x: hostname.split('.')[1] in x, all_db_hosts)
    db_len = len(db_hosts)
    if db_len == 1:
        status, outmsg = 0, 'OK - This environment contains single DB node'
        return status, outmsg
    elif db_len == 0:
        status, outmsg = 2, 'CRITICAL - DB nodes not reachable'
        return status, outmsg

    con_err, conn_err_msg = [], ''
    for host in db_hosts:
        try:
            conn = pymssql.connect(server=host, user=username, password=password, database=database)
        except Exception as e:
            con_err.append(host)
            continue
        cursor = conn.cursor()
        cursor.execute("SELECT CURRENT_TIMESTAMP")
        current_server_timestamp.append(cursor.fetchall())
        cursor.execute(DB_STATE_QUERY)
        state_result.append(cursor.fetchone()[0])
        cursor.execute(SYNC_QUERY.format(counterval=counterval))
        sync_result.append(cursor.rowcount)
        cursor.execute(FAILOVER_QUERY)
        failover_result.append(cursor.fetchone())
    if con_err:
        status, outmsg = 2, 'CRITICAL - Connection error with {node}'.format(node=con_err)
        return status, outmsg
    return get_formatted_data(current_server_timestamp, db_hosts, state_result, sync_result, failover_result, counterval)


def main():
    try:
        status, msg = get_db_mirror_status(*check_arg())
    except Exception, err:
        status, msg = 2, "CRITICAL - {0}".format(str(err))
    print(msg)
    exit(status)


if __name__ == '__main__':
    main()

