#!/usr/bin/env python
# Version:0.1
###############################################
# Shinken Plugin - HTTP based Health Status Check
#
# Check the health of an application over HTTP
# Upon 200 HTTP response STATUS, application status
# will be displayed based on the status received
# through response content.
#
# CRITICAL status with appropriate message will be
# displayed for non 200 response statuses.
#
# {
#  "productid": "UDM",
#  "productname": "UDM Connector",
#  "version": "1.2.0.0",
#  "status": "OK",
#  "data": {
#      "message": "Cumulative Calls : 0 , Success Calls : 0, Failed Calls : 0"
#    }
# }
################################################
from __future__ import print_function

import sys
import requests
import argparse

from requests.exceptions import RequestException
from scanline.utilities import token_mgr


# https://192.168.219.59/InstanceDiscovery/Diagnostics/v1/healthstatus
URL_TEMPLATE = '{protocol}://{host}{service_url}'
GRAPHER = '| service status={0};1;2;0;2'

STATUSES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def parse_cmd_args(args=None):
    parser = argparse.ArgumentParser(
        description='Check the Health of an Application over HTTP')
    parser.add_argument('-H', '--host', required=True,
                        help='The hostname of the server')
    parser.add_argument('-s', '--service_url', required=True,
                        help='The Service URL')
    parser.add_argument('-p', '--protocol', default='https',
                        help='The Request protocol HTTP vs HTTPS, defaults to HTTPS')
    parser.add_argument('-c', '--hmac', default=True,
                        help='The rest service if use HMAC authentication')
    results = parser.parse_args(args)
    return (results.host, results.service_url, results.protocol, results.hmac)


def get_url(host, service_url, protocol):
    return URL_TEMPLATE.format(protocol=protocol, host=host, service_url=service_url)

def search_response(resp_json, item):
    if isinstance(resp_json, dict):
        for key, value in resp_json.items():
            if key == item or key == item.title() or key == item.upper():
                return value
            if isinstance(value, (dict, list)):
                return search_response(value, item)
    elif isinstance(resp_json, list):
        for val in resp_json:
            return search_response(val, item)

def process_output(response, url):
    status, msg = CRITICAL, 'CRITICAL : did not get, application status from the URL {0}, status_code - {1}'
    resp_json = response.json() if hasattr(response, 'json') else None
    if resp_json:
        status = search_response(resp_json, 'status')
        msg = search_response(resp_json, 'message')
        if status:
            if status not in STATUSES:
                status, msg = 2, 'CRITICAL : Invalid status `{status}` received, status should be any of `{known_statuses}`'.format(
                    status=status, known_statuses=','.join(STATUSES)) + GRAPHER.format(2)

            else:
                msg = status + ' - ' + msg + GRAPHER.format(STATUSES.index(status))
                status = STATUSES.index(status)
        else:
            status, msg = 2, 'CRITICAL : Improper JSON response, {0}'.format(resp_json)
            msg = msg + GRAPHER.format(2)
    else:
        msg = msg.format(url, response.status_code)
        msg = msg + GRAPHER.format(2)
    return status, msg


def health_status(url, hmac, verify=False):
    if hmac:
        response = token_mgr.get_authenticated_response(url)[0]
    else:
        response = requests.get(url, verify=verify, timeout = 20)
        response.raise_for_status()
    return process_output(response, url)


def main(host, service_url, protocol, hmac):
    status, msg = CRITICAL, ''
    try:
        status, msg = health_status(get_url(host, service_url, protocol), hmac)
    except RequestException as e:
        msg = 'CRITICAL : Request Error - {0}'.format(e) + GRAPHER.format(2)
    except Exception as e:
        msg = 'CRITICAL : Error - {0}'.format(e) + GRAPHER.format(2)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*parse_cmd_args())

