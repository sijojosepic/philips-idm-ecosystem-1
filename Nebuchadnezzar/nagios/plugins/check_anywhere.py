#!/usr/bin/env python
#This script will check for a version number in a web page
#  The version is expected in 0.0.0.0 format within a node with property id="APP_VERSION"
#  The first string matching this pattern in the document will be used.
#  The check will be done via https, certificate validity is ignored.
import sys
import argparse
import logging
from phimutils.perfdata import INFO_MARKER
from StringIO import StringIO
from scanline.component.isp import AnywhereComponent

# the following requires python-argparse to be installed
def check_arg():
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to check the Anywhere product version')
    parser.add_argument('-H', '--address', help='hostname or ip address of the remote server', required=True)
    results = parser.parse_args()
    return results.address


def check_anywhere(address):
    spit_bucket = logging.getLogger('phim.scanline')
    out_file = StringIO()
    handler_console = logging.StreamHandler(out_file)
    spit_bucket.addHandler(handler_console)
    anywhere_version = AnywhereComponent().scan(address)
    if not anywhere_version:
         return 2, "Could not get version from host '%s'" % address
    return 0, "Found {marker} Version = {version}".format(marker=INFO_MARKER, version=anywhere_version)


def main():
    states = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')
    state, message = check_anywhere(check_arg())
    print "%s - %s" % (states[state], message)
    sys.exit(state)
    

if __name__ == '__main__':
    main()
