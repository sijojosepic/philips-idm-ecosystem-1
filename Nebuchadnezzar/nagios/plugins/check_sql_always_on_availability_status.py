#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import logging
import pymssql
from scanline.utilities import dbutils
logger = logging.getLogger(__name__)

QUERY_NODE_STATE = """SELECT replica_server_name,role_desc,operational_state_desc, connected_state_desc 
                    FROM sys.dm_hadr_availability_replica_states AS drs
                    INNER JOIN sys.availability_replicas AS ar 
                    ON drs.replica_id = ar.replica_id;"""


class SQLHandler:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    def execute(self, query):
        return self.cursor.execute(query)


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database)


def get_availability_status(hostname, username, password, database):
    nodes = dbutils.get_dbnodes()
    if not nodes:
        nodes = [hostname]
    status, outmsg = 2, ''
    disconnected_nodes = []
    error_connection = []
    results = []
    for node in nodes:
        try:
            conn = pymssql.connect(server=node, user=username, password=password, database=database, login_timeout=10)
        except Exception as e:
            error_connection.append(node)
            continue
        cursor = conn.cursor()
        cursor.execute(QUERY_NODE_STATE)
        results.append(cursor.fetchall())
    if results:
        result_count = list(map(lambda x: len(x), results))
        node_results = results[result_count.index(max(result_count))]
        for node_result in node_results:
            if 'PRIMARY' in node_result and 'ONLINE' in node_result and 'CONNECTED' in node_result:
                status, outmsg = 0, "OK: Primary up on {0}.".format(node_result[0])
            elif 'RESOLVING' in node_result and not outmsg:
                status, outmsg = 2, "CRITICAL: DB role change stuck at RESOLVING State."
            elif 'SECONDARY' in node_result and 'DISCONNECTED' in node_result:
                disconnected_nodes.append(node_result[0])

    if disconnected_nodes and status == 0:
        status, outmsg = 1, outmsg + 'And disconnected Secodary nodes are {0}.'.format(
            ', '.join(i for i in disconnected_nodes))
    if error_connection and not outmsg:
        status, outmsg = 2, 'CRITICAL: Connection error with {0}. '.format(', '.join(i for i in error_connection))
        return status, outmsg
    if not outmsg:
        status, outmsg = 2, "CRITICAL: Replica unexpected dropdown or not available or disconnected."
    return status, outmsg


def main():
    state, msg = get_availability_status(*check_arg())
    print(msg)
    exit(state)


if __name__ == '__main__':
    main()