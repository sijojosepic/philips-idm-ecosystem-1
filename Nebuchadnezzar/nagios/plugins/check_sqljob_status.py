#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import logging
import linecache
import pymssql

logger = logging.getLogger(__name__)

QUERY = """select top 1 j.Name as 'Job Name', j.job_id,
           case j.enabled  when 1 then 'Enable'
           when 0 then 'Disable'  end as 'Job Status',
           jh.run_date as [Last_Run_Date(YY-MM-DD)] ,
           STUFF(STUFF(RIGHT(REPLICATE('0', 6) +
           CAST(jh.run_time as varchar(6)), 6), 3, 0, ':'), 6, 0, ':') [Last_Run_Time] ,
           case jh.run_status when 0 then 'Failed'
           when 1 then 'Successful' when 2 then 'Retry'
           when 3 then 'Cancelled'
           when 4 then 'In Progress' end as Job_Execution_Status from sysJobHistory jh
           inner join  sysJobs j on j.job_id = jh.job_id
           where j.name ='{job_name}' and jh.run_date = (select max(hi.run_date)
           from sysJobHistory hi where jh.job_id = hi.job_id) order by jh.run_time desc"""
QUERY_PRIMARY_CHECK = """SELECT mirroring_role_desc
                        FROM  sys.database_mirroring m
                        WHERE  mirroring_state_desc IS NOT NULL"""


SITEID_FILE = '/etc/siteid'
TOKEN = ''
SITEID = ''

class SQLHandler:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    def execute(self, query):
        return self.cursor.execute(query)

def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=False, help='argument to post')
    parser.add_argument('-P', '--password', required=False, help='argument to post', default=2)
    parser.add_argument('-D', '--database', required=False, help='argument to post', default='')
    parser.add_argument('-J', '--job', required=False, help='The user name of the server', default='')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.job)

def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_job_status(hostname, username, password, database, job):
    global TOKEN
    global SITEID
    SITEID = get_siteid()
    result = ''
    is_success = False
    is_enabled = False
    msg_dict = {'AdminDBfullBackup': 'full back up',
                'AdminDBTrnBackup': 'transactional back up',
                'AdminDBdiffBackup': 'differential back up',
                'AdminDBDBCCCheck': 'consistency check'}
    ok_msg = 'OK: Database {job_type} on {node} successful.'
    disable_critical = 'CRITICAL: Database {job_type} job is not running on {node}'
    failed_critical = 'CRITICAL: Database {job_type} Failed on the database node {node}'

    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database)
    except Exception as e:
        status, outmsg = 2, 'CRITICAL - Connection error with {node}. '.format(node=hostname)
        return status, outmsg

    cursor = conn.cursor()
    cursor.execute(QUERY_PRIMARY_CHECK)
    db_role = [str(i[0]) for i in cursor.fetchall()]
    if 'PRINCIPAL' in db_role:
        query = QUERY.format(job_name=job)
        cursor.execute(query)
        result = cursor.fetchone()
    else:
        status, outmsg = 0, 'OK: Database {job_type} is not applicable for {node}.'.format(job_type=msg_dict[job], node=hostname)
        return status, outmsg
    if result:
        is_enabled = True if 'Enable' in result else False
        is_success = True if 'Successful' in result else False

    if not is_enabled:
        status, outmsg = 2, disable_critical.format(job_type=msg_dict[job], node=hostname)
        return status, outmsg
    if not is_success:
        status, outmsg = 2, failed_critical.format(job_type=msg_dict[job], node=hostname)
        return status, outmsg

    status, outmsg = 0, ok_msg.format(job_type=msg_dict[job], node=hostname)
    return status, outmsg

def main():
    state, msg = get_job_status(*check_arg())
    print(msg)
    exit(state)

if __name__ == '__main__':
    main()
