#!/usr/bin/env python
from __future__ import print_function

import argparse
import sys
from subprocess import Popen, PIPE


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a command deciding whether to run it')
    parser.add_argument('-X', '--condition', help='the string to decide if to run', required=True)
    parser.add_argument('-C', '--command', help='the command to run', nargs=argparse.REMAINDER, required=True)
    results = parser.parse_args(args)
    return results.condition, results.command


def get_unknown_output(message):
    return 3, 'UNKNOWN - {0}'.format(message), None


def scrub_output(message):
    if message:
        return message.replace('$', '#')


def short_c(condition, command):
    if condition != 'OK':
        return get_unknown_output('Condition to run not met.')

    try:
        pp = Popen(command, stdout=PIPE, stderr=PIPE)
        _stdout, _stderr = pp.communicate()
    except OSError:
        return get_unknown_output('Error running command.')
    except Exception:
        return get_unknown_output('Unexpected.')

    return pp.returncode, scrub_output(_stdout), _stderr


def main():
    state, output, error = short_c(*check_arg())
    if output:
        print(output, end='')
    if error:
        print(error, file=sys.stderr, end='')
    sys.exit(state)


if __name__ == '__main__':
    main()
