#!/usr/bin/env python
# Version:0.1
############################################
# Shinken Plugin - File Age Monitoring using WinRM.
#
# The aim of this plugin is to monitor the age of files
# recursively in the given path, based on the threshold
# provided.
# Plugin uses WinRM, to execute PowerShell scripts
# Plugin exit with OK or Critical statuses.
# Critical Status will be triggered
#   1) When the age of at least one file is more than the
#      treshold provided.
#   2) Occurance of an error.
############################################
from __future__ import print_function
import sys


import argparse
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
from requests.exceptions import RequestException

from scanline.utilities.win_rm import WinRM

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def parse_cmd_args(args=None):
    parser = argparse.ArgumentParser(
        description='Monitor the age of files inside a folder, and its subfolders.')
    parser.add_argument('-T', '--threshold', type=int,
                        help='Threshold in minutes', required=True)
    parser.add_argument('-H', '--host', required=True,
                        help='The hostname of the server')
    parser.add_argument('-U', '--username', required=True,
                        help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-D', '--drive', required=True,
                        help='The Drive Name (eg S)')
    parser.add_argument('-P', '--password', required=True,
                        help='The password for the server')
    parser.add_argument('-F', '--folderpath', required=True,
                        help='The folder path (eg `\\Stentor\\forward`)')
    results = parser.parse_args(args)
    return (results.host, results.username,
            results.password, results.threshold, results.drive, results.folderpath)


def get_path(drive, path):
    if not path.endswith('\\'):
        path = path + '\\'
    return drive + ':\\' + path


def ps_script(path, threshold):
    ps_script = """Get-childitem -path """+path+"""  -Recurse| Where {!$_.PsIsContainer} | Where-object {($_.LastWriteTime -lt (Get-Date).AddMinutes(-"""+str(threshold)+"""))}| % {
 $dir=$_.directory
 $fullname=$_.FullName
 Write-Output $dir`t$fullname`t`t`t
 }"""
    return ps_script


def format_err(std_err, path):
    if 'pathnotfound' in std_err.lower():
        return 'WARNING: {0} PathNotFound'.format(path)
    return std_err


def compose_status_msg(directory_name, file_count, threshold):
    status = CRITICAL
    msg = '{0}: Found {1} {2}, aged more than {3} minutes \n{4}'
    directory_name = '\n'.join(directory_name)
    if directory_name:
        count_msg = 'files' if file_count > 1 else 'file'
        msg = msg.format('CRITICAL', file_count,
                         count_msg, threshold, directory_name)
    else:
        status = OK
        msg = msg.format('OK', file_count, 'file', threshold, directory_name)
    return status, msg


def process_output(out_put, path, threshold):
    if out_put.status_code == OK:
        out_put = out_put.std_out.split('\t\t\t')
        directory = set()
        filename = set()
        for result in out_put:
            result = result.replace('\r\n', '')
            if result:
                directory.add(result.split('\t')[0])
                filename.add(result.split('\t')[1])
        return compose_status_msg(directory, len(filename), threshold)
    return WARNING, format_err(out_put.std_err, path)


def main(host, user, passwd, threshold, drive, path):
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, user, passwd)
        out_put = win_rm.execute_ps_script(
            ps_script(get_path(drive, path), threshold))
        status, msg = process_output(out_put, path, threshold)
    except RequestException as e:
        msg = 'CRITICAL : Request Error {0}'.format(e)
    except (AuthenticationError, WinRMOperationTimeoutError,
            WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*parse_cmd_args())
