#!/usr/bin/env python

import json
import sys
import argparse
import requests
from collections import namedtuple

MEMORY_STATUS_PARAM = {
                     "memory_used": ["query_top", {"metric_name": "memory__used__of__cluster__in__MiB"}],
                     "memory_provisioned": ["query_top", {"metric_name": "memory__total__of__cluster__in__MiB"}],
                     "memory_used_by_vms": ["query_top", {"metric_name": "memory__used_by_vms__of__cluster__in__MiB"}],
                     "memory_used_by_system": ["query_top",
                                               {"metric_name": "memory__used_by_system__of__cluster__in__MiB"}],
                     "memory_reserved_for_vms": ["query_top",
                                                 {"metric_name": "memory__reserved_for_vms__of__cluster__in__MiB"}],
                     "memory_reserved_for_system": ["query_top", {
                         "metric_name": "memory__reserved_for_system__of__cluster__in__MiB"}],
                     "memory_available_for_vms": ["query_top",
                                                  {"metric_name": "memory__available_for_vms__of__cluster__in__MiB"}]
                     }

CPU_STATUS_PARAM =  {
                     "cpu_count": ["query_top", {"metric_name": "cpu__count__of__cluster__in__CPU"}],
                     "cpu_used": ["query_top", {"metric_name": "cpu__used__of__cluster__in__MHz"}],
                     "cpu_provisioned": ["query_top", {"metric_name": "cpu__total__of__cluster__in__MHz"}]
                    }

def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check StratoScale infra health')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of StratoScale instance'),
    parser.add_argument('-d', '--domainname', required=True,
                        help='Domain name on Stratoscale')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the Stratoscale')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the Stratoscale')
    parser.add_argument('-w', '--warningthreshold', required=True, type=int, choices=range(1, 101),
                        help='The threshold value in percentage for warning status')
    parser.add_argument('-c', '--criticalthreshold', required=True, type=int, choices=range(1, 101),
                        help="The threshold value in percentage for critical status")
    parser.add_argument('-r', '--resourceType', required=True, choices=['MEMORY', 'CPU', 'STORAGE'],
                        help="Resource Type for which status needs to be checked")

    results = parser.parse_args(args)
    return (results.hostname,
            results.domainname,
            results.username,
            results.password,
            results.warningthreshold,
            results.criticalthreshold,
            results.resourceType)

def get_memory_status(hostname, token, warn_threshold, crit_threshold):
    try:
        url = 'https://{0}/api/v2/metrics/queries'.format(hostname)
        param_val = MEMORY_STATUS_PARAM
        param_val_json_str = json.dumps(param_val)
        param_pay_load = {'queries': param_val_json_str}
        headers = {'content-type': 'application/json', 'x-auth-token': token}
        response = requests.get(url, verify=False, params=param_pay_load, headers=headers)
        response.raise_for_status()
        result = response.json()
        mem_used_percentage = compute_memory_usage(result)
        status, message =  get_health_status(mem_used_percentage, warn_threshold, crit_threshold, 'MEMORY')
        return (status, message)
    except Exception as e:
        return (2, 'GET Query for Memory API failed : ' + str(e))

def get_cpu_status(hostname, token, warn_threshold, crit_threshold):
    try:
        url = 'https://{0}/api/v2/metrics/queries'.format(hostname)
        param_val = CPU_STATUS_PARAM
        param_val_json_str = json.dumps(param_val)
        param_pay_load = {'queries': param_val_json_str}
        headers = {'content-type': 'application/json', 'x-auth-token': token}
        response = requests.get(url, verify=False, params=param_pay_load, headers=headers)
        response.raise_for_status()
        result = response.json()
        cpu_used_percentage = compute_cpu_usage(result)
        status, message = get_health_status(cpu_used_percentage, warn_threshold, crit_threshold, 'CPU')
        return (status, message)
    except Exception as e:
        return (2, 'GET Query for Cpu API failed : '  + str(e))

def get_storage_status(hostname, token, warn_threshold, crit_threshold):
    try:
        url = 'https://{0}/api/v2/storage/pools'.format(hostname)
        headers = {'content-type': 'application/json', 'x-auth-token': token}
        response = requests.get(url, headers=headers, verify=False)
        response.raise_for_status()
        result = response.json()
        storage_percentage_used = compute_storage_usage(result)
        status, message = get_health_status(storage_percentage_used, warn_threshold, crit_threshold, 'STORAGE')
        return (status, message)
    except Exception as e:
        return (2, 'GET Query for storage failed : ' + str(e))

def create_token_payload(domainname, username, password):
    payload = {'auth': {'identity': {'methods': ['password'], 'password': {
        'user': {'name': username, 'password': password, 'domain': {'name': domainname}}}},
                        'scope': {'domain': {'name': domainname}}}}
    headers = {'content-type': 'application/json'}
    body = json.dumps(payload)
    TokenPayload = namedtuple('TokenPayload', 'headers body')
    token_payload = TokenPayload(headers, body)
    return token_payload

def get_token(hostname, token_payload):
    try:
        url = 'https://{0}/api/v2/identity/auth'.format(hostname)
        response = requests.post(url, verify=False, headers=token_payload.headers, data=token_payload.body)
        token = response.headers['x-subject-token']
        return token
    except Exception as e:
        raise Exception('Token generation failed : ' + str(e))


def get_stratoscale_resources_status(hostname, domainname, username, password, warn_threshold, crit_threshold, resource_type):
    try:
        resource_mths = {
            'MEMORY': get_memory_status,
            'CPU': get_cpu_status,
            'STORAGE': get_storage_status
        }

        token_payload = create_token_payload(domainname, username, password)
        token = get_token(hostname, token_payload)

        s, m = resource_mths[resource_type](hostname, token, warn_threshold, crit_threshold)
        return s, m
    except Exception as e:
        return (2, 'Exception Occurred : ' + str(e))


def compute_cpu_usage(cpu_attributes):
    cpu_used = float(cpu_attributes['cpu_used'][0][1])
    cpu_provisioned = float(cpu_attributes['cpu_provisioned'][0][1])
    calculated_result = round((cpu_used / cpu_provisioned) * 100)
    return calculated_result


def compute_memory_usage(memory_attributes):
    memory_used_by_system = float(memory_attributes['memory_used_by_system'][0][1])
    memory_used_by_vms = float(memory_attributes['memory_used_by_vms'][0][1])
    memory_reserved_for_system = float(memory_attributes['memory_reserved_for_system'][0][1])
    memory_reserved_for_vms = float(memory_attributes['memory_reserved_for_vms'][0][1])
    memory_provisioned = float(memory_attributes['memory_provisioned'][0][1])
    calculated_result = (round(((
                                    memory_used_by_system + memory_used_by_vms + memory_reserved_for_system + memory_reserved_for_vms) / memory_provisioned) * 100))
    return calculated_result


def compute_storage_usage(storage_attributes):
    total_capacity_mb = 0.0
    free_capacity_mb = 0.0
    for storage_info in storage_attributes:
        total_capacity_mb = total_capacity_mb + storage_info['total_capacity_mb']
        free_capacity_mb = free_capacity_mb + storage_info['free_capacity_mb']
    calculated_result = round(((total_capacity_mb - free_capacity_mb) / total_capacity_mb) * 100)
    return calculated_result


def get_health_status(currentvalue, warn_threshold, crit_threshold, resource_type):
    currentvalue = int(currentvalue)
    warn_threshold = int(warn_threshold)
    crit_threshold = int(crit_threshold)
    grapher = '|message info={0};{1};{2};1;100'

    if warn_threshold <= currentvalue < crit_threshold:
        return 1, '{0} usage = {1}%, Status is Warning'.format(resource_type, currentvalue) + grapher.format(
            currentvalue, warn_threshold, crit_threshold)
    elif currentvalue >= crit_threshold:
        return 2, '{0} usage = {1}%, Status is Critical'.format(resource_type, currentvalue) + grapher.format(
            currentvalue, warn_threshold, crit_threshold)
    else:
        return 0, '{0} usage = {1}%, Status is Ok'.format(resource_type, currentvalue) + grapher.format(
            currentvalue, warn_threshold, crit_threshold)


def main():
    status, message = get_stratoscale_resources_status(*check_arg())
    print message
    sys.exit(status)


if __name__ == '__main__':
    main()
