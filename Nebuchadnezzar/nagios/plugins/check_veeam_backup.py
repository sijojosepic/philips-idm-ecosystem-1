#!/usr/bin/env python
import sys
import requests
import argparse

from scanline.utilities.win_rm import WinRM
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check message count of individual Queue')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address to post to')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-j', '--jobtype', required=True,
                        help='The password of the rabbitmq server')

    results = parser.parse_args(args)
    return (results.hostname,
            results.username,
            results.password,
            results.jobtype)

def get_ps_script():
    ps_script = """ Add-PSSnapin VeeamPSSnapin
        $VJobs = Get-VBRJob
        ForEach ($VJob in $VJobs) {
	    $name = $VJob.Info.Name
	    $jobtype = $VJob.Info.JobType
    	$result = $VJob.Info.LatestStatus
	    Write-Output $name`t$jobtype`t$result
        } """
    return ps_script

def get_status(failed_job, success_job, warning_job):

    if len(failed_job) == 0 and len(success_job) == 0 and len(warning_job) == 0:
		status = OK
		msg = 'OK - No jobs are running'
    elif failed_job:
        status = CRITICAL
        msg = 'CRITICAL - Failed Jobs ', str(len(failed_job)),'\n','\n'.join(failed_job)
        if warning_job:
		    msg = 'CRITICAL - Failed Jobs ', str(len(failed_job)),'\n','\n'.join(failed_job), \
		    '\n\nWarning Jobs ', str(len(warning_job)),'\n','\n'.join(warning_job)
    elif warning_job:
        status = WARNING
        msg = 'WARNING - Warning Jobs ', str(len(warning_job)),'\n','\n'.join(warning_job)
    else:
        status = OK
        msg = 'OK - All jobs run successfully'

    return status, ''.join(msg)



def get_job_status(hostname, username, password, jobtypee):
    jobname = []
    jobtype = []
    result = []
    failed_job = []
    success_job = []
    warning_job = []
    try:
        win_rm = WinRM(hostname, username, password)
        ps_script = get_ps_script()
        powershell_output = win_rm.execute_ps_script(ps_script)

        if powershell_output.std_err:
        	status, msg = CRITICAL, 'Error while running cmdlets -'+ powershell_output.std_err
        	return status, msg

        powershell_stdout = powershell_output.std_out.split('\r\n')
        for results in powershell_stdout:
        	if results:
	        	jobname.append(results.split('\t')[0])
	        	jobtype.append(results.split('\t')[1])
	        	result.append(results.split('\t')[2])

        for job_name, job_result, job_type in zip(jobname, result, jobtype):
        	if job_type == jobtypee:
        		if job_result == 'Failed':
        			failed_job.append(job_name)
        		if job_result == 'Success':
        			success_job.append(job_name)
        		if job_result == 'Warning':
        			warning_job.append(job_name)

        status, msg = get_status(failed_job, success_job, warning_job)

    except (AuthenticationError, WinRMOperationTimeoutError,
            WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
        status = 2
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
        status = 2

    return status, msg


def main():
    status, message = get_job_status(*check_arg())
    print message
    sys.exit(status)


if __name__ == '__main__':
    main()
