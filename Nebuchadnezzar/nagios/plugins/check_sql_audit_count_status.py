#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import redis
import json
import linecache
import pymssql
from scanline.utilities.http import HTTPRequester

QUERY = """SELECT COUNT (*) as number
     FROM [PiiMainJobService].[dbo].[JobData]
     where jobType = 'audit' and jobDeletionDateTime IS NULL"""

DB_STATE_QUERY = """DECLARE @dbcount int;
     DECLARE @dbcountgood int;
     SET @dbcount = (select count(*) from master.sys.database_mirroring where mirroring_partner_instance <> 'NULL');
     SET @dbcountgood = (select count(*) from master.sys.database_mirroring where mirroring_role = '1');
     IF (@dbcount <> @dbcountgood)
     Select 'Flag Error'
     ELSE Select 'OK'"""

SITEID_FILE = '/etc/siteid'
SITEID = ''
HOST_URL = 'pma/facts/{site_id}/hosts'
MODULE_URL = 'pma/facts/{site_id}/modules/Database'
MODULE_URL_DEFAULT = 'pma/facts/{site_id}/modules/ISP/keys/module_type'
FACT_URL = 'pma/facts/{site_id}/nodes'


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='argument to post')
    parser.add_argument('-P', '--password', required=True, help='argument to post', default=2)
    parser.add_argument('-V', '--vigilanturl', required=True, help='argument to post', default='')
    parser.add_argument('-C', '--critical', required=True, help='argument to post', default='')
    parser.add_argument('-W', '--warning', required=True, help='argument to post', default='')
    parser.add_argument('-D', '--database', required=True, help='argument to post', default='')
    results = parser.parse_args(args)
    return (results.hostname, results.username, results.password, results.database,results.critical, results.warning, results.vigilanturl)


def get_siteid(siteid='', siteid_file=SITEID_FILE):
    """ This method returns the site id."""
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_backoffice_data(fact_type='host', MODULE_URL=MODULE_URL_DEFAULT, vig_url=None):
    SITEID = get_siteid()
    host_url = vig_url + HOST_URL.format(site_id=SITEID)
    module_url = vig_url + MODULE_URL.format(site_id=SITEID)
    fact_url = vig_url + FACT_URL.format(site_id=SITEID)
    fact_dict = {'host': host_url, 'module': module_url, 'fact': fact_url}
    http_obj = HTTPRequester()
    result = http_obj.suppressed_get(fact_dict[fact_type], headers={'X-Requested-With': "True"})
    return json.loads(result.content)


def get_db_hosts(vig_url):
    """ This method returns MSSQL server hosts."""
    redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)
    db_host = [key.split('#')[1] for key in redis_con.keys(pattern="*ISPACS*") if key.split('#')[-1] == 'Database']
    if not db_host:
        facts = get_backoffice_data('module', vig_url=vig_url)['result']
        db_host = [host['hostname'] for host in facts if host['ISP']['module_type'] == 'Database']
        if not db_host:
            facts = get_backoffice_data(fact_type='module', MODULE_URL=MODULE_URL, vig_url=vig_url)['result']
            db_host = [str(host['hostname']) for host in facts]
    return db_host


def get_primary_db_obj(db_hosts, username, password, database):
    """ This method returns the primary db object. """
    for host in db_hosts:
        try:
            conn = pymssql.connect(server=host, user=username, password=password, database=database, login_timeout=60)
        except Exception as e:
            continue
        cursor = conn.cursor()
        cursor.execute(DB_STATE_QUERY)
        db_status = [str(row[0]) for row in cursor.fetchall()]
        if 'OK' in db_status:
            return conn.cursor()


def get_aduit_count_status(hostname, username, password, database, critical, warning, vig_url=None):
    """ This method is used to collect the audit job status from MSSQL server."""
    db_hosts = filter(lambda x: hostname.split('.')[1] in x, get_db_hosts(vig_url=vig_url))
    cursor = get_primary_db_obj(db_hosts, username, password, database)
    if cursor:
        cursor.execute(QUERY)
        audit_count_result = cursor.fetchone()[0]
        if audit_count_result >= int(critical):
            status, outmsg = 2, 'CRITICAL - The Number of audit entries is {0}, which has crossed the threshold value\
             provided {1}. This may result in audit job failure.'.format(str(audit_count_result),str(critical))
        elif audit_count_result >= int(warning):
            status, outmsg = 1, 'WARNING - The Number of audit entries is {0}, which has crossed the threshold value\
             provided {1}.'.format(str(audit_count_result),str(warning))
        elif audit_count_result > 0:
            status, outmsg = 0, 'OK - The Number of audit entries is {0}, audit job is healthy.'.format(str(audit_count_result))
        else:
            status, outmsg = 2, 'CRITICAL - The Number of audit entries is {0}. Unexpected result from audit \
            job table.'.format(audit_count_result)
    else:
        status, outmsg = 2, 'CRITICAL - Cannot establish a connection to primary database.'
    return status, outmsg


def main():
    try:
        status, msg = get_aduit_count_status(*check_arg())
    except Exception, err:
        status, msg = 2, "CRITICAL - {0}".format(str(err))
    print(msg)
    exit(status)


if __name__ == '__main__':
    main()
