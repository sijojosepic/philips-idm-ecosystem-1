#! /usr/bin/env python

#################################################################################
#
# Plugin name : check_hp_san.py
# Author      : manoj.chandrankutty@philips.com
# Date        : 5 Dec 2017
# Usage       : ./check_hp_san.py -H hostname -u username -p password
#               -c 'show controllers' -x 'controllers'
# Description : This plugin is aimed to have a better monitoring of HP SAN.
#               The original plugin was too verbose and was not giving the
#               response in the correct format.
#
#               Paramaters monitored are
#                   1. show controllers
#                   2. show disks
#                   3. show volumes
#                   4. show power-supplies
#
#               The plugin takes the usual paramters like hostname, username,
#               password and along with this there are 2 additional parameters.
#               They are the command and xpath.
#               command : The command to be executed using SSH.
#               xpath   : Upon executing the command, the response is in an xml
#                         format. The xpath query shoud be for the nodes with a
#                         basetype="controllers" attribute. In this case it wou
#                         ld be "controllers"
#
################################################################################


import sys
import argparse
import xml.etree.ElementTree as ET
import shlex

from subprocess import Popen
from subprocess import PIPE as pp


XPATH_STR = ".//OBJECT[@basetype='{0}']/PROPERTY[@name='health']"

def get_xpath_str(replace_str):
    return XPATH_STR.format(replace_str)


def cleanup_xml(in_xml):
    in_list = in_xml.splitlines()
    for ctr in range(2):
        in_list.pop(0)
    in_list.pop()
    return ''.join(in_list)


def get_cmd_formatted(command):
    return '_'.join([cmd.strip() for cmd in command.split(' ') if cmd != ''])


# works like butter in 2.7 but :( in 2.6
# def ok_status(elem_list):
#     return all([1 if elem.text == 'OK' else 0 for elem in elem_list])


def ok_status(root, xpath):
    unhealthy_list = []
    healthy_list_cnt = 0
    o_nodes = root.findall('OBJECT')
    for o_node in o_nodes:
        if o_node.attrib.get('basetype') == xpath:
            healthy_list_cnt += 1
            p_nodes = o_node.findall('PROPERTY')
            for p_node in p_nodes:
                if p_node.attrib.get('name') == 'health':
                    if p_node.text != 'OK':
                        unhealthy_list.append(o_node)
    return (len(unhealthy_list) == 0, unhealthy_list, healthy_list_cnt)


# works like butter in 2.7 but :( on 2.6
# def get_formatted_status(command, parent_list):
#     def node_text(el, name):
#         return el.findall(".//PROPERTY[@name='"+ name  +"']")[0].text
#
#     def formatted_response(parent_list):
#         unhealthy =  [el for el in parent_list if node_text(el, 'health') != 'OK']
#         response = []
#         for el in unhealthy:
#             response.append('Health Status  : ' + node_text(el, 'health'))
#             response.append('Serial No.     : ' + node_text(el, 'serial-number'))
#             response.append('Reason         : ' + node_text(el, 'health-reason'))
#             response.append('Recommendation : ' + node_text(el, 'health-recommendation'))
#             return '\n'.join(response)
#
#     return 2, formatted_response(parent_list)


def get_formatted_status(unhealthy_list):
    response = []
    for ul in unhealthy_list:
        for node in ul.findall('PROPERTY'):
            node_name = node.attrib.get('name')
            if node_name in ['health', 'serial-number', 'health-reason', 'health-recommendation']:
                if node.text:
                    response.append(node_name + ' : ' + node.text)
        response.append('*' * 130)
    return 2, ' | '.join(response)


def get_drive_count_status(drive_count):
    return drive_count % 12 == 0


def get_san_status(hostname, username, password, command, xpath):
    try:
        addl_checks = dict()
        addl_checks['show_disks'] = get_drive_count_status
        pw_list = [password + '!', password]

        for pw in pw_list:
            cmd = 'sshpass -p ' + pw + ' ssh -o StrictHostKeyChecking=no ' + username + '@' + hostname
            p_out = Popen(shlex.split(cmd) + [command], stdout=pp, stderr=pp)
            xml_out, _ = p_out.communicate()
            if xml_out:
                break
            xml_out = ''

        if xml_out == '':
            return 2, 'Unable to connect to SAN. Invalid credentials.'

        tree = ET.ElementTree(ET.fromstring(cleanup_xml(xml_out)))
        root = tree.getroot()
        status, unhealthy_list, healthy_list_cnt = ok_status(root, xpath)
        if not status:
            return get_formatted_status(unhealthy_list)

        expected_disks = lambda x: ((x / 12) + 1) * 12

        #additional check needed for 'show disks' command
        if command == 'show disks':
            addl_status = addl_checks[command.replace(' ', '_')](healthy_list_cnt)
            if addl_status == False:
                ret_msg = 'Drive(s) missing. Expected disk(s): {1}. Available disk(s): {0}'
                return 2, ret_msg.format(healthy_list_cnt, expected_disks(healthy_list_cnt))

        return 0, 'HP SAN : {0} health is bright and sunny.'.format(xpath)
    except Exception as ex:
        return 2, 'HP SAN - Critical error. ' + str(ex)


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Advanced HP SAN monitoring')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of HP SAN.')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of HP SAN.')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of HP SAN.')
    parser.add_argument('-c', '--command', default='show controllers',
                        help='The command to be executed on HP SAN.')
    parser.add_argument('-x', '--xpath', default='controllers',
                        help='The id of xpath query for the XML returned on SSH.')
    results = parser.parse_args(args)

    return (
        results.hostname,
        results.username,
        results.password,
        results.command,
        results.xpath
    )


def main():
    state, msg = get_san_status(*check_args())
    print(msg)
    sys.exit(state)

if __name__ == "__main__":
    main()
