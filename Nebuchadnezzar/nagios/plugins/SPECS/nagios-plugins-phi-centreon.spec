# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-phi-centreon
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugins from Centreon.

Group:     none
License:   GPL
URL:       http://www.centreon.com/
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   centreon-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins

Requires:  perl(Config::IniFiles)
Requires:  perl(Getopt::Long)
Requires:  perl(Net::SNMP)

Provides:  nagios-plugins-phi-centreon = %{version}-%{release}

%description
Plugins from Centreon

%prep
%setup -q -n centreon-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%define plugins_src %{_builddir}/centreon-%{version}/plugins/src
%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__cp} -R %{plugins_src}/Centreon  %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_cpu %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_loadaverage %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_memory %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_multiple_process %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_packetErrors %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_process %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_process_detailed %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_remote_storage %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_string %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_TcpConn %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_traffic %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_uptime %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{plugins_src}/check_centreon_snmp_value %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0644 %{plugins_src}/centreon.conf %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0644 %{plugins_src}/centreon.pm %{buildroot}%{nagiospluginsdir}/
%{__sed} -i 's#@NAGIOS_PLUGINS@#%{nagiospluginsdir}#g' %{buildroot}%{nagiospluginsdir}/{check_centreon_snmp_,centreon}*
%{__sed} -i 's#@CENTPLUGINS_TMP@#/tmp#g' %{buildroot}%{nagiospluginsdir}/{check_centreon_snmp_,centreon}*
%{__sed} -i 's#@INSTALL_DIR_NAGIOS@#%{_datadir}/nagios#g' %{buildroot}%{nagiospluginsdir}/{check_centreon_snmp_,centreon}*
%{__sed} -i 's#@NAGIOS_ETC@#/%{_sysconfdir}/nagios#g' %{buildroot}%{nagiospluginsdir}/{check_centreon_snmp_,centreon}*

%clean
rm -rf %{buildroot}


%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_cpu
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_loadaverage
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_memory
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_multiple_process
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_packetErrors
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_process
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_process_detailed
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_remote_storage
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_string
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_TcpConn
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_traffic
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_uptime
%attr(0755,root,root) %{nagiospluginsdir}/check_centreon_snmp_value
%attr(0644,root,root) %{nagiospluginsdir}/centreon.pm
%config %{nagiospluginsdir}/centreon.conf
%{nagiospluginsdir}/Centreon


%changelog
* Fri Oct 31 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
