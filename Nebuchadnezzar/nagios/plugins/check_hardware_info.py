#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import zlib
import logging
import linecache
import json
import redis
from base64 import b64decode
from lxml import objectify
from scanline.utilities.http import HTTPRequester
from scanline.utilities.dns import get_address
from scanline.utilities.wmi import WMIHostDriller
from scanline.product.isp import ISPProductScanner, ISPAuthenticationError

logger = logging.getLogger(__name__)

SITEID_FILE = '/etc/siteid'
RETRIEVE = '<Retrieve><Host>{hostname}</Host><ConfigurationName>iSyntaxServer\\{config}</ConfigurationName></Retrieve>'
MACHINEMAP = '<Message><GetStackMachineMap/></Message>'
MACHINEMAP_URL = 'https://{hostname}/DicomServices/StudyLocationServices/studylocationproxyservice.ashx'
RETRIEVE_URL = 'https://{hostname}/Infrastructureservices/configurationservice/configurationservice.ashx'
STUDY_CONFIG = {'MSMQ_COMMON': 'MSMQCommon',
                'STRATAGY_CONFIG': 'StrategyConfiguration',
                'LOCATION': 'Location'}
TOKEN = ''
SITEID = ''
VIGILANT_URL = 'http://vigilant/'
HOST_URL = 'pma/facts/{site_id}/hosts'
MODULE_URL = 'pma/facts/{site_id}/modules/ISP/keys/module_type'
FACT_URL = 'pma/facts/{site_id}/nodes'



def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=False, help='The user name of the server', default='')
    parser.add_argument('-P', '--password', required=False, help='The password for the server', default='')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password)

def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid

def get_xml_blob(data):
    packet = b64decode(data)
    xml_stream = zlib.decompress(packet[4:])
    xml_stream = ''.join(xml_stream.split('\r\n'))
    xml_obj = objectify.fromstring(xml_stream)
    return xml_obj

def get_data(url, payload, encrypt=False):
    headers = {'content-type': 'text/xml'}
    headers.update(TOKEN)
    http_obj = HTTPRequester()
    result = http_obj.suppressed_post(url, payload, headers=headers)
    if encrypt:
        result = objectify.fromstring(result.content)
        result = get_xml_blob(result.Configuration.text)
    return result

def get_machine_map(url):
    response = get_data(url, MACHINEMAP)
    return response.content

def get_machine_map_data(hostname):
    machine_map = []
    logger.info('Trying to fetch machine-map configuration')
    machinemap_blob = get_machine_map(MACHINEMAP_URL.format(hostname=hostname))
    xml_obj = objectify.fromstring(machinemap_blob)
    for node in xml_obj.GetStackMachineMapResponse.stackMachineMap.iterchildren():
        machine_map.append({'MachineName': node.MachineName,
                            'ModuleType': node.ModuleType,
                            'StackUid': node.StackUid,
                            'Offline': node.Offline})
    return machine_map

def get_stratagies(url, hostname, study):
    payload = RETRIEVE.format(hostname=hostname, config=study)
    response = get_data(url, payload, encrypt=True)
    return response

def hardware_config(hostname, username, password, strategy_stack):
    msg_list = []
    msg_template = 'This node {node} have {ram}GB RAM and {cpu} cores CPU'
    cpu_cmd = 'SELECT * FROM Win32_Processor'
    ram_cmd = 'SELECT Capacity FROM CIM_PhysicalMemory'
    status = 0
    outmsg = 'OK - RAM and CPU Cores configured properly.'

    try:
        machine_map = get_machine_map_data(hostname)
    except Exception as s:
        logger.error('Error parsing xml {err}'.format(err=s))
        status = 2
        outmsg = 'CRITICAL - Service check failed to read the hardware configurations.'
        return status, outmsg

    active_stack_list = [stack['StackUid'] for stack in strategy_stack if stack['IsStackActive']]
    waiting_stack_list = [stack['StackUid'] for stack in strategy_stack if stack['IsStackUnused']]
    full_stack = active_stack_list + waiting_stack_list

    host_list = [str(machine['MachineName']) for machine in machine_map
                 if machine['StackUid'] in full_stack]

    if hostname not in set(host_list):
        status = 0
        outmsg = 'OK - Host is not an active archive.'
        return status, outmsg

    driller = WMIHostDriller(host=hostname, user=username, password=password)
    ram_out = driller.query_wmi(ram_cmd)
    ram = 0
    try:  # there could be multiple ram slots
        for ram_slot in ram_out:
            ram += int(ram_slot['Capacity']) / (1024 * 1024 * 1024)
    except:
        ram = int(ram_out.next()['Capacity']) / (1024 * 1024 * 1024)
    cpu_out = driller.query_wmi(cpu_cmd)
    cpu = sum([int(i['NumberOfCores']) for i in cpu_out])
    if ram < 16 or cpu < 8:
        msg_list.append((hostname, ram, cpu))
    if msg_list:
        status = 2
        prefix_msg = '''This hardware configuration may result in slow study migration rate and image access.
                        This node does  not meet the recommended RAM and CPU configuration
                        as per the threshold (RAM : 16 GB, CPU : 8 vcpu):\n'''
        msg = ','.join([msg_template.format(node=data[0], ram=data[1], cpu=data[2]) for data in msg_list])
        outmsg = prefix_msg + msg
    return status, outmsg


def get_backoffice_data(fact_type='host'):
    host_url = VIGILANT_URL + HOST_URL.format(site_id=SITEID)
    module_url = VIGILANT_URL + MODULE_URL.format(site_id=SITEID)
    fact_url = VIGILANT_URL + FACT_URL.format(site_id=SITEID)
    fact_dict = {'host': host_url, 'module': module_url, 'fact': fact_url}
    http_obj = HTTPRequester()
    result = http_obj.suppressed_get(fact_dict[fact_type])
    return json.loads(result.content)

def get_hardware_info(hostname, username, password):
    strategy_stack = []
    global TOKEN
    global SITEID
    version = 0.0
    SITEID = get_siteid()

    redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)
    infra_host = [key.split('#')[1] for key in redis_con.keys(pattern="*ISPACS*") if key.split('#')[-1] == '128']
    if not infra_host:
        facts = get_backoffice_data('module')['result']
        logger.info('Fetching Infra nodes for {host}'.format(host=hostname))
        infra_host = [host['hostname'] for host in facts if host['ISP']['module_type'] == 128]
    infra_host = infra_host[-1]

    status = 2
    outmsg = 'CRITICAL - Error while creating TOKEN to connect to ISPACS.'
    scanned = ISPProductScanner(scanner='ISP', address=infra_host, username=None, password=None)
    try:
        logger.info('Fetching host details for {host}'.format(host=hostname))
        if scanned.isp:
            TOKEN = scanned.isp.get_auth_header(scanned.isp.get_auth_token())
    except ISPAuthenticationError, AttributeError:
        logger.error('Error while fetching host details')
        return status, outmsg

    if not TOKEN:
        return status, outmsg

    if scanned.isp.software_version:
        version = scanned.isp.software_version
        version = float('.'.join(version.split(',')[-2:]))

    if TOKEN.get('Cookie', '') or not version >= 553.20:
        status = 0
        outmsg = 'OK - Service check is not applicable.'
        return status, outmsg

    try:
        retrieve_url = RETRIEVE_URL.format(hostname=infra_host)
        if TOKEN.get('Cookie', ''):
            retrieve_url = RETRIEVE_URL.format(hostname=get_address(infra_host))
        logger.info('Trying to fetch stratagy configuration')
        startagies_blob = get_stratagies(retrieve_url, infra_host.lower(), STUDY_CONFIG['STRATAGY_CONFIG'])
        stratagy_stream = startagies_blob.AvailableStacks.iterchildren()

        for node in stratagy_stream:
            strategy_stack.append({'StackUid': node.StackUid,
                                   'IsStackIncludedInStrategy': node.IsStackIncludedInStrategy,
                                   'IsStackActive': node.IsStackActive,
                                   'IsStackRetired': node.IsStackRetired,
                                   'IsStackUnused': node.IsStackUnused,
                                   'SequenceNumber': node.SequenceNumber})

    except Exception as s:
        logger.error('Error parsing xml {err}'.format(err=s))
        status = 2
        outmsg = 'CRITICAL - Service check failed to read the hardware configurations.'
        return status, outmsg

    return hardware_config(hostname, username, password, strategy_stack)


def main():
    state, msg = get_hardware_info(*check_arg())
    print(msg)
    exit(state)


if __name__ == '__main__':
    main()
