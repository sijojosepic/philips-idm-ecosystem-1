#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import zlib
import logging
import linecache
import json
import redis
from base64 import b64decode
from lxml import objectify
from itertools import chain
from scanline.utilities.http import HTTPRequester
from scanline.utilities.dns import get_address
from scanline.product.isp import ISPProductScanner, ISPAuthenticationError
from phimutils.resource import NagiosResourcer

logger = logging.getLogger(__name__)

SITEID_FILE = '/etc/siteid'
DISCOVERY = {
    'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/threshold.cfg',
    'SECRET_FILE': '/etc/philips/secret'
}
RETRIEVE = '<Retrieve><Host>{hostname}</Host><ConfigurationName>iSyntaxServer\\{config}</ConfigurationName></Retrieve>'
MACHINEMAP = '<Message><GetStackMachineMap/></Message>'
MACHINEMAP_URL = 'https://{hostname}/DicomServices/StudyLocationServices/studylocationproxyservice.ashx'
RETRIEVE_URL = 'https://{hostname}/Infrastructureservices/configurationservice/configurationservice.ashx'
STUDY_CONFIG = {'MSMQ_COMMON': 'MSMQCommon',
                'STRATAGY_CONFIG': 'StrategyConfiguration',
                'LOCATION': 'Location'}
TOKEN = ''
SITEID = ''
VIGILANT_URL = 'http://vigilant/'
HOST_URL = 'pma/facts/{site_id}/hosts'
MODULE_URL = 'pma/facts/{site_id}/modules/ISP/keys/module_type'
FACT_URL = 'pma/facts/{site_id}/nodes'



def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The name of the server originating the message')
    parser.add_argument('-A', '--hostaddress', required=True, help='The address of the server originating the message')
    parser.add_argument('-O', '--options', required=False, help='argument to post')
    parser.add_argument('-S', '--study', required=False, help='argument to post', default='')
    parser.add_argument('-U', '--username', required=False, help='The user name of the server', default='')
    parser.add_argument('-P', '--password', required=False, help='The password for the server', default='')
    results = parser.parse_args(args)

    return (results.hostname, results.hostaddress, results.options, results.study,
            results.username, results.password)


def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_xml_blob(data):
    packet = b64decode(data)
    xml_stream = zlib.decompress(packet[4:])
    xml_stream = ''.join(xml_stream.split('\r\n'))
    xml_obj = objectify.fromstring(xml_stream)
    return xml_obj


def get_data(url, payload, encrypt=False):
    headers = {'content-type': 'text/xml'}
    headers.update(TOKEN)
    http_obj = HTTPRequester()
    result = http_obj.suppressed_post(url, payload, headers=headers)
    if encrypt:
        result = objectify.fromstring(result.content)
        result = get_xml_blob(result.Configuration.text)
    return result


def get_machine_map(url):
    response = get_data(url, MACHINEMAP)
    return response.content


def get_machine_map_data(hostname):
    machine_map = []
    logger.info('Trying to fetch machine-map configuration')
    machinemap_blob = get_machine_map(MACHINEMAP_URL.format(hostname=hostname))
    xml_obj = objectify.fromstring(machinemap_blob)
    for node in xml_obj.GetStackMachineMapResponse.stackMachineMap.iterchildren():
        machine_map.append({'MachineName': node.MachineName,
                            'ModuleType': node.ModuleType,
                            'StackUid': node.StackUid,
                            'Offline': node.Offline})
    return machine_map


def get_stratagies(url, hostname, study):
    payload = RETRIEVE.format(hostname=hostname, config=study)
    response = get_data(url, payload, encrypt=True)
    return response


def active_config(archive_count, strategy_stack):
    status = 0
    outmsg = 'OK - Number of active archive stacks is meeting the configured count of {count}.'.format(count=archive_count)
    active_stack_list = len([stack['StackUid'] for stack in strategy_stack if stack['IsStackActive']])
    if not archive_count or not archive_count == active_stack_list:
        status = 2
        msg = 'CRITICAL - Number of active archive stacks of {active} not meeting the configured count of {count}'
        outmsg = msg.format(count=archive_count, active=active_stack_list)
    return status, outmsg


def mirror_config(hostname, mirror_host, mirror_stack, strategy_stack):
    if not mirror_host:
        status = 0
        outmsg = 'OK - The site is not enabled with Archive mirroring.'
        return status, outmsg

    status = 0
    outmsg = 'OK - All archive stacks are mapped with archive mirror stacks.'
    missing_stack = []
    active_stack_list = (stack['StackUid'] for stack in strategy_stack if stack['IsStackActive'])
    waiting_stack_list = (stack['StackUid'] for stack in strategy_stack if stack['IsStackUnused'])
    persist_stack_list = [stack['PersistentStackGuid'] for stack in mirror_stack]
    mirror_stack_list = [stack['MirrorStackGuid'] for stack in mirror_stack]
    for stack in chain(active_stack_list, waiting_stack_list):
        if stack not in persist_stack_list + mirror_stack_list:
            missing_stack.append(stack)

    try:
        machine_map = get_machine_map_data(hostname)
    except Exception as s:
        logger.error('Error parsing xml {err}'.format(err=s))
        status = 2
        outmsg = 'CRITICAL - Service check unable to read the archive-mirror mapping configurations.'
        return status, outmsg

    missing_stack = [(str(machine['MachineName']), machine['StackUid']) for machine in machine_map
                     if machine['StackUid'] in missing_stack]

    if missing_stack:
        status = 2
        outmsg = 'CRITICAL - Missing Mirror mapping! Following archive stacks do not have corresponding mirror stacks.\n'
        for node, stack in missing_stack:
            outmsg += ''.join([node, '--', stack.text, '\n'])

    return status, outmsg

def get_msg(profile_dict, cache_host, missing_node):
    outmsg = ''
    extra_msg = ''
    msg = '\nMigration chain for following remote locations are configured correctly:'
    for key, value in profile_dict.items():
        if set(value) & set(cache_host):
            rem_msg = '\n- {key} :'.format(key=key)
            for val in value:
                if val in cache_host and not val in missing_node:
                    node_msg = '\n\t- {val}'.format(val=val)
                    outmsg = ''.join([outmsg, rem_msg, node_msg])

    if outmsg:
        outmsg = ''.join([msg, outmsg])

    msg = '\nFollowing remote locations do not have any cache node:'
    for key, value in profile_dict.items():
        if not set(value) & set(cache_host):
            extra_msg = ''.join([extra_msg, '\n\t- {key}'.format(key=key)])
    if extra_msg:
        outmsg = ''.join([outmsg, msg, extra_msg])
    return outmsg


def migration_chain(cache_host, cache_list, server_list, profile_dict):
    final_dict = {}
    if not server_list:
        return 0, 'OK - None of the remote locations have cache nodes.'

    status = 0
    outmsg = 'OK - The Remote cache nodes are corrrectly configured in Migration chain.'
    msmq_cache = [node.split(':')[-1].split('\\')[0] for node in cache_list]
    location_cache = [node for node in cache_host if node in server_list]
    missing_node = list(set(msmq_cache).symmetric_difference(set(location_cache)))
    if missing_node:
        status = 2
        outmsg = '''CRITICAL - The Remote cache node/s is/are not part of Migration chain
                    The following Remote location's cache node/s is/are not part of migration chain.'''
        for key, value in profile_dict.items():
            final_dict[key] = list(set(missing_node) & set(value))
        for key, value in final_dict.items():
            temp = '\n{key} :'.format(key=key)
            for val in value:
                temp_msg = ''.join([temp, '\n\t- {val}'.format(val=val)])
                outmsg = ''.join([outmsg, temp_msg])

    # extra_msg = '\nMigration chain for following remote locations are configured correctly:'
    addon_msg = get_msg(profile_dict, cache_host, missing_node)
    if addon_msg:
        outmsg = ''.join([outmsg, addon_msg])

    return status, outmsg

def get_threshold(study):
    secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
    nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
    logger.info('Fetching value for $WAITING_WARNING$')
    warning = nr.get_resource('$WAITING_WARNING$')
    logger.info('Fetching value for $WAITING_CRITICAL$')
    critical = nr.get_resource('$WAITING_CRITICAL$')
    logger.info('Fetching value for $WAITING_THRESHOLD$')
    threshold = nr.get_resource('$WAITING_THRESHOLD$')
    if study and not TOKEN.get('Cookie', ''):
        logger.info('Fetching value for ${study}_WARNING$'.format(study=study))
        warning = nr.get_resource('${study}_WARNING$'.format(study=study))
        logger.info('Fetching value for ${study}_CRITICAL$'.format(study=study))
        critical = nr.get_resource('${study}_CRITICAL$'.format(study=study))
        logger.info('Fetching value for ${study}_WAITING$'.format(study=study))
        threshold = nr.get_resource('${study}_WAITING$'.format(study=study))
    return warning, critical, threshold

def waiting_stack(strategy_stack, study):
    warning, critical, threshold = get_threshold(study)
    waiting_stack_list = [stack['StackUid'] for stack in strategy_stack if stack['IsStackUnused']]
    logger.info('Fetched waiting stack list are {stack}'.format(stack=waiting_stack_list))
    waiting_count = len(waiting_stack_list)
    status = 0
    outmsg = 'OK - Found {waiting} number of waiting stacks meeting threshold.'.format(waiting=waiting_count)

    if waiting_count <= int(critical):
        status = 2
        msg = 'CRITICAL -Found {waiting} waiting stacks not meeting required threshold of {threshold}'
        outmsg = msg.format(waiting=waiting_count, threshold=threshold)
    elif waiting_count <= int(warning) and waiting_count > int(critical):
        status = 1
        msg = 'WARNING - Found {waiting} waiting stacks not meeting threshold of {threshold}'
        outmsg = msg.format(waiting=waiting_count, threshold=threshold)

    return status, outmsg

def get_backoffice_data(fact_type='host'):
    host_url = VIGILANT_URL + HOST_URL.format(site_id=SITEID)
    module_url = VIGILANT_URL + MODULE_URL.format(site_id=SITEID)
    fact_url = VIGILANT_URL + FACT_URL.format(site_id=SITEID)
    fact_dict = {'host': host_url, 'module': module_url, 'fact': fact_url}
    http_obj = HTTPRequester()
    result = http_obj.suppressed_get(fact_dict[fact_type], headers={'X-Requested-With': "True"})
    return json.loads(result.content)


def get_active_stack(hostname, hostaddress, options, study, username, password):
    global TOKEN
    global SITEID
    SITEID = get_siteid()
    version = 0.0
    strategy_stack = []
    mirror_stack = []
    conf_dict = {'migration': 'migration chain',
                 'waiting': 'waiting stack',
                 'active': 'active archive',
                 'mirror': 'migration chain'}

    status = 2
    outmsg = 'CRITICAL - Error while creating TOKEN to connect to ISPACS.'
    scanned = ISPProductScanner(scanner='ISP', address=hostaddress, username=None, password=None)
    try:
        logger.info('Fetching host details for {host}'.format(host=hostaddress))
        if scanned.isp:
            TOKEN = scanned.isp.get_auth_header(scanned.isp.get_auth_token())
    except ISPAuthenticationError, AttributeError:
        logger.error('Error while fetching host details')
        return status, outmsg

    if not TOKEN:
        return status, outmsg

    if scanned.isp.software_version:
        version = scanned.isp.software_version
        version = float('.'.join(version.split(',')[-2:]))

    if options in ['migration', 'mirror'] and TOKEN.get('Cookie', ''):
        status = 0
        outmsg = 'OK - Service check is not applicable.'
        return status, outmsg

    if options == 'active' and not version >= 553.20:
        status = 0
        outmsg = 'OK - Service check is not applicable.'
        return status, outmsg

    redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)
    cache_host = [key.split('#')[1] for key in redis_con.keys(pattern="*ISPACS*") if key.split('#')[-1] == '4']
    mirror_host = [key.split('#')[1] for key in redis_con.keys(pattern="*ISPACS*") if key.split('#')[-1] == '32']
    if options in ['migration', 'mirror'] and not cache_host and not mirror_host:
        facts = get_backoffice_data('module')['result']
        logger.info('Fetching Cache nodes for {host}'.format(host=hostname))
        cache_host = [host['hostname'] for host in facts if host['ISP']['module_type'] == 4]
        logger.info('Fetching Mirror nodes for {host}'.format(host=hostname))
        mirror_host = [host['hostname'] for host in facts if host['ISP']['module_type'] == 32]

    try:
        retrieve_url = RETRIEVE_URL.format(hostname=hostname)
        if TOKEN.get('Cookie', ''):
            retrieve_url = RETRIEVE_URL.format(hostname=get_address(hostname))
        logger.info('Trying to fetch stratagy configuration')
        startagies_blob = get_stratagies(retrieve_url, hostname.lower(), STUDY_CONFIG['STRATAGY_CONFIG'])
        logger.info('Trying to fetch location configuration')
        location_blob = get_stratagies(retrieve_url, hostname.lower(), STUDY_CONFIG['LOCATION'])
        logger.info('Trying to fetch msmq-common configuration')
        msmq_blob = get_stratagies(retrieve_url, hostname.lower(), STUDY_CONFIG['MSMQ_COMMON'])

        stratagy_stream = startagies_blob.AvailableStacks.iterchildren()
        location_stream = location_blob.LocationProfiles.iterchildren()
        msmq_stream = msmq_blob.LocationMigrationChains.iterchildren()
        if options == 'active':
            archive_count = int(startagies_blob.ActiveArchiveNodesPoolSize.text)

        if options == 'migration':
            server_dict = [(node.ProfileName.text, sub_node.text) for node in location_stream
                           for sub_node in node.LocalServers.iterchildren()
                           if node.LocationType == 1]
            profile_list = set([i[0] for i in server_dict])
            profile_dict = dict((el, []) for el in profile_list)
            for elem in server_dict:
                profile_dict[elem[0]].append(elem[1])

            server_list = [node for profile_name, node in server_dict]
            cache_list = [cache_node.text for node in msmq_stream
                          for cache_node in node.CacheTargets.iterchildren()
                          if node.LocationName in profile_dict.keys()]

        for node in stratagy_stream:
            strategy_stack.append({'StackUid': node.StackUid,
                                   'IsStackIncludedInStrategy': node.IsStackIncludedInStrategy,
                                   'IsStackActive': node.IsStackActive,
                                   'IsStackRetired': node.IsStackRetired,
                                   'IsStackUnused': node.IsStackUnused,
                                   'SequenceNumber': node.SequenceNumber})
        for node in msmq_blob.Mirrors.iterchildren():
            mirror_stack.append({'MirrorStackGuid': node.MirrorStackGuid,
                                 'PersistentStackGuid': node.PersistentStackGuid})

    except Exception as s:
        logger.error('Error parsing xml {err}'.format(err=s))
        status = 2
        outmsg = 'CRITICAL - Service check unable to read the {conf} configurations.'.format(conf=conf_dict[options])
        return status, outmsg

    if options == 'active':
        return active_config(archive_count, strategy_stack)
    elif options == 'mirror':
        return mirror_config(hostname, mirror_host, mirror_stack, strategy_stack)
    elif options == 'migration':
        return migration_chain(cache_host, cache_list, server_list, profile_dict)
    elif options == 'waiting':
        return waiting_stack(strategy_stack, study)


def main():
    state, msg = get_active_stack(*check_arg())
    print(msg)
    exit(state)


if __name__ == '__main__':
    main()
