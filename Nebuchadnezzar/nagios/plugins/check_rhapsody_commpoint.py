#!/usr/bin/env python
import re
import sys
import argparse
import requests
import json


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to get message counts in error and hold queues')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The name of the Rahpsody server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the Rahpsody server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the Rahpsody server')
    parser.add_argument('-p', '--password', required=True,
                        help='The passowrd of the Rahpsody server')
    parser.add_argument('-r', '--component_path', required=True,
                        help='The path of the resource to be monitored')
    parser.add_argument('-w', '--warning_val', required=True,
                        help='The warning threshold for the queue')
    parser.add_argument('-c', '--critical_val', required=True,
                        help='The critical threshold for the queue')
    results = parser.parse_args(args)
    return (
        results.hostname,
        results.port,
        results.username,
        results.password,
        results.component_path,
        results.warning_val,
        results.critical_val)


def get_queue_messages(hostname, port, username, password,
                       component_path, warning_val, critical_val):
    headers = {
        'Accept': 'application/json',
        'Accept': 'application/vnd.orchestral.rhapsody.6_1+json',
    }
    auth = (username, password)
    result = {}
    component_paths = component_path.split(',')
    for c_path in component_paths:
        result[c_path] = {}
        component_data = fetch_component_id(hostname, port, auth, c_path)
        try:
            if component_data[1]:
                url = 'https://{0}:{1}/api/commpoint/{2}'.format(hostname, port,
                                                                 component_data[1])
                response = requests.get(url, headers=headers, verify=False,
                                        auth=auth, timeout=30)
                if response.status_code == 200:
                    response_json = json.loads(response.content)['data']
                    out_queue_size = response_json['outQueueSize']
                    in_queue_size = response_json['inQueueSize']
                    status = get_output_state(in_queue_size, out_queue_size,
                                                 warning_val, critical_val)
                    status_msg = '{0} : Message(s) found in inQueue = {1}, outQueue = {2}'
                    result[c_path]['status'] = status
                    result[c_path]['msg'] = status_msg.format(c_path,in_queue_size, out_queue_size)
            else:        
                error_msg = 'Could not fetch attributes for {0}, status code: {1} \n'
                result[c_path]['status'] = 2
                result[c_path]['msg'] = error_msg.format(c_path,component_data[0])
        except Exception:
            result[c_path]['status'] = 2
            result[c_path]['msg'] = 'Could not connect to Raphsody end point {0} \n'.format(hostname)
    return result


def fetch_component_id(hostname, port, auth, component_path):
    url = 'https://{0}:{1}/api/components/find'.format(hostname, port)
    headers = {
        'Accept': 'text/plain',
        'Content-Type': 'text/plain',
    }
    try:
        response = requests.post(url, headers=headers, data=component_path,
                                 verify=False, auth=auth, timeout=30)
        if response.status_code == 200:
            return response.status_code, response.content
        return response.status_code, None
    except Exception:
        return None


def get_output_state(in_queue_size, out_queue_size, warning_val, critical_val):
    warn_condition1 = in_queue_size > warning_val and in_queue_size <= critical_val
    warn_condition2 = out_queue_size > warning_val and out_queue_size <= critical_val
    if in_queue_size <= warning_val and out_queue_size <= warning_val:
        return 0
    elif warn_condition1 and warn_condition2:
        return 1
    elif in_queue_size > critical_val or out_queue_size > critical_val:
        return 2

def get_status(result, warning_val, critical_val):
    status = 0
    status_msg = ''
    for path,service in result.items():
        pathname = path.split('/')[-2]
        if int(service['status']) > status:
            status = int(service['status'])
        if 'inQueue' in service['msg']:
            queue_size = re.findall(r'\d+', service['msg'])
            warn, crit = warning_val, critical_val
            updated_msg = '{0} | {1}-inQueue-Counts={2};{3};{4} {1}-outQueue-Counts={5};{3};{4} \n'
            status_msg += updated_msg.format(service['msg'], pathname, queue_size[-2], warn, crit, queue_size[-1])
        else:
            status_msg += service['msg']
    return status, status_msg


def main():
    hostname, port, username, password, component_path, warning_val,\
    critical_val = check_arg()
    result = get_queue_messages(hostname, port, username, password,
                                    component_path, warning_val, critical_val)
    
    status, msg = get_status(result, warning_val, critical_val)
    print msg
    sys.exit(status)

if __name__ == '__main__':
    main()
