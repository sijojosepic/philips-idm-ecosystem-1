#!/usr/bin/env python

import argparse
import sys
import requests

def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check access for RabbitMq')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of the rabbitmq server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the rabbitmq server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-r', '--protocol', default='https',
                        help='The password of the iis application server')
    results = parser.parse_args(args)

    return (
        results.hostname,
        results.port,
        results.username,
        results.password,
        results.protocol)

def get_rabbitmq_status(hostname, port, username, password, protocol):
	try:
		url = '{0}://{1}:{2}/api/aliveness-test/%2F'.format(protocol, hostname, port)
		auth = (username,password)
		response = requests.get(url,auth=auth,verify=False)
		if response.status_code == 401:
			return 2, 'CRITICAL: Username or Password is not valid'
		elif response.status_code < 200 or response.status_code > 400:
			return 2, 'CRITICAL: RabbitMq is not accessible'
		else:
			return 0, 'Ok! RabbitMq Alive'
	except:
		return 2, 'Error while connecting to {0}'.format(hostname)

def main():
    status, message = get_rabbitmq_status(*check_arg())
    print message
    exit(status)

if __name__ == '__main__':
    main()
    