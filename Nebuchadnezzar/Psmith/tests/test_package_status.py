import unittest
from mock import MagicMock, patch, Mock


class PackageUpdateTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_logging = MagicMock(name='logging')
        self.mock_linecache = MagicMock(name='linecache')
        self.mock_requests = MagicMock(name='requests')
        self.mock_enums = MagicMock(name="enums")
        modules = {
            'tbconfig': self.mock_tbconfig,
            'scanline.component.localhost': self.mock_scanline.component.localhost,
            'phimutils': self.mock_phimutils,
            'phimutils.pcm': self.mock_phimutils.pcm,
            'phimutils.message': self.mock_phimutils.message,
            'linecache': self.mock_linecache,
            'requests': self.mock_requests,
            'logging': self.mock_logging,
            'scanline': self.mock_scanline,
            'scanline.component': self.mock_scanline.component,
            'enums': self.mock_enums
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import package_status
        self.package_status = package_status
        self.PackageUpdate = package_status.PackageUpdate

        self.package_status.VERSION = {'ENDPOINT': "backoffice_url"}
        self.package_status.PackageStatus.STARTED = 'Started'
        self.package_status.PackageStatus.FAILED = 'Failed'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_xml_file(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.assertEqual(pkg_update.xml_file, "somefile.xml")

    def test_file_extension(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.assertEqual(pkg_update.file_extension, ".zip")

    def test_site_id(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.mock_linecache.getline.return_value = "RAJ00"
        self.assertEqual(pkg_update.site_id, 'RAJ00')

    def raise_exception(self, pkg_update):
        try:
            pkg_update.send_data_to_backoffice("backoffice_url", {'site_id': "RAJ00"})
        except Exception:
            return "Exception! Could not post due to missing siteid"

    def test_send_data_to_backoffice_case_one(self):
        # With site ID
        self.mock_linecache.getline.return_value = "RAJ00"
        pkg_update = self.PackageUpdate("somefile.zip")

        mock_message_obj = MagicMock(name='message')
        mock_message_obj.to_dict.return_value = {'site_id': 'RAJ00'}
        self.mock_phimutils.message.Message.return_value = mock_message_obj

        self.mock_requests.post.status_code.return_value = 200
        pkg_update.send_data_to_backoffice("backoffice_url", {'site_id': "RAJ00"})
        self.mock_requests.post.assert_called_once_with("backoffice_url", json={'site_id': "RAJ00"})

    def test_send_data_to_backoffice_case_two(self):
        # Without site ID
        self.mock_linecache.getline.return_value = ""
        pkg_update = self.PackageUpdate("somefile.zip")

        reponse = self.raise_exception(pkg_update)
        self.assertEqual(reponse, "Exception! Could not post due to missing siteid")
        self.mock_requests.post.assert_not_called()

    def test_get_package_data_case_one(self):
        mock_pcm_package_obj = MagicMock(name='pcm_package_obj')
        mock_pcm_package_obj.Name = 'name'
        mock_pcm_package_obj.Version = '1.3.12.0.1862.24'
        self.mock_phimutils.pcm.PCMPackage.return_value = mock_pcm_package_obj

        mock_localhost_obj = MagicMock(name='LocalHost')
        mock_localhost_obj.get_attributes.return_value = {'a': 'b'}
        self.mock_scanline.component.localhost.LocalHost.return_value = mock_localhost_obj

        updator_obj = self.PackageUpdate('something.zip')
        response = updator_obj.get_package_data('falied')
        expected_output = {'facts': {'localhost': {'a': 'b', 'PCM': [{'status': 'falied', 'version': '1,3,12,0,1862,24', 'name': 'name13120'}]}}}

        self.assertEqual(response, expected_output)

        self.mock_phimutils.pcm.PCMPackage.assert_called_once_with('something.xml')
        self.mock_scanline.component.localhost.LocalHost.assert_called_once_with()
        mock_localhost_obj.get_attributes.assert_called_once_with()

    def test_get_package_data_case_two(self):
        self.mock_phimutils.pcm.PCMPackage.side_effect = IOError

        pkg_obj = self.PackageUpdate('something.zip')
        self.assertEqual(pkg_obj.get_package_data('falied'), None)
        self.mock_scanline.component.localhost.LocalHost.assert_not_called()

    def test_update_status_case_one(self):
        pkg_update = self.PackageUpdate("somefile.zip")

        self.PackageUpdate.get_package_data = MagicMock(name='get_package_data')
        self.PackageUpdate.get_package_data.return_value = {'Key1' : 'val1'}
        self.PackageUpdate.send_data_to_backoffice = MagicMock(name='send_data_to_backoffice')

        pkg_update.update_status("Failed")
        self.PackageUpdate.send_data_to_backoffice.assert_called_once_with("backoffice_url", data={'Key1' : 'val1'})
        self.PackageUpdate.get_package_data.assert_called_once_with("Failed")

    def test_update_status_case_two(self):
        # With Empty package data
        self.PackageUpdate.get_package_data = MagicMock(name='get_package_data')
        self.PackageUpdate.get_package_data.return_value = None
        self.PackageUpdate.send_data_to_backoffice = MagicMock(name='send_data_to_backoffice')

        pk_update = self.PackageUpdate("somefile.zip")
        pk_update.update_status("Failed")

        self.PackageUpdate.get_package_data.assert_called_once_with("Failed")
        self.PackageUpdate.send_data_to_backoffice.assert_not_called()

    def test_update_download_status(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.PackageUpdate.update_status = MagicMock(name='update_status')

        pkg_update.update_download_status()
        self.PackageUpdate.update_status.assert_called_once_with(status="Started")

    def test_update_failed_status(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.PackageUpdate.update_status = MagicMock(name='update_status')

        pkg_update.update_failed_status()
        self.PackageUpdate.update_status.assert_called_once_with(status="Failed")

if __name__ == '__main__':
    unittest.main()
