# Define your all enums here!

class PackageStatus:
    SUBMITTED = 'Submitted'
    STARTED = 'Started'
    INPROGRESS = 'In Progress'
    FAILED = 'Failed'
    AVAILABLE = 'Available'
    NONE = 'None'


class FileExtensions:
    PART_SUFFIX = '.part'
    ZIP_SUFFIX = '.zip'
    XML_SUFFIX = '.xml'
