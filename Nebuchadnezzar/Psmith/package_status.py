import os
import requests
import linecache
import logging
from phimutils.pcm import PCMPackage
from phimutils.message import Message
from scanline.component.localhost import LocalHost
from tbconfig import VERSION, SITEID_FILE
from enums import PackageStatus

logger = logging.getLogger(__name__)

class PackageUpdate(object):
    def __init__(self, filename):
        self.__filename = filename

    @property
    def xml_file(self):
        return os.path.splitext(self.__filename)[0] + ''.join('.xml')

    @property
    def file_extension(self):
        return os.path.splitext(self.__filename)[1]

    def get_package_data(self, status):
        """
        Returns package data which will get posted to backoffice
        :param status: Package status
        :return: package data type dict
        """
        package_list = []
        package_data = None

        # If zip file being downloaded which means respected xml is already downloaded.
        # Read the respected xml data to post it to backoffice.
        if self.file_extension == '.zip':
            try:
                xml_file = self.xml_file
                package = PCMPackage(xml_file)
            except IOError as e:
                logger.exception("No Xml found: Failed with Exception: %s", str(e))
                return None
            package_list.append({
                'name': package.Name + ''.join(package.Version.split('.')[:4]),
                'version': package.Version.replace('.', ','),
                'status': status
            })
            data = {'localhost': {'PCM': package_list}}
            data['localhost'].update(LocalHost().get_attributes())
            package_data = {'facts': data}
        return package_data

    @property
    def site_id(self, siteid=None, siteid_file=SITEID_FILE):
        """
        Returns site ID from linecache
        """
        if not siteid:
            siteid = linecache.getline(siteid_file, 1).strip()
        return siteid

    def send_data_to_backoffice(self, url, data):
        """
        Posting data to backoffice
        :param url: Destination url type str
        :param data: type dict
        :return: None
        """
        siteid = self.site_id
        if not siteid:
            raise Exception('Could not post due to missing siteid')
        message = Message(siteid=siteid, payload=data)
        req = requests.post(url, json=message.to_dict())
        req.raise_for_status()

    def update_status(self, status):
        """
        This method sends Downloading started/Failed status to backoffice
        :param status: Package status i.e Downloading started or Failed
        :return: None
        """
        package_data = self.get_package_data(status)

        if package_data:
            self.send_data_to_backoffice(VERSION['ENDPOINT'], data=package_data)

    def update_download_status(self):
        self.update_status(status=PackageStatus.STARTED)

    def update_failed_status(self):
        self.update_status(status=PackageStatus.FAILED)
