from cached_property import cached_property

from scanline.host import HostScanner
from scanline.utilities import config_reader
from phimutils.resource import Encrypter


class DatabaseHostScanner(HostScanner):
    module_name = 'Database'
    general_properties = HostScanner.general_properties.union(['credentials'])

    def __init__(self, hostname, endpoint, username, password, tags=None, **kwargs):
        super(DatabaseHostScanner, self).__init__(hostname, endpoint, tags=tags, **kwargs)
        self.username = username
        self.password = password
        self.dbuser = kwargs.get('dbuser')
        self.dbpassword = kwargs.get('dbpassword')

    @cached_property
    def credentials(self):
        return self.get_credentials()

    @cached_property
    def encrypter(self):
        return Encrypter(config_reader.get_idm_secret())

    def get_credentials(self):
        _credentials = {}
        if self.dbuser:
            _credentials['dbuser'] = self.encrypter.encrypt(self.dbuser)
        if self.dbpassword:
            _credentials['dbpassword'] = self.encrypter.encrypt(self.dbpassword)
        return _credentials
