import logging
from pysphere import VIProperty, VIServer
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


def sizeof_fmt(num):
    for item in ['bytes', 'KB', 'MB', 'GB']:
        if num < 1024.0:
            return "%3.1f%s" % (num, item)
        num /= 1024.0
    return "%3.1f%s" % (num, 'TB')


class vCenterHostScanner(HostScanner):
    module_name = 'vCenter'

    def __init__(self, hostname, endpoint, username, password, tags=None, **kwargs):
        super(vCenterHostScanner, self).__init__(
            hostname, endpoint, tags=tags, **kwargs)
        self.username = username
        self.password = password
        self.host = hostname
        self.server = None

    @property
    def vserver(self):
        if not self.server:
            self.server = VIServer()
            self.server.connect(self.host, self.username, self.password)
        return self.server

    def get_child_facts(self):
        datacenter_fact = {}
        try:
            for dc_mor, dc_name in self.vserver.get_datacenters().items():
                datacenter_fact.update(DataCenter(
                    self.vserver, dc_mor, dc_name).get_facts())
            return {'datacenter': datacenter_fact}
        except:
            return datacenter_fact

    def get_facts(self):
        child_facts = self.get_child_facts()
        if child_facts:
            return {'vCenter': child_facts}
        return child_facts

    def to_dict(self):
        logger.info('Gathering vCenter site facts from host %s', self.hostname)
        result = self.get_facts()
        if result:
            if self.address:
                result['address'] = self.address
            result['vCenter']['endpoint'] = self.endpoint
            if self.tags:
                result['vCenter']['tags'] = self.tags
            return result
        return result


class DataCenter(object):
    hosts = []

    def __init__(self, vserver, dc_mor, dc_name):
        self.vserver = vserver
        self.dc_mor = dc_mor
        self.dc_name = dc_name

    def get_child_facts(self):
        cluster_fact = {}
        for c_mor, c_name in self.vserver.get_clusters(from_mor=self.dc_mor).items():
            cluster_fact.update(
                Cluster(self.vserver, c_mor, c_name).get_facts())
        return {'cluster': cluster_fact}

    def get_child_facts_host(self):
        host_fact = {}
        for h_mor, h_name in self.vserver.get_hosts(from_mor=self.dc_mor).items():
            if h_mor not in DataCenter.hosts:
                host_fact.update(
                    EXSiHost(self.vserver, h_mor, h_name).get_facts())
        return {'host': host_fact}

    def get_facts(self):
        facts = {'name': self.dc_name}
        facts.update(self.get_child_facts())
        host_fact = self.get_child_facts_host()
        if host_fact['host']:
            facts.update(self.get_child_facts_host())
        return {self.dc_mor: facts}


class Cluster(object):
    def __init__(self, vserver, c_mor, c_name):
        self.vserver = vserver
        self.c_mor = c_mor
        self.c_name = c_name

    def get_child_facts(self):
        host_fact = {}
        for h_mor, h_name in self.vserver.get_hosts(from_mor=self.c_mor).items():
            DataCenter.hosts.append(h_mor)
            host_fact.update(
                EXSiHost(self.vserver, h_mor, h_name).get_facts())
        return {'host': host_fact}

    def get_facts(self):
        facts = {'name': self.c_name}
        facts.update(self.get_child_facts())
        return {self.c_mor: facts}


class EXSiHost(object):
    def __init__(self, vserver, h_mor, h_name):
        self.vserver = vserver
        self.h_mor = h_mor
        self.h_name = h_name
        self.props = VIProperty(vserver, h_mor)
        self.facts = {}

    def get_child_facts(self):
        vm_fact = []
        for vm in self.props.vm:
            vm_fact.append(
                VM(self.vserver, vm.name).get_facts())
        return {'vms': vm_fact}

    def get_datastore_info(self):
        store_info = {}
        if hasattr(self.props, 'datastore'):
            for datastore in self.props.datastore:
                store_info.update(EXSiDataStore(datastore).info())
        return {'datastore': store_info}

    def get_product_info(self):
        product_info = {}
        if hasattr(self.props.summary, 'config'):
            if hasattr(self.props.summary.config, 'product'):
                product_info.update(EXSiProduct(
                    self.props.summary.config.product).info())
        return {'product': product_info}

    def get_hardware_info(self):
        hardware_info = {}
        if hasattr(self.props.summary, 'hardware'):
            hardware_info.update(EXSiHardware(
                self.props.summary.hardware).info())
        return {'hardware': hardware_info}

    def get_runtime_info(self):
        runtime_info = {}
        if hasattr(self.props.summary, 'runtime'):
            runtime_info.update(EXSiRuntime(
                self.props.summary.runtime).info())
        return {'runtime': runtime_info}

    def collect_facts(self):
        host_facts = {}
        facts_methods = [self.get_child_facts, self.get_datastore_info,
                         self.get_product_info, self.get_hardware_info, self.get_runtime_info]
        for method in facts_methods:
            host_facts.update(method())
        return host_facts

    def get_facts(self):
        facts = {'name': self.h_name}
        facts.update(self.collect_facts())
        return {self.h_mor: facts}


class EXSiDataStore(object):
    def __init__(self, datastore):
        self.datastore = datastore

    def info(self):
        datastore_info = {}
        info = {'freeSpace': sizeof_fmt(self.datastore.info.freeSpace),
                'capacity': sizeof_fmt(self.datastore.summary.capacity), 'type': self.datastore.summary.type}
        datastore_info[self.datastore.info.name.replace('.', '_')] = info
        return datastore_info


class EXSiProduct(object):
    PRODUCT_ATTR = ('fullName', 'osType', 'vendor', 'version')

    def __init__(self, product):
        self.product = product

    def info(self):
        product_info = {}
        for attr in self.PRODUCT_ATTR:
            product_info[attr] = getattr(self.product, attr)
        return product_info


class EXSiHardware(object):
    HARDWARE_ATTR = ('cpuMhz', 'cpuModel', 'model', 'numCpuCores', 'numCpuPkgs',
                     'numCpuThreads', 'numHBAs', 'numNics', 'uuid', 'vendor')

    def __init__(self, hardware):
        self.hardware = hardware

    def info(self):
        hardware_info = {}
        for attr in self.HARDWARE_ATTR:
            hardware_info[attr] = getattr(self.hardware, attr)
        hardware_info['memorySize'] = sizeof_fmt(self.hardware.memorySize)
        return hardware_info


class EXSiRuntime(object):
    RUNTIME_ATTR = ('connectionState', 'inMaintenanceMode')

    def __init__(self, runtime):
        self.runtime = runtime

    def info(self):
        runtime_info = {}
        for attr in self.RUNTIME_ATTR:
            runtime_info[attr] = getattr(self.runtime, attr)
        return runtime_info


class VM(object):
    SUMMARY_CONFIG_ATTR = ('name', 'guestFullName', 'guestId',
                           'memorySizeMB', 'numCpu', 'numEthernetCards', 'numVirtualDisks')
    RUNTIME_ATTR = ('connectionState', 'powerState')

    def __init__(self, vserver, vm_name):
        self.vm = vserver.get_vm_by_name(vm_name)
        self.facts = {}

    def get_child_facts(self):
        pass

    def get_vm_guest_info(self):
        guest_info = {}
        if hasattr(self.vm.properties, 'guest'):
            guest_info.update(VMGuest(self.vm.properties.guest).info())
        return {'guest': guest_info}

    def get_vm_disk_info(self):
        disk_info = {}
        if hasattr(self.vm.properties.guest, 'disk'):
            count = 1
            for disk in self.vm.properties.guest.disk:
                disk_info.update(
                    {'virtual_disk' + str(count): VMDisk(disk).info()})
                count += 1
        return {'disks': disk_info}

    def overall_status(self):
        return {'overallStatus': self.vm.properties.summary.overallStatus}

    def get_runtime_attr(self):
        runtime_attrs = {}
        for attr in self.RUNTIME_ATTR:
            runtime_attrs[attr] = getattr(self.vm.properties.runtime, attr)
        return runtime_attrs

    def get_nic_info(self):
        nics_dev_info = {}
        for device in self.vm.get_property("devices").values():
            nics_dev_info.update(VMNICDevice(device).info())
        return {'nics': nics_dev_info}

    def get_summary_config_attrs(self):
        summary_attrs = {}
        for attr in self.SUMMARY_CONFIG_ATTR:
            if hasattr(self.vm.properties.summary.config, attr):
                summary_attrs[attr] = getattr(
                    self.vm.properties.summary.config, attr)
        return summary_attrs

    def get_facts(self):
        facts_methods = (self.get_vm_guest_info, self.get_vm_disk_info, self.overall_status,
                         self.get_runtime_attr, self.get_nic_info, self.get_summary_config_attrs)
        for method in facts_methods:
            self.facts.update(method())
        return self.facts


class VMGuest(object):
    GUEST_PROPS = ('guestState', 'guestFamily', 'guestFullName',
                   'guestId', 'hostName', 'ipAddress', 'toolsStatus', 'toolsVersion')

    def __init__(self, guest):
        self.guest = guest
        self._guest_props = {}

    def info(self):
        for prop in self.GUEST_PROPS:
            if hasattr(self.guest, prop):
                self._guest_props[prop] = getattr(self.guest, prop)
        return self._guest_props


class VMDisk(object):
    DISK_PROPS = ('capacity', 'freeSpace')

    def __init__(self, disk):
        self.disk = disk
        self._disk_props = {}

    def info(self):
        for prop in self.DISK_PROPS:
            if hasattr(self.disk, prop):
                self._disk_props[prop] = sizeof_fmt(getattr(self.disk, prop))
        self._disk_props['diskPath'] = self.disk.diskPath
        return self._disk_props


class VMNICDevice(object):
    NIC_ATTR = ('type', 'macAddress', 'summary')
    NIC_DEVICE_TYPES = ('VirtualE1000', 'VirtualE1000e',
                        'VirtualPCNet32', 'VirtualVmxnet', 'VirtualVmxnet3')

    def __init__(self, device):
        self.device = device

    def info(self):
        nic_info = {}
        if self.device.get('type') in self.NIC_DEVICE_TYPES:
            nic_info[self.device['label']] = {}
            for attr in self.NIC_ATTR:
                nic_info[self.device['label']][attr] = self.device.get(attr)
        return nic_info
