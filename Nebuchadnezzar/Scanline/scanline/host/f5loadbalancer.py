from scanline.host import HostScanner
import requests
import logging


logger = logging.getLogger(__name__)


class F5LBHostScanner(HostScanner):
    module_name = 'F5LB'

    def __init__(self, hostname, endpoint, username, password, tags=None, **kwargs):
        super(F5LBHostScanner, self).__init__(hostname, endpoint, tags=tags, **kwargs)
        self.username = username
        self.password = password

    def get_request(self, url):
        response_data = requests.get(url, auth=(self.username, self.password), verify=False)
        if response_data.status_code == 200:
            return response_data.json()
        else:
            return False

    def val_dict(self, **pool_stats):
        if 'status.availabilityState' in pool_stats.keys():
            result = pool_stats['status.availabilityState']['description']
            return result
        for k, v in pool_stats.items():
            if isinstance(v, dict):
                return self.val_dict(**v)
        return False

    def get_facts(self):
        facts = {}
        logger.info('Gathering f5 asset facts from host %s', self.hostname)
        f5_api_url = "https://" + self.hostname + "/mgmt/tm/ltm/pool"
        pool_data = self.get_request(f5_api_url)
        if not pool_data:
            logger.error(f5_api_url+" is down")
            return facts
        if 'items' not in pool_data.keys():
            logger.info("There are no pools")
            return facts
        for pool in pool_data['items']:
            pool_name = pool['name']
            facts[pool_name] = {}
            tilde_path = pool['fullPath'].replace('/', '~')
            pool_stats = self.get_request(f5_api_url + '/' + tilde_path + "/stats")
            if pool_stats:
                pool_status = self.val_dict(**pool_stats)
                facts[pool_name]['status'] = pool_status
            else:
                logger.info(f5_api_url + tilde_path + "/stats is down")
                facts[pool_name]['status'] = 'Not Available'
            pool_members = self.get_request(f5_api_url + '/' + tilde_path + "/members")
            if not pool_members:
                logger.info(f5_api_url + '/' + tilde_path + "/members is down")
                continue
            if len(pool_members['items']) == 0:
                logger.info("There are no pool members in " + pool_name)
                facts[pool_name]['pool_members'] = {}
                continue
            facts[pool_name]['pool_members'] = {}
            for member in pool_members['items']:
                pool_member = member['name']
                facts[pool_name]['pool_members'][pool_member] = {}
                facts[pool_name]['pool_members'][pool_member]['address'] = member['address']
                facts[pool_name]['pool_members'][pool_member]['state'] = member['state']
        return facts

    def get_certs(self):
        facts = {}
        cert_url = "https://" + self.hostname + "/mgmt/tm/sys/file/ssl-cert"
        cert_data = self.get_request(cert_url)
        if not cert_data:
            logger.error(cert_url+" is down")
            return facts
        if 'items' not in cert_data.keys():
            logger.info("There are no certificates installed")
            return facts
        for cert in cert_data['items']:
            cert_name = cert['name'].replace('.crt', '')
            cert_expiry = cert['expirationString']
            facts[cert_name] = {}
            facts[cert_name]['expiration date'] = cert_expiry
        return facts

    def to_dict(self):
        logger.info("F5 Asset Discovery")
        result = {'modules': self.module_name, 'F5LB': {'Pool': self.get_facts(), 'Certificate': self.get_certs()}}
        return result
