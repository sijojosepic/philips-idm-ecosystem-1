import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class LinuxHostScanner(HostScanner):
    module_name = 'Linux'
    module_properties = HostScanner.module_properties.union(['fact'])

    @property
    def fact(self):
        return 'ExampleAPFact'
