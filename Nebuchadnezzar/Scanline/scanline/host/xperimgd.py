import logging
import logging
from cached_property import cached_property

from scanline.host import HostScanner
from scanline.utilities import config_reader
from phimutils.resource import Encrypter


logger = logging.getLogger(__name__)


class XPERIMGDHostScanner(HostScanner):
    module_name = 'XPERIMGD'
    general_properties = HostScanner.general_properties.union(['credentials'])

    def __init__(self, hostname, endpoint, username, password, tags=None, **kwargs):
        super(XPERIMGDHostScanner, self).__init__(hostname, endpoint, tags=tags, **kwargs)
        self.username = username
        self.password = password

    @cached_property
    def credentials(self):
        return self.get_credentials()

    @cached_property
    def encrypter(self):
        return Encrypter(config_reader.get_idm_secret())

    def get_credentials(self):
        _credentials = {}
        if self.username:
            _credentials['username'] = self.encrypter.encrypt(self.username)
        if self.password:
            _credentials['password'] = self.encrypter.encrypt(self.password)
        return _credentials
