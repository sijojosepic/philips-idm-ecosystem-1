import logging

from scanline.product import GenericProductScanner


logger = logging.getLogger(__name__)


class AnalyticsPublisherProductScanner(GenericProductScanner):
    pass
