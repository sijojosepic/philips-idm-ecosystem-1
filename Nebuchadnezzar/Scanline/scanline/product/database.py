from scanline.product import ProductScanner


class DatabaseProductScanner(ProductScanner):

    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        super(DatabaseProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)

    def site_facts(self):
        facts = super(DatabaseProductScanner, self).site_facts()
        return self.segregate_credentials(facts)
