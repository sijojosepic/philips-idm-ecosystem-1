import logging
from scanline.product import ProductScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class WindowsBasedProductScanner(ProductScanner):

    def __init__(self, scanner, address, username, password, domain=None, tags=None, host_scanner=None, **kwargs):
        super(WindowsBasedProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)
        self.username = username
        self.password = password
        self.domain = domain
        self.product_id = kwargs.get('product_id')

    def scan_host(self, host):
        hostname = self.get_hostname(host)
        params = dict(
            hostname=hostname,
            endpoint=self.endpoint,
            username=self.username,
            password=self.password,
            domain=self.domain,
            tags=self.tags
        )
        windows_scanner = WindowsHostScanner(**params)
        result = windows_scanner.to_dict()
        result.update({"product_id": self.product_id})
        if self.host_scanner is not None:
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
        return result
