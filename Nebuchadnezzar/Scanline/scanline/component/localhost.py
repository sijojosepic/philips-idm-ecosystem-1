import platform
from cached_property import cached_property

import requests
from requests.exceptions import RequestException


class LocalHost(object):
    """
        Purpose of this class is to,  figure out certain localhost attributes
        such as OS Version, status HTTP/HTTPS transport, SSL_port status
        etc. Which will be pushed to backoffice during the
        'Administrative__Philips__PCM_Discovery_Transmit__Task__Trigger'
        service check, this information helps to identify
        the secure and non sercure Neb nodes, as well as status of secure
        connection from the neb to vigilant

        @transport - The transport attribute will be fetched from the ansible_pull file
    """
    lhost_attributes = ['transport', 'ssl_status',
                        'distribution', 'kernel', 'python_version']
    ANSIBLE_PULL_FILE = '/etc/cron.d/ansible_pull'
    SECURE_HEALTH_URL = 'https://vigilant/health'

    def __init__(self):
        pass

    @cached_property
    def transport(self):
        return self._get_transport(self.ANSIBLE_PULL_FILE)

    @cached_property
    def ssl_status(self):
        return self._get_ssl_status()

    @cached_property
    def distribution(self):
        return self._get_distribution()

    @cached_property
    def kernel(self):
        return self._get_kernel()

    @cached_property
    def python_version(self):
        return self._get_python_version()

    def _get_transport(self, ab_file_path):
        _transport = ''
        with open(ab_file_path) as fd:
            contents = fd.read()
            if 'http://' in contents:
                _transport = 'HTTP'
            elif 'https://' in contents:
                _transport = 'HTTPS'
        return _transport

    def _get_ssl_status(self):
        try:
            response = requests.get(self.SECURE_HEALTH_URL, verify=False, timeout=5)
            is_ssl_enabled, message = True, getattr(response, 'reason', '')
        except RequestException as e:
            is_ssl_enabled, message = False, str(e)
        return {
            "is_ssl_port_open": is_ssl_enabled,
            "operation": self.SECURE_HEALTH_URL,
            "message": message
        }

    def _get_distribution(self):
        return '-'.join(platform.linux_distribution())

    def _get_kernel(self):
        return platform.release()

    def _get_python_version(self):
        return platform.python_version()

    def get_attributes(self):
        return dict((attr, getattr(self, attr)) for attr in self.lhost_attributes)


if __name__ == '__main__':
    print LocalHost().get_attributes()
