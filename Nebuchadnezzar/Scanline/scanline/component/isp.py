import logging
from scanline.utilities.app_version import AppVersionHTTPScanner, AppVersionHTMLParser, AppVersionJSONRetriever


logger = logging.getLogger(__name__)


class ISPComponent(object):
    def scan(self, hostname):
        logger.info('Scanning for %s on %s', self.app_name, hostname)
        result = AppVersionHTTPScanner(hostname, self.app_name, self.targets).get_version()
        if result:
            logger.info('"%s" found at %s with value %s', self.app_name, hostname, result)
            return result
        logger.info('"%s" on %s not found', self.app_name, hostname)


class AnywhereComponent(ISPComponent):
    app_name = 'IntelliSpace Anywhere'
    targets = [('anywhere/version.html', AppVersionHTMLParser), ('Anywhere/Version.json', AppVersionJSONRetriever)]


class VLCaptureComponent(ISPComponent):
    app_name = 'IntelliSpace VL Capture'
    targets = [('VLCapture/version.json', AppVersionJSONRetriever)]


class CCAComponent(ISPComponent):
    app_name = 'CCA'
    targets = [('CCA/version.json', AppVersionJSONRetriever)]
