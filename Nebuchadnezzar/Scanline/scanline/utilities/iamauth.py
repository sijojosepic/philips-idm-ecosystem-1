import requests
import time
from base64 import b64encode, encodestring
import hmac
import hashlib
from proxy import get_proxy


class HSDPIAMUser(object):
    requests = requests
    req_session = requests.session()
    response = ''
    url = '' # IAM_API_ENDPOINTS['IAM_URL']
    uri_path = ''
    headers = {}
    json = ''
    secret_key = ''
    shared_key = ''
    status_code = ''

    def __init__(self):
        pass

    def post(self):
        request = self.requests
        proxies = get_proxy()
        if proxies:
            request = self.req_session
            request.proxies.update(proxies)
        res = request.post(self.get_url(), json=self.get_payload(), headers=self.get_headers())
        res.raise_for_status()
        self.status_code = res.status_code
        self.response = res.json()

    def create_hmac(self, data, secret):
        h = hmac.new(secret, digestmod=hashlib.sha256)
        h.update(data)
        return h.digest()

    def sig_header(self):
        secret_key_prefix = 'DHPWS'
        sign_time = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())
        secret = ''.join([secret_key_prefix, self.secret_key])
        sig = self.create_hmac(b64encode(sign_time), secret)
        sig_header = ("HmacSHA256;Credential:{0};SignedHeaders:SignedDate;"
                      "Signature:{1}".format(self.shared_key, b64encode(sig)))
        return sig_header

    def get_response(self):
        return self.response

    def get_status(self):
        return self.status_code

    def get_headers(self):
        return self.headers

    def get_payload(self):
        return self.json

    def get_url(self):
        return self.url + self.uri_path


class UserLogin(HSDPIAMUser):
    requests_obj = None
    uri_path = '/security/authentication/token/login'

    def __init__(self, url, secret_key, shared_key, user, password):
        HSDPIAMUser.__init__(self)
        self.url = url
        self.requests_obj = requests
        self.secret_key = secret_key
        self.shared_key = shared_key
        self.user = user
        self.password = password

    def get_payload(self):
        return {"password": self.password, "loginId": self.user}
        # return {
        #     'loginId': self.requests_obj.get('loginId'), 'password': self.requests_obj.get('password')
        # }

    def get_headers(self):
        return {
            'hsdp-api-signature': self.sig_header(),
            'Content-Type': 'application/json',
            'SignedDate': time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()),
            'Accept': 'application/json'
        }


if __name__=='__main__':
    pass
    # url = 'https://iam-integration.us-east.philips-healthsuite.com'
    # secret_key = 'UT/7eVRccYe9IOrna7EJdIwNSZZ6X55QLb+daeGcSA8='
    # shared_key = 'yluKLXeeKKfX/Xuce5g2P7Cpl/wSDjfcONbYiGFkZ38='
    # user = "atul.shrivastav@philips.com"
    # password = "Idm@1234"
    # obj = UserLogin(url, secret_key, shared_key, user, password)
    # obj.post()
    # print obj.get_response()