import redis
import json
import logging
import linecache
import pymssql
from http import HTTPRequester

logger = logging.getLogger(__name__)

QUERY_GET_PRIMARY = """SELECT replica_server_name
                    FROM sys.dm_hadr_availability_replica_states AS drs
                    INNER JOIN sys.availability_replicas AS ar 
                    ON drs.replica_id = ar.replica_id and
                       role_desc='PRIMARY';"""

SITEID_FILE = '/etc/siteid'
VIGILANT_URL = 'http://vigilant/'
HOST_URL = 'pma/facts/{site_id}/hosts'
MODULE_URL_DEFAULT = 'pma/facts/{site_id}/modules/ISP/keys/module_type'
FACT_URL = 'pma/facts/{site_id}/nodes'


class SQLHandler:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    def execute(self, query):
        return self.cursor.execute(query)


def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_backoffice_data(fact_type='host', MODULE_URL=MODULE_URL_DEFAULT):
    SITEID = get_siteid()
    host_url = VIGILANT_URL + HOST_URL.format(site_id=SITEID)
    module_url = VIGILANT_URL + MODULE_URL.format(site_id=SITEID)
    fact_url = VIGILANT_URL + FACT_URL.format(site_id=SITEID)
    fact_dict = {'host': host_url, 'module': module_url, 'fact': fact_url}
    http_obj = HTTPRequester()
    result = http_obj.suppressed_get(fact_dict[fact_type], headers={'X-Requested-With': "True"})
    return json.loads(result.content)


def get_dbnodes():
    redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)
    db_hosts = [key.split('#')[1] for key in redis_con.keys(pattern="*ISPACS*") if key.split('#')[-1] == 'Database']
    if not db_hosts:
        facts = get_backoffice_data('module')['result']
        db_hosts = [host['hostname'] for host in facts if host['ISP']['module_type'] == 'Database']
        if not db_hosts:
            facts = get_backoffice_data(fact_type='module', MODULE_URL='pma/facts/{site_id}/modules/Database')['result']
            db_hosts = [str(host['hostname']) for host in facts]
    return db_hosts


def get_cluster_primary_db(hostname, username, password, database):
    result = ''
    error_connection = []
    db_nodes = get_dbnodes()
    for node in db_nodes:
        try:
            conn = pymssql.connect(server=node, user=username, password=password, database=database, login_timeout=10)
        except Exception as e:
            error_connection.append(node)
            continue
        cursor = conn.cursor()
        cursor.execute(QUERY_GET_PRIMARY)
        result = cursor.fetchall()
        conn.close()
        if result:
            return result[0][0] + hostname[hostname.find('.'):]
    return False


if __name__ == '__main__':
    print get_cluster_primary_db('zvm01_cluster.zvm01.isyntax.net', 'phisqlmonitor', 'Nsah2damsh@60s!', 'msdb')
    print get_dbnodes()
