import requests
import logging

class PCMStats(object):
    def __init__(self):
            self._pcm_components = {}

    def get_pcmcomponent(self, host):
            try:
                url = 'http://{0}:8182/pcm/version'.format(host)
                response = requests.get(url, verify=False, timeout=5)
                data = response.json()
                name = data['productname']
                version = data['productversion']
                self._pcm_components['name'] = name
                self._pcm_components['version'] = version
                return self._pcm_components
            except:
                return {}