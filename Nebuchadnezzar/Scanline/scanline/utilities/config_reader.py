import linecache
import tbconfig
import yaml


def get_idm_secret():
    return linecache.getline(tbconfig.DISCOVERY['SECRET_FILE'], 1).strip()


def discovery_yml_contents():
    with open(tbconfig.DISCOVERY['CONFIGURATION'], 'rb') as f:
        return yaml.load(f)


def get_scanners():
    """
        Get the product scanner information from the discovery.yml file
        which will be used during the utility server discovery for enabling
        the back up for any site, based on its ISEE products subscription
    """
    scanner_set = set()
    for end_point in discovery_yml_contents():
        scanner_set.add(end_point['scanner'].lower())
    return list(scanner_set)
