import winrm
import requests


def connect(hostname, username, password):
    conn = winrm.Session(hostname, auth=(username, password),
                         transport ='ntlm', server_cert_validation='ignore')
    return conn


def check_package(conn, service_name, hostname):
    cmd = 'Get-Service "{0}" -ErrorAction SilentlyContinue'.format(service_name)
    status_code = ''
    msg = 'OK: {0} package is installed successfully.'.format(service_name)
    try:
        response = conn.run_ps(cmd)
    except winrm.exceptions.InvalidCredentialsError:
        status_code = 2
        msg = 'CRITICAL: Invalid credentials for server: {0}'.format(hostname)
    except requests.exceptions.ConnectionError:
        status_code = 2
        msg = 'Failed to establish a new connection.'
    else:
        if response.status_code == 0:
            status_code = 0
        else:
            status_code = 2
            msg = 'CRITICAL: {0} service not found.'.format(service_name)
    return status_code, msg