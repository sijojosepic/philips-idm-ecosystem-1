from time import sleep
import winrm


class WinRM(object):

    def __init__(self, host_name, user, passwd, transport='ntlm', server_cert_validation='ignore'):
        self.session = winrm.Session(host_name, auth=(
            user, passwd), transport=transport, server_cert_validation=server_cert_validation)

    def execute_ps_script(self, ps_script):
        return self.session.run_ps(ps_script)

    def execute_cmd(self, cmd, args=()):
        return self.session.run_cmd(cmd, args)


def extract_credentials(host, user_list, pwd_list, attempt=0):
    """
       This method extracts the right username and password from the given
       list of username and password for the host.
       As wmi call may not be successfull at the first attempt, in the worst case
       function will attempt to figure out the right credentials twice.
    """
    if attempt < 2:
        for i in range(len(user_list)):
            try:
                resp = WinRM(host, user_list[i], pwd_list[i]).execute_cmd('echo test')
                if 'test' in resp.std_out:
                    return {'username': user_list[i], 'password': pwd_list[i]}
            except Exception:
                pass
        else:
            sleep(.5)
            return extract_credentials(host, user_list, pwd_list, attempt + 1)
    return {}
