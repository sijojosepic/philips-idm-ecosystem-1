import unittest
from mock import MagicMock, patch


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class DatabaseProdTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_utilities = MagicMock(name='utilities')
        self.mock_resource = MagicMock(name='resource')
        modules = {
            'logging': MagicMock(name='logging'),
            'scanline.utilities': self.mock_utilities,
            'scanline.host': self.mock_utilities.host,
            'scanline.utilities.http': self.mock_utilities.http,
            'phimutils.resource': self.mock_resource,
            'cached_property': MagicMock(name='cached_property', cached_property=property)}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product import ProductScanner
        from scanline.product.database import DatabaseProductScanner
        DatabaseProductScanner.__bases__ = (Fake.imitate(ProductScanner),)
        self.scanner_obj = DatabaseProductScanner('scanner', '10.55.66.1', 'u1', 'p1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_site_facts(self):
        mock_seg_cred = MagicMock(name='segregate_credentials')
        self.scanner_obj.segregate_credentials = mock_seg_cred
        site_fact = self.scanner_obj.site_facts()
        self.assertEquals(site_fact, mock_seg_cred.return_value)
        self.assertEquals(mock_seg_cred.call_count, 1)
