import unittest
from mock import MagicMock, patch, call



class Fake(object):
    """Create Mock()ed methods that match another class's methods."""
    extra_params = {}

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
            for k, v in Fake.extra_params.iteritems():
                setattr(cls, k, v)
        return cls


class TivoliUtilityTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_utilities = MagicMock(name='utilities')
        self.mock_cached_property = MagicMock(name='cached_property',
                                              cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'scanline.utilities': self.mock_utilities,
            'scanline.utilities.dns': self.mock_utilities.dns,
            'scanline.utilities.wmi': self.mock_utilities.wmi,
            'logging': MagicMock(name='logging')
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host import tivoli_utility
        self.module = tivoli_utility
        from scanline.host.windows import WindowsHostScanner
        self.module.ISEEUtilHostScanner.__bases__ = (Fake.imitate(WindowsHostScanner),)
        self.isee_obj = self.module.ISEEUtilHostScanner('10.55.66.1',
                                                        {'scanner': 'S1'},
                                                        'u1', 'p1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_isee_backups(self):
        mock_extract_isee_backups = MagicMock(name='isee_backups')
        self.isee_obj.extract_isee_backups = mock_extract_isee_backups
        self.assertEqual(self.isee_obj.isee_backups,
                         mock_extract_isee_backups.return_value)

    def test_find_product_name_when_prd_name(self):
        mapping = {'abc': ['abcd']}
        result = self.isee_obj.find_product_name(mapping, 'abcd')
        self.assertEqual(result, 'abc')

    def test_find_product_name_no_prd_name(self):
        mapping = {'abc': ['abcd']}
        result = self.isee_obj.find_product_name(mapping, 'abcdef')
        self.assertEqual(result, None)

    def test_extract_isee_backups(self):
        mock_find_prd_name = MagicMock(name='find_prd_name')
        self.isee_obj.PRODUCT_MAPPING = {'p1': ['p1', 'p2']}
        self.mock_utilities.config_reader.get_scanners.return_value = ['sc1', 'sc2', 'sc3']
        mock_find_prd_name.side_effect = ['p1', None, 'p1']
        self.isee_obj.find_product_name = mock_find_prd_name
        result = self.isee_obj.extract_isee_backups()
        self.assertEqual(result, ['p1'])
        exp_call = [call({'p1': ['p1', 'p2']}, 'sc1'),
                    call({'p1': ['p1', 'p2']}, 'sc2'),
                    call({'p1': ['p1', 'p2']}, 'sc3'),
                    ]
        self.assertEqual(mock_find_prd_name.call_args_list, exp_call)
        self.mock_utilities.config_reader.get_scanners.assert_called_once_with()
