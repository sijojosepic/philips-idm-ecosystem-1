import unittest
from mock import MagicMock, patch


class ScanlineESXiProductScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_host = MagicMock(name='scanline_host')
        self.mock_pyVim = MagicMock(name='pyVim')
        self.mock_pyVmomi = MagicMock(name='pyVmomi')
        modules = {
            'pyVim': self.mock_pyVim,
            'pyVmomi': self.mock_pyVmomi,
            'scanline.host.esxi': self.mock_host.esxi
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.esxi import ESXiProductScanner
        self.ESXiProductScanner = ESXiProductScanner
        self.esxi_scanner = self.ESXiProductScanner('ESXi', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], product_id='PID')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_hosts(self):
        service_instance = self.mock_pyVim.connect.SmartConnect.return_value
        content = service_instance.RetrieveContent.return_value
        container_view = content.viewManager.CreateContainerView.return_value
        container_view.view = ['host1', 'host2']
        self.assertEqual(
            list(self.esxi_scanner.get_hosts()),
            ['host1', 'host2']
        )
        self.mock_pyVim.connect.SmartConnect.assert_called_once_with(host='10.220.3.1', pwd='CryptThis', user='tester')
        content.viewManager.CreateContainerView.assert_called_once_with(
            container=content.rootFolder,
            type=[self.mock_pyVmomi.vim.HostSystem],
            recursive=True
        )
        self.mock_pyVim.connect.Disconnect.assert_called_once_with(service_instance)

    @patch('scanline.product.esxi.ESXiHostScanner')
    def test_get_hostname(self, mock_esxi_scanner):
        mock_host = MagicMock()
        self.assertEqual(
            self.esxi_scanner.get_hostname(mock_host),
            mock_esxi_scanner.get_hostname.return_value
        )
        mock_esxi_scanner.get_hostname.assert_called_once_with(mock_host)

    @patch('scanline.product.esxi.ESXiHostScanner')
    def test_scan_host(self, mock_esxi_scanner):
        mock_esxi_scanner.return_value.to_dict.return_value = {
            'address': '10.4.5.6',
            'ESXi': {'manufacturer': 'IBM', 'model': 'X3550'}
        }
        self.esxi_scanner.get_hostname = MagicMock(return_value='host1')
        self.esxi_scanner.host_scanner = MagicMock()
        self.esxi_scanner.host_scanner.return_value.to_dict.return_value = {'ModR': {'key1': 'val1'}}
        mock_host = MagicMock()
        self.assertEqual(
            self.esxi_scanner.scan_host(mock_host),
            {
                'ESXi': {'manufacturer': 'IBM', 'model': 'X3550'},
                'ModR': {'key1': 'val1'},
                'address': '10.4.5.6',
                'product_id': 'PID'
            }
        )
        self.esxi_scanner.get_hostname.assert_called_once_with(mock_host)
        mock_esxi_scanner.assert_called_once_with(
            endpoint={'scanner': 'ESXi', 'address': '10.220.3.1'},
            vmware_host=mock_host,
            tags=['Prod'],
        )
        self.esxi_scanner.host_scanner.assert_called_once_with(
            endpoint={'scanner': 'ESXi', 'address': '10.220.3.1'},
            hostname='host1',
            password='CryptThis',
            tags=['Prod'],
            username='tester'
        )


if __name__ == '__main__':
    unittest.main()
