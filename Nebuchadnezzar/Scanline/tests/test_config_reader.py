import unittest
from mock import MagicMock, patch, mock_open


class ConfigReaderTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_linecache = MagicMock(name='linecache')
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_yaml = MagicMock(name='yaml')
        modules = {
            'linecache': self.mock_linecache,
            'tbconfig': self.mock_tbconfig,
            'yaml': self.mock_yaml
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import config_reader
        self.module = config_reader
        self.module.DISCOVERY = {'SECRET_FILE': 'etc',
                                 'CONFIGURATION': '/etc/philips/discovery.yml'
                                 }

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_idm_secret(self):
        self.mock_linecache.getline.return_value = 'secret\n'
        self.assertEquals(self.module.get_idm_secret(), 'secret')

    def test_get_idm_secret_plain_text(self):
        self.mock_linecache.getline.return_value = 'secret'
        self.assertEquals(self.module.get_idm_secret(), 'secret')

    def test_discovery_yml_contents(self):
        mock_opn = mock_open(read_data='\n enp:// \n\r')
        with patch('scanline.utilities.config_reader.open', mock_opn, create=True):
            self.assertEqual(self.module.discovery_yml_contents(),
                             self.mock_yaml.load.return_value)

    def test_get_scanners(self):
        mock_discovery_yml_contents = MagicMock(name='yml_contents')
        mock_discovery_yml_contents.return_value = [{'scanner': 'ISP'}]
        self.module.discovery_yml_contents = mock_discovery_yml_contents
        self.assertEqual(self.module.get_scanners(), ['isp'])
