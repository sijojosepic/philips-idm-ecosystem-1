import unittest
from mock import MagicMock, patch, call, mock_open


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls

class DatabaseHostTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_utilities = MagicMock(name='utilities')
        self.mock_resource = MagicMock(name='resource')
        modules = {
            'logging': MagicMock(name='logging'),
            'scanline.utilities': self.mock_utilities,
            'scanline.utilities.dns': self.mock_utilities.dns,
            'phimutils.resource': self.mock_resource,
            'cached_property': MagicMock(name='cached_property', cached_property=property)}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host import HostScanner
        from scanline.host.database import DatabaseHostScanner
        DatabaseHostScanner.__bases__ = (Fake.imitate(HostScanner),)
        self.dbscanner = DatabaseHostScanner
        self.prd_obj = DatabaseHostScanner('scanner', '10.55.66.1', 'u1', 'p1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_credentials(self):
        mock_get_credentials = MagicMock(name='get_credentials')
        self.prd_obj.get_credentials = mock_get_credentials
        self.assertEquals(self.prd_obj.credentials, mock_get_credentials.return_value)
        mock_get_credentials.assert_called_once_with()

    def test_encrypter(self):
        self.assertEquals(self.prd_obj.encrypter, self.mock_resource.Encrypter.return_value)
        self.mock_resource.Encrypter.assert_called_once_with(
            self.mock_utilities.config_reader.get_idm_secret.return_value)
        self.mock_utilities.config_reader.get_idm_secret.assert_called_once_with()

    def test_get_credentials(self):
        self.mock_resource.Encrypter().encrypt.side_effect = ['dbusr', 'dbpass']
        self.prd_obj = self.dbscanner('scanner', '10.55.66.1', 'u1', 'p1', dbuser='dbuser', dbpassword='dbpassword')
        self.assertEquals(self.prd_obj.get_credentials(), {'dbuser': 'dbusr', 'dbpassword': 'dbpass'})
        exp_call = [call('dbuser'), call('dbpassword')]
        self.assertEquals(self.mock_resource.Encrypter().encrypt.call_args_list, exp_call)

    def test_get_credentials_no_dbuser(self):
        self.mock_resource.Encrypter().encrypt.return_value = 'dbpass'
        self.prd_obj = self.dbscanner('scanner', '10.55.66.1', 'u1', 'p1', dbpassword='dbpassword')
        self.assertEquals(self.prd_obj.get_credentials(), {'dbpassword': 'dbpass'})
        self.mock_resource.Encrypter().encrypt.assert_called_once_with('dbpassword')


    def test_get_credentials_no_dbpassword(self):
        self.mock_resource.Encrypter().encrypt.return_value = 'dbuser'
        self.prd_obj = self.dbscanner('scanner', '10.55.66.1', 'u1', 'p1', dbuser='dbuser')
        self.assertEquals(self.prd_obj.get_credentials(), {'dbuser': 'dbuser'})
        self.mock_resource.Encrypter().encrypt.assert_called_once_with('dbuser')

    def test_get_credentials_empty(self):
        self.assertEquals(self.prd_obj.get_credentials(), {})
