import unittest
from mock import MagicMock, patch, call


class ScanlineWinRMTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        modules = {
            'winrm': self.mock_winrm,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import win_rm
        self.module = win_rm

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_WinRM_initialization(self):
        self.module.WinRM('host', 'user', 'passwd')
        self.mock_winrm.Session.assert_called_once_with('host', auth=(
            'user', 'passwd'), transport='ntlm', server_cert_validation='ignore')

    def test_execute_ps_script(self):
        win_rm_obj = self.module.WinRM(
            'host', 'user', 'passwd').execute_ps_script('ps')
        self.mock_winrm.Session().run_ps.assert_called_once_with('ps')
        self.assertEqual(
            win_rm_obj, self.mock_winrm.Session().run_ps.return_value)

    def test_execute_cmd(self):
        win_rm_obj = self.module.WinRM(
            'host', 'user', 'passwd').execute_cmd('ps')
        self.mock_winrm.Session().run_cmd.assert_called_once_with('ps', ())
        self.assertEqual(
            win_rm_obj, self.mock_winrm.Session().run_cmd.return_value)


class ExtractCredentilasTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        modules = {
            'winrm': self.mock_winrm,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import win_rm
        self.module = win_rm
        self.mock_winRM = MagicMock(name='WinRM')
        self.module.WinRM = self.mock_winRM
        self.host = '1.2.3.4'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_extract_credentials_in_first_attempt(self):
        mock_execute_mock = MagicMock(name='execute_cmd')
        mock_execute_mock.std_out = 'test'
        self.mock_winRM.return_value.execute_cmd.return_value = mock_execute_mock
        user_list = ('u1', 'u2')
        pwd_list = ('p1', 'p2')
        response = self.module.extract_credentials(self.host, user_list, pwd_list)
        exp_resp = {'username': 'u1', 'password': 'p1'}
        self.assertEqual(response, exp_resp)
        exp_mock_call = [call('1.2.3.4', 'u1', 'p1')]
        self.assertEqual(self.mock_winRM.call_args_list, exp_mock_call)

    def test_extract_credentials_in_second_attempt(self):
        mock_execute_mock = MagicMock(name='execute_cmd')
        mock_execute_mock.std_out = 'test'
        self.mock_winRM.return_value.execute_cmd.side_effect = [MagicMock(), mock_execute_mock]
        user_list = ('u1', 'u2')
        pwd_list = ('p1', 'p2')
        response = self.module.extract_credentials(self.host, user_list, pwd_list)
        exp_resp = {'username': 'u2', 'password': 'p2'}
        self.assertEqual(response, exp_resp)
        exp_mock_call = [call('1.2.3.4', 'u1', 'p1'), call('1.2.3.4', 'u2', 'p2')]
        self.assertEqual(self.mock_winRM.call_args_list, exp_mock_call)

    def test_extract_credentials_failed(self):
        mock_execute_mock = MagicMock(name='execute_cmd')
        self.mock_winRM.return_value.execute_cmd.return_value = mock_execute_mock
        user_list = ('u1', 'u2')
        pwd_list = ('p1', 'p2')
        response = self.module.extract_credentials(self.host, user_list, pwd_list)
        self.assertEqual(response, {})
        self.assertEqual(mock_execute_mock.std_out.__contains__.call_count, 4)
        exp_call = [call('1.2.3.4', 'u1', 'p1'), call('1.2.3.4', 'u2', 'p2'), call('1.2.3.4', 'u1', 'p1'), call('1.2.3.4', 'u2', 'p2')]
        self.assertEqual(self.mock_winRM.call_args_list, exp_call)

    def test_extract_credentials_exception(self):
        mock_execute_mock = MagicMock(name='execute_cmd')
        mock_execute_mock.std_out = 'test'
        self.mock_winRM.return_value.execute_cmd.side_effect = [Exception(), mock_execute_mock]
        user_list = ('u1', 'u2')
        pwd_list = ('p1', 'p2')
        response = self.module.extract_credentials(self.host, user_list, pwd_list)
        exp_resp = {'username': 'u2', 'password': 'p2'}
        self.assertEqual(response, exp_resp)
        exp_mock_call = [call('1.2.3.4', 'u1', 'p1'), call('1.2.3.4', 'u2', 'p2')]
        self.assertEqual(self.mock_winRM.call_args_list, exp_mock_call)


if __name__ == '__main__':
    unittest.main()

if __name__ == '__main__':
    unittest.main()
