import unittest
from mock import MagicMock, patch, call


class ScanlineISPHostScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(
            name='cached_property', cached_property=property)
        self.mock_component = MagicMock()
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.component.isp': self.mock_component.isp,
            'scanline.component.windows': self.mock_component.windows,
            'scanline.utilities.win_rm': self.mock_component.win_rm,
            'phimutils.resource': self.mock_component.resource
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.isp import ISPHostScanner
        self.ISPHostScanner = ISPHostScanner
        self.mock_isp = MagicMock(name='ISP', spec=['get_federation_info',
                                  'get_module_type', 'software_version', 'identifier', 'noncore_node', 'get_software_version'])
        self.isp_host_scanner = ISPHostScanner(
            'host1', '167.81.183.99', self.mock_isp)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_type(self):
        self.assertEqual(self.isp_host_scanner.module_type,
                         self.mock_isp.get_module_type.return_value)
        self.mock_isp.get_module_type.assert_called_once_with('host1')

    def test_version(self):
        self.assertEqual(self.isp_host_scanner.version,
                         self.mock_isp.software_version)

    def test_identifier(self):
        self.assertEqual(self.isp_host_scanner.identifier,
                         self.mock_isp.identifier)

    @patch('scanline.host.isp.ISPHostScanner.address', '10.2.3.4')
    @patch('scanline.host.isp.ISPHostScanner.noncore_node', None)
    def test_get_components(self):
        self.assertEqual(
            self.isp_host_scanner.get_components(),
            {
                'anywhere': self.mock_component.isp.AnywhereComponent.return_value,
                # Commenting VL Capture as for workaround VL Capture version is not responding for many sites as well as other sites its None
                # 'vlcapture': self.mock_component.isp.VLCaptureComponent.return_value,
                'cca': self.mock_component.isp.CCAComponent.return_value
            }
        )

    @patch('scanline.host.isp.ISPHostScanner.address', None)
    @patch('scanline.host.isp.ISPHostScanner.noncore_node', None)
    def test_get_components_no_address(self):
        self.assertEqual(self.isp_host_scanner.get_components(), {})

    def test_properties_present(self):
        mock_scanner = MagicMock(spec=self.isp_host_scanner)
        for prop in self.ISPHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.ISPHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))

    def test_user_password_encrypter(self):
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username='u1',
                                  password='p1',
                                  secret='secret')
        self.assertEqual(isp.username, 'u1')
        self.assertEqual(isp.password, 'p1')
        self.assertEqual(
            isp.encrypter, self.mock_component.resource.Encrypter.return_value)
        self.mock_component.resource.Encrypter.assert_called_once_with(
            'secret')

    def test_credentials_when_user_empty(self):
        self.mock_component.resource.Encrypter.return_value.encrypt.return_value = 'p1'
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username=None,
                                  password='p1',
                                  secret='secret')
        self.assertEqual(isp.credentials, {'password': 'p1'})
        self.mock_component.resource.Encrypter().encrypt.assert_called_once_with('p1')

    def test_get_credentials_when_pwd_empty(self):
        self.mock_component.resource.Encrypter.return_value.encrypt.return_value = 'u1'
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username='username',
                                  password=None,
                                  secret='secret')
        self.assertEqual(isp.credentials, {'username': 'u1'})
        self.mock_component.resource.Encrypter().encrypt.assert_called_once_with('username')

    def test_credentials(self):
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username='u1',
                                  password='p1',
                                  secret='secret')
        isp.get_credentials = MagicMock(name='get_credentials', return_value={'username': 'u1'})
        self.assertEqual(isp.credentials, {'username': 'u1'})

    def test_get_credentials(self):
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username='u1',
                                  password='p1',
                                  secret='secret')
        self.mock_component.resource.Encrypter.return_value.encrypt.side_effect = [
            'u1', 'p1']
        isp.format_username = MagicMock(name='encrypter', return_value='u')
        self.assertEqual(isp.credentials, {'username': 'u1', 'password': 'p1'})
        isp.format_username.assert_called_once_with('u1')

    def test_get_credentials_db_credentials(self):
        self.mock_isp.get_module_type.return_value = 'Database'
        self.mock_component.resource.Encrypter.return_value.encrypt.side_effect = [
            'u1', 'p1', 'dbuser', 'dbpassword']
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username='u1',
                                  password='p1',
                                  secret='secret',
                                  dbuser='dbuser',
                                  dbpassword='dbpassword')
        credentials = isp.get_credentials()
        exp_encrpyt_call = [call('u1'), call('p1'), call('dbuser'), call('dbpassword')]
        exp_credentials = {'username': 'u1', 'dbuser': 'dbuser', 'password': 'p1', 'dbpassword': 'dbpassword'}
        self.assertEqual(exp_credentials, credentials)
        self.assertEqual(self.mock_component.resource.Encrypter().encrypt.call_args_list, exp_encrpyt_call)
        self.mock_isp.get_module_type.assert_called_once_with('host1')

    def test_get_credentials_dbuser(self):
        self.mock_isp.get_module_type.return_value = 'Database'
        self.mock_component.resource.Encrypter.return_value.encrypt.return_value = 'dbuser'
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  secret='secret',
                                  dbuser='dbuser'
                                  )
        credentials = isp.get_credentials()
        exp_credentials = {'dbuser': 'dbuser'}
        self.assertEqual(exp_credentials, credentials)
        self.mock_isp.get_module_type.assert_called_once_with('host1')
        self.mock_component.resource.Encrypter().encrypt.assert_called_once_with('dbuser')

    def test_get_credentials_dbpassword(self):
        self.mock_isp.get_module_type.return_value = 'Database'
        self.mock_component.resource.Encrypter.return_value.encrypt.return_value = 'dbpassword'
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  secret='secret',
                                  dbpassword='dbpassword')
        credentials = isp.get_credentials()
        exp_credentials = {'dbpassword': 'dbpassword'}
        self.assertEqual(exp_credentials, credentials)
        self.mock_isp.get_module_type.assert_called_once_with('host1')
        self.mock_component.resource.Encrypter().encrypt.assert_called_once_with('dbpassword')

    def test_raw_credentials_tuple(self):
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username=('u1', 'u2'),
                                  password=('p1', 'p2'),
                                  secret='secret')
        self.assertEqual(
            isp.raw_credentials, self.mock_component.win_rm.extract_credentials.return_value)
        self.mock_component.win_rm.extract_credentials.assert_called_once_with(
            'host1', ('u1', 'u2'), ('p1', 'p2'))

    def test_get_federation_status(self):
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username=('u1', 'u2'),
                                  password=('p1', 'p2'),
                                  secret='secret')
        self.mock_isp.get_module_type.return_value = 16
        fed_status = isp.get_federation_status()
        self.assertEqual(
            fed_status, self.mock_isp.get_federation_info.return_value)

    def test_get_federation_status_none(self):
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username=('u1', 'u2'),
                                  password=('p1', 'p2'),
                                  secret='secret')
        self.assertEqual(isp.get_federation_status(), None)

    def test_federation_status(self):
        isp = self.ISPHostScanner('host1', '167.81.183.99',
                                  self.mock_isp, tags='tags',
                                  username=('u1', 'u2'),
                                  password=('p1', 'p2'),
                                  secret='secret')
        federation_mock = MagicMock(name='fed_mock')
        isp.get_federation_status = federation_mock
        self.assertEqual(isp.federation_status, federation_mock.return_value)


if __name__ == '__main__':
    unittest.main()
