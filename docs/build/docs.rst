-------------------
Documentation Build
-------------------

Documentation is built using sphinx.

Set Up
------

Setting up sphinx in the build server ::

    pip install sphinx
    yum install python-graphviz

.. Note:: This should be moved to an ansible role.
