..  _build_manifest:

==============
Build Manifest
==============

A build manifest file, in yaml, is used to drive the creation of packages and run the tests existing in the source. The
build manifest located at build_conf/manifest.yml is read from the fabfile build_scripts/linux/fabfile.py to produce
the packages for IDM Ecosystem.

The manifest yaml file consists of a list of items to build each of the items in a list consists minimally of a name and
and a dir. Where the name refers to the package that is being built, it will by default be used for output pertinent to
the item being built. The name should not contain spaces or special characters.

The dir is the directory inside the source that is to be used as root for the item baing built, and it is used to
reference anything in the item relatively to it.

There are two possible options to produce an rpm using the manifest, "spec" and "setup". The spec type is to be used
when the rpm is to be generated from a spec file. The setup type is to be used when the rpm is to be built by distutils
using a setup.py.

Each item may have a tests element with a list of tests to perform, these are ran using nosetests.

A "thirdparty" element may be used and set to True for third party components, this will result in packages not getting
version information from the build.

If a "skip" element may be used and set to True for items to be skipped during build.

spec
----
The "spec" type built may have the following configuration elements:

version
  The version of the package, build number will be added to this if not third party.
  **REQUIRED**

specfile
  The specfile to use for rpm generation.
  **REQUIRED**

files:
  A list of files to gather for the package, items in this list with url will be downloaded.

generate_source
  A boolean value, stating if the files should be compressed into tgz before building. The default is True.

package
  The package name to be used, if not present, the name will be used.


setup
-----
The "setup" type built may have the following configuration elements:

package
  The package name to be used, if not present, the name will be used.

version
  The version of the package, build number will be added to this if not third party.
  **REQUIRED**

requires
  A string of the packages that the resulting rpm requires, space separated just as setup.py would use in the command
  line.

obsoletes
  A string of the packages that the resulting rpm obsoletes, space separated just as setup.py would use in the command
  line.

arch
  The architecture the resulting rpm should use, the default is 'noarch'.

.. note:: setup requires dir to be the directory where setup.py resides.


tests
-----
If any of the tests in the list fail, the build will be marked as failure.
Each item in the tests list can may have the following configuration elements:

package_name
  The name of the package to test.
  **REQUIRED**

src_dir
  A subdirectory of dir to add as source for the test.

packages_to_test
  A comma separated list as nose would use in the command of the packages to test, if not set package_name will be used.

test_dir
  The subdirectory inside dir where the tests are stored, the default is 'test'.

test_pattern
  The pattern to use to find the test files in the test dir, the default is '*'.


Examples
--------

Fully Populated Spec With Tests
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

    - name: pack1
      dir: Neb/pack1
      spec:
        version: 1.1
        package: package1
        specfile: spec/package1.spec
        files:
          - file1.txt
          - deeper/file2.py
          - http://thirdpartyrepo/pack1/init.sh
        generate_source: True
      tests:
        - src_dir: deeper/deeperstill
          package_name: pack1
          packages_to_test: 'pack1,pack2'
          test_dir: 'testme'
          test_pattern: 'test_pp_*_file.py'
        - src_dir: deeper/deeperstill2
          package_name: pack1

Setup
^^^^^
::

    - name: superpack
      dir: Nebuchadnezzar/SuperPack
      setup:
        version: 0.5
        requires: 'python-lxml python-blist'
        obsoletes: NotSoSuperPack
        arch: x86_64

Skiping an item
^^^^^^^^^^^^^^^
::

    - skip: True
      name: check_xmlelement
      dir: Nebuchadnezzar/nagios/plugins
      spec:
        version: 0.3
        specfile: SPECS/nagios-plugins-xmlelement.spec
        files:
          - check_xmlelement.py

Test Only
^^^^^^^^^
::

    - name: fry
      dir: Sub1/Fry
      tests:
        - package_name: fry1
          test_pattern: '*.py'
