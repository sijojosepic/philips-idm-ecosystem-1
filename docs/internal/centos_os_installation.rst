..  _centos_os_installation:

======================
CentOS OS Installation
======================

Installation Steps
------------------

1. Power on the system with the appropriate ISO mounted or media available for installation to the instance
2. The following set of screen will be presented as the wizard to walk through the base system install

   - Highlight **Install or Upgrade an Eexisting system**

.. image:: ../images/centos_installation_01.png

______

- At the **Disk Found**, Highlight **Skip**, Press **Enter**

.. image:: ../images/centos_installation_02.png

______

- At the **Unsupported Hardware Detected**, This is a known issue and has no side-effects, Press **Enter**

.. image:: ../images/centos_installation_03.png

______

- At the **CentOS 6**, This will begin the guided setup, Press **Next**

.. image:: ../images/centos_installation_04.png

______

- At the Language choice screen, leave the default *English (English)*, Press **Next**

.. image:: ../images/centos_installation_05.png

______

- At the Keyboard choice screen, leave the default *U.S. English*, Press **Next**

.. image:: ../images/centos_installation_06.png

______

- At the Storage Types choice screen, leave the default *Basic Storage Devices*, Press **Next**

.. image:: ../images/centos_installation_07.png

______

- At the Storage Device Warning screen, select *Yes, Discard any data*

.. image:: ../images/centos_installation_08.png

______

- At the Hostname Definition screen, leave the default *localhost.localdomain*, Press **Next**

.. image:: ../images/centos_installation_09.png

______

- At the Timezone screen, leave the default *America/New York*, Press **Next**

.. image:: ../images/centos_installation_10.png

______

- At the Root password screen, set both fields to the root known password, Press **Next**

.. image:: ../images/centos_installation_11.png

______

- At the Installation type screen, leave the default *Replace Existing Linux System(s)*, Press **Next**

.. image:: ../images/centos_installation_12.png

______

- At the Write storage configuration to disk screen, select *Write changes to disk*

.. image:: ../images/centos_installation_13.png

______

- At this point the base system is being installed, when presented with the following screen, Press **Reboot**

.. image:: ../images/centos_installation_14.png
