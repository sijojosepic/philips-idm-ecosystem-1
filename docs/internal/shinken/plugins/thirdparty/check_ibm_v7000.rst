check_ibm_v7000
===============

------------
Intended Use
------------

The intended purpose of this document is to outline the service checks that are available for IBM v7000 SAN and
plugin prerequisites.

Prerequisite
------------
An user account with the name monitor and password configured should be created in IBM v7000 SAN and it should be part
of monitor group.

Execution
---------

The plugin expects the following information as input parameters:

Plugin Required Parameters
""""""""""""""""""""""""""

- IBM v7000 SAN address (IP Address )
- IBM v7000 monitor account and password
- IBM v7000 SAN commands(lsdisk, lsarray, lspowersupply etc..)

The following table defines the configuration of service checks for IBM v7000 devices.

+---------------------+----------------------+-------------------+--------------------+
| Service Check       | Notification Enabled | Warning Threshold | Critical Threshold |
+=====================+======================+===================+====================+
| RAID Status         | Yes                  | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Disk Status         | Yes                  | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Mdisk Status        | Yes                  | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Canister Status     | Yes                  | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Power Supply Status | Yes                  | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Enclosure Status    | Yes                  | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Battery Status      | Yes                  | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+


From:

https://github.com/pasancario/nagios_plugins/tree/master/ibm_v7000


