..  _vmware_guest_information:

========================
VMWare Guest Information
========================

Overview
--------

VMWare Guest Information Script will return the name of the VMWare guest virtual machine object and the name of the
cluster to which the guest VM belongs.

Deployment
----------

VMWare Information script can be deployed either by manually copying the script to Neb Node or by pushing it through the
IDM pipeline.

Execution
---------

The script will be triggered from a job scheduler. The script expects the following information as input parameters:

Required Parameters
"""""""""""""""""""

- vCenter server address (IP Address or FQDN)
- vCenter account name
- vCenter account password

Optional Parameters
"""""""""""""""""""

.. NOTE:: These parameters are optional, but at least one of the VM identifying parameters must be provided in order to
          find the VM in vCenter.  If more than one of the VM identifying parameters is passed, the precedence by which
          the VM will be located is reflected in the order listed below.

- VM UUID
- VM host address (IP address)
- VM host name
- Windows flag (denotes if the guest VM is running a Windows OS, only required for the UUID option)

Examples
--------

::

    vmware_guest_information.py -s [IP Address or FQDN of vCenter] -u [vCenter user] -p [vCenter password] -u [UUID of the guest VM]
    OR
    vmware_guest_information.py -s [IP Address or FQDN of vCenter] -u [vCenter user] -p [vCenter password] -H [IP of the guest VM]
    OR
    vmware_guest_information.py -s [IP Address or FQDN of vCenter] -u [vCenter user] -p [vCenter password] -a [host name of the guest VM]

    For a server address in a file
    vmware_guest_information.py -s @/etc/philips/config/server.conf -u [vCenter user] -p [vCenter password] -a [host name of the guest VM]

    For a Windows VM when UUID is provided
    vmware_guest_information.py -s [IP Address or FQDN of vCenter] -u [vCenter user] -p [vCenter password] -u [UUID of the guest VM] --windows


Frequency
---------

The script should be executed once every night.

Return Code
-----------

0. OK
1. WARNING
2. CRITICAL
3. UNKNOWN

Output
------

::

    Status : OK, UNKNOWN
    Object Name : vCenter name of the guest VM
    Cluster Name : name of the cluster the VM belongs
    Disks : list of all the vmdk files that represent virtual disks in the VM object

