===================================================
Check Status of fliebeat running on SHDB Client
===================================================

Overview
--------

Check the status of filebeat running on the SHDB client along with the version.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework. The script expects IP address (or hostname), domain, username, password and service name
as input parameters.

Example: If filebeat is running as as service, the following happens
ExecutablePath is set to 'C:\\Filebeat\\filebeat-6.2.2-windows-x86_64\\filebeat.exe using WMI Query; name and version are set to filebeat and 6.2.2

Example
-------

Run the script with command below ::

    python check_shdb_server_status.py -D [domain]  -H [IP Address of the Host or hostname] -U [username] -P [password] -S [service name]

Frequency
---------

The script is executed every 5 minutes.

Return Code
-----------

0. OK

2. CRITICAL

Output
------

| The script outputs the following data

1. OK Status ::
    OK - filebeat is running; version:<version number>

2. Critical Status ::
    Critical - filebeat is not running