================================================
Check Status of Windows Failover cluster status
================================================

Overview
--------

This plugin shall provide the windows Failover cluster statistics(cluster status, cluster nodes status
and Fileshare witness status).
This plugin uses python WinRM module to establish the connection to windows cluster and executes certain cluster related
commands remotely(powershell commands) to get the desired output.


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect windows Failover cluster IP address, User Name, Password, Command and Subcommand.


Frequency
---------

The frequency of the service check - 5 minutes
Retry interval - 2 minutes
Retry attempts - 3


Service Check Name
------------------

Following are the service checks associated to Rhapsody windows failover cluster monitoring.

Product__IntelliSpace__Rhapsody__Windows_Cluster__Status - This service check, checks the failover cluster status.
If the cluster status is Offline then the plugin shall return CRITICAL state.
If the cluster status is Online then the plugin shall return OK state.

Product__IntelliSpace__Rhapsody__Windows_ClusterNodes__Status - This service check, checks the failover cluster nodes
status.
If one of the node in cluster is Offline then the plugin shall return CRITICAL state.
If one of the node in cluster is Online then the plugin shall return OK state.
If one of the node in cluster is in Paused state then the plugin shall return WARNING state.

Product__IntelliSpace__Rhapsody__Windows_ClusterFileShareWitness__Status - This service check, checks the File Share
Witness status.
If the File Share Witness status is Offline then the plugin shall return CRITICAL state.
If the File Share Witness status is Online then the plugin shall return OK state.



Following are the service checks associated to Database windows failover cluster monitoring.

Product__IntelliSpace__Database__Windows_Cluster__Status.
Product__IntelliSpace__Database__Windows_ClusterNodes__Status.
Product__IntelliSpace__Database__Windows_ClusterFileShareWitness__Status.

The service checks functionality or return code remains the same as mentioned above for these service checks too.


Example
-------

Run the script with command below ::

    check_windows_cluster_status.py -H [IP Address of cluster] -U [User Name] -P [Password] -Command ['ClusterResource']
    -Subcommand ['Cluster Name']
    OR
    check_windows_cluster_status.py -H [IP Address of cluster] -U [User Name] -P [Password] -Command ['ClusterNode']
    -Subcommand ['Node']
