======
Psmith
======

Overview
--------

Psmith is a feature allowing synchronization of a filestructure over http. Utilizes zsync to download the files
reliably. Psmith is set up to run as a celery task, it can be triggered from shinken, service name
"Administrative__Philips__Localhost__Task__Psmith" on the host "localhost".

A repository is to be created before this can be used. A repository consists of http available files with a .zsync file
 accessible in their immediacy.

Repository Creation
-------------------

Creating a repository hosted with Apache consists of adding the alias to configuration ::

    vi /etc/httpd/conf.d/psmith.conf
    Alias /philips  "/var/www/html/philips"

    <Directory "/var/www/html/philips">
        Options Indexes FollowSymLinks
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>

And then seting up the files and file structure in the location ::

    mkdir -pv /var/www/html/philips

Create .zsync file for each file that is to be mirrored. ::

    cd /path/to/folder/
    zsyncmake <filename>

(This will generate a warning, this can be ignored.)

A restart of httpd may be required.

Manifest
--------

The manifest of things to download is an xml file describing the directory structure to replicate. The root element is
"manifest". The filesystem structure is to be defined with "dir" and "file" elements, these elements
must contain a "name" attribute with the appropriate name for the file/directory
For example to mirror the filesystem structure: ::

    philips/
    ├── compute_environment
    │   ├── deployment_automation
    │   │   └── bundle.zip
    │   └── ova_templates
    │       ├── win2k12r2‐base.ova
    │       └── win2k8r2‐base.ova
    ├── intellispace
    │   ├── anywhere
    │   │   └── 1.1.0.1.zip
    │   └── pacs
    │       └── bundle
    │           └── 4.4.0.509.zip
    ├── platform_data
    │   ├── anti_virus
    │   └── os_patches
    └── tools
        └── service_tooling
            └── 1.1.0.1.zip

The xml file to use should be: ::

    <manifest>
        <dir name="philips">
            <dir name="compute_environment">
                <dir name="deployment_automation">
                    <file name="bundle.zip"/>
                </dir>
                <dir name="ova_templates">
                    <file name="win2k12r2‐base.ova"/>
                    <file name="win2k8r2‐base.ova"/>
                </dir>
                <dir name="intellispace">
                    <dir name="anywhere">
                        <file name="1.1.0.1.zip"/>
                    </dir>
                    <dir name="pacs">
                        <dir name="bundle">
                            <file name="4.4.0.509.zip"/>
                        </dir>
                    </dir>
                </dir>
                <dir name="platform_data">
                    <dir name="anti_virus"/>
                    <dir name="os_patches"/>
                </dir>
                <dir name="tools">
                    <dir name="service_tooling">
                        <file name="1.1.0.1.zip"/>
                    </dir>
                </dir>
            </dir>
        </dir>
    </manifest>

Psmith can be configured with multiple manifest files, these will be combined and added together. Care must be
exercised when creating filesystem structure as having conflicts in the manifest may yield undesired results.

.zsync files are not to be added to the manifest, they do need to be present in the repository.

Ansible/EMP
-----------
Psmith is to be configured with EMP.

A manifest for all sites is to be created and placed in the "common" folder of the configuration source control archive,
this file "common-repo.xml" will be added to all sites.

Site specific manifests are to be created in the site's configuration directory source control, creating a directory
"software-distribution" directory and placing additional manifest xml files in there. Once these are committed EMP will
setup the configuration on next run.

Subscription Manifests
~~~~~~~~~~~~~~~~~~~~~~

Additional site specific manifests based on subscriptions may be used.

These are used by creating empty files in the "subscription_links" directory inside the site's configuration directory,
 the name of these files is used to find the manifest to be used. The name of the files in this directory must match
 the name of the subscription manifes excluding the ".xml" extention. That is for a subscription manifest "example.xml"
 the file to use must be "example".

The actual manifest files will be copied from "common/subscriptions/". If the xml file in "common/subscriptions" is not
 present for any file listed in "subscription_links" this will cause EMP to fail.

The mechanism used to place the original xml manifest in "common/subscriptions" is outside of the scope of this
 document. The needed manifests must be present there before doing site specific configuration.

Example:

Assuming the following tree in "common/subscriptions": ::

    subscriptions/
    ├── Product_one.xml
    ├── Product_sde.xml
    └── Product_two.xml

Placing the files in the "subscription_links" directory for the site to generate the following tree: ::

    subscription_links/
    ├── Product_one
    └── Product_two

After the site executes EMP, the manifest files "Product_one.xml" and "Product_two.xml" will be present and used as
 manifest for synchronization.
