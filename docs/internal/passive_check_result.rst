=====================
Passive Check Results
=====================
Overview
--------
The passive check result submission functionality is provided by a shinken receiver module, this module is based on the
ws module.

The service takes the following pieces of information submitted as a JSON object and tries to submit it as a check
result into shinken:

- service, The name of the service for the check result as configured in shinken or a keyword mapping it to one, mapping
  is done using the service_map file.
- return_code, The return code for the check, values from 0 to 3 are valid.
- output, The output for the check result, this may be a string or an array which will be turned into a string joined
  with new lines.
- host, The hostname or IP address to use for the check result [Optional (Defaults to 'localhost')]
- timestamp, The unix epoch timestamp to use for the check result [Optional (Defaults to the current timestamp)]

Validation is not performed on submittal, it is handled by shinken, results that are not considered valid are discarded.

The service communicates with the running arbiter to get the ip addresses for the configured nodes in the system,
this allows the host to be specified by IP address or by hostname configured in shinken (Not DNS hostname).

The return_code is the traditional Nagios return code values where:

0. OK
1. WARNING
2. CRITICAL
3. UNKNOWN


Endpoint
--------
The url:
https://<neb node address>/check_result

The expected Content-Type: application/json

Examples
--------
An example using a mapping for service name and the host address::

    {
        "service": "super",
        "return_code": "2",
        "output": "passive submission\nmore than one line",
        "host": "10.0.25.5"
    }

An example using service name, and array as output. The host will default to localhost::

    {
        "service": "Administrative__Philips__Localhost__Information__IPAddress",
        "return_code": "0",
        "output": [
            "passive The IP address was found, and it is 10.0.0.1",
            "I thought you should know",
            "This makes me ok"
        ]
    }


Configuration
-------------
The module can be configured in shinken-specific.cfg the module type being "ws_passive_service_map".
The possible configuration options are:

===============  ===============================================  ===================================
Option           Description                                      Default Value
===============  ===============================================  ===================================
host             The host address to bind to, 0.0.0.0 means all   0.0.0.0
port             The port to bind to                              7764
username         The username to use for authentication,          anonymous
                 'anonymous' means not to use one
password         The password to use for authentication           <empty>
arbiter_address  The address of the arbiter                       localhost
arbiter_port     The port the arbiter runs on                     7770
arbiter_name     The arbiter name                                 arbiter-master
service_map      The service map YAML file to use                 /etc/philips/shinken/servicemap.yml
===============  ===============================================  ===================================

The service_map is a yaml file shoud contain a yaml dictionary, with the key being the keyword to map to an actual
configured service name and the value the service name. eg::

    ---
    super: Administrative__Philips__Localhost__Information__IPAddress
    aservice: Administrative__Philips__SomeService__Power__Status

