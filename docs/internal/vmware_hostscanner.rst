
==================
VMWare Host Scanner
==================

Overview
--------

The purpose of VMware host scanner is to retrieve ESXi host and vCenter specific items as part of the vCenter discovery
and place them in the facts collection format.

In the fact format, information is split up into modules with hierarchical structure, module holds information specific
to a particular part of the Datacenter, Cluster, ESXi host, and VMs running on particular ESXi host of vCenter.
For example, The information of Datacenter would be found under the Datacenter module. Cluster information would be
found under the hierarchy of datacenter -> cluster module. ESXi host information would be found under the hierarchy of
datacenter -> cluster -> esxi host. VMs running on particular ESXi host would be found under the hierarchy of
datacenter -> cluster -> esxi host -> vms.


Frequency
---------

VMware host scanner will be executed once every night.


Fact format example:
--------------------

{
    "_id" : "IDM01-192.168.215.35",
    "vCenter" : {
        "datacenter" : {
            "datacenter-2" : {
                "cluster" : {
                    "domain-c7" : {
                        "host-19" : {
                            "product" : {
                                "osType" : "vmnix-x86",
                                "fullName" : "VMware ESXi 6.0.0 build-2809209",
                                "version" : "6.0.0",
                                "vendor" : "VMware, Inc."
                            },
                            "name" : "192.168.215.24",
                            "hardware" : {
                                "numCpuThreads" : 24,
                                "numCpuPkgs" : 2,
                                "vendor" : "HP",
                                "cpuModel" : "Intel(R) Xeon(R) CPU E5-2620 v3 @ 2.40GHz",
                                "numHBAs" : 2,
                                "memorySize" : "383.9GB",
                                "numCpuCores" : 12,
                                "model" : "ProLiant DL360 Gen9",
                                "cpuMhz" : 2397,
                                "numNics" : 8,
                                "uuid" : "32353537-3835-4753-4836-343057324541"
                            },
                            "datastore" : {
                                "DevOps-SANVol1" : {
                                    "freeSpace" : "438.5GB",
                                    "capacity" : "13.6TB",
                                    "type" : "VMFS"
                                },
                                "datastore1 (3)" : {
                                    "freeSpace" : "546.8GB",
                                    "capacity" : "551.2GB",
                                    "type" : "VMFS"
                                },
                                "DevOps-SANVol2" : {
                                    "freeSpace" : "1.2TB",
                                    "capacity" : "17.3TB",
                                    "type" : "VMFS"
                                }
                            },
                            "runtime" : {
                                "connectionState" : "connected",
                                "inMaintenanceMode" : false
                            },
                            "vms" : [
                                {
                                    "powerState" : "poweredOn",
                                    "name" : "OPS47IF1",
                                    "numEthernetCards" : 1,
                                    "numCpu" : 2,
                                    "nics" : {
                                        "Network adapter 1" : {
                                            "macAddress" : "00:50:56:a0:9d:6d",
                                            "type" : "VirtualVmxnet3",
                                            "summary" : "VM Network"
                                        }
                                    },
                                    "disks" : {
                                        "virtual_disk1" : {
                                            "path" : "S:\\",
                                            "Capacity" : "34.9GB",
                                            "Free space" : "31.9GB"
                                        },
                                        "virtual_disk2" : {
                                            "path" : "C:\\",
                                            "Capacity" : "99.7GB",
                                            "Free space" : "74.2GB"
                                        }
                                    },
                                    "connectionState" : "connected",
                                    "memorySizeMB" : 4096,
                                    "guest" : {
                                        "guestFamily" : "windowsGuest",
                                        "toolsStatus" : "toolsOk",
                                        "hostName" : "OPS47IF1.OPS47.iSyntax.net",
                                        "guestId" : "windows8Server64Guest",
                                        "toolsVersion" : "10246",
                                        "ipAddress" : "192.168.215.128",
                                        "guestState" : "running",
                                        "guestFullName" : "Microsoft Windows Server 2012 (64-bit)"
                                    },
                                    "guestId" : "windows8Server64Guest",
                                    "overallStatus" : "green",
                                    "numVirtualDisks" : 2,
                                    "guestFullName" : "Microsoft Windows Server 2012 (64-bit)"
                                }]
