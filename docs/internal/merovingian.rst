Merovingian Overview
====================

Merovingian is the link between IDM scheduling and monitoring and CE PCM functionality. 

High Level Workflow
-------------------

* Merovingian celery task is invoked via Shinken (currently not on any automated schedule) 
* Merovingian invokes ansible and generates an ansible inventory file from the PCM SiteInfo.xml file that resides on the Neb node
* The pcm_boot.yml playbook is executed which executes the inventory files in the correct sequence. 
* The ansible role pcm_trigger executes the embedded trainman.py file with the correct arguments, derived from ansible facts.
* Trainman uses the callback to set the initial status to UNKNOWN, in case the host PCM process fails to finish.
* Trainman invokes the PCM url to start the PCM process.
* PCM on the invoked node will POST back to the callback the final status
* Trainman polls the callback for results, waiting for a finished status (OK, CRITICAL) for a given time period before timing out. 

* The Merovingian celery task gathers all PCM states and computes the passive service shinken results. 

Trainman
--------

Trainman is an embedded python file for invoking PCM on a node. It should only be invoked via celery however for debugging it can executed via command line. 

Trainman Example ::

    trainman.py -u "http://167.81.184.155:8182/pcm/bootstrap" -c "http://172.16.4.30/phim/pcm" -p PCM-1.0.2.0.88.0 -k /repo/Tools/PCM/packages/ -s /info/SiteInfo.xml -t 30

