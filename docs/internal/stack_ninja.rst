..  _stack_ninja:

Stack Detach/Re-attach for IntelliSpace PACS Migrations
=======================================================

Design Assumptions
------------------

- IDM Ecosystem On-site Node (Nebuchadnezzar Node) is in place for the site.
- Only Virtual Compute Environment will be supported.
- A single vCenter will be used for all Vms that are requested to attach/detach disks.
- The source and destination Vmware VM object name must be provided (not the guest OS host name).
- The Vms in question should be in a powered off state and all vmdk files of both Vms should reside on the same data
  store.  The data store should have ample space (> 10% free space).
- The source and destination Vms should not have any snapshots.


Workflow
--------

- The Stack Mapping Report is an Excel Spreadsheet produced by the migration team which details the drive from a
  particular VM that will be removed from its configuration and replace a disk drive on another VM.

    .. NOTE::  Currently the Stack Mapping Report list the host name of the guest OS of the VM objects in question.
               The report will need to add additional columns for the names of the VM objects of the source and
               destination VM objects.  This is required to search vCenter for a VM object by name.  The host name is
               not reliably provided by vCenter and the host name does not have to be the same as the name of the VM in
               vCenter.  In most cases the host name and vCenter name of a VM will match (provided the VM was deployed
               by the Compute Environment Deployment Orchestration automation), but the name of the VM object should be
               provided to be safe.

- The Stack Mapping Report will need to be converted to csv format.  After the conversion, copy and paste the csv file
  to the Disk Move web site. The web site can be found at http://<Neb_Node_IP_Address>/phim/disk/move.

    .. NOTE::  To access the web service the user will need to authenticate with the same credentials utilized to access
               Thruk.

.. image:: ../images/diskmovesite.PNG

- Once the user clicks submit, the data is grouped so that the disk moves can be performed in parallel to the various
  destination VM objects.  If a destination VM is to receive more than one disk, the disks will be moved serially and in
  order to the destination.
- Each individual move is passed to the StackNinja class.  The StackNinja class checks the preconditions (see Design
  Assumptions) and if the conditions are met it will attempt to move the stack disk from the source VM to the
  destination VM.

    .. NOTE:: IDM will utilize the vCenter node that was listed for the site in the back office.


    - The following information is passed to StackNinja from celery:
        - source_vm - vCenter name of the VM that has the vmdk file to be moved.  The VM name will likely match the
          host name of the system if it was deployed with the Deployment Orchestration layer of the Compute Environment.
        - source_disk - VMDK file from the source VM to be moved to the destination VM.
        - destination_vm - vCenter name of the VM that will receive the stack from the source VM.  The VM name will
          likely match the host name of the system if it was deployed with the Deployment Orchestration layer of the
          Compute Environment.
        - destination_disk - VMDK file representing the disk (usually the 'stub' stack) that is to be removed from the
          configuration of the destination VM.
    - The VM objects required are located.
    - If found, the preconditions for moving disks are verified.
    - The disk is removed from the source VM.
    - The destination VM will have a disk removed from its configuration (This will remove 'stub' stack if it is
      provided.  If a disk is removed, the new disk will be put in it's place on the SCSI bus; otherwise, it will use
      the next available drive number on the SCSI bus.)
    - The source drive is attached to the destination VM.
- Once the disks have been moved, an outgoing status message is sent back to celery.
    - Successful messages have status of SUCCESS
    - If a failure has occurred a message will be sent with status of FAIL.
