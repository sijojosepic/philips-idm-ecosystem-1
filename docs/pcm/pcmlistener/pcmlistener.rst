..  _pcmlistener:

-----------------------
PCM Listener Automation
-----------------------

Introduction
------------

PCM Listener is the prerequisite for Software Deployment. It takes package and installs on the windows server.

-------
Plugins
-------

Two plugins are created to perform the complete workflow:-

01. check_install_package.py

    This plugins does following things:

    a. It copies powershell script to each target nodes.

    b. It executes downloadMSI.ps1 on windows server to download the PCM Listener msi.

    c. It executes installMSI.ps1 to install PCM Listener on windows machine.

02. check_package_status.py

    This plugins does following things:

    a. It checks for the PCM Listener status.

    b. If PCMListener's exe is found, then it return OK status else CRITICAL.


----------------------------------------------------
PCM Listener Package Installation and status command
----------------------------------------------------

define command {
    command_name    pcmlistener_installation_trigger
    command_line    $PLUGINSDIR$/check_install_package.py -H $HOSTNAME$ -a $HOSTADDRESS$ -p $_HOSTKEY$ -u $_HOSTUSR$ -D $ARG1$ -d $ARG2$ -i $ARG3$ -r $ARG4$ -R $ARG5$ -s $ARG6$
    module_type     cryptresource
    }

define command {
    command_name    pcmlistener_installation_status
    command_line    $PLUGINSDIR$/check_package_status.py -H $HOSTNAME$ -u $_HOSTUSR$ -p $_HOSTKEY$ -D $ARG1$ -s $ARG2$
    module_type     cryptresource
    }

---------------------------------------------
PCM Listener Installation and status Services
---------------------------------------------

define service {
    use                     standard-service
    hostgroup_name          windows-servers
    service_description     Administrative__Philips__PCMListener__Task__Trigger
    check_command           pcmlistener_installation_trigger!$PCM_DOMAIN$!$PCM_DOWNLOAD_PS_PATH$!$PCM_INSTALL_PS_PATH$!$PCM_REPO_PATH$!$PCM_WIN_FOLDER_PATH$!$PCM_SERVICE_NAME$
    }

define service {
    use                     standard-service
    hostgroup_name          windows-servers
    service_description     Administrative__Philips__PCMListener__Task__Status
    check_command           pcmlistener_installation_status!$PCM_DOMAIN$!$PCM_SERVICE_NAME$
    check_interval          10
    }
