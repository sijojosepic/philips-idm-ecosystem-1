..  _api_pcm_site_information:

================
Site Information
================

High Level Overview
-------------------

The Site Information xml file is the central controlling file for a customer site in regards to the PCM and other CI/CD
actions. 

The site information file is structured as such

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <SiteMap xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Version="4.2">
      <SiteBag>
        <Keys />
      </SiteBag>
      <Roles>
        <Role RoleType="Foo">
          <IPv4></IPv4>
          <FQDN></FQDN>
          <Packages />
            <PackageInfo Name="Package" Version="1.2.54.0.5432" />
          </Packages>
          <RoleBag>
            <Keys />
          </RoleBag>      
        </Role> 
      </Roles>
    </SiteMap>


Data Bags
---------

Data bags are the main way of passing customer specific material into otherwise static product packages. Using a custom
notation inside the PCM package meta-data xml allows data to inserted into PCM packages as they execute. 

- ``SiteBag`` Keys in here are globally available to all packages. Good examples for here are domain credentials,
  central fileshares, flags for federated deployment etc.
- ``RoleBag`` Keys in here are specific to the role (machine) being used. A good variable to store here is the Role Type
  for Deploy.ps1 deployment scripts
- ``PackageBag`` There is an option for a package to have a unique bag as well, however this is not considered ideal
  construction and should be avoided if possible. 

Data Bags substitution goes from Package->Role->Site when attempting to find a value. Meaning a specific role data bage
entry will over-rule a site one.

Site Example :

.. code-block:: xml

    <SiteBag>
        <Keys>
            <Key Encrypted="false" KeyName="DomainAdmin">KONA5EID04\Administrator</Key>
            <Key Encrypted="false" KeyName="DomainAdminPassword">st3nt0r</Key>
            <Key Encrypted="false" KeyName="SolutionRootFolder">S:\\Philips\\Apps\\iSite</Key
            <Key Encrypted="false" KeyName="DeploymentRootFolder">C:\\provision\\isite\\</Key
            <Key Encrypted="false" KeyName="DeploymentScripts">C:\\provision\\isite\\deploymentscripts</Key>
            <Key Encrypted="false" KeyName="LocalizationCode">ENG</Key>    
        </Keys>
    </SiteBag>


Role Example :

.. code-block:: xml

    <RoleBag>
        <Keys>
            <Key Encrypted="false" KeyName="RoleType">EID</Key>
            <Key Encrypted="false" KeyName="ISiteServiceAccountUser">PEGASST2\iSiteService</Key>
            <Key Encrypted="true" KeyName="ISiteServiceAccountPassword">mJvIHQNgaILCc8QSpirQjUbj+lb0ShbwMeXiflRrSHxiKzvRIgmd1ag4bE6KRXsjOmiCWgBgsjQm9+8oIc9ZJBZAJZQ2mYvsoNk0Lx09KSOslucLPTNpot5Uvi57u+iYMmSH6r2DcF4wwVTrJnLnVsDbRKbw1TSUla9GUeRQkUIFYgjXYBmu5YqaJwq58Ed63VtpMG9JmzKqQThIsuuRKlvq54Zj2yKHn/U+ZWnp8xMHmEIjwdf4Gl0DIb3X459YoBNUVXEcJhas1Mev5xx5PjK6UwmxxqjrWjKVtbZU9jM3OfJ/pLF1QzZesAK2JgacOhsaHHavlgQnKaIP1WbqQQ==</Key>
        </Keys>
    </RoleBag>

Roles
-----

Roles are individual machines in a customer solution.

- ``RoleType`` is the free text name of the role's purpose. Value like EID, Proc, Infra should be used here
- ``IPv4`` is the main IP of the role. This is used by PCM to determine what role it should execute. As long as one if the interfaces queried by PCM is listed here, it will match and use that role to execute. 
- ``FQDN`` Secondary match if no IPs match

Packages
--------

Package Info entries are the way that packages are maintained on the customer sites. The product name and desired
version are specified on each entry.

- If a fully qualified version is given, then that version, and only that version, will be installed for this package.
  For example, if ApplicationServices-1.2.4.0.34522 is specified, then only that version will be used, even if ApplicationServices-1.2.5.0.34643 may be present on the Neb node.

- If a partial match is given, such as AWV 1.3, then the latest available version of that family will be installed. If a
  newer version of AWV, say 1.3.1, is made available on the Neb node, then the next pass will attempt to install it.

- If just a name is given such as IMACS and no version then each PCM execution will make sure the latest available IMACS
  software is installed. If a newer version of IMACS is made available on the Neb node, then the next pass will attempt
  to install it. This is very useful for System Integration and Test environments

Example :

.. code-block:: xml

    <Role RoleType="EID">
      <IPv4>167.81.184.135</IPv4>
      <FQDN>PEGASST2</FQDN>
      <Packages>
        <PackageInfo Name="ApplicationServices" Version="1.2.4.0.34522" />
        <PackageInfo Name="AWV" Version="1.3" />
        <PackageInfo Name="IMACS" Version="" />
      </Packages>
      <RoleBag>
        <Keys></Keys>
      </RoleBag>
    </Role>
