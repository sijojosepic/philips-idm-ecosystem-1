=================================================
Migrating existing CE Monitoring to IDM Ecosystem
=================================================

The IDM Ecosystem onsite node allows to for migrating existing monitored instances into the IDM solution seamless. There
is a supplied module that handles the heavy lifting.

============
Introduction
============

Wrapper script used to initiate migration of previously released CE Monitoring solutions into the IDM Ecosystem. The
script calls the LegacyNagiosScanner class from scanline/legacy_nagios.py which will connect to the CE Monitoring
solution and migrate the settings.

Arguments:

 - CE Monitoring end point - (default - None) IP address of the CE Legacy Nagios Node
 - User name - (default - None) Account used to access migrator
 - Cookie location (default - /etc/philips/migrator) File used to store settings for migrator

=======
Actions
=======

The migrator script needs to be supplied information to know where to gather the information of the hsots and services.

Executing the following will provide help information ::

    /usr/lib/philips/initialization/migrator.py -h

The following will explain the parameters:
-i : IP Address of the target system (ie CE Monitoring instance)
-u : Username to access the system  (ie CE Monitoring instance)
-d : Path containing the cookie file with an obscured password, this defaults to /etc/philips/migrator

A prompt will appear for the password to be supplied and then this is stored in the cookie file.

The system can utilize a cookie that is defined in the directory location, but the cookie should look like the following
json object ::

    {
        "ip": "192.168.100.2",
        "user": "admin",
        "password": "cGghbHQwcg=="
    }

If the cookie is in place, then the values will be read and not needed to be enter via the parameters.

The script will verify that the backoffice vigilant system is available to be trying to connect
the 'http://vigilant/health'
