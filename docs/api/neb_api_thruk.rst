..  _neb_api_thruk:

---------
Thruk API
---------

Customer API from Thruk
-----------------------

String available from a web browser to display json output::

    https://{{ idm-ip-addr }}/thruk/cgi-bin/status.cgi?view_mode=json&host=all&columns=host_alias,host_address,host_plugin_output,last_check,state,display_name,perf_data,last_state_change

The connection will require basic http auth credentials which are provided by Philips.

The response object will be a JSON list that contains all services for all hosts. To query for a single host, the
following URL would be used where the ``{{ host_alias }}`` is the FQDN of the host.::

    https://{{ idm-ip-addr }}/thruk/cgi-bin/status.cgi?view_mode=json&host={{host_alias}}&columns=host_alias,host_address,host_plugin_output,last_check,state,display_name,perf_data,last_state_change

Example output::

    [
       {
          "last_state_change" : 1430248118,
          "host_alias" : "demo0st1.stdemo0.isyntax.net",
          "display_name" : "Product__IntelliSpace__PACS__iSyntaxServer__Aggregate",
          "state" : 2,
          "host_address" : "127.0.0.1",
          "host_plugin_output" : "PING OK - Packet loss = 0%, RTA = 0.04 ms",
          "perf_data" : "",
          "last_check" : 1430324557
       },
       {
          "last_state_change" : 1430247963,
          "host_alias" : " demo0st1.stdemo0.isyntax.net",
          "display_name" : "Product__IntelliSpace__PACS__iSyntaxServer__Port",
          "state" : 2,
          "host_address" : "127.0.0.1",
          "host_plugin_output" : "PING OK - Packet loss = 0%, RTA = 0.04 ms",
          "perf_data" : "",
          "last_check" : 1430324462
       }
    ]

Definition of the object values

- "last_state_change" : This is EPOCH time when the state of the service has last changed. EPOCH time does not have a
  timezone association. It is defined as: the number of seconds since 00:00:00 UTC on January 1, 1970

- "host_alias" : FQDN of the host that is defined as part of the PACS solution

- "display_name" : Refers to the service check name

- "state" : will return an integer value that maps to the following human readable states.

  0. OK
  1. WARNING
  2. CRITICAL
  3. UNKNOWN

- "host_address" : The IPv4 Address that is assigned to the host to monitor against for services

- "host_plugin_output" : The output status of plugin.

- "perf_data" : The output performance data for any plugins that have available performance data

- "last_check" :  This is EPOCH time when the state of the service has last changed. EPOCH time does not have a timezone
  association. It is defined as: the number of seconds since 00:00:00 UTC on January 1, 1970

Sample Code - Python
--------------------

.. code-block:: python

    import json
    import requests
    import base64

    schema = 'https'
    auth_uname = 'thrukro'
    auth_pw = base64.b64decode('MmJldGFYMTU=')
    hosts = 'all'
    url = 'https://localhost/thruk/cgi-bin/status.cgi'
    params = {'view_mode': 'json','host': all ,'columns': 'host_alias,host_address,host_plugin_output,last_check,state,display_name,perf_data,last_state_change'}

    r = requests.get(url, auth=(auth_uname, auth_pw), verify=False, params=params)

    json.dumps(r.json())
