===================
Site Rules REST API
===================
Provides means to query IDM for Site Rules Data.


Workflow Instructions
---------------------
* Site Assign and Site Maintenance
    * Site Assign Scope
        * Toggle button shall be available for each Site Id in Nodes page.
        * When a Site is owned by the user, the events displayed in Service Events and Host Status page shall display
          the email id of the User who owns the site in a seperate column for each event.
        * A user can enable Scheduled/Running Maintenance for a Site Id only when user owns the Site.
        * A user who owns the site is only allowed to diswon it.
    * Site Maintenance Scope
        * IDM site view has a capability to set a site in maintenance mode.
        * User should assign the site to himself/herself before setting a Site ID to maintenance mode as the
          Maintenance button will be enabled only when a site ID is owned by the user.
        * Only the user who owns the site can put it under maintenance and turn off maintenance.
        * Maintenance Mode can be either Scheduled(future) or Running(current).
        * Maintenance Running Scope
            * Alert from the sites in maintenance mode will not be seen on IDM Portal.
            * Existing event will still stay on the Dashboard.
            * User can set site to maintenance mode only for maximum of 7 days.
            * User cannot edit the Maintenance when it has started. The ‘Edit Maintenance’ option shall be disabled
            * User can turn off the maintenance mode when the Maintenance is Running.
        * Maintenance Scheduled Scope
            * User has the ability to edit the maintenance duration or Remarks for Scheduled Maintenance.
            * User cannot turn off the Maintenance Mode when the Maintenance is scheduled.
              The ‘Turn Off Maintenance’ option shall be disabled.
            * Alerts from Site with scheduled Maintenance shall be displayed in IDM Portal.
            * User can disown the Site when Maintenance is Scheduled.

Saving Site Maintennace Rule
----------------------------
To save a maintenance rule post request with json information needs to be used.

There fields are: siteid, user, start_time, end_time, maintanance_remark, nodes, services
(nodes and services keys have empty list value)

For example to save the site maintenance rule for siteid "ABC", issuing a POST request with JSON to  the
endpoint `/save_rules` will result in a new rule: ::

    {
        'type': 'maintenance',
        'siteid': 'ABC',
        'nodes': [],
        'services': [],
        'user': 'user1',
        'start_time': '2018-07-01 22:00:00',
        'end_time': '2018-07-02 22:00:00',
        'remark': 'Os Upgrade'
    }

Query Sites Under Maintennace
-----------------------------
The way to query is by issuing a GET request to:

/site_rules/rules/list/

For example `/site_rules/rules/list/` may return::

    {
        'sites' : ['ABC01', 'ABC02', 'ABC03']
    }

Query
-----
The way to query is by issuing a GET request to:

/site_rules/rules

Filters may be passed via query string (siteid and type).
For example querying `/site_rules/rules?siteid=ABC&type=assignment` may return::

    {"result": [
        {
            'siteid': 'ABC',
            'user': 'user1',
            'type':'assignment',
            'start_time': '2018-07-02 22:00:00'

        }
    ]}

Saving Site Assignment Rules
----------------------------
To save a rule a post request with JSON information to be used for saving must be sent.

There fields are: siteid, type, user, start_time

For example to save the site assignment rule for siteid "ABC", issuing a POST request with JSON to  the
endpoint `/site_rules/rules/assignment` will result in a new rule: ::

        {
            'siteid': 'ABC',
            'user': 'user1',
            'type':'assignment',
            'start_time': '2018-07-02 22:00:00'

        }

Delete Site Rule
----------------
To delete a site data a DELETE request with the siteid and type to be deleted must be sent to the endpoint:

`/site_rules/rules/<siteid>/<rule_type>`

For example to delete a site rule entry for siteid='ABC' and type=assignment  is by issuing a DELETE request::

    /site_rules/rules/ABC/assignment

will result in deleted site rule
