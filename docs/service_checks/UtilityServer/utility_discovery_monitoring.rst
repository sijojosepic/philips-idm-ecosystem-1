..  _UtilityServer:

Utility Server Discovery & Monitoring
--------------------------------------
In the Utility server, by default Veeam and Tivoli Backjobs are enabled.
Regarding the other ISEE products like DWP, IBE, ISCV, Concerto and iECG will be enabled based on the availability of the products in the respective site.

Example for a site, if there are two of the ISEE products say IBE & DWP available, then in the utitility server only these backup monitoring will be added (DWP backup & IBE backup) apart from the default ones.
The presence of ISEE products for a site will be captured from the lhost.yml file, during the discovery of the UtilityServer. Based on the ISEE product information, the respective hostgroups will be added to the utility server's cfg file, which will in turn enable backup monitoring for the respective product.

In the source code *tivoli_utility.py* under the Scanline library host directory (scanline/host) the ISEE products Scanner mapping information has been kept.

**ProductMapping**::

    IBE = ['ibe', 'ibeapp', 'ibedb', 'ibegd']
    ISCV = ['iscv', 'iscvgd']
    IECG = ['iecg', 'iecggd']
    DWP = ['dwp']
    XPERIM = ['xperim', 'xperimgd', 'xperconnect', 'xperdatacenter']
    ISPORTAL = ['isportal']
    CONCERTO = ['concerto']
    PRODUCT_MAPPING = {'ibe': IBE,
                       'iscv': ISCV,
                       'iecg': IECG,
                       'dwp': DWP,
                       'xperim': XPERIM,
                       'isportal': ISPORTAL,
                       'concerto': CONCERTO
                       }


In the product mapping, applicable scanners for a particular product is mapped against it


**ISEEUtilityServer Nagious Rules**::

 - ISEEUtilityServer:
    - attribute: use
      value: '"utility-server"'

    - attribute: hostgroups
      value: '"+dwp-utility-servers"'
      when: '"dwp" in isee_backups'

    - attribute: hostgroups
      value: '"+ibe-utility-servers"'
      when: '"ibe" in isee_backups'

    - attribute: hostgroups
      value: '"+concerto-utility-servers"'
      when: '"concerto" in isee_backups'

    - attribute: hostgroups
      value: '"+iecg-utility-servers"'
      when: '"iecg" in isee_backups'

    - attribute: hostgroups
      value: '"+isportal-utility-servers"'
      when: '"isportal" in isee_backups'

    - attribute: hostgroups
      value: '"+xperim-utility-servers"'
      when: '"xperim" in isee_backups'

    - attribute: hostgroups
      value: '"+iscv-utility-servers"'
      when: '"iscv" in isee_backups'
