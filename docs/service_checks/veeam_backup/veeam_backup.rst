..  _Veeam_Backup_services:

==============================
 Veeam Backup Services
==============================

**HostScanner** - ISEEUtilityServer

**Hostgroups** - utility-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "24h 0m 0s", "0h 15m 0s"

Service Checks
##############
 - Utility Server - Veeam Backup

.. csv-table:: **Veeam Backup**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Veeam", "Product__ISEE__UtilityServer__VEEAM_Backup__Status", "check_veeam_backup!Backup"
   "Veeam", "Product__ISEE__UtilityServer__VEEAM_Copy__Status", "check_veeam_backup!Copy"