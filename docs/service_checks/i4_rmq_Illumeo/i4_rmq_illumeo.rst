..  _i4_rmq_illumeo:

==============================
 I4 RabbitMQ Services
==============================

**HostScanner** - ISP

**Hostgroups** - i4-rmq-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - I4 Server

.. csv-table:: **I4 Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 20, 40

   "I4", "Product__IntelliSpace__Illumeo__I4PreProcessing_Queue__Status", "check_i4_rabbitmq_q!I4PreProcessing"
   "I4", "Product__IntelliSpace__Illumeo__I4PreProcessingError_Queue__Status", "check_i4_rabbitmq_q!I4PreProcessingError"