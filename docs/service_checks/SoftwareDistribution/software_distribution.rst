============================================
Software Distribution and Deployment in IDM 
============================================

Software distribution and Deployment in IDM provides the flexibility to distribute & install software packages across the sites based on the package’s Geo rule configuration.

.. image::  images/swd.png
    :align: center
    :alt: default values



**Software Distribution and Deployment involves,**

    1.  Registration of Package
    2.  Distribution of the packages to the Neb, from central repository
    3.  Deployment - copy and install the packages on the windows servers.
    


1. Registration
----------------


    Registration of a  package is a manual process for now, with each package there will be an xml file associated with it, which contains the  meta information about the package.
    During registration of a package, respetive *zsync* files will be created, for the package and for the XML file in the repository using the **zsyncmake** command, which is necessary to download the package at a later stage using zsynccurl utility.
    Along with this there will be an entry in to the Mongo Database subscriptions collection, as well as an XML file will be updated in the **SVN** */root/ibc/common/subscriptions* path, with package details including package-country association.
    

    **The steps involved in Registration are,**
        #. Repo Node
            -  Go to path */repo/philips/Tools/PCM/packages/*
            -  Create the package folder if its not there:
                *Eg.  ISP or AWV*
            - Now copy the *pkg.zip* and *pkg.xml* file to this location
            - Then execure the **zyncmake** command in order to generate the respective zsync files:

                *Eg*:
                 |       `zsyncmake AWV- 1.3.4.0.1969.76.xml`
                 |       `zsyncmake AWV-1.3.4.0.1969.76.zip`

            .. image::  images/zyncmake.png
                    :align: center
                    :alt: Zyncmake
    
        #. SVN
            -   Go to the path */root/ibc/common/subscriptions* or */root/ibc/common-sec/subscriptions* (for secure neb)
            -       Update the newly created XML and ZIP files path in an XML file.
                        `In the above case- AWV134.xml`
            -       Update the same to SVN

                *Eg*:
                    .. code:: javascript

                        <manifest>
                          <dir name="Tools">
                            <dir name="PCM">
                              <dir name="packages">
                                <dir name="AWV">
                                  <file name="AWV-1.2.12.0.1067.46.xml" />
                                  <file name="AWV-1.2.12.0.1067.46.zip" />
                                </dir>
                              </dir>
                            </dir>
                          </dir>
                        </manifest> 

        #. Create an entry in MongoDB, similar as below

            .. code:: javascript

                    {  
                   "_id":"ISP445530",
                   "path":"/philips/Tools/PCM/packages/ISP",
                   "dependencies":{  
                      "Rhapsody":"6.2.2.0.201708081800"
                   },
                   "countries":[  
                      "United States"
                   ],
                   "version":[  
                      "4.4.553.0.201801291721"
                   ],
                   "rules":[  
                      "multisitedeployment"
                   ]
                }

Geo Rule Configuration
======================
    IDM provides capability to restrict package distribution to specific countries with the help of Geo rules configuration option available in the IDM Portal.
    All products that uses IDM as a Distribution channel for the released software's and documentation, shall have to configure GEO rules in order to align with the NOR-D compliance.

Why IDM Geo Rules restriction ?
*******************************
        -  In the normal Distribution workflow, Geo rules configuration is done by the Operations engineer.
        -  This leads to a situation where the SCS may distribute the Software to a Customer Site, with non *NOR-D compliant Geo rules* configuration.
        -  In the IDM Geo Rules configuration, the ownership to set up *NOR-D compliant Geo rules* is been transferred to the Product team (Project Manager, Q&R).
        -   Now the SCS Engineer can no longer configure Geo Rules, but distribute packages to the Sites based on the defined Geo rules.


Setting up Geo Rules
*********************

    |    **a.** Select “Geo Rules configuration” link [*2*]
    |    **b.** On the Geo Rules Configuration pop up, associate the registered package to country by selecting the package from the “package name” list [*3*], and then select the approved countries for release from the respective region according to the NOR-D [*4*], and click on apply [*5*].
    |    **c.** Close the Geo Rules Configuration page.

                .. image::  images/geo_rule.png
                    :align: center
                    :alt: Geo Rules

2. Distribution
----------------

    Distribution of a package is done based on the subscription. During distribution, the package will be downloaded from the repository to the Neb using **zsync-curl** utility. Distribution requires, actions from both the IDM portal and Neb node.

    **IDM Portal**

        1.  Launch IDM Portal and navigate to Software Management tab
        2.  On the Software Distribution page, the registered package shall be available
        3.  Click on Rule Configuration and enable Multi Site if required
        4.  Now on the Software Distribution page, select a Package, site and click on submit button
        5.  The submitted distribution request shall be seen on the Software Distribution reports list.

                - An empty file, with the name same as the xml file created during Registration, will be created in the SVN root/ibc/sites/<SITEID>/subscription_links path.

                - In the Database there will be an entry in to the Audit collection


            .. image::  images/distribution_portal.png
                :align: center
                :alt: default values


    **Neb Node**

        #. Trigger Ansible pull (*Configured to run every 3 hours*)

            -   With Ansible pull the respective package xml file created during registration in the svn common directory will be copied to /etc/Philips/psmith

        #. In the Thruk portal choose **localhost** and trigger Software distribution **Administrative__Philips__Distribution__Task__Trigger** task (*Configured to run every 4 hours*)
            -   The software packages [*pkg.xml.zsync and pkg.zip.zsync*] are copied to NEB, based on the path defined in the  xml folder structure [*Eg: /var/philips/repo/Tools/PCM/packages/*]
            -   The format of copied zsync files will be changed to .sha1
            -   The **Administrative__Philips__Distribution__Task__status** service on the Thruk shows **OK** status with appropriate message (*number of files/directories copied/deleted*)

            .. image::  images/distribution_neb.png
                :align: center
                :alt: default values


    **PCM Discovery Transmit Task Trigger  [Thruk]**

        In the Thruk portal under **localhost** the **Administrative__Philips__PCM_Discovery_Transmit__Task__Trigger** status gives the status of the downloaded pakcages in the Neb. This status can be seen at the **IDM Portal**  *Software Distribution and Depolyment* Report page as well, once the package is available in the Neb its ready for deployment.

    **PCM Package Status:**

        Below are the different package distribution statuses. All these statuses are available in audit collection and
        distribution report on IDM portal.

        **1. Submitted:** A package will be in this status when a user submits a request from IDM portal.

        **2. Started:** When **Administrative__Philips__Distribution__Task__Trigger** task is triggered from the
        thruk and it starts downloading package files. We move package status into Started.

        **3. Failed:** Once package downloading is started but if it fails because of any reason we mark that package into failed status.

        **4. In Progress:** This status tracks the downloading progress of a package. Every 5 mins a task **Administrative__Philips__PCM_Discovery_Transmit__Task__Trigger**
        is triggered from thruk which marks a package into In Progress status if package downloading is not completed. It
        also captures the downloaded file size and store into audit collection.

        **5. Available:** A task **Administrative__Philips__PCM_Discovery_Transmit__Task__Trigger** marks a package status into
        available if package is fully downloaded.


3. Deployment
--------------

        In this stage, the package will be installed on the targeted windows servers with the help of PCM tool. 

        For Deployment, following actions needs to be performed from both IDM Portal & NEB Node.
      
        **IDM Portal**


            .. image::  images/deployment_portal.png
                :align: center
                :alt: default values


    **Map the package to the targeted hosts, this involves**
        1. Launch IDM Portal and navigate to Software Management tab -> Software Deployment
        2. On the Software Deployment page, the distributed [awv-1.2] shall be available when the respective site is selected
        3. Now select the host to which the package has to deployed and click on submit button.
        4. The submitted deployment request shall be seen on the Software Deployment Reports list.


                    -    Creating an entry in the audit collection about the package to host association
                    -    In the site's SVN a directory named *siteinfo*, will be created.
                    -    An **site_info.xml** and **pcm_manifest.json** files containing package to host association will be updated to the *siteinfo* directory.

                *Eg site_info.xml*:

                        .. code:: javascript

                                <?xml version="1.0" encoding="UTF-8"?>
                                    <SiteMap>
                                       <SiteBag>
                                          <Keys>
                                             <Key Encrypted="true"KeyName="ISiteServiceAccountPassword">Gk0qaeQBFjK05zCAio36u3sauyJ5x4a1dT2IMYug+x5mt0qqmmwbMQpIJC8HSCYgK/mjqQxfBPY5XQh5poK+ZKsAwo4ymIj+mpYkgzI0Uc9HN+JVBGst6Mu4hli3negztMTPt01uuzrsCpK9qxX7Yn6WVsTw53ZtP8usuPdzMNETIOFxVpnPpehD9XCOOt5bdQ4wi+Y0n3IG3+S4q3kTJ+3Dok7nyj85mYNf/l2NXdEdhtD7gw6GOgaeE8IHis8R2sjPyiWxbMCg/n8nUZM0PwE45YwErRrRkz3/d2AzuRUNzzo2ae67++uw+XtAt+3UGvCeZo7trixbq8eO1Uno0A==</Key>
                                             <Key Encrypted="false" KeyName="DeploymentRootFolder">C:\provision\isite</Key>
                                             <Key Encrypted="false" KeyName="ISiteServiceAccountUser">IDM02\iSiteService</Key>
                                             <Key Encrypted="true" KeyName="DomainAdminPassword">Gk0qaeQBFjK05zCAio36u3sauyJ5x4a1dT2IMYug+x5mt0qqmmwbMQpIJC8HSCYgK/mjqQxfBPY5XQh5poK+ZKsAwo4ymIj+mpYkgzI0Uc9HN+JVBGst6Mu4hli3negztMTPt01uuzrsCpK9qxX7Yn6WVsTw53ZtP8usuPdzMNETIOFxVpnPpehD9XCOOt5bdQ4wi+Y0n3IG3+S4q3kTJ+3Dok7nyj85mYNf/l2NXdEdhtD7gw6GOgaeE8IHis8R2sjPyiWxbMCg/n8nUZM0PwE45YwErRrRkz3/d2AzuRUNzzo2ae67++uw+XtAt+3UGvCeZo7trixbq8eO1Uno0A==</Key>
                                             <Key Encrypted="false" KeyName="LocalizationCode">ENG</Key>
                                             <Key Encrypted="false" KeyName="DomainAdmin">IDM02\Administrator</Key>
                                             <Key Encrypted="false" KeyName="Federated">False</Key>
                                             <Key Encrypted="false" KeyName="SolutionRootFolder">S:\Philips\Apps\iSite</Key>
                                             <Key Encrypted="false" KeyName="DeploymentScripts">C:\provision\isite\deploymentscripts</Key>
                                             <Key Encrypted="false" KeyName="SiteID">IST01</Key>
                                             <Key Encrypted="false" KeyName="HL7Node">NA</Key>
                                             <Key Encrypted="false" KeyName="DBNode">idm02db1.idm02.isyntax.net</Key>
                                          </Keys>
                                       </SiteBag>
                                       <Roles>
                                          <Role Sequence="2">
                                             <IPv4>192.168.180.94</IPv4>
                                             <FQDN>idm02pr1.idm02.isyntax.net</FQDN>
                                             <Packages>
                                                <PackageInfo Version="4,4,542,0,201609012214" Name="ISP" />
                                             </Packages>
                                             <RoleBag>
                                                <Keys>
                                                   <Key Encrypted="false" KeyName="RoleType">2</Key>
                                                </Keys>
                                             </RoleBag>
                                          </Role>
                                          <Role Sequence="3">
                                             <IPv4>192.168.180.91</IPv4>
                                             <FQDN>idm02if1.idm02.isyntax.net</FQDN>
                                             <Packages>
                                                <PackageInfo Version="4,4,542,0,201609012214" Name="ISP" />
                                             </Packages>
                                             <RoleBag>
                                                <Keys>
                                                   <Key Encrypted="false" KeyName="RoleType">128</Key>
                                                </Keys>
                                             </RoleBag>
                                          </Role>
                                          <Role Sequence="1">
                                             <IPv4>192.168.180.97</IPv4>
                                             <FQDN>idm02ar1.idm02.isyntax.net</FQDN>
                                             <Packages>
                                                <PackageInfo Version="4,4,542,0,201609012214" Name="ISP" />
                                             </Packages>
                                             <RoleBag>
                                                <Keys>
                                                   <Key Encrypted="false" KeyName="RoleType">8</Key>
                                                </Keys>
                                             </RoleBag>
                                          </Role>
                                       </Roles>
                                    </SiteMap>




        **Neb Node**


                    -   With Ansible pull the *site_info.xml* and *pcm_manifest.json* files, which contains the package to the host mapping, will be copied to the Neb's */var/philips/info/* path.
                    -   In the Thruk portal choose **localhost** and trigger the **Administrative__Philips__Deployment __Task__Trigger** task, then copying and installation of the package to the specified hosts initiated.
                    -   PCM Listener utility should be installed on the Windows servers, which will take care of the installation of the package and exit with appropriate statuses (0 Success or 1 Failures).
                    -   Once the Deployment succeeded, the *site_info.xml* & *pcm_manifest.json* files will be removed from the Neb.

Actions Center Distribution and Deployment
-----------------------------------------------

            .. image::  images/Orchestration.png
                :align: center
                :alt: default values