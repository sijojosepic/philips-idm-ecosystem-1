..  _database:

==============================
Database Services
==============================

**HostScanner** - ISP(ISPACS version > 4.4.552), Database(ISPACS version < 4.4.552)

**Hostgroups** - isp-db-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 10m 0s", "0h 2m 0s"

Service Checks
##############
 - Database Server

.. csv-table:: **Database Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Database", "OS__Microsoft__Windows__Databasedisk__Usage", "check_mount_point_usage!checkvolsize!.!75!85"
   "Database", "Product__IntelliSpace__DB__SQLSERVERAGENT__Status", "check_win_process!SQLAGENT"