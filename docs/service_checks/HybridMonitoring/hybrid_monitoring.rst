..  _HybridMonitoring:

============================================
IDM Hybrid Monitoring [FEATURE 823305]
============================================
Hybrid monitoring in IDM extends the monitor capability to more than one instance of a product (ISPACS) from a single Neb node, where each product instance is having a different username and password.
And also to discover and monitor an **iVault** where the login credentials differs from host to host (presence of both 2008 & 2012 hosts). This is achieved by adding the *username* and *password* to the respective host's cfg file, given in the endpoint configuration during discovery.

1. Monitoring more than one iVault’s from a Neb
------------------------------------------------
In the IDM Neb node by default only one set of credentials can be given at a time in the resource.cfg file, monitoring works seamlessly for the ISPACS version prior to 552, as the username and password are common for all the nodes across the iVaults, even when there is more than one iVault is supposed to be monitored from the same Neb node. But when it comes to 552+ versions of ISPACS Domain name is part of username, that is the host's username differs from iVault to iVault. But with the post 552 ISPACS versions,when it comes to monitoring more than one iVaults, from a single Neb, with present scenario (prior to Hybrid Monitoring in IDM) monitoring passes for only those iVault where the default username and password matches.

Production Scenario
-----------------------
Monitoring Production and Test nodes from a single Neb node.

In order to monitor more than one iVault having ISPACS version 552 plus, the credentials are been added to each host file, this will be made use by Shinken during the service checks.



i. lhost.yml
*****************
Implementation of Monitoring more than one iVault’s from a single Neb, did not introduce any changes to the *lhost.yml* file




ii. Nagious Rules YAML file
***************************

The credentials entry is been added in the **nagious_configuration_rules.yml** file. If the credentials are available with host's discovery facts then it will be added to the respective host's cfg file.


            .. image::  images/nagious_rules.PNG
                    :align: center
                    :alt: nagious_configuration_rules.yml


iii. Hosts
-----------

The username and password will be added during discovery in to the hosts file towards the fields _usr and _key respectively.
At the time of service execution these values will be, used where all the host macros (_HOSTUSR, _HOSTKEY) definition is there (mainly in the Shinken command definition)


            .. image::  images/hosts_cfg.PNG
                    :align: center
                    :alt: hosts.cfg


2. Discover & Monitor an iVault, where nodes are having different credentials
------------------------------------------------------------------------------
With the up-gradation of ISPACS to the latest version, there is a new scope to monitor nodes which are having different credentials under the same iVault. Here all the applicable credentials for an iVault have to be provided through the lhost.yml endpoint configuration. During discovery IDM figures out the valid credentilas for each host from the given list and adds the same to the host's cfg file.

Production Scenario
-----------------------
Upgrading ISPACS version of a site from ISPACS 4.4.54X(Windows 2008) to 4.4.55X(Windows 2008), once its done then each node will be upgraded from windows 2008 to 2012 in an incremental fashion. During this phase there will be nodes in the iVault having different  Windows OS version ((2008, 2012)thereby the credentials also differs. 

lhost.yml
-----------
When there are more than one credential applicable for an endpoint it has to be supplied through the lhost.yml file as a list.

Eg::


     address: IDM02IF1.IDM02.iSyntax.net
     scanner: ISP
     username: [u1, u2, $USER56$]
     password: [p1, p2, $USER57$]

- Here order of username and password in the list is very important, because for the username u1,  p1 would be considered as the password,  u2, p2 and so on, in an ordered manner.

- When the username Or password need to be referred from the resource.cfg file, it should start and end within a $ for example $USER56$ will refer the value in the resource.cfg against it.

- Regardless of the username, password entries (encrypted or plaintext) in the lhost.yml, credentials will always be encrypted in the host's cfg file.

- Auth check will be performed only when, there are multiple entries for username and password, If there is only one entry for username and password then that would be treated as the valid credentials for all hosts in that iVault and the encrypted form of the same will be added to the host's file.

- When there are Multiple credentials applicable for an iVault, and if the system in not able to figure out the right credentials for a host from the given list, then neither username or password will be added for that host in the host's cfg file.

- For some hosts there are service checks, which uses a different credentials other than windows credentials (Database, RabbitMQ, Rapsody etc), When it comes to Hybrid Monitoring, for the Database service checks the provision is added (details below) to support different credentials per iVault, for all the remaining services will use the same credentials across iVaults, so there is no provision available to provide different credentials for an iValut.

            .. image::  images/lhost_yml.PNG
                    :align: center
                    :alt: lhost.yml



Database Service Checks
-----------------------

In Hybrid Monitoring in order to make database service checks to pass, the database username and password have been added to the host's cfg file in order to cover the following scenarios

      1). Monitor 2008 DB node by discovering Manually

      2). 2008 DB node discovered with ISPACS, where username & password can be either of (SA/phisqlmonitor) its depends upon the ISPACS version

      3). 2012 DB node discovered with ISPACS.

In order to address all these scenarios, there is a provision to provide the database username and password with the ISP & Database Scanners. During discovery of nodes database credentials also will be added to the respective database nodes cfg file as _dbuser & _dbkey, value against these in the host's file will be replaced by Shinken macro $_HOSTDBUSR$ & $_HOSTDBKEY$ respectively

**lhost.yml Entry**::

 endpoint::

    address : 192.168.0.1
    scanner : ISP
    username: u1
    password: p1
    dbuser: dbusr1
    dbpassword: dbpwd

    address: 192.168.0.1
    scanner: Database
    username: u1
    password: p1
    dbuser: dbusr
    dbpassword: dbpwd

Example
        .. image::  images/db_host.PNG
                :align: center
                :alt: db_host.png




