=================================
Deploy a standalone Vigilant node
=================================

#. Deploy a Vigilant appliance
#. Setup network information

- Change /etc/sysconfig/network-scripts/ifcfg-eth0
- Change /etc/sysconfig/network

#. Turn off all service for now ::

    # chkconfig celerybeat off
    # chkconfig celeryd off
    # chkconfig elasticsearch off
    # chkconfig httpd off
    # chkconfig mongod off
    # chkconfig mysqld off
    # chkconfig nagios off
    # chkconfig nginx off
    # chkconfig postfix off
    # chkconfig rabbitmq-server off
    # chkconfig redis off
    # chkconfig shinken-arbiter off
    # chkconfig shinken-broker off
    # chkconfig shinken-poller off
    # chkconfig shinken-reactionner off
    # chkconfig shinken-receiver off
    # chkconfig shinken-scheduler off
    # chkconfig squid off
    # chkconfig supervisord off
    # chkconfig thruk off
    # chkconfig uwsgi off
    # chkconfig webmin off

#. Reboot the system
#. Remove the proxy.sh file ::

    # rm /etc/profile.d/proxy.sh

#. Correct the host file to point to the master internal repository or edit the **/etc/resolv.conf** for a DNS to use the internet repositories ::

    {{ETH0 IP}}      idm-vig-ss-NN.phim.isyntax.net
    {{LB ENDPOINT}}  repo.phim.isyntax.net
    {{DEFINED ENDPOINT}}  phirepo.phim.isyntax.net
    {{LOCAL_IPADDR}}  sb.phim.isyntax.net
    {{LOCAL_IPADDR}}  mongo.phim.isyntax.net
    {{LOCAL_IPADDR}}  el.phim.isyntax.net
    {{LB ENDPOINT}}  sc.phim.isyntax.net
    {{IPADDR}}  smtp.phim.isyntax.net

#. Bring the all the proper services to be started at boot time ::

    # chkconfig celerybeat on
    # chkconfig celeryd on
    # chkconfig elasticsearch off
    # chkconfig httpd off
    # chkconfig mongod on
    # chkconfig mysqld off
    # chkconfig nagios off
    # chkconfig nginx on
    # chkconfig postfix off
    # chkconfig rabbitmq-server on
    # chkconfig redis off
    # chkconfig shinken-arbiter off
    # chkconfig shinken-broker off
    # chkconfig shinken-poller off
    # chkconfig shinken-reactionner off
    # chkconfig shinken-receiver off
    # chkconfig shinken-scheduler off
    # chkconfig squid off
    # chkconfig supervisord on
    # chkconfig thruk off
    # chkconfig uwsgi on
    # chkconfig webmin off

#. Reboot the system
#. Update packages ::

    # yum update

- For any Vigilant instance that is a Release 1 Drop 1 standalone, the following package must be removed ::

    # yum -y remove phim-shinkencfg

- The following packages will be update for the Release 1 Drop 1 Vigilant instance ::

     nagios-plugins-queue_sperfdata    x86_64    0.1.1.0-1.el6     phirepo
     nagios-plugins-wmi_lh             x86_64    1.57.2-1.el6      phirepo
     phim-celery-tasks-server          noarch    1.1.7-1.el6       phirepo
     phim-gateway                      noarch    1.1.3-1.el6       phirepo
     phim-nagioscfg                    x86_64    1.2.20-1.el6      phirepo
     phimutils                         noarch    0.2.8-1           phirepo


#. Restart the uWSGI service ::

     service uwsgi restart

#. Send a test message to localhost to make sure it makes it through the pipelines ::

     # curl -XPOST 'http://localhost/notification' -d '{
          "siteid" : "fake01",
          "timestamp" : "19701212",
          "hostname" : "somehost",
          "hostaddress" : "127.0.0.1",
          "service" : "OS__some__event",
          "type" : "PROBLEM",
          "status" : "CRITICAL",
          "output" : "nope",
          "perfdata" : ""
     }' -H "Content-Type: application/json"

- This notification should be present in the /var/log/uswgi.log

#. Check mongodb to make sure data is being inserted ::

     # mongo
     > use somedb
     > db.events.find({ "siteid" : "fake01" });
     > quit()
